---
layout: page
title: PGP
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/pgp.md
parent: /ar/
published: true
---

برمجيّة PGP و&nbsp;نظيرتها الحُرَّة GPG تتيح كلٌّ منهما لمِنْ يستخدمها حفظ سرِّيَّة فحوى المراسلات البريدية وذلك عبر تشفير محتواها لمنع مُقدِّم خدمة البريد الإلكتروني من الاطلاع عليها أو أيَّ طرف آخر لديه نفاذ إلى المراسلات. رغم ذلك تبقى واقعة إرسالك رسالة مشفرة إلى شخص أو منظّمة ما معلومة من طرف مزود خدمة الانترنت و السلطات المخولة بمراقبة الانترنت.  لحَوْل دون ذلك يمكنك إنشاء حساب بريد بعنوان لا يدلُّ على هويتك (باسم مستعار مثلا) و استعماله مع أداة التشفير.

موارد: [Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)
 
 [Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-beginners-guide/)
 
 [Privacy Tools: Private Email Providers](https://www.privacytools.io/providers/email/)
