---
layout: page
title: الهاتف
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/phone-number.md
parent: /ar/
published: true
---

المحادثات عبر الهواتف المحمولة و&nbsp;خطوط الهاتف الأرضيَّة ليست مُعمّاة إلى متلقّي الاتّصال لذا ففحوى المحادثات و&nbsp;بيانات مَنْ تحادثه تكون معلومة للحكومات و&nbsp;لجهات إنفاذ القانون، و&nbsp;لجهات غيرها مِمَّن لديها القدرة التقنيّة اللازمة للتنصُّت على اتصالك.
