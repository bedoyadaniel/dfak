---
layout: page
title: تور
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/tor.md
parent: /ar/
published: true
---

متصفِّح تور متصفِّحُ وِب يركِّز على الخصوصيَّة يتيح مطالعة مواقع الوِب و&nbsp;التفاعل معها بمجهوليَّة (anonymity) بطريق تمويه الموضع الجغرافي للمستخدم (الذي يفشيه عنوان IP) عن مواقع الوِب.

موارد: [Overview of Tor](https://www.torproject.org/about/overview.html.en).
