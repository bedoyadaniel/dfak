---
layout: page
title: "إنّهم يتحرَّشون بي عبر الإنترنت"
AUThor: Floriana Pagano
language: ar
summary: "هل أنت مستهدفة بالتحرّش عبر الإنتَرنِت؟"
date: 2019-04-01
permalink: /ar/topics/harassed-online/
parent: /ar/
---

# إنّهم يتحرَّشون بي عبر الإنترنت

الإنْتَرنِت عمومًا و&nbsp;شبكات التواصل الاجتماعي خصوصًا صارت فضاءات هامَّة لأفراد المجتمع المدني و&nbsp;منظَّماته، بالذات النساء و&nbsp;ناس الميم و&nbsp;أقليَّات أخرى، يعبِّرون فيها عن ذواتهم و&nbsp;يوصلون أصواتهم. لكنَّها في الوقت نفسه صارت فضاءات تُستهدَف فيها تلك المجموعات بسبب التعبير عن ذواتها. العنف و&nbsp;الانتهاكات عبر الإنترنت تحرم النساء و&nbsp;أفراد الميم و&nbsp;المجموعات الأخرى الأقل امتيازًا الحقّ في التعبير عن أنفسهم بحريّة بلا خوف.

العنف و&nbsp;الانتهاكات عبر الإنترنت لها أشكال متعدّدة و&nbsp;الفاعلون الخبيثون يركنون إلى القدرة على الإفلات، جزئيًا بسبب غياب تشريعات تحمي ضحايا الانتهاكات في بلاد عديدة، و&nbsp;كذلك، و&nbsp;هو الأهمُّ، لأنَّ استراتيجيات الحماية يتوجَّب تفصيلها بإبداع لتلائم نوع الهجوم.

لذا فمِنْ المهمِّ التعرُّف على طوبولوجيّة الهجوم الذي يستهدفك لتعيين الخطوات الواجب اتِّباعها.

هذا القسم في عدَّة الإسعاف الأوَّلي الرقمي سيعرِّفكم بالخطوات الأساسيّة للتخطيط لكيفيّة الحماية مِنْ الهجوم الذي تتعرَّضون له.

فإذا كنت مستهدفة بالتحرُّش عبر الإنترنت اتّبعي هذا الاستقصاء لتحديد طبيعة المشكلة ثمَّ إيجاد حلٍّ ملائم محتمل.

## Workflow

### physical-wellbeing

هل تخشين على سلامتك الجسدية؟

- [نعم](#physical-risk_end)
- [لا](#no-physical-risk)

### no-physical-risk

هل تعتقد أنَّ مهاجمك قد نَفَذَ أو&nbsp;هو حاليًا نافذ إلى جهازك؟

- [نعم](#device-compromised)
- [لا](#account-compromised)

### device-compromised

> غيّري كلمة سرِّ الجهاز إلى كلمة فريدة طويلة معقَّدة:
>
> - [ماك أوإس](https://support.apple.com/ar-eg/HT202860)
> - [وِندوز](https://support.microsoft.com/ar-eg/help/14087/windows-7-change-your-windows-password)
> - [آي‌أوإس -أپِل آي‌دي](https://support.apple.com/ar-eg/HT201355)
> - [أندرُويد](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=ar)

هل أفلحت في صَدِّ المهاجم عن جهازك؟

- [نعم](#account-compromised)
- [لا](../../../device-acting-suspiciously)

### account-compromised

> إذا أفلح أحدهم في النفاذ إلى جهازك فقد يمكنه كذلك النفاذ إلى حساباتك، فيتمكَّن مثلا مِنْ قراءة بريدك و&nbsp;معرفة مَنْ تتواصلين معهم و&nbsp;نشر تدوينات باسمك، أو&nbsp;صور أو&nbsp;فيديوات منتحلا هويَّتك.

هل لاحظت اختفاء منشورات كنت قد نشرتيها أو&nbsp;رسائل أرسلتها؟ أو&nbsp;غير ذلك مِمّا يجعلك تعتقد أنَّ حسابك مخترق؟ افحص كذلك صندوق البريد الصادر.

- [نعم](../../../account-access-issues)
- [لا](#impersonation)

### impersonation

هل ينتحل شخص ما هويَّتك؟

- [نعم](../../../impersonated)
- [لا](#doxing)

### doxing

هل نشر شخصٌ ما معلومات أو&nbsp;صورًا خاصة بغير إذنك؟

- [نعم](#doxing-yes)
- [لا](#hate-speech)

### doxing-yes

أين نُشِرت بياناتك الخاصَّة أو&nbsp;الصور؟

- [في منصّة تواصل اجتماعي](#doxing-sn)
- [في موقع وِب](#doxing-web)

### doxing-sn

> إذا نشر أحدهم بيانات أو&nbsp;صورًا خاصة في منصَّة تواصل اجتماعي فيمكنك الإبلاغ عن الانتهاك باتِّباع إجراءات الإبلاغ التي تعيّنها منصَّة التواصل الاجتماعي المعنيّة. تجدون فيما يلي إرشادات الإبلاغ في المنصّات الرئيسيّة:
>
> - [گُوگِل](https://www.cybercivilrights.org/online-removal/#google)
> - [فيسبوك](https://www.cybercivilrights.org/online-removal/#facebook)
> - [تويتر](https://www.cybercivilrights.org/online-removal/#twitter)
> - [تمبلر](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [إنستگرام](https://www.cybercivilrights.org/online-removal/#instagram)

هل أزيلت البيانات أو&nbsp;الصور؟

- [نعم](#one-more-persons)
- [لا](#harassment_end)

### doxing-web

> اتَّبعي [الإرشادات مِنْ withoutmyconsent.org](https://withoutmyconsent.org/resources/take-down) لحذف المحتوى مِنْ الموقع.

هل أزيل المحتوى مِنْ الموقع؟

- [نعم](#one-more-persons)
- [لا](#harassment_end)

### hate-speech

هل الهجوم منطلق مَنْ خصائص مثل العرق أو&nbsp;الجندر أو&nbsp;الدين؟

- [نعم](#one-more-persons)
- [لا](#harassment_end)


### one-more-persons

هل مهاجمك فرد أم جماعة؟

- [فرد](#one-person)
- [جماعة](#more-persons)

### one-person

هل تعرفين المتحرِّش شخصيًا؟

- [نعم](#known-harasser)
- [لا](#block-harasser)

### known-harasser

> إنْ كنت تعرفين مَنْ يتحرّش بك ففكِّري في الإبلاغ عنه إلى السلطات المختصة في بلدك. لكُلِّ بلد قوانين لحماية الناس مِنْ التحرّش، فينبغي عليك التعرُّف على عليها.
>
> إنْ قرّرت مقاضاة المتحرّش فالجئي إلى محام.

هل تريدين مقاضاة المتحرّش؟

- [نعم](#legal_end)
- [لا](#block-harasser)


### block-harasser

> سواء كنت تعرفين مَنْ يتحرّش بك أم لا فمِنْ المستحسن عمومًا حظره في منصّات التواصل الاجتماعي إن كان أمكن.
>
> - [فيسبوك](https://www.facebook.com/help/290450221052800)
> - [تويتر](https://help.twitter.com/ar/using-twitter/blocking-and-unblocking-accounts)
> - [گُوگِل](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=ar)
> - [تمبلر](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [إنستَگرام](https://help.instagram.com/426700567389543)

هل تمكَّنت مِنْ حظر المتحرِّش؟

- [نعم](#resolved_end)
- [لا](#harassment_end)

### more-persons

> إنْ كان المتحرِّشون بك جماعة فقد تكونين قد وقعت ضحيّة حملة تحرُّش و&nbsp;عندها عليك التفكير في الاستراتيجية المثلى لحالتك.
>
> للمزيد عن الاستراتيجيات المعتبرة طالعي ما [في takebackthetech.net](https://www.takebackthetech.net/be-safe/hate-speech-strategies)

هل وجدت استراتيجية مناسبة؟

- [نعم](#resolved_end)
- [لا](#harassment_end)

### harassment_end

> إذا كنت لاتزالين تتعرضين لتحرُّش و&nbsp;تحتاجين إلى حلٍّ مفصَّل على حالتك فتواصلي مع المنظَّمات المذكورة فيما يلي:

:[](organisations?services=harassment)


### physical-risk_end

> إذا كنت تخشين على سلامتك الجسديّة فتواصلي مع المنظَّمات المذكورة فيما يلي:

:[](organisations?services=physical-security)


### ختام-قانوني

> إذا كنت تحتاجين إلى مشورة قانونيّة فتواصلي مع المنظَّمات المذكورة فيما يلي:

:[](organisations?services=legal)

### resolved_end

عسى أن تكون عدَّة الإسعاف اﻷولي الرقمي قد أفادتكم، و&nbsp;نحبُّ معرفة رأيكم [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **توثيق التحرّش:** مِنْ المفيد دومًا توثيق الهجوم أو&nbsp;أيّ واقعة تتعرضين لها؛ خذي لقطات شاشة، احفظي الرسائل التي تتلقينها مِنْ المتحرِّش، إلخ. كما يُستحسن إنشاء سجلٍّ للتوثيق المنهجي لتسجيل التواريخ و&nbsp;المواقع و&nbsp;معرِّفات المستخدمين و&nbsp;لقطات الشاشة و&nbsp;وصف الوقائع. السجلُّ قد يفيد في تحسُّس الأنماط و&nbsp;يعطيك مؤشِّرات على المهاجمين المحتملين. إنْ شعرت بالاستنزاف فالجئي إلى شخص تثقين فيه و&nbsp;يمكنه مواصلة تسجيل الوقائع لك لبرهة. هذا الشخص يجب أنْ يكون محلَّ ثقة بالغة إذْ أنَّك قد تضطرين إلى تمكينهم مِنْ النفاذ إلى حساباتك الشخصية. قبل مشاركة كلمة السرّ مع غيرك يُستحسن أن تغيِّريها، كما يجب إلا ترسيلها إلى من يساعدك إلا عبر [قناة اتّصال مُعمّاة](https://www.frontlinedefenders.org/ar/resource-publication/guide-secure-group-chat-and-conferencing-tools). و&nbsp;عندما تحسين أنَّ بوسعك الإمساك مجدّدًا بزمام الأمور، [فغيّري مرَّة أخرى كلمات السرِّ](https://ssd.eff.org/ar/module/%D9%88%D8%B6%D8%B9-%D9%83%D9%84%D9%85%D8%A7%D8%AA-%D8%B3%D8%B1-%D9%82%D9%88%D9%8A%D8%A9) التي كشفتها.

    - توجد [إرشادات إلى كيفية تسجيل الوقائع](https://www.techsafety.org/documentationtips).

- **تفعيل وظيفة الاستيثاق بمعاملين** في جميع حساباتك مفيد للغاية في منع المهاجمين مِنْ النفاذ إلى حساباتك. و&nbsp;إذا كان بوسعك الاختيار مِنْ بين أكثر مْنْ وسيلة فيُستحسن اختيار وسيلة غير رسائل الهاتف المحمول القصيرة (SMS)؛ مثل استعمال تطبيق على الهاتف أو&nbsp;مفتاح أمان.

    - إنْ كنت لا تعلمين أيُّ الخيارات أنسب لك فطالعي [الرسم البياني الذي نشرته أكسِسناو](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) و&nbsp;[الدليل الذي نشرته EFF](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - نَشَرَت EFF [إرشادات إلى كيفية تفعيل الاستيثاق بمعاملين في منصّات التواصل الاجتماعي الشهيرة](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **استقصي حضورك على الإنترنت**. بالبحث بنفسك عن معلومات عن هويَّتك أو&nbsp;هويّاتك الرقمية في محركات البحث و&nbsp;وقواعد البيانات الشخصيّة و&nbsp;المنصّات الاجتماعيّة تمكنك معرفة ما سيجده مَنْ قد يحاول انتحال شخصيتك أو&nbsp;;كشف بياناتك الخاصَّة.


#### resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
