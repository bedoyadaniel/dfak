---
layout: page
title: "لا أستطيع النفاذ إلى حسابي"
author: RaReNet
language: ar
summary: "هل تواجهين صعوبة في النفاذ إلى حساب بريدك أو&nbsp;حسابك على منصّة تواصل اجتماعي أو&nbsp;موقع ما على الوِب؟ هل يبدي الحساب نشاطًا لم تقم أنت به؟ توجد حلول لتدارك هذه المشكلة."
date: 2015-08
permalink: /ar/topics/account-access-issues/
parent: /ar/
---

# لا أستطيع النفاذ إلى حسابي

يستعمل أفراد المجتمع المدني حسابات التواصل الاجتماعي بكثافة لنشر المعلومات و&nbsp;لمناصرة قضاياهم، و&nbsp;هذا يؤدي إلى استهداف حساباتهم مِنْ قِبَل الفاعلين المُعادين الذين يسعون إلى اختراق تلك الحسابات، ممّا يضرُّ أفراد المجتمع المدني و&nbsp;الذين يتواصلون معهم.

الغرض مِنْ هذا الدليل مساعدتك في حال ما تعذََّر عليك النفاذ إلى أحد حساباتك بسبب اختراقه.

المسار التالي الغرض مِنْه تحديد طبيعة المشكلة و&nbsp;إيجاد الحلول المحتملة.

## Workflow

### Password-Typo

> أحيانا يتعذَّر عليك الولوج إلى حساباتك لأنَّك تُدخِلين كلمة سرٍّ خاطئة دون أنْ تعي، أو&nbsp;لأنّ لغة لوحة المفاتيح مضبوطة على غير ما تستعملينه عادة أو&nbsp;لأنّ زرّ الأحرف اللاتينيّة الكبيرة CAPS مضغوط.
>
> جرِّبي كتابة اسم المستخدم و&nbsp;كلمة السرِّ في مُحرِّر نصوص صافية (plaintext) ثم انسخيها ثم ألصقيها في استمارة الولوج. و&nbsp;تحقّق أيضًا مِنْ إعدادات لغة لوحة المفاتيح، و&nbsp;إذا ما كان زرُّ CAPS مضغوطا.

هل حلَّ أيُّ المقترحات السالفة المشكلة و&nbsp;تمكَّنت مِنْ الولوج إلى حسابك؟

- [نعم](#resolved_end)
- [لا](#What-Type-of-Account-or-Service)

### أيّ*حساب*أو-خدمة

أيُّ حساب أو&nbsp;خدمة يتعذَّر الولوج إليها؟

- [فيسبوك](#Facebook)
- [صفحة فيسبوك](#Facebook-Page)
- [تويتر](#Twitter)
- [گُوگِل\جيميل](#Google)
- [ياهو](#Yahoo)
- [هُتميل\أوْتلُك\لايڤ](#Hotmail)
- [پروتونميل](#ProtonMail)
- [إنستَگرام](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

هل للصفحة مديرون آخرون؟

- [نعم](#Other-admins-exist)
- [لا](#Facebook-Page-recovery-form)

### Other-admins-exist

هل يواجه المديرون الآخرون نفس المشكلة؟

- [نعم](#Facebook-Page-recovery-form)
- [لا](#Other-admin-can-help)

### Other-admin-can-help

> اطلب مِنْ مدير آخر إعطاءك صلاحيات الإدارة مجدَّدًا.

هل يحلُّ هذا المشكلة؟

- [نعم](#Fb-Page_end)
- [لا](#account_end)

### Facebook-Page-recovery-form

> عاود الولوج إلى فيسبوك و&nbsp;استعملي [استمارة الإبلاغ عن مشاكل الصفحات](https://www.facebook.com/help/contact/164405897002583).
>
> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاستعادة؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I-have-access-to-recovery-email-google)
- [لا](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

هل تلقّيت مؤخرًا تنويهًا حرجًا بشأن خصوصيّة حساب گُوگِل المربوط؟

- [نعم](#Email-received-google)
- [لا](#Recovery-Form-google)

### Email-received-google

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery-Link-Found-google)
- [لا](#Recovery-Form-google)

### Recovery-Link-Found-google

> اتبع رابط استرداد حسابك.

هل تمكّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-google)

### Recovery-Form-google

> جرب اتّباع الإجراء في استمارة "[كيفية استرداد حسابك على Google أو Gmail](https://support.google.com/accounts/answer/7682439?hl=ar)"
>
> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمُعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I-have-access-to-recovery-email-yahoo)
- [لا](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب ياهو؟

- [نعم](#Email-received-yahoo)
- [لا](#Recovery-Form-Yahoo)

### Email-received-yahoo

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery-Link-Found-Yahoo)
- [لا](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> جرّبي اتّباع الإجراء في [استمارة استرداد الحسابات](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html).
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I-have-access-to-recovery-email-Twitter)
- [لا](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب تويتر؟

- [نعم](#Email-received-Twitter)
- [لا](#Recovery-Form-Twitter)

### Email-received-Twitter

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery-Link-Found-Twitter)
- [لا](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> جرِّبي اتّباع الإجراء في [استمارة استرداد الحسابات](https://help.twitter.com/forms/restore)
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> جرّب اتّباع إجراء [تصفير كلمة السرّ](https://protonmail.com/support/knowledge-base/reset-password)

> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I-have-access-to-recovery-email-Hotmail)
- [لا](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب هُتميل؟

- [نعم](#Email-received-Hotmail)
- [لا](#Recovery-Form-Hotmail)

### Email-received-Hotmail

هل في الرسالة رابط لتصفير كلمة السرّ؟

- [نعم](#Recovery-Link-Found-Hotmail)
- [لا](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> اتبع رابط تصفير كلمة السرّ.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> جرِّبي اتّباع الإجراء في استمارة "[ااسترداد حسابك](https://account.live.com/acsr)"
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Facebook

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I-have-access-to-recovery-email-Facebook)
- [لا](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب فيسبوك؟

- [نعم](#Email-received-Facebook)
- [لا](#Recovery-Form-Facebook)

### تلقيت*رسالة*فيسبوك

هل في الرسالة رابط بما معناه "إذا لم تكن قد طلبت هذا التغيير فأمّن حسابك"؟

- [نعم](#Recovery-Link-Found-Facebook)
- [لا](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> اتبع رابط تأمين الحساب.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> جرِّبي الإجراء في استمارة "[البحث عن حسابك](https://www.facebook.com/login/identify)"
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيّنين للاسترداد؟

- [نعم](#I-have-access-to-recovery-email-Instagram)
- [لا](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب إنستگرام؟

- [نعم](#Email-received-Instagram)
- [لا](#Recovery-Form-Instagram)

### Email-received-Instagram

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery-Link-Found-Instagram)
- [لا](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> جرِّبي اتّباع الإجراء في "[أعتقد أن حسابي على Instagram قد تعرّض للاختراق](https://help.instagram.com/149494825257596)"
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Fb-Page_end

عظيم أنّ مشكلتك انحلّت. طالع التوصيات التالية لتقليل احتمال فقدناك النفاذ إلى حسابك في المستقبل.

- فعّل الاستيثاق بمعاملين (2FA) لكل مديري الصفحة
- لا تسند صلاحيات الإدارة إلا للذين تثق فيهم و&nbsp;يردون عند الحاجة

### account_end

إذا لم يفلح الإجراء الموصوف هنا في استرداد حسابك فيمكنك التواصل مع المنظَّمات التالية لطلب العون:

:[](organisations?services=account)

### resolved_end

عسى أن تكون عدَّة الإسعاف اﻷولي الرقمي قد أفادتكم، و&nbsp;نحبُّ معرفة رأيكم [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

طالعي التوصيات التالية لتقليل احتمال فقدانك النفاذ إلى حسابك في المستقبل.

- مِنْ المفيد دوما تفعيل الاستيثاق بمعاملين (2FA) في كلّ الحسابات التي تدعم هذه الوظيفة.
- لا تضع أبدا كلمة السّر نفسها لأكثر مِنْ حساب. و&nbsp;إذا كنت قد فعلت ذلك بالفعل فغيّريها في أقرب فرصة واضعة كلمة سرّ فريدة في كلّ حساب. حتى ما إذا تسرّبت معلومات مِنْ إحدى الخدمات لَمْ يستطع أحد استعمالها للنفاذ إلى حساباتك الأخرى.
- استعمال مدير لكلمات السرّ سيعينك على وضع كلمات سرّ فريدة قوية لكلّ حساباتك دون الاعتماد على ذاكرتك
- احتَط عند الاتصال عبر شبكات وايفاي عمومية، و&nbsp;يستحسن استعمال VPN في هذه الحالة.

#### resources

- [وضع كلمات سر قوية | الدفاع عن النفس ضد الرقابة](https://ssd.eff.org/ar/module/%D9%88%D8%B6%D8%B9-%D9%83%D9%84%D9%85%D8%A7%D8%AA-%D8%B3%D8%B1-%D9%82%D9%88%D9%8A%D8%A9)
- [حماية نفسك على مواقع التواصل الاجتماعي | الدفاع عن النفس ضد الرقابة](https://ssd.eff.org/ar/module/%D8%AD%D9%85%D8%A7%D9%8A%D8%A9-%D9%86%D9%81%D8%B3%D9%83-%D8%B9%D9%84%D9%89-%D9%85%D9%88%D8%A7%D9%82%D8%B9-%D8%A7%D9%84%D8%AA%D9%88%D8%A7%D8%B5%D9%84-%D8%A7%D9%84%D8%A7%D8%AC%D8%AA%D9%85%D8%A7%D8%B9%D9%8A)
- [عدّة الأمان: إنشاء كلمات مرور قويّة وحفظها](https://securityinabox.org/ar/guide/passwords/)
- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)


<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
