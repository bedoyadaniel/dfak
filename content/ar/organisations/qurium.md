---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: من الاثنين إلى الأحد من 08:00 إلى 18:00 CET
response_time: 4 ساعات
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

مؤسسة كوريم للميديا تقدّم حلول الأمان إلى المؤسسات الإعلامية المستقلة و&nbsp;منظّمات حقوق الإنسان و&nbsp;الصحافيين الاستقصائين و&nbsp;النشطاء، كما تقدّم إضبارة من حلول الأمان الاحترافية مع دعم الشخصي للمنظمات و&nbsp;الأشخاص المهددين، تتضمّن:

- الاستضافة المؤمنّة ضد DDoS للمواقع المهدَّدة
- الاستجابة السريعة لطوارئ المنظمات و&nbsp;الأفراد المعرضين للخطر
- المراجعة الأمنية لخدمات الوِب و&nbsp;تطبيقات المحمول
- تجاوز حجب مواقع الوِب
- الفحص الجنائي للهجمات السبرانية و&nbsp;التطبيقات الاحتيالية و&nbsp;البرمجيات الخبيثة الموجّهة و&nbsp;الأخبار الزائفة

