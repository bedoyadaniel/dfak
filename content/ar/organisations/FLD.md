---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: emergency 24/7, عالميًّا; الدَّوام المعتاد من الاثنين إلى الجمعة, IST (UTC+1), الموظّفون موجودون في مناطق زمنية مختلفة
response_time: نفس اليوم أو التالي في الحالات الطارئة
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: ‭+353-1-210-0489‬ للطوارئ; ‭+353-1-212-3750‬ المكتب
skype: front-line-emergency
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

فرُنْتْلَيْن دِفِندَرْز (المدافعون في الخط الأمامي) منظَّمة دوليَّة مقرُّها أيرلندا تعمل على الحماية المتكاملة للمدافعين عن حقوق الإنسان المعرّضين لخطر، و&nbsp;هي تقدِّم دعمًا سريعًا عمليًّا عند مداهمة الخطر بطريق المنحات، و&nbsp;تدريبات الأمان المادي و&nbsp;الرّقمي، و&nbsp;المناصرة و&nbsp;الحشد.

تدير المنظَّمة خطًّ طوارئ ساخن للمدافعين الحقوقيين يعمل طوال ساعات اليوم على الرقم ‪+353-121-00489‬ باللغات العربيّة و&nbsp;الإنجليزيّة و&nbsp;الفرنسيّة و&nbsp;الروسيّة و&nbsp;الإسبانيّة. عند تعرُّض المدافعين لتهديد لحياتهم فإنَّ فرُنْتْلَيْن دِفِندَرْز يمكنها أن تسعى إلى إخراجهم مؤقتَّا مِنْ منطقة الخطر. كذلك تقدِّم المنظَّمة تدريبات في الأمان المادي و&nbsp;الرقمي، و&nbsp;تنشر أخبار المدافعين المعرَّضين للخطر و&nbsp;تنسِّق حملات المناصرة على المستوى الدولي بما في ذلك في الاتحاد الأورُپي و&nbsp;الأمم المتّحدة و&nbsp;الآليات الإقليميّة و&nbsp;الحكومات المنوط بها حمايتهم.
