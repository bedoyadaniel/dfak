---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7 في أنحاء العالم
response_time: ساعتين
contact_methods: web, email, pgp
email: help@accessnow.org
pgp_key_fingerprint: ‭6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC‬
initial_intake: yes
---

خَطُّ مساعدة الأمان الرقمي لمنظمة أكْسِسْناوْ يعمل مع الأفراد و&nbsp;المنظَّمات مِنْ أنحاء العالم لزيادة أمانهم على الإنترنت. إذا كنتم مُعرّضين للخطر فتمكننا مساعدتكم على تحسين ممارساتكم للأمان الرقمي لتنأوا بأنفسكم عن الخطر. أمَّا إذا كنتم تتعرَّضون فعليًّا لهجوم فيمكننا تقديم الدعم العاجل إليكم.
