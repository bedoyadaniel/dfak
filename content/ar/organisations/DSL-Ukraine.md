---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: من الاثنين إلى الخميس من 09:00 إلى 17:00 EET/EEST
response_time: يومين
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

معمل الأمان الرقمي الأوكراني مؤسسة أهليّة مقرُّها كييف تأسّست سنة 2017 بهدف تطبيق مبادئ حقوق الإنسان ببناء القدرات في منظّمات المجتمع المدني و&nbsp;الإعلام المستقل بالاستجابة لهمومهم المتعلّقة بالأمان الرقمي، و&nbsp;التأثير على سياسات الحكومة و&nbsp;الشركات فيما يتعلّق بالحقوق الرقميّة.

