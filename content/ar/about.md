---
layout: page.pug
title: "About"
language: ar
summary: "عن عدَّ الإسعاف الأوَلي الرقمي"
date: 2019-03-13
permalink: /ar/about/
parent: Home
---

عدَّة الإسعاف الأوَّلي الرقمي نتيجة تعاون بين [RaReNet (Rapid Response Network)](https://www.rarenet.org) و&nbsp;[CiviCERT](https://www.civicert.org).

شبكة الاستجابة السريعة (The Rapid Response Network) شبكة عالمية من المُسعِفين و&nbsp;المؤسسات المتخصصة في أمن المعلومات هي Access Now و&nbsp;Amnesty Tech و&nbsp;Center for Digital Resilience و&nbsp;CIRCL و&nbsp;EFF و&nbsp;Freedom House و&nbsp;Front Line Defenders و&nbsp;Global Voices و&nbsp;Greenhost و&nbsp;Hivos & the Digital Defenders Partnership و&nbsp;Internews و&nbsp;La Labomedia و&nbsp;Open Technology Fund و&nbsp;Virtualroad، و&nbsp;كذلك أفرادًا خبراء في أمن المعلومات و&nbsp;الاستجابة الطارئة.

بعض هذه المنظَّمات و&nbsp;هؤلاء الأفراد أعضاء في CiviCERT، و&nbsp;هي شبكة عالمية مِنْ مقدّمي الدعم في الأمان الرقمي و&nbsp;مقدّمي البنية التحتيّة الذين يركِّزون على دعم المجموعات و&nbsp;المنظَّمات الساعية إلى العدالة الاجتماعية و&nbsp;الدفاع عن حقوق الإنسان و&nbsp;الحقوق الرقمية. و&nbsp;سِڤيسٍرت إطار تنظيمي لجهود فرق الاستجابة لحوادث الحواسيب (CERT)، و&nbsp;هي معتمدة مِنْ قِبَل الشبكة الأوربية لفرق الاستجابة لحوادث الحواسيب (Trusted Introducer)

عدَّة الإسعاف الأوَّلي الرقمي [مفتوحة المصدر تمكنكم المساهمة في تحسينها](https://gitlab.com/rarenet/dfak)

إذا أردتم استعمال عدَّة الإسعاف الأوَّلي الرقمي في سياقٍ فيه الاتّصال بالإنترنت ضعيف أو منعدم فيمكنكم [تنزيل نسخة منها](https://digitalfirstaid.org/dfak-offline.zip).

تمكنكم مكاتبتنا بالتعليقات أو المقترحات أو الأسئلة عن عدَّة الإسعاف الأوَّلي الرقمي على العنوان  dfak @ digitaldefenders . org

بصمة مفتاح GPG: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
