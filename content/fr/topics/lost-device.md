---
layout: page
title: "J'ai perdu mon appareil"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: fr
summary: "J'ai perdu mon appareil, que dois-je faire ?"
date: 2021-01-28
permalink: /fr/topics/lost-device
parent: /fr/
---

# I lost my device

Votre appareil est-il perdu ? A-t-il été volé ou saisi par une autre personne ?

Dans ces situations, il est important de prendre des mesures immédiates pour réduire le risque qu'une autre personne accède à vos comptes, à vos contacts et à vos renseignements personnels.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques questions de base afin que vous puissiez évaluer comment réduire les dommages possibles liés à la perte d'un appareil.

## Workflow

### question-1

L'appareil est-il toujours manquant ?

 - [Oui, il est manquant](#device-missing)
 - [Non, il m'a été retourné](#device-returned)

### device-missing

> Il est bon de réfléchir sur les protections de sécurité dont disposait l'appareil :
>
> L'accès à l'appareil a-t-il été protégé par un mot de passe ou une autre mesure de sécurité ?
> L'appareil est-il doté d'un dispositif de chiffrement (cryptage) activé ?
> Dans quel état était votre appareil lorsqu'il a été perdu - Etiez-vous connecté ? L'appareil était-il allumé mais verrouillé par mot de passe ? Il dormait ou hibernait ? Était-il complètement éteint ?

En gardant cela à l'esprit, vous pouvez mieux comprendre dans quelle mesure il est probable que quelqu'un·e d'autre ait accès au contenu de votre appareil.

- [Supprimons l'accès de l'appareil à mes comptes](#accounts)

### accounts

> Listez tous les comptes auxquels cet appareil a eu accès. Il peut s'agir de comptes pour le courrier électronique, les médias sociaux, la messagerie, les rencontres et les opérations bancaires, ainsi que les comptes qui peuvent utiliser cet appareil pour une double authentification.
>
> Pour tous les comptes auxquels votre terminal a eu accès (tels que le courrier électronique, les médias sociaux ou le compte Web), vous devez supprimer l'autorisation de cet appareil à ces comptes. Ceci peut être fait en vous connectant à vos comptes et en retirant l'appareil des appareils autorisés.
>
> * [Compte Google](https://myaccount.google.com/device-activity)
> * [Compte Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Compte iCloud](https://support.apple.com/fr-fr/HT205064)
> * [Compte Twitter](https://twitter.com/settings/sessions)
> * [Compte Yahoo](https://login.yahoo.com/account/activity)

Une fois que vous avez terminé de déconnecter vos comptes, sécurisez vos mots de passe qui se trouvaient peut-être sur l'appareil.

- [D'accord, occupons-nous des mots de passe](#passwords)

### passwords

> Réfléchissez à tous les mots de passe enregistrés directement sur l'appareil ou à tous les navigateurs qui ont enregistré des mots de passe.
>
> Modifiez les mots de passe de tous les comptes accessibles par cet appareil. Si vous n'utilisez pas de gestionnaire de mots de passe, envisagez fortement d'en utiliser un afin de mieux créer et gérer des mots de passe forts.

Après avoir changé les mots de passe de vos comptes issus de votre appareil, réfléchissez si vous avez utilisé l'un de ces mots de passe pour d'autres comptes - si oui, veuillez changer ces mots de passe également.

- [J'ai utilisé certains de mes mots de passe sur l'appareil perdu pour d'autres comptes](#same-password)
- [Tous mes mots de passe qui ont pu être compromis étaient uniques](#2fa)

### same-password

Utilisez-vous le même mot de passe sur d'autres comptes ou appareils que l'appareil perdu ? Si c'est le cas, changez le mot de passe de ces comptes, car ils pourraient également être compromis.

- [ok, j'ai changé tous les mots de passe concernés maintenant](#2fa)

### 2fa

> L'activation de l'authentification à deux facteurs sur les comptes qui risquent d'être consultés réduira la probabilité qu'une autre personne puisse y accéder.
>
> Activer l'authentification à deux facteurs pour tous les comptes accessibles par cet appareil. Veuillez noter que tous les comptes ne supportent pas l'authentification à 2 facteurs. Pour obtenir une liste des services et des plates-formes qui prennent en charge l'authentification à deux facteurs, veuillez consulter [Two Factor Auth](https://brainstation.io/cybersecurity/two-factor-auth).

- [J'ai activé l'authentification à deux facteurs sur mes comptes pour les sécuriser davantage. Je voudrais essayer de trouver ou d'effacer mon appareil](#find-erase-device)

### find-erase-device

> Pensez à l'utilisation que vous avez faite de cet appareil - y a-t-il des informations sensibles sur cet appareil, telles que les contacts, la localisation ou les messages ? Cette fuite de données peut-elle être problématique pour vous, votre travail ou d'autres personnes que vous-même ?
>
> Dans certains cas, il peut être utile d'effacer à distance les données sur l'appareil d'une personne détenue, afin d'éviter que les données la concernant ne soient utilisées à mauvais escient et utilisées contre elle ou d'autres militant·e·s. Dans le même temps, cela peut être problématique pour la personne détenue (en particulier dans les situations où la torture et les mauvais traitements sont possibles) - surtout si la personne détenue a été forcée de donner accès à l'appareil, la disparition soudaine des données de l'appareil peut rendre les autorités détentrices plus suspicieuses. Si vous faites face à une situation similaire, lisez notre section ["Quelqu'un que je connais a été arrêté"](../../../../arrested).
>
> Il convient également de noter que, dans certains pays, l'effacement à distance peut être juridiquement difficile, car il peut être interprété comme une destruction de preuves.
>
> Si vous connaissez clairement les conséquences directes et légales pour toutes les personnes concernées, vous pouvez tenter d'effacer l'appareil à distance, en suivant les instructions de votre système d'exploitation :
>
> * [Appareils Android](https://support.google.com/accounts/answer/6160491?hl=fr)
> * [iPhone ou Mac](https://www.icloud.com/#find)
> * [les appareils iOS (iPhone et iPad) en utilisant iCloud](https://support.apple.com/kb/ph2701?locale=fr_FR)
> * [Téléphones Windows](https://support.microsoft.com/fr-fr/help/11579/microsoft-account-find-and-lock-lost-windows-device)
> * Pour les périphériques Windows ou Linux, vous avez peut-être installé un logiciel (tel qu'un antivol ou un antivirus) qui vous permet d'effacer à distance les données et l'historique de votre périphérique. Si oui, utilisez-le.

Que vous ayez réussi ou non à effacer à distance les informations sur l'appareil, il est bon d'en informer vos contacts.

- [Passez aux étapes suivantes pour trouver des conseils sur la façon d'informer vos contacts de la perte de votre appareil](#inform-network)


### inform-network

> Outre vos propres comptes, votre terminal contiendra probablement des informations sur d'autres personnes. Il peut s'agir de vos contacts, de vos communications avec d'autres personnes, de groupes de messagerie.
>
> Lorsque vous envisagez d'informer votre réseau et votre communauté au sujet de l'appareil perdu, utilisez les principes de [réduction des risques](../../../../arrested#harm-reduction) pour vous assurer qu'en communiquant avec les autres, vous ne les exposez pas à un risque accru.

Informez votre réseau de l'appareil perdu. Cela pourrait se faire en privé avec des personnes soumises à un risque élevé, ou en affichant une liste de comptes potentiellement compromis publiquement sur votre site Web ou un compte de médias sociaux si vous jugez approprié de le faire.

- [J'ai informé mes contacts](#review-history)


### review-history

> Si possible, vérifiez l'historique de connexion/compte de tous les comptes connectés à l'appareil. Vérifiez si votre compte a été utilisé à un moment où vous n'étiez pas en ligne ou si vous avez accédé à votre compte à partir d'un endroit ou d'une adresse IP inconnue.
>
> Vous pouvez passer en revue votre activité sur certains fournisseurs populaires listés ci-dessous :
>
> * [Compte Google](https://myaccount.google.com/device-activity)
> * [Compte Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Compte iCloud](https://support.apple.com/fr-fr/HT205064)
> * [Compte Twitter](https://twitter.com/settings/sessions)
> * [Compte Yahoo](https://login.yahoo.com/account/activity)

Avez-vous passé en revue l'historique de vos connexions ?

- [Je l'ai fait, et je n'ai rien trouvé de suspect](#check-settings)
- [J'ai trouvé des activités suspectes](../../../account-access-issues/)


### check-settings

> Vérifiez les paramètres de compte de tous les comptes connectés à l'appareil. Ont-ils été changés ? Pour les comptes de messagerie, vérifiez les transferts automatiques, les changements possibles des adresses e-mail et/ou des numéros de téléphone de sauvegarde/réinitialisation des comptes, la synchronisation vers différents périphériques, y compris les téléphones, ordinateurs ou tablettes, et les permissions données aux applications ou autres permissions de compte.
>
> Répétez l'examen de l'historique des activités du compte au moins une fois par semaine pendant un mois, pour vous assurer que votre compte continue de ne montrer aucune activité suspecte.


- [L'historique des activités de mon compte montre une activité suspecte](../../../account-access-issues/)
- [L'historique des activités de mon compte ne montre aucune activité suspecte, mais je continuerai à l'examiner au fil du temps.](#resolved_end)


### device-returned

> Si votre appareil a été perdu, pris par une autre personne ou a dû être remis à un poste frontière, mais que vous l'avez récupéré, faites attention car vous ne savez pas qui y a eu accès. Selon le niveau de risque auquel vous êtes exposé, vous voudrez peut-être traiter l'appareil comme s'il n'était plus fiable ou qu'il était compromis.
>
> Posez-vous les questions suivantes et évaluez le risque que votre appareil ait été compromis :
>
> Combien de temps l'appareil a-t-il été hors de votre vue ?
> Qui aurait pu potentiellement y avoir accès ?
> Pourquoi voudraient-iels y avoir accès ?
> Y a-t-il des signes que l'appareil a été manipulé physiquement ?
>
> Si vous ne faites plus confiance à votre appareil, pensez à le formater et à le réinstaller ou à vous procurer un nouvel appareil.

Souhaitez-vous de l'aide pour l'achat d'un appareil de remplacement ?

- [Oui](#new-device_end)
- [Non](#resolved_end)


### accounts_end
Si vous avez perdu l'accès à vos comptes ou si vous pensez que quelqu'un·e a pu y accéder, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account)

### new-device_end

Si vous avez besoin d'un soutien financier externe pour obtenir un appareil de remplacement, veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider.

:[](organisations?services=equipment_replacement)

### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Voici une série de conseils pour réduire les risques de fuites de données et d'accès non autorisés à vos comptes et informations :

- Ne laissez jamais votre appareil sans surveillance. Si vous avez besoin de le faire, éteignez-le.
- Activez le chiffrement complet du disque.
- Utilisez un mot de passe fort pour verrouiller votre appareil.
- Activez la fonction Rechercher/Effacer mon téléphone chaque fois que cela est possible, mais veuillez noter qu'elle peut être utilisée pour vous suivre ou effacer votre appareil, si votre compte associé (Gmail/iCloud) est compromis.

#### resources

* [La documentation publique d’assistance à la sécurité numérique d’Access Now: Conseil pour chiffrer intégralement le disque dur](https://accessnowhelpline.gitlab.io/community-documentation/166-Full-Disk_Encryption.html)
* Envisagez d'utiliser un logiciel antivol tel que [Prey](https://preyproject.com)
