---
layout: page
title: "Mon site web ne marche plus, que se passe t-il ?"
author: Rarenet
language: fr
summary: "Une menace à laquelle font face de nombreuses ONG, médias indépendants et blogueurs est de voir leur voix étouffée parce que leur site Web est en panne ou a été défiguré."
date: 2021-01-28
permalink: /fr/topics/website-not-working/
parent: /fr/
---

# My Website is down, what is going on?

Beaucoup d'ONG, de médias indépendants et de blogueurs font face à une menace : leur voix est étouffée parce que leur site Web est en panne ou a été défiguré. C'est un problème frustrant qui peut avoir de nombreuses causes comme une mauvaise maintenance du site Web, un hébergement peu fiable, [une attaque de script-kiddies](https://fr.wikipedia.org/wiki/Script_kiddie), une attaque par déni de service ou une prise de contrôle du site. Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour diagnostiquer les problèmes potentiels en utilisant le matériel de [Mon site Web est en panne](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Il est important de savoir qu'il y a plusieurs raisons pour lesquelles votre site Web peut tomber en panne. Il peut s'agir de problèmes techniques au niveau de la société qui héberge le site Web ou le système de gestion de contenu (CMS) non mis à jour comme Joomla ou Wordpress. Trouver le problème et les solutions possibles pour le problème de votre site Web peut être contraignant. C'est une bonne pratique de **contacter votre webmanager et l'hébergeur du site** après le diagnostic ci-dessous qui aborde les problèmes les plus communs. Si aucune de ces options ne répond à votre problème, [demandez l'aide d'une organisation en qui vous avez confiance](#website-down_end).

## Consider

Tout d'abord, réfléchissez :

- Qui a construit votre site web ? Sont-ils disponibles pour vous aider ?
- A-t-il été construit à l'aide de Wordpress ou d'une autre plate-forme CMS populaire ?
- Qui est votre fournisseur d’hébergement ? Si vous ne savez pas, vous pouvez utiliser un [service en ligne WHOIS](https://www.whoishostingthis.com/) pour vous aider.


## Workflow

### error-message

Vous voyez des messages d'erreur ?

 - [Oui, je vois des messages d'erreur](#error-message-yes)
 - [Non](#hosting-message)


### hosting-message

Vous voyez un message de votre hébergeur ?

- [Oui, je vois un message de mon hébergeur](#hosting-message-yes)
- [Non](#site-not-loading)


### site-not-loading

Votre site ne se charge pas du tout ?

- [Oui, le site ne se charge pas du tout](#site-not-loading-yes)
- [Non, le site se charge](#hosting-working)


### hosting-working

Le site Web de votre fournisseur d'hébergement fonctionne-t-il, mais votre site Web n'est pas disponible ?

- [Oui, je peux accéder au site web de mon hébergeur](#hosting-working-yes)
- [Non](#similar-content-censored)


### similar-content-censored

Pouvez-vous visiter d'autres sites avec du contenu comme le vôtre ?

- [Je ne peux pas non plus visiter d'autres sites avec un contenu similaire](#similar-content-censored-yes)
- [D'autres sites fonctionnent bien, mais je ne peux pas visiter mon site](#loading-intermittently)


### loading-intermittently

Votre site se charge-t-il de façon intermittente ou de façon exceptionnellement lente ?

- [Oui, mon site se charge par intermittence ou est lent](#loading-intermittently-yes)
- [Non, mon site se charge, mais il a peut-être été piraté](#website-defaced)


### website-defaced

Est-ce que votre site web se charge mais l'apparence et le contenu ne sont pas ce que vous vous attendez à voir ?

- [Oui, mon site n'a pas le contenu/apparence attendue](#defaced-attack-yes)
- [Non](#website-down_end)


### error-message-yes

> Cela pourrait être un problème ***logiciel*** -- vous devriez réfléchir à tout changement récent que vous ou votre équipe pourriez avoir fait, et contacter votre webmanager. Envoyer une capture d'écran à votre webmanager, le lien de la page avec laquelle vous rencontrez des problèmes et tout message d'erreur que vous voyez, ce qui l'aidera à trouver la cause possible du problème. Vous pouvez également copier les messages d'erreur dans un moteur de recherche pour voir s'il existe des solutions faciles.

Est-ce que cela vous a aidé ?

- [Oui](#resolved_end)
- [Non](#website-down_end)


### hosting-message-yes

> Vous auriez pu être mis hors ligne pour des raisons légales, [pour des raisons liées à la propriété intelectuelle](https://www.eff.org/issues/bloggers/legal/liability/IP), de facturation ou autres. Contactez votre hébergeur pour plus de détails sur les raisons de la suspension de votre hébergement web.

Est-ce que cela vous a aidé ?

- [Oui](#resolved_end)
- [Non, j'ai besoin d'un soutien juridique](#legal_end)
- [Non, j'ai besoin de support technique](#website-down_end)


### site-not-loading-yes

> Votre hébergeur peut avoir des problèmes, auquel cas vous pouvez être confronté à un ***problème d'hébergement***. Pouvez-vous visiter le site web de votre hébergeur ?  Notez que ce n'est **pas** la section admin de votre propre site, mais la société ou l'organisation avec laquelle vous travaillez pour héberger votre site.
>
> Cherchez ou recherchez un blog "status" (par exemple status.dreamhost.com), et recherchez également sur twitter.com d'autres personnes discutant de problèmes chez l'hébergeur - une simple recherche comme "(nom de la société) down" peut souvent révéler si plusieurs autres ont le même problème.

Est-ce que cela vous a aidé ?

- [Oui](#resolved_end)
- [Non, le site de mon hébergeur n'est pas en panne](#hosting-working-yes)
- [Non, j'ai besoin de support technique](#website-down_end)


### hosting-working-yes

> Vérifiez que le site web marche avec [Down for Everyone or Just Me](https://downforeveryoneorjustme.com/) - votre site est peut-être en fonctionnement mais vous ne pouvez pas le voir.
>
> Si votre site est en ligne mais que vous ne pouvez pas le voir, il s'agit probablement d'un ***problème de réseau*** : votre propre connexion Internet pourrait avoir des problèmes ou bloquer l'accès à votre site.

Avez-vous besoin d'aide supplémentaire ?

- [Non](#resolved_end)
- [Oui, j'ai besoin d'aide pour rétablir ma connexion réseau](#website-down_end)
- [Oui, ce n'est pas un problème de réseau et mon site est en panne pour tout le monde](#similar-content-censored)


### similar-content-censored-yes

> Essayez de visiter des sites Web dont le contenu est similaire à celui de votre site Web. Essayez également d'utiliser [Tor](https://gettor.torproject.org/fr/) ou [Psiphon](https://psiphon.ca/fr/index.html) pour accéder à votre site.
>
> Si vous pouvez visiter votre site via Tor ou Psiphon, vous avez un problème de ***censure*** - vous êtes toujours en ligne pour d'autres parties du monde, mais vous êtes censuré dans votre propre pays.

Aimeriez-vous faire quelque chose contre cette censure ?

- [Oui, j'aimerais le signaler publiquement et j'ai besoin de soutien pour ma campagne de mobilisation](#advocacy_end)
- [Oui, j'aimerais trouver une solution pour rendre mon site Web accessible](#website-down_end)
- [Non](#resolved_end)


### loading-intermittently-yes

> Votre site peut être submergé par le nombre et la rapidité des demandes de pages qu'il reçoit - c'est un problème de ***performance***.
>
> Cela pourrait être "une bonne chose" dans la mesure où votre site est devenu plus populaire et qu'il a simplement besoin de quelques améliorations pour répondre à un plus grand nombre de lecteurs·rices - vérifiez les statistiques de votre site pour envisager une croissance à long terme.  Contactez votre webmanager ou votre hébergeur pour obtenir des conseils. De nombreuses plateformes de blogs et de systèmes de gestion de contenu (CMS) populaires (Joomla, Wordpress, Drupal...) disposent de plugins pour aider à mettre en cache votre site web localement et à intégrer les [Réseau de diffusion de contenu](https://fr.wikipedia.org/wiki/R%C3%A9seau_de_diffusion_de_contenu), ce qui peut améliorer considérablement les performances et la résilience du site. Bon nombre des solutions ci-dessous peuvent également aider à résoudre les problèmes de performance.
>
> Si vous rencontrez un problème grave de **performance**, votre site est peut être victime d'une attaque par [**"déni de service ou par saturation"**](https://ssd.eff.org/fr/glossary/attaque-par-saturation) (ou DDoS). Suivez les étapes ci-dessous pour atténuer une telle attaque :
>
> Etape 1 : Contactez une personne de confiance qui peut vous aider avec votre site web (votre webmanager, les personnes qui vous ont aidé à mettre en place votre site, votre personnel interne ou votre hébergeur).
>
> Etape 2 : Travaillez avec l'entreprise auprès de laquelle vous avez acheté votre nom de domaine et changez le "Time to Live" ou TTL à 1 heure (vous pouvez trouver des instructions sur la façon de le faire sur les sites de nombreux fournisseurs, comme [Gandi](https://docs.gandi.net/fr/noms_domaine/faq/type_enregistrements_dns/a_record.html) ou [GoDaddy](https://fr.godaddy.com/help/gerer-les-dns-680)). Cela peut vous aider à rediriger votre site beaucoup plus rapidement une fois qu'il est attaqué (la valeur par défaut est 72 heures, ou trois jours). Ce paramètre se trouve souvent dans les propriétés "avancées" de votre domaine, parfois dans les enregistrements SRV ou Service.
>
> Etape 3 : Déplacer votre site vers un service d'atténuation DDoS. Pour commencer :
>
>     - [Deflect.ca](https://deflect.ca/)
>     - [Google's Project Shield](https://projectshield.withgoogle.com/landing?hl=fr)
>     - [CloudFlare Projet Galileo](https://www.cloudflare.com/fr-fr/galileo/)
>
> Pour une liste complète des organisations de confiance qui peuvent aider à atténuer une attaque DDoS, passez [à la dernière section de ce processus de travail qui traite des attaques DDoS](#ddos_end).
>
> Etape 4 : Dès que vous avez repris le contrôle, passez en revue vos besoins et décidez entre un hébergeur sécurisé ou tout simplement de continuer avec votre service d'atténuation DDoS.

- Pour une liste complète des organisations de confiance qui peuvent fournir un hébergement sécurisé, passez [à la dernière étape de ce processus de travail qui traite des problèmes d'hébergement web](#web_hosting_end).


### defaced-attack-yes

> La dégradation ou défacement (defacing) d'un site Web est une pratique par laquelle un·e attaquant·e remplace le contenu ou l'apparence visuelle du site Web par son propre contenu. Ces attaques sont généralement menées soit en exploitant les vulnérabilités des plates-formes CMS non maintenues et donc sans les dernières mises à jour de sécurité, soit en utilisant des noms d'utilisateurs·rices/mots de passe de comptes volés.
>
> Etape 1 : Vérifiez qu'il s'agit d'une prise de contrôle malveillante de votre site Web. Une pratique malheureuse mais légale consiste à acheter des noms de domaine récemment expirés pour "reprendre" le trafic qu'ils avaient à des fins publicitaires. Il est très important d'être rigoureux·se·s sur les paiements pour votre nom de domaine.
> Étape 2 : Si votre site Web a été endommagé, reprenez d'abord le contrôle de votre compte de connexion à votre site Web et réinitialisez votre mot de passe, consultez la section "Je ne peux pas accéder à mon compte" pour obtenir de l'aide.
> Etape 3 : Faites une sauvegarde du site défacé qui pourra être utilisée plus tard pour investiguer.
> Etape 4 : Désactivez temporairement votre site web - utilisez une simple page d'accueil ou une page 'parking'.
> Etape 5 : Déterminez comment votre site a été piraté. Votre hébergeur peut être en mesure de vous aider. Les problèmes les plus courants peuvent provenir des parties plus anciennes de votre site avec des scripts/outils personnalisés qui s'exécutent, des systèmes de gestion de contenus (CMS) obsolètes, et une programmation personnalisée avec des failles de sécurité.
> Etape 6 : Restaurer votre site d'origine à partir de sauvegardes. Si ni vous, ni votre hébergeur n'avez de sauvegardes, vous devrez peut-être reconstruire votre site à partir de zéro ! Notez également que si vos seules sauvegardes sont chez votre hébergeur, un·e attaquant·e peut être capable de les supprimer lorsqu'iel prend le contrôle de votre site !

Ces recommandations vous ont-elles aidé ?

- [Oui](#resolved_end)
- [Non](#website-down_end)


### website-down_end

> Si vous avez encore besoin d'aide après toutes les questions auxquelles vous avez répondu, vous pouvez contacter une organisation de confiance et demander de l'aide.
>
> Avant de prendre contact avec une organisation, posez-vous les questions suivantes :
>
> Comment l'entreprise/organisation est-elle structurée et soutenue ? Quels types de vérification ou de rapports doivent-ils faire, s'il y a lieu ?
> Examinez dans quel(s) pays ils ont une présence légale et dans quel(s) pays ils seraient tenus de se conformer aux demandes des forces de l'ordre et autres demandes légales.
> Quels sont les logs créés, et pour combien de temps sont-ils disponibles ?
> Existe-t-il des restrictions concernant le type de contenu que le service hébergera / proxy, et pourraient-elles avoir un impact sur votre site ?
> Existe-t-il des restrictions quant aux pays où ils peuvent fournir des services ?
> Accepte-t-il un mode de paiement que vous pouvez utiliser ? Pouvez-vous vous permettre de payer leur service ?
> Communications sécurisées - vous devriez pouvoir vous connecter en toute sécurité et communiquer en privé avec le fournisseur de services.
> Existe-t-il une option d'authentification à deux facteurs, pour améliorer la sécurité des accès administratif ? Cette technique ou des politiques d'accès sécurisé connexes peuvent aider à réduire la menace vis à vis d'autres formes d'attaques contre votre site Web.
> A quel type de soutien en continu aurez-vous accès ? Y a-t-il un coût supplémentaire pour l'assistance, et/ou recevrez-vous une assistance suffisante si vous utilisez un niveau 'gratuit' ?
> Pouvez-vous 'tester' votre site web avant de le déplacer sur un espace en production ?

Voici une liste d'organisations qui peuvent vous aider à régler votre problème :

:[](organisations?services=web_protection)

### legal_end

> Si votre site Web est en panne pour des raisons juridiques et que vous avez besoin d'un soutien juridique, veuillez communiquer avec un organisme qui peut vous aider :

:[](organisations?services=legal)

### advocacy_end

> Si vous souhaitez obtenir de l'aide pour lancer une campagne contre la censure, veuillez prendre contact avec des organisations qui peuvent vous aider dans vos efforts de mobilisation :

:[](organisations?services=advocacy)

### ddos_end

> Si vous avez besoin d'aide pour atténuer une attaque DDoS contre votre site Web, veuillez vous référer aux organisations spécialisées dans l'atténuation DDoS :

:[](organisations?services=ddos)


### web-hosting_end

> Si vous recherchez une organisation de confiance pour héberger votre site Web dans un serveur sécurisé, veuillez consulter la liste ci-dessous :
>
> Avant de vous mettre en contact avec ces organisations, réfléchissez à ces questions :
>
> - Offrent-ils un support complet pour le transfert de votre site vers leur service ?
> - Les services sont-ils égaux ou supérieurs à ceux de votre hébergeur actuel, au moins pour les outils/services que vous utilisez ? Les principales choses à vérifier sont :
>     - Tableaux de bord de gestion comme cPanel
>     - Comptes de messagerie (combien, quotas, accès via SMTP, IMAP)
>     - Bases de données (nombre, types, accès)
>     - Accès à distance via SFTP/SSH
>     - Support du langage de programmation (PHP, Perl, Ruby, cgi-bin access...) ou CMS (Drupal, Joomla, Wordpress...) que votre site utilise

Voici une liste d'organismes qui peuvent vous aider pour les questions d'hébergement Web :

:[](organisations?services=web_hosting)


### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Sauvegardes** - En plus des services et suggestions ci-dessous, c'est toujours une bonne idée de vous assurer d'avoir des sauvegardes (que vous stockez ailleurs que sur le même site Web !). De nombreux hébergeurs et plates-formes de sites Web l'ont inclus, mais il est préférable d'en avoir des copies supplémentaires hors ligne.

- **Maintenir les logiciels à jour** - Si vous utilisez un système de gestion de contenu (CMS) tel que WordPress ou Drupal, assurez-vous que la technologie de votre site Web est à jour, spécialement s'il y a des mises à jour de sécurité à faire.

- **Surveillance** - Il existe de nombreux services qui peuvent constamment surveiller votre site et vous envoyer des emails ou des messages texte s'il tombe en panne. [Cet article de Mashable](http://mashable.com/2010/04/09/free-uptime-monitoring/) en énumère 10 populaires. Sachez que le courriel ou le numéro de téléphone que vous utilisez pour la surveillance sera clairement associé à la gestion du site Web.


#### Resources

- [EFF: Maintenir votre site en vie](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: DDoS mesures proactives et réactives](https://www.cert.be/en/paper/ddos-proactive-and-reactive-measures)
- [Sucuri: Qu’est-ce qu’une attaque DDoS ?](https://sucuri.net/guides/what-is-a-ddos-attack/)
