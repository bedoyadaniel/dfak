---
layout: page
title: "Numéro de téléphone"
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/phone-number.md
parent: /fr/
published: true
---

Les communications téléphoniques mobiles et fixes vers vos destinataires ne sont pas chiffrées, de sorte que le contenu de votre conversation et les informations sur les personnes que vous contactez sont accessibles par les gouvernements, les organismes d'application de la loi ou d'autres parties disposant de l'équipement technique nécessaire.

