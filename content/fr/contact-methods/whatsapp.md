---
layout: page
title: WhatsApp
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/whatsapp.md
parent: /fr/
published: true
---

L'utilisation de WhatsApp garantit que votre conversation avec le destinataire est protégée afin que seuls vous et le destinataire puissiez lire les communications, mais le fait que vous ayez communiqué avec le destinataire peut être accessible par les gouvernements ou les forces de l'ordre.

Ressources : [Comment : Utiliser WhatsApp sur Android](https://ssd.eff.org/fr/module/guide-pratique-utiliser-whatsapp-pour-android)[Comment : Utiliser WhatsApp sur iOS](https://ssd.eff.org/fr/module/guide-pratique-utiliser-whatsapp-pour-ios).

