---
layout: page
title: Tor
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/tor.md
parent: /fr/
published: true
---

Le navigateur Tor est un navigateur Web axé sur la confidentialité qui vous permet d'interagir avec des sites Web de manière anonyme en ne partageant pas votre position (via votre adresse IP) lorsque vous accédez au site Web. Ressources : 
[Vue d'ensemble de Tor](https://www.torproject.org/about/overview.html.en).

