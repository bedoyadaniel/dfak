---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: du lundi au vendredi GMT+5.30
response_time: En une journée en dehors des heures d’activité, 2 heures sur les heures de travail
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Le Tibetan Computer Emergency Readiness Team (TibCERT) cherche à créer une organisation formelle et collective pour réduire et atténuer les menaces en ligne vis à vis de la communauté tibétaine, ainsi qu’à développer la capacité de recherche technique des Tibétains sur les menaces qui pèse sur la diaspora, sur la surveillance et la censure à l'intérieur Tibet, garantissant ainsi une plus grande liberté et sécurité en ligne pour la société tibétaine dans son ensemble.

Les missions de TibCERT's incluent:

- Créer et maintenir une plate-forme pour une collaboration à long terme entre les parties prenantes de la communauté tibétaine sur les problèmes et les besoins en matière de sécurité numérique
- Approfondir les connexions et développer un processus formel de collaboration entre les Tibétains et les chercheurs à l’échelle mondiale sur les logiciels malveillants et la cybersécurité afin d'assurer un partage mutuellement avantageux
- Augmenter les ressources dont disposent les Tibétains pour se défendre et atténuer les attaques en ligne en publiant régulièrement des informations et des recommandations sur les menaces auxquelles la communauté est confrontée
- Aider les Tibétains au Tibet à contourner la censure et la surveillance en fournissant des informations et des analyses régulières et détaillées, ainsi que des solutions potentielles.
