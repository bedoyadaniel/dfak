---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost offre des services numériques en respectant une approche éthique et durable. Les services proposés incluent: l’hébergement et le stockage en ligne de type cloud, et des solutions efficaces pour la sécurité numérique. Greenhost est engagée activement dans le développement open source, et participe à des projets variés dans les domaines de la technologie, du journalisme, de la culture, de l’éducation, de la durabilité et des libertés sur le net.
