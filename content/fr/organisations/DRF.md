---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 jours sur 7 de 9h à 17h UTC+5 (PST)
response_time: 56 heures
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) est une organisation non-gouvernementale de défense des droits numériques basée au Pakistan. Crée en 2012, DRF est spécialisée dans les technologies numériques pour soutenir les droits de l’homme, favoriser l’inclusion, les processus démocratiques et de gouvernance. DRF travaille également sur les enjeux liés à la liberté d’expression, la protection des données et de la vie privée ainsi que sur les violences en ligne faites aux femmes.
