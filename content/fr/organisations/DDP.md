---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, organisation_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrd, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Le Digital Defenders Partnership offre son soutien aux défenseurs des droits humains qui sont victimes de menace numérique et travaille à solidifier les réseaux de réponse rapide localement. DDP coordonne le soutien d’urgence pour les individus et les organisations comme les défenseurs des droits de l’homme, les journalistes, les activistes sociaux et les blogueurs.

DDP distribue trois types d’aide financière pour répondre aux situations d’urgence et propose également des fonds à plus long terme dédiés au développement des compétences au sein des organisations. En outre, ils coordonnent un réseau de réponse rapide et une bourse "Digital Integrity Fellowship" où les organisations reçoivent des accompagnements  personnalisées sur la sécurité et la confidentialité numériques.
