---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 2 hours
contact_methods: web, email, pgp
email: help@accessnow.org
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

La ligne d'assistance en sécurité numérique d'Access Now travaille avec des individus et des organisations du monde entier pour assurer leur sécurité en ligne. Si vous êtes dans des situations à risques, nous pouvons vous aider à améliorer vos pratiques de sécurité numérique afin de vous mettre à l'abri du danger. Si vous êtes déjà attaqué, nous fournissons une aide d'urgence rapide.

