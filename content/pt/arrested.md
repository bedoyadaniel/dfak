---
layout: sidebar.pug
title: "Alguém que eu conheço foi preso"
author: Peter Steudtner, Shakeeb Al-Jabri
language: pt
summary: "Um amigo, colega ou membro da família foi detido pelas forças de segurança. Você deseja limitar o impacto de sua prisão sobre eles e qualquer outra pessoa que possa estar implicada com eles."
date: 2019-03-13
permalink: /pt/arrested/
parent: Home
sidebar: >
  <h3>Leia mais sobre o que fazer se alguém que você conhece foi preso:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspiração e orientações para pessoas encarceradas e suas famílias, advogades e apoiadores</a></li>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Obtendo suporte para campanhas em nome da pessoa detida</a></li>
  </ul>
---

# Alguém que eu conheço foi preso

Detenções de defensores de direitos humanos, jornalistas e ativistas os colocam e aqueles com quem trabalham e vivem em grande risco.

Este guia é especialmente orientado para países que não seguem os direitos humanos e devido processo legal, ou onde as autoridades podem contornar procedimentos legais, ou onde atores não estatais operam livremente, detenções ou prisões representam um risco maior para as vítimas, seus colegas e parentes.

Neste guia, pretendemos aliviar o perigo que eles enfrentam e limitar o acesso dos detentores a dados sensíveis que possam incriminar as vítimas e seus colegas, ou que possam ser usados para comprometer outras operações.

Cuidar do detido assim como dos impactos digitais dessa detenção pode ser desgastante e desafiador. Tente encontrar outras pessoas para apoiá-lo e coordenar suas ações com a comunidade envolvida.

Também é importante que você cuide de si mesmo e de outras pessoas afetadas por essa detenção:

- [cuidando do seu bem-estar psicossocial e necessidades](../self-care),
<!-- - [cuidando do lado legal do seu trabalho de suporte](***link para a seção RaReNet sobre organizações ajudando em questões legais***) -->
- [obter apoio para fazer campanha em nome da pessoa detida](https://www.newtactics.org/search/solr/arrest)

Se você se sentir tecnicamente ou emocionalmente sobrecarregado ou (por qualquer motivo) que não esteja em condições de seguir as etapas descritas abaixo, entre em contato com o suporte e a orientação das organizações listadas [aqui](../support) que oferecem triagem inicial entre seus serviços.


## Faça um plano

Antes de agir nas diferentes seções que descrevemos abaixo, siga os seguintes passos:

- Tente ler todo o guia e tenha uma visão geral de todas as áreas importantes do impacto antes de agir sobre aspectos singulares. O motivo é que as diferentes seções destacam diferentes cenários de ameaças que podem se sobrepor, portanto, você precisará criar sua própria sequência de ações.
- Reserve um tempo com amigos ou uma equipe para realizar as diferentes avaliações de risco necessárias nas diferentes seções.

<a name="harm-reduction"></a>
## Prepare-se antes de agir

Você tem motivos para acreditar que essa prisão ou detenção pode levar a repercussões para os membros da família, amigos ou colegas do detento, incluindo você mesmo?

Este guia orientará você em uma série de etapas para ajudar a fornecer soluções que possam ajudar a reduzir a exposição dos detalhes e a qualquer pessoa envolvida com eles.

**Dicas para resposta coordenada**:

Em todas as situações em que uma pessoa foi detida, a primeira coisa a notar é que, muitas vezes, quando tais incidentes ocorrem, vários amigos ou colegas reagem imediatamente, resultando em esforços duplicados ou contraditórios. Por isso, é importante lembrar que a coordenação e a ação combinada, em nível local e internacional, são necessárias tanto para apoiar o detento quanto para cuidar de todos os demais dentro de suas redes de apoio, família e amigos.

- Estabeleça uma equipe de resposta crítica que coordenará todas as atividades de apoio, cuidado, campanha etc.
- Envolva os membros da família, parceiros, etc., tanto quanto possível (respeitando os seus limites se, cuidando para nao sobrecarrega-los).
- Tenha metas bem estabelecidas para sua campanha de apoio (e as reveja frequentemente): por exemplo, priorizar a soltura do detido, para assegurar seu bem estar, ou proteger sua família e apoiadores e garantir o bem estar deles.
- Acorde canais de comunicação seguros, e também a frequência e limitações destas comunicações (sem comunicações entre 10 da noite e 8 da manhã exceto por novidades sobre o caso ou emergências, por exemplo)
- Distribua tarefas entre os membros da equipe e busque apoio de ajuda externa especializada (forense, advocacy, mídia, documentação, jurídico etc)
- Peça por ajuda fora da equipe de resposta crítica para prover necessidades básicas para o grupo durante esta fase (alimentação, contabilidade etc)

**Cuidados de Segurança Digital**

Se você tem motivos para temer repercussões para si ou outros apoiadores, antes de enfrentar qualquer emergência digital relacionada com a detenção de companheiros, também é crucial tomar algumas medidas preventivas com relação à segurança digital para proteger a si e aos outros do perigo imediato.

- Acordar em quais canais de comunicação seguros sua rede de apoiadores irá coordenar estratégias de mitigação e comunicar sobre a detenção e sua repercussão.
- Reduzir o tráfego de dados em seus dispositivos ao mínimo necessário, e proteger os dados em seus dispositivos com [criptografia](https://ssd.eff.org/pt-br/module/mantendo-seus-dados-seguros).
- Criar [cópias seguras e criptografas](https://helplinedocs.accessnow.org/182-Secure_Backup.html) de todos os seus dados e mantê-los em um lugar que não seria encontrado durante mandatos de busca e apreensões.
- Compartilhar as senhas de dispositivos, contas online etc. com uma pessoa de confiança que não está em perigo imediato.
- Acordar as primeiras ações em caso de possíveis detenções (como suspensões de conta, formatação de dispositivos à distância etc.)

Este guia conduzirá você através de uma série de passos para ajudar a fornecer soluções que possam reduzir a exposição de pessoas detidas ou envolvidas com elas, por exemplo, porque informações sobre contatos podem ser encontrados nos dispositivos da pessoa detida, ou em suas contas de mídia social e email.

## Gestão de riscos
### Redução de possíveis danos causados por suas próprias ações

Geralmente, você deve tentar basear suas ações na seguinte questão:

- Quais os impactos das ações individuais e combinadas sobre a pessoa detida, e também sobre suas comunidades, companheiros ativistas, amigos, família, etc. incluso você?

Cada uma das seções a seguir descreverá os aspectos especiais desta avaliação dos riscos.

As considerações básicas são:

- Antes de apagar contas, dados, postagens de redes sociais, etc., certifique-se de que documentou o conteúdo e a informação que irá apagar, especialmente se tiver de restaurar esse conteúdo ou informação, ou precisar dela para provas posteriores.
- Se apagar contas ou arquivos, esteja ciente de que:
    - As autoridades podem interpretar isto como destruição ou remoção de provas
    - Isso poderia tornar mais difícil a situação da pessoa detida. Caso o acesso a estas informações fosse solicitado e as autoridades não pudessem encontrá-las, a pessoa detida passaria a suspeita e ações por omissão poderiam ser desencadeadas contra ela.
- Caso você informar às pessoas que suas informações pessoais estão sendo armazenadas em um dispositivo ou conta solicitada pelas autoridades, e esta comunicação for interceptada, isso pode ser usado com o intuito de conectá-las como cúmplices da pessoa detida.
- Alterações nos procedimentos de comunicação (incluindo apagamento de contas) pode despertar a atenção das autoridades


### Informando contatos

Em geral, é impossível determinar se as autoridades possuem a capacidade de mapear a rede de contatos da pessoa detida, e se o fizeram ou não. Portanto, precisamos assumir o pior caso; que elas o fizeram ou farão.

Antes de começar a informar os contatos, avalie o risco de informá-los:

 - Você tem uma cópia da lista de contatos da pessoa detida? Você tem acesso às listas de contatos em seus dispositivos, contas de email, plataformas de redes sociais? Compile uma lista de possíveis contatos para obter uma visão geral de pessoas que podem ser afetadas.
- Existe risco de que informar os contatos possa conectá-los à pessoa detida com a intenção de criminalizá-las?
- Todos devem ser informados, ou apenas um grupo específico de contatos?
- Quem informará os contatos? Quem já está em contato com quem? Qual é o impacto desta decisão?
- Estabelecer o canal de comunicação mais seguro, incluindo reuniões pessoais em espaços onde não há câmeras de circuito interno, para informar os contatos implicados.


### Documentar para manter evidências

Antes de excluir qualquer conteúdo de sites, postagens de mídia social, etc., certifique-se de ter documentado tudo antes. Uma das razões para documentar este conteúdo seria manter vestígios ou prova de contas invadidas - como conteúdo modificado ou fraude de identidade - além de manter evidências jurídicas.

Dependendo do site ou plataforma de mídia social que você deseje documentar conteúdo online, diferentes abordagens podem ser utilizadas:

- Você pode printar trechos relevantes (certifique-se de que os horários, URLs, etc. estão incluídos nas imagens).
- Você pode verificar se sites ou blogs relevantes estão indexados na [Wayback Machine](https://archive.org/web), ou baixar uma cópia dos sites ou blogs para a sua máquina local.

*Lembre-se que é importante manter a informação que você baixou armazenada em um dispositivo e uma localização segura*


### Apreensão de dispositivos

Se quaisquer dispositivos da pessoa detida forem confiscados durante ou após a prisão, leia atentamente o guia [Perdi meus dispositivos](../topics/lost-device), em particular a seção sobre [formatação remota](../topics/lost-device/questions/find-erase-device), que inclui recomendações em caso de detenção.


### Dados e contas incriminantes

Se a pessoa detida possuir informação em seus dispositivos que possa ser danosa a si mesma ou outras pessoas, é importante delimitar o acesso a estas informações.

Antes de fazer isto, compare o risco causado por esta informação com o risco causado pela insatisfação das forças de operação perante à falta de acesso a esta informação (ou o risco de uma ação judicial por destruição de evidência). Se o risco for maior, remova os dados em questão e remova contas e referências a ela seguindo as instrução abaixo.

#### Suspenda ou encerre contas online

Se você tiver acesso às contas que deseja fechar, você pode seguir os processos específicos de cada plataforma. Por gentileza, certifique-se de ter uma cópia dos dados e conteúdos excluídos! Esteja ciente que, após fechar uma conta, o conteúdo não se tornará imediatamente inacessível: no caso do Facebook, por exemplo, pode levar até duas semanas para que o conteúdo seja excluído de todos os servidores.

Se você não tiver acesso às contas da pessoa detida ou precisar de uma ação mais urgente nas contas das redes sociais, busque o apoio das organizações listadas [aqui](../ suporte) que oferecem entre seus serviços a segurança de contas.


#### Remover contas de dispositivos

Em alguns casos, você pode querer desconectar as contas dos dispositivos, uma vez que mantê-las conectadas pode dar acesso a dados sensíveis a quaisquer pessoas com acesso a controlar o dispositivo. Para fazer isto, siga as instruções contidas no [fluxo diagnóstico "Perdi meu dispositivo"](../topics/lost-device/questions/accounts).

Não se esqueça de remover também [contas bancárias online](#online_bank_accounts) destes dispositivos.


#### Trocar as senhas

Caso decida não suspender as contas, ainda é uma boa ideia mudar as senhas de todas, seguindo as instruções no fluxo ["Perdi meu dispositivo"](../topics/lost-device/questions/passwords).

Considere também habilitar a autenticação em dois fatores para aumentar a segurança das contas da pessoa detida, seguindo as instruções no fluxo ["Perdi meu dispositivo"](../topics/lost-device/questions/2fa).

Caso a mesma senha seja usada em contas diferentes, você precisa mudar todas as contas em que isso acontece, uma vez que elas já podem estar comprometidas.

**Outras dicas sobre troca de senhas**

- Use um gerenciador de senhas (como o [KeepassXC](https://keepassxc.org)) para registrar as senhas trocadas para uso posterior, ou para entregá-las de volta à pessoa detida quando estiver em liberdade.
- Lembre-se de dizer à pessoa detida sobre a necessidade de troca das senhas após sua soltura, e de devolver a ela a propriedade de suas contas.


#### Remoção de grupos e compartilhamentos

Se a pessoa detida está em qualquer grupo (como, mas não limitado a) grupos do Facebook, WhatsApp, Signal, ou Wire, ou pode acessar pastas compartilhadas online, e sua presença nesses grupos dá aos seus detentores acesso a informações privilegiadas e potencialmente perigosas, você deve removê-la de grupos ou espaços compartilhados online.

**Instruções sobre como remover membros de grupos em diferentes serviços de mensagem:**

- [WhatsApp](https://faq.whatsapp.com/pt-br/android/26000116/?category=5245251)
- Telegram - A pessoa que criou o grupo pode remover participantes selecionando “informações do grupo “ e deslizando o usuário que desejam remover para a esquerda.

- Wire
    - [instruções para celular](https://support.wire.com/hc/en-us/articles/203526410)
    - [instruções para o app no desktop ](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- Signal - você pode não pode remover participantes dos grupos do Signal - o que você pode fazer é criar um novo grupo excluindo a pessoa que está sob custódia

**instruções sobre como remover compartilhamento de pastas em diferentes serviços online :**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- GDrive - busque pelos arquivos da pessoa usando o operador de busca “para:” ou “to:” (`para:usuario@gmail.com`), e então selecione todos os resultados e clique no ícone Compartilhar. Clique em Avançado, remova o endereço dela da caixa de diálogo e salve.
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/pt-BR/HT201081)


### Deletar postagens

Em alguns casos você pode querer remover conteúdo das linhas de tempo da pessoa detida ou de outros feeds de postagens vinculadas à sua conta, pois podem ser forçados como evidência contra ela ou gerar desconfiança e conflitos para com sua comunidade.

Alguns serviços facilitam a exclusão de conteúdo. Guias para o Twitter, Facebook e Instagram serão indicados abaixo. Por gentileza, certifique-se de ter documentado o conteúdo que você deseja deletar, caso precise dele como evidência de adulteração etc

- No Twitter, você pode usar [Tweet Deleter](https://tweetdeleter.com/).
- Para o Facebook, você pode seguir o guia ["Como deletar seu feed de notícias do Facebook", de Louis Barclay](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), baseado na extensão [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac), para Google Chrome.
- Para o Instagram, você pode seguir as instruções sobre [como editar e deletar postagens](https://help.instagram.com/997924900322403) em seu site (levando em conta que, ainda que delete as postagens, pessoas que tiverem acesso à sua conta terão acesso ao histórico destas interações).


### Apague associações negativas online

Se houver qualquer informação online que possa ter impacto negativo na situação da pessoa detida ou seus contatos, é uma boa ideia removê-la caso não cause outros danos futuros.

- Faça uma lista de espaços e informações que precisam ser modificadas ou removidas.
- Se você identificou conteúdo que deve ser modificado ou removido, mantenha uma cópia antes de prosseguir com a remoção ou solicitar remoções.
- Avalie se a exclusão da conta da pessoa detida terá impacto negativo em sua situação (remover seu nome da lista de colaradores de uma organização pode protegê-la, mas pode também retirar um possível álibi quanto à posição da pessoa dentro dela).
- Se você tem acesso às respectivas contas e sites, remova a informação ou conteúdo sensível.
 - Se não possuir acesso, solicite a pessoas com acesso que façam a remoção do conteúdo.
- Você pode encontrar instruções sobre remover conteúdo em serviços do Google [aqui](https://support.google.com/webmasters/answer/6332384?hl=pt-br#get_info_off_web)
- Verifique se os sites que possuem informações foram indexados pela Wayback Machine ou pelo Google Cache. Se sim, este conteúdo deve ser removido também.


<a name="online_bank_accounts"></a>
### Contas bancárias online

Frequentemente, contas bancárias são gerenciadas e acessadas online, e verificações pelo celular são necessárias para transferências ou mesmo acesso à conta. Se a pessoa detida não estiver acessando suas contas online por longos períodos de tempo, descuidar destes acessos pode trazer implicações financeiras no futuro. Nestes casos, certifique-se do seguinte:

- Desvincule aparelhos apreendidos das contas da pessoa detida.
- Busque autorização judicial da pessoa detida para movimentar suas contas em seu nome já nas fases iniciais do processo (em acordo com seus familiares).


## Dicas finais

- Certifique-se de retornar toda a propriedade de dados de volta à pessoa após sua liberdade.
- Leia [as dicas encontradas em "Perdi meu dispositivo"](../topics/lost-device/questions/device-returned) sobre como lidar com dispositivos apreendidos após sua devolução pelas autoridades.
