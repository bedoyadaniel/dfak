---
layout: sidebar.pug
title: "Lidando com sobrecarga emocional?"
author: FP
language: pt
summary: "Assédio online, ameaças e outros tipos de ataques digitais podem criar sentimentos avassaladores e estados emocionais muito delicados: você pode sentir-se culpado, envergonhado, ansioso, zangado, confuso, desamparado ou até mesmo com medo de seu bem-estar psicológico ou físico."
date: 2023-04
permalink: /pt/self-care/
parent: Home
sidebar: >
  <h3>Leia mais sobre como se proteger emocionalmente:</h3>

  <ul>
    <li><a href="https://www.eff.org/pt-br/deeplinks/2015/01/enfrentando-o-desafio-do-assedio-online">Enfrentando o Desafio do Assédio Online</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Cuidar de si proprio para que você possa continuar defendendo os direitos humanos</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Autocuidado para pessoas com experiência em assédio</a></li>
    <li><a href="https://escoladeativismo.org.br/wp-content/uploads/2022/09/LabCuidados_Zine2_Ansiedade_WEB_pag_soltas.pdf">Lab Cuidados de Escola de Ativismo</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Módulo de treinamento de autocuidado de uma mulher na era digital</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care">Bem-estar e Comunidade</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Vinte maneiras de ajudar alguém que está sendo intimidado on-line</a></li>
    <li><a href="https://mariadajuda.org/">Maria d'Ajuda, a primeira linha de ajuda em segurança digital feita por feministas do Brasil voltada a mulheres, pessoas não binárias, LGBTQIAP+ e organizações da América Latina.</a></li>
  </ul>
  
---

# Lidando com sobrecarga emocional?

Assédio online, ameaças e outros tipos de ataques digitais podem criar sentimentos avassaladores e estados emocionais muito delicados: você pode sentir-se culpado, envergonhado, ansioso, zangado, confuso, desamparado ou até mesmo com medo de seu bem-estar psicológico ou físico.

Não existe um modo "certo" de sentir, pois o seu estado de vulnerabilidade e o que a sua informação pessoal significa para você é diferente de pessoa para pessoa. Qualquer emoção é justificada e você não deve se preocupar se sua reação é ou não a correta.

A primeira coisa que você deve se lembrar é que o que está acontecendo com você não é sua culpa e você não deve se culpar, mas, possivelmente, chegar a alguém confiável que possa apoiá-lo no atendimento desta emergência.

Para atenuar um ataque on-line, você precisará coletar informações sobre o que aconteceu, mas não precisa fazê-lo sozinho - se tiver uma pessoa da sua confiança, peça a ele que o apoie, seguindo as instruções em este site ou conceda a eles acesso a seus dispositivos ou contas para coletar informações para você.
