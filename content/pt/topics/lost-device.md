---
layout: page
title: "Perdi meu dispositivo"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: pt
summary: "Perdi meu dispositivo, o que devo fazer?"
date: 2023-04
permalink: /pt/topics/lost-device
parent: /pt/
---

# Perdi meu dispositivo

Seu dispositivo foi perdido ou levado por outra pessoa?

Nestas situações, é importante tomar medidas imediatas para reduzir o risco de ter outras pessoas acessando suas contas, contatos e informações pessoais.

Esta seção do Kit de Primeiros Socorros Digitais irá te guiar através de questões básicas para avaliar como reduzir possíveis danos relacionados com perdas de dispositivo.

## Workflow

### question-1

O dispositivo continua perdido?

 - [Sim, continua perdido](#device-missing)
 - [Não, ele já retornou para mim](#device-returned)

### device-missing

> É bom refletir sobre quais proteções de segurança seu dispositivo possuía:
>
> * O acesso ao seu dispositivo era protegido por senha ou outra medida de proteção?
> * O dispositivo possui criptografia habilitada?
> * Qual o status do dispositivo quando foi perdido - ele estava desbloqueado? Conectado em alguma de suas contas? Em suspensão ou hibernação? Desligado?

Com isto em mente, você poderá entender o quão possível seria alguém em posse do dispositivo ter acesso ao seu conteúdo.

- [Vamos remover o acesso do dispositivo às minhas contas](#accounts)

### accounts

> Liste todas as contas a que este dispositivo tem acesso. Estas contas podem ser de email, mídia social, aplicativos de mensagem, encontros, ou banco, bem como contas que usam o dispositivo como segundo fator de autenticação.
>
> Para cada conta que seu dispositivo tiver acesso, você deve remover conectar a ela e remover a autorização para este aparelho. Isto pode ser feito removendo-o da lista de dispositivos permitidos nas configurações de cada conta.
>
> * [Google Account](https://myaccount.google.com/device-activity)
> * [Facebook Account](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud Account](https://support.apple.com/pt-br/HT201472)
> * [Twitter Account](https://twitter.com/settings/sessions)
> * [Yahoo Account](https://login.yahoo.com/account/activity)

Uma vez que tenha terminado de desvincular suas contas, vamos fortalecer as senhas que eram utilizadas neste dispositivo.

- [Certo, vamos cuidar das senhas](#passwords)

### passwords

> Tente se lembrar de qualquer senha que tenha sido salva direto no dispositivo ou navegadores que tinham senhas salvas para os seus sites.
>
> Mude suas senhas para todas as contas que eram acessíveis por este dispositivo. Se você não usa um gerenciador de senhas, considere começar a usar um a partir de agora para manter e criar senhas com maior qualidade.

Após trocar as senhas das contas que estavam no seu dispositivo, reflita se você utilizava alguma destas senhas para outras contas ou sites - se sim, troque estas senhas também, pois podem ter sido comprometidas.

- [Eu usei algumas das senhas no dispositivo em outras contas](#same-password)
- [Todas as senhas que foram comprometidas não eram repetidas](#2fa)

### same-password

Você usa a mesma senha em outras contas ou dispositivos além daquele que foi perdido? Se sim, troque estas senhas também, pois podem ter sido comprometidas.

- [Certo, agora eu troquei todas as senhas importantes](#2fa)

### 2fa

> Habilitar a autenticação em dois fatores nas contas que você imagina que podem estar em risco reduzirá a possibilidade de serem acessadas por terceiros.
>
> Faça isso para todas as contas que estavam acessíveis no dispositivo perdido. Note que nem todas as contas suportam autenticação em dois fatores. Para uma lista de serviços e plataformas que suportam este método, visite [Two Factor Auth](https://twofactorauth.org).

- [Eu habilitei a autenticação em dois fatores em todas as minhas contas para aumentar sua segurança. Agora vamos tentar encontrar e formatar o dispositivo](#find-erase-device)

### find-erase-device

> Pense sobre os usos cotidianos do dispositivo perdido - haviam dados sensíveis nele, como contatos, localizações ou mensagens? Caso sejam expostos, estes dados podem causar problemas a você, ao seu trabalho ou a pessoas próximas?
>
> Em alguns casos, é possível excluir estes dados remotamente do dispositivo mesmo que esteja em posse de outra pessoa, para prevenir que estes dados sejam usados de má fé. É um recurso que pode ser útil em casos de roubo, mas também em casos como por exemplo na ocorrência de uma prisão ilegal que possa expor uma pessoa ativista e suas companheiras. Evidente que, em situações mais delicadas como esta, é preciso medir as consequências que isso gera para a pessoa dona do aparelho (especialmente em situações onde possa haver coação e tortura) - visto que isto pode ser encarado como obstrução da lei caso seja forçada a entregar o acesso ao dispositivo, ou ainda colocar pessoas próximas em risco desnecessário. Leia a seção ["Uma pessoa próxima foi presa"](../../../../arrested) para estratégias mais detalhadas nestes casos.
>
> Também é importante notar que, em alguns países, formatar remotamente um dispositivo pode ser encarado como destruição de evidências.
>
> Se formatar o dispositivo em questão não traz consequências posteriores e parece uma boa opção, siga as instruções abaixo para seu sistema operacional:
>
> * [Dispositivos Android](https://support.google.com/accounts/answer/6160491?hl=pt-br)
> * [iPhone ou Mac](https://www.icloud.com/#find)
> * [Dispositivos iOS (iPhone and iPad) usando iCloud](https://support.apple.com/kb/PH2701?viewlocale=pt_BR)
> * [Windows phone](https://support.microsoft.com/pt-br/help/11579/microsoft-account-find-and-lock-lost-windows-device)
> * [Blackberries](https://docs.blackberry.com/en/endpoint-management/blackberry-uem/12_16/administration/device-features-it-policies/Setting-up-factory-reset-protection-for-Android-Enterprise-devices
)
> * Para computadores Windows ou Linux, você pode ter softwares específicos instalados (como anti-virus ou anti-roubo) que permitam este tipo de exclusão de dados remota. Se tiver, use-os.

Uma vez que tenha conseguido (ou não) excluir remotamente as informações no dispositivo, é uma boa ideia informar seus contatos sobre a perda.

- [Prosseguir com os próximos passos para dicas sobre como informar meus contatos sobre a perda do dispositivo](#inform-network)

### inform-network

> Além de suas próprias contas, seus dispositivos certamente possuem diversas informações sobre outras pessoas. Estas informações incluem seus contatos, suas comunicações com outras pessoas, os grupos que você participava etc.
>
> Ao informar sua rede e sua comunidade sobre a perda do dispositivo, considere os [princípios de redução de danos](../../../../arrested#harm-reduction) para tentar garantir que ao comunicar estas pessoas você não estará as colocando em risco.

Informe sua rede sobre a perda do dispositivo. Isto pode ser feito de forma individual e privada com contatos de alto risco, ou postando uma lista das suas contas comprometidas de forma pública em seu site ou conta de mídia social se sentir que é a forma mais apropriada.

- [Já informei meus contatos](#review-history)


### review-history

> Se possível, revise as atividades da conta e o histórico de conexão de todas as suas contas conectadas ao dispositivo. Verifique se sua conta foi utilizada em algum momento que você não se lembra de estar online ou de algum lugar ou endereço IP incomum ou desconhecido.
>
> Você pode revisar suas atividades nas plataformas populares:
>
> * [Google](https://myaccount.google.com/device-activity)
> * [Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud](https://support.apple.com/pt-br/HT205064)
> * [Twitter](https://twitter.com/settings/sessions)
> * [Yahoo](https://login.yahoo.com/account/activity)

Você revisou seu histórico de conexões?

- [Sim, e não encontrei nada suspeito](#check-settings)
- [Sim, e encontrei atividades suspeitas na minha conta](../../../account-access-issues/)

### check-settings

> Verifique as configurações de todas as contas conectadas com o dispositivo. Alguma delas mudou? Verifique se algum endereço de encaminhamento automático foi adicionado ao seu email, se houve mudanças mudanças nos endereços secundários ou de recuperação, ou nos números de telefone, sincronização com dispositivos estranhos, e novas permissões de aplicativos.
>
> Repita esta revisão de atividade pelo menos uma vez por semana pelo próximo mês, para ter certeza de que suas contas continuam sem atividade suspeita.

- [Meus históricos de atividade estão mostrando atividade suspeita](../../../account-access-issues/)
- [Meus históricos de atividade não mostram nada suspeito, mas continuarei revisando por algum tempo](#resolved_end)


### device-returned

> Se o seu dispositivo foi perdido, tomado por outra pessoa, ou teve que ser entregue durante algum procedimento, mas já retornou a você, mantenha algumas precauções uma vez que você não sabe quem teve acesso a ele. Dependendo do nível de risco que você passou, você pode estar desconfiando que seu dispositivo tenha sido comprometido de algum modo.

> Pergunte-se o seguinte e avalie qual o risco de o dispositivo ter sido violado:

> * Por quanto tempo você ficou sem acesso ao dispositivo?
> * Neste tempo, quem poderia ter tido acesso a ele?
> * Por que iriam querer acessá-lo?
> * Há algum sinal de que tentaram abrir ou alterar fisicamente o aparelho?

> Se você não confia mais na segurança deste dispositivo, considere formatar e reinstalar o sistema, ou em caso extremo avalie adquirir outro aparelho.

Você gostaria de apoio para obter uma novo aparelho em caso específico?

- [Sim](#new-device_end)
- [Não](#resolved_end)


### accounts_end

Se você perdeu acesso às suas contas ou acredita que outra pessoa teve acesso a elas, por gentileza contate uma das organizações abaixo que poderão auxiliar melhor nesta situação.

:[](organisations?services=account)

### new-device_end

Se você precisa de apoio financeiro externo para conseguir um dispositivo novo em casos de comprometimento específicos, contate uma das organizações abaixo que poderão auxiliar nesta situação.

:[](organisations?services=equipment_replacement)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Seguem algumas dicas para diminuir o risco de vazamento de dados e acessos não autorizados às suas contas e informações:

- Nunca deixe seu dispositivo descuidado. Se precisar fazer isso, desligue-o.
- Habilite a criptografia de disco sempre que possível em todos os dispositivos.
- Use uma frase forte como senha dos dispositivos.
- Habilite o Encontrar/Apagar Meu Telefone sempre que possível, mas note que isto poderá ser usado para rastrear seu dispositivo ou excluir seus dados caso sua conta associada (Gmail/iCloud) for comprometida, por isso proteja estas senhas!

#### Resources

* [Autodefesa: Dicas para habilitar criptografia de disco](https://autodefesa.org/#portfolioModal6)
* [Access Now Helpline Community Documentation: Tips on how to enable full-disk encryption](https://communitydocs.accessnow.org/166-Full-Disk_Encryption.html)
*  [MariaLab: Como evitar que agressores tenham acesso ao seu celular](https://www.marialab.org/como-evitar-que-agressores-tenham-acesso-ao-seu-celular/) 
 
* Considere usar software anti-roubo como o [Prey](https://preyproject.com)
