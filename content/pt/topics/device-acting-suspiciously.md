---
layout: topic
title: "Meu dispositivo está com um comportamento estranho"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: pt
summary: "Se o seu computador ou celular está agindo de forma suspeita, pode haver um programa malicioso ou indesejado no dispositivo"
date: 2023-04
permalink: /pt/topics/device-acting-suspiciously
parent: /pt/
---

# Meu dispositivo está com um comportamento estranho

Ataques de malware (software malicioso) evoluíram e se tornaram mais sofisticados com o passar dos anos. Estes ataques representam diferentes ameaças à sua vida digital e podem trazer consequências sérias a ela e aos seus dados e os das suas organizações.

Malwares existem em diferentes formatos, como vírus, iscas digitais (phising), sequestro digital (ransomware), cavalos-de-tróia e controle de dispositivos (rootkits ou C&C). Algumas das ameaças são: comprometimento do sistema, roubo de dados (como senhas de serviços pessoais e de contas bancárias, informações financeiras e cadastrais etc), sequestro de dados e computadores com finalidade de chantagem ou com a finalidade de usá-los como parte de um ataque distribuído de negativação de servidores (DDoS).

Alguns métodos usados frequentemente por malfeitores comprometem a você e aos seus dispositivos através de atividades cotidianas, por exemplo:

- Um email ou post em rede social que seduz você a abrir um link ou baixar um arquivo voluntariamente.

- Forçar pessoas a baixar e instalar um programa ou app de fonte duvidosa.

- Forçar alguém a colocar usuário e senha em um site feito para parecer verdadeiro mas que é uma fraude

Esta seção do Kit de Primeiros Socorros Digitais irá conduzir você através de simples passos para determinar se seu dispositivo pode ou não estar infectado.

Se você acredita que seu computador ou telefone está funcionando de maneira suspeita, comece tentando entender os sintomas que este dispositivo apresenta.

Sintomas que normalmente são interpretados como atividade suspeita, mas que geralmente não são motivos para preocupação, incluem:


- Ouvir cliques durante uma ligação
- Consumo de bateria acima do normal
- Dispositivo esquentando mesmo sem uso
- Dispositivo funcionando muito devagar

Estes sintomas geralmente são mal interpretados como motivos para se preocupar com o comportamento do dispositivo. Ainda assim, nenhum destes sintomas sozinhos são indicadores confiáveis de atividade suspeita.

Geralmente, os sintomas abaixo são confiáveis para determinar um dispositivo comprometido:

- O dispositivo reinicia sozinha sem causa ou defeito aparente
- Aplicativos param de funcionar depois de dar um comando
- Atualizações de segurança falham repetidamente na instalação
- A luz da câmera acende sozinha quando não deveria estar em uso
- Falhas críticas frequentes, como a ["Tela Azul"](https://pt.wikipedia.org/wiki/Tela_azul_da_morte), Kernel Panic (Linux e Android) ou telas "vazias" em outros dispositivos
- Janelas piscando ou aparecendo de repente
- Alertas de vírus

## Workflow

### start

Se você acredita que, mesmo com as informações sugeridas durante a introdução, seus dispositivos possam estar comprometidos, o guia a seguir pode ajudar a identificar o problema.

 - [Acredito que meu telefone esteja comprometido](#phone-intro)
 - [Acredito que meu computador esteja comprometido](#computer-intro)
 - [Acredito que meu dispositivo não esteja mais comprometido](#device-clean)


### device-clean

> Muito bom! No entanto, tenha em mente que estas instruções ajudam apenas a fazer um diagnóstico rápido. Ainda que ele possa ser suficiente para identificar anormalidades mais visíveis, programas espiões melhor elaborados conseguiriam burlar estas regras e se ocultar no sistema de forma eficiente.

Se você ainda suspeita que o dispositivo está comprometido as alternativas mais interessantes seriam:

- [Buscar ajuda externa](#malware_end)
- [Tentar uma formatação completa do dispositivo](#reset).


### phone-intro

> É importante considerar como o seu dispositivo pode estar comprometido.
>
> - Há quanto tempo você começou a suspeitar que seu dispositivo estava agindo de forma suspeita?
> - Você se lembra de clicar em algum link de fontes desconhecidas?
> - Você recebeu mensagens de pessoas que não reconhece?
> - Você instalou algum software não assinado?
> - O aparelho esteve fora de sua posse?

Reflita sobre estas questãoes e tente identificar as circunstâncias, se conseguir chegar a alguma, que levaram seu dispositivo a ser comprometido.

 - [Tenho um dispositivo Android](#android-intro)
 - [Tenho um dispositivo iOS](#ios-intro)


### android-intro

> Primeiro, verifique se tem algum aplicativo desconhecido instalado no seu dispositivo Android.
>
> Você pode encontrar uma lista na seção "Apps" entrando no ícone de configurações. Identifique todos os aplicativo que não tenham vindo instalados de fábrica com o seu dispositivo e que não se lembre de ter instalado.
>
> Se você suspeitar de qualquer um dos apps na lista, abra um navegador de internet e busque recursos para saber se existem denúncias que identifiquem aquele app como malicioso.

Você encontrou algum aplicativo suspeito?

 - [Não encontrei](#android-unsafe-settings)
 - [Sim, identifiquei um app possivelmente malicioso](#android-badend)


### android-unsafe-settings

> O Android permite que usuários concedam a seus dispositivos acesso a camadas mais baixas do sistema. Isto pode ser útil para desenvolvedores de aplicativos, mas também pode expor o sistema a ataques adicionais. Você deve revisar as configurações de segurança para ter certeza que estão marcadas com as opções mais seguras. Fabricantes em sua maioria irão vender os dispositivos com padrões de segurança baixos. Estas configurações devem ser revisadas mesmo se você não tiver feito nada no seu aparelho ainda.
>
> #### Apps de fontes desconhecidas
>
> O Android normalmente bloqueia a instalação de aplicativos que não foram colocados na loja da Google (Play Store), que possui processos de revisão e identifição de aplicativos maliciosos para verificar os desenvolvedores. Invasores geralmente tentam burlar estas verificações tentando compartilhar apps com usuários diretamente através de links ou arquivos vindos de fontes não confiáveis. É importante confirmar que seu dispositivo não permita instalação de apps por este método.
>
> Vá à seção "Segurança" nas configuração de seu Android e confirme que a opção "Instalar apps de fontes desconhecidas" está desabilitada.
>
> #### Modo desenvolvedor e acesso ao ADB (Android Debug Bridge)
>
> No Android, desenvolvedores são permitidos rodar comandos diretamente na camada do sistema quando estão no "modo desenvolvedor". Quando ele está habilitado, pode expor os aparelhos a ataques físicos caso alguns cuidados não sejam tomados. Alguém com acesso físico ao dispositivo pode usar o modo desenvolvedor para baixar cópias de dados sensíveis do dispositivo com alguma facilidade, ou mesmo instalar apps maliciosos.
>
> Se você ver um modo desenvolvedor aparecendo nas suas configurações, deve se assegurar de que o acesso ao ADB (aparece assim mesmo) está desabilitado.
>
> #### Google Play Protect
>
> O serviço de proteção do Google Play está disponível em todos os aparelhos Android recentes. Ele vai fazer verificações regularmente em todos os aplicativos e tem a capacidade de remover automaticamente qualquer aplicativo malicioso conhecido do seu dispositivo. Habilitar o Play Protect não envia informações sobre o seu dispositivo automaticante para o Google (como os aplicativos que estão instalados).
>
> A proteção pode ser habilitada nas opções de segurança do seu aparelho. Mais informações estão disponíveis no site [Play Protect](https://www.android.com/intl/pt-BR_br/play-protect/).

Você identificou alguma configuração insegura no seu dispositivo?

- [Não identifiquei](#android-bootloader)
- [Sim, identifiquei possíveis inseguranças](#android-badend)


### android-bootloader

> O gerenciador de inicialização do Android (também conhecido como bootloader), é um componente chave do sistema que executa assim que você liga o aparelho. O bootloader permite que o sistema operacional inicie e use o hardware. Um bootloader comprometido dá a um invasor acesso completo ao hardware do dispositivo. A maioria dos fabricantes vende seus dispositivos com um bootloader bloqueado contra alterações. Uma forma comum de identifcar se a assinatura da fabricante foi modificada é reiniciar o aparelho e observar o logotipo do boot. Se aparecer um triângulo com uma exclamação, o bootloader original foi alterado. Seu dispositivo também pode ter sido comprometido se estiver sendo mostrada uma tela de alerta de desbloqueio e você deve desconfiar caso não tenha feito o desbloqueio para instalar uma versão alternativa do Android, como por exemplo o CyanogenMod (atual LineageOS). Faça uma redefinição de fábrica (reset) de seu aparelho se estiver mostrando um desbloqueio inesperado do bootloader.

O seu aparelho está usando o bootloader original, ou ele está comprometido?

- [O bootloader do meu aparelho está comprometido](#android-badend)
- [O meu aparelho está usando o bootloader original](#android-goodend)


### android-goodend

> Seu aparelho não parece estar comprometido.

Você ainda tem receio de que seu dispositivo esteja comprometido?

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, consegui resolver meus problemas](#resolved_end)


### android-badend

> Seu dispositivo pode estar comprometido. Uma [redefinição de fábrica](#reset) possivelmente irá remover qualquer ameaça presente no seu aparelho. No entanto, nem sempre é a melhor solução. Além disso, você pode querer investigar mais a fundo para identificar o nível de exposição e a natureza exata do ataque que você sofreu.
>
> Se quiser, uma ferramenta de auto-diagnóstico chamada [Emergency VPN (em inglês)](https://www.civilsphereproject.org/emergency-vpn) pode ser usada para auxiliar na resposta, ou você pode buscar assistência através de uma organização parceira.

Você gostaria de buscar assistência posterior?

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, eu tenho uma rede de suporte locak que pode me auxiliar](#resolved_end)


### ios-intro

> Olhe as configurações do iOS para ver se tem alguma coisa estranha.
>
> No app de Configurações, verifique se seu dispositivo está conectado com o seu ID Apple. O primeiro item do menu no lado da sua mão esquerda deverá ser o seu nome ou o nome que você utiliza com a sua conta Apple. Clique nele e verifique se está mostrando o seu endereço de e-mail correto. No final da tela, na parte de baixo, você verá uma lista com nomes e modelos de todos os aparelhos conectados com o seu ID Apple.

 - [Todas as informações estão corretas e eu ainda estou no controle do meu ID Apple](#ios-goodend)
 - [O meu nome ou outros detalhes estão incorretos, ou estou vendo dispositivos na lista que não são meus](#ios-badend)


### ios-goodend

> Seu aparelho não parece estar comprometido.

Você ainda tem receio de que seu dispositivo esteja comprometido?

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, consegui resolver meus problemas](#resolved_end)


### ios-badend

> Seu dispositivo pode estar comprometido. Uma [redefinição de fábrica](#reset) possivelmente irá remover qualquer ameaça presente no seu aparelho. No entanto, nem sempre é a melhor solução. Além disso, você pode querer investigar mais a fundo para identificar o nível de exposição e a natureza exata do ataque que você sofreu.
>
> Se quiser, uma ferramenta de auto-diagnóstico chamada [Emergency VPN (em inglês)](https://www.civilsphereproject.org/emergency-vpn) pode ser usada para auxiliar na resposta, ou você pode buscar assistência através de uma organização parceira.

Você gostaria de buscar assistência posterior?

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, eu tenho uma rede de suporte local que pode me auxiliar](#resolved_end)


### computer-intro

> **Nota: caso você esteja sob ataque de ransomware, siga diretamente para o site [No More Ransom!](https://www.nomoreransom.org/pt/index.html).**
>
> Este diagnóstico irá ajudar a investigar atividade suspeita em seu computador. Se você estiver auxiliando uma pessoa remotamente pode tentar seguir os passos descritos nos links abaixos sobre como usar uma ferramenta de acesso remoto como o TeamViewer, ou pode pesquisar uma infraestrutura forense remota como o Google Rapid Response (GRR)](https://github.com/google/grr). Leve em consideração que as condições de conectividade de rede de ambas as pessoas determinam a possibilidade deste tipo de procedimento.

Por favor, selecione seu sistema operacional:

 - [Estou usando um computador Windows](#windows-intro)
 - [Estou usando um computador Mac](#mac-intro)


### windows-intro

> Você pode seguir este guia inicial (em inglês) para investigar atividades suspeitas em dispositivos Windows:
>
> - [Como fazer investigação em sistemas Windows ativos](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Estas instruções ajudaram a identificar alguma atividade maliciosa?

 - [Sim, acredito que meu computador esteja infectado](#device-infected)
 - [Não identifiquei atividade maliciosa em meu computador](#device-clean)


### mac-intro

> Para identificar uma potencial infecção em um computador Mac, os passos são os seguintes:
>
> 1. Verifique programas suspeitos iniciando automaticamente
> 2. Verifique processos suspeitos
> 3. Verifique extensões de kernel suspeitas
>
> O site [Objective-See (em inglês)](https://objective-see.com) oferece diversas ferramentas livres que facilitam este processo:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) pode ser usado para identificar todos os programas registrados para iniciar automaticamente.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) pode ser usado para verificar processos em execução e identificar aqueles que parecem se comportar de maneira suspeita (por exemplo os que não possuem assinatura de desenvolvedor, ou são marcados suspeitos no site VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) pode ser usado para identificar qualquer extensão de kernel (que atuam basicamente como os drivers do sistema no Windows ou módulos no Linux e Android) que possam ser suspeitas no funcionamento do Mac.
>
> Em caso de nenhuma delas não revelar nada imediatamente suspeito e você desejar fazer uma análise posterior mais elaborada, você pode usar a ferramenta [Snoopdigg](https://github.com/botherder/snoopdigg). Snoopdigg é um utilitário que simplfica a coleta de informação do sistema e tira uma imagem completa da memória do sistema.
>
> Uma ferramenta adiciona que pode ser útil para coletar detalhes mais imersivos (mas que requer familiaridade com terminal de linha de comando) é a  [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/), da empresa de cibersegurança estadunidense CrowdStrike.

Estas instruções ajudaram a identificar alguma atividade maliciosa?

- [Sim, acredito que meu computador esteja infectado](#device-infected)
- [Não identifiquei atividade maliciosa em meu computador](#device-clean)

### device-infected

Ih, azedou! Pra se livrar da infecção você precisará:

- [Buscar ajuda adicional](#malware_end)
- [Seguir com a formatação do aparelho](#reset).

### reset

> Você pode optar por resetar seu dispositivo como uma medida cautelar extraordinária. Os guias abaixo irão trazer instruções apropriadas para seu tipo de dispositivo:
>
> - [Android](https://tecnoblog.net/215758/formatar-resetar-android/)
> - [iOS](https://support.apple.com/pt-br/HT201252)
> - [Windows](https://support.microsoft.com/pt-br/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/pt-br/HT201314)

Sente que irá precisar de apoio adicional?

- [Sim](#malware_end)
- [Não](#resolved_end)


### malware_end

Se você precisa de informação adicional ao lidar com um dispositivo infectado, pode contatar uma das organizações listadas abaixo.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Algumas dicas para se prevenir contra os esforços de invasores em comprometer seus dados e dispositivos:

-  Verifique com atenção dobrada a legitimidade de qualquer e-mail que receber, de arquivos que tiverem sido baixados ou links que exijam seu login e informações de contas
- Leia mais sobre como proteger seus dispositivos de infecções de malware nos guias listados em recursos

#### Resources

- [Security in a Box - Avoiding Malware and Phishing Attacks"](https://securityinabox.org/pt/guide/malware/#avoiding-malware-and-phishing-attacks)
- [Security in a Box - Protect your device from malware and phishing attacks](https://securityinabox.org/pt/guide/malware/)
-  [Autodefesa contra Vigilância: Como evitar ataques de Pesca (Phishing)](https://ssd.eff.org/pt-br/module/como-evitar-ataques-de-pesca-phishing)
+- [Avast: Guia Essencial sobre Phishing](https://www.avast.com/pt-br/c-phishing)
- [Security without borders: Guide to Phishing](https://guides.securitywithoutborders.org/guide-to-phishing/)
