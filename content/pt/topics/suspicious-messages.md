---
layout: page
title: "Recebi uma mensagem suspeita"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: pt
summary: "Recebi uma mensagem, link, ou email suspeito, o que devo fazer a respeito?"
date: 2023-04
permalink: /pt/topics/suspicious-messages
parent: /pt/
---

# Recebi uma mensagem suspeita

Você pode receber mensagens _suspeitas_ na sua caixa de email, contas de mídia social, ou aplicativos de mensagem. A forma mais comum de email suspeito é a tentativa de golpe digital, ou phishing. Emails de phishing tentam te fazer entregar informações pessoais, financeiras ou de acesso, tentando se passar por um endereço legítimo para induzir uma visita a um site, ou ligação a uma central de atendimento que, na verdade, são falsos. Estas mensagens também podem conter imagens ou arquivos infectados com programas maliciosos que serão instalados ao abrí-los.

Se você não tem certeza sobre a autenticidade de uma mensagem que recebeu, ou sobre o que fazer a respeito, você pode usar o seguinte questionário como um guia para diagnosticar a situação ou compartilhar a mensagem com organizações de confiança que poderão prover uma análise mais detalhada da sua mensagem.

Tenha em mente que receber uma mensagem suspeita não significa necessariamente que sua conta foi comprometida. Se você acredita que seu email ou mensagem é suspeita, não abra. Não responda o email, não clique em nenhum link, e nem baixe quaisquer arquivos anexados.

## Workflow

### intro

Você fez alguma destas coisas ao receber a mensagem ou link?

- [Eu cliquei no link](#link-clicked)
- [Eu coloquei minha senha](#account-security_end)
- [Eu baixei um arquivo](#device-security_end)
- [Eu respondi o email](#reply-personal-info)
- [Eu ainda não fiz nada](#do-you-know-sender)

### do-you-know-sender

Você reconhece a pessoa ou organização que enviou a mensagem? Tenha em vista que a mensagem pode ter sido [fraudada](https://en.wikipedia.org/wiki/Email_spoofing) para parecer alguém que você confia.

 - [É uma pessoa ou organização que eu confio](#known-sender)
 - [É um provedor de serviço (serviço de email, hospedagem, mídia social, banco etc)](#service-provider)
 - [A mensagem foi enviada por uma pessoa ou organização desconhecida](#share)

### known-sender

> É possível usar outros canais de comunicação para falar com a pessoa que pode ter enviado a mensagem? Por exemplo, se a mensagem tiver vindo por e-mail, é possível verificar a legitimidade desta pessoa usando o Signal, WhatsApp ou telefone? Assegure-se de utilizar um método de contato conhecido. Não dá para confiar que o número de telefone da mensagem suspeita seja o número real de quem aparece como remetente.

Você confirmou a veracidade do remetente?

 - [Sim](#resolved_end)
 - [Não](#share)

### service-provider

> Neste cenário, um provedor de serviço é uma empresa que provê algum tipo de serviço que você utilize ou possua inscrição. Esta lista pode conter seus serviços de email (Google, Yahoo, Microsoft, Protonmail etc), suas plataformas de mídia social (Facebook, Twitter, Instagram) ou plataformas online que tenham informações financeiras suas (Paypal, Amazon, Netflix, bancos etc).
>
> Existe alguma forma de verificar se a mensagem que recebeu é autêntica? Muitos destes provedores de serviço também fornecem cópias de notificações ou outros documentos na página de informações da sua conta. Por exemplo, se a mensagem for do Facebook, você deve ter uma cópia na sua [lista de emails de notificação](https://www.facebook.com/settings?tab=security&section=recent_emails), ou no caso do seu banco, você pode ligar para a central de atendimento.

Escolha uma das opções abaixo:

 - [Eu consegui verificar que a mensagem era legítima](#resolved_end)
 - [Eu não consegui verificar a mensagem](#share)
 - [Eu não tem cadastro neste serviço/Não tinha motivo para receber uma mensagem deste serviço](#share)

### link-clicked

> Algumas mensagens suspeitas podem te conduzir a páginas de login falsas que poderão roubar suas credenciais, ou mesmo páginas que podem solicitar informações pessoais e financeiras. Em outros casos, o link também poderá pedir para baixar arquivos que, ao serem abertos, instalam software malicioso em seu computador. Este link também pode levar a páginas especialmente preparadas para tentar infectar seu dispositivo com um software malicioso ou espião.

Poderia nos dizer o que aconteceu após clicar no link?

 - [Ele me pediu para entrar com usuário e senha](#account-security_end)
 - [Ele baixou um arquivo](#device-security_end)
 -  [Não aconteceu nada muito visual, mas ainda estou em dúvida](#clicked_but_nothing_happened)


### clicked-but-nothing-happened

> O fato de ter clicado em um link suspeito e não ter notado comportamentos estranhos não significa que não houve nenhuma ação maliciosa nos bastidores. Há alguns cenários que você deve considerar. O menos preocupante seria que a mensagem fosse apenas um spam utilizado para fins publicitários. Neste caso, alguns pop-ups de anúncio surgirão. Em outros casos, estes anúncios também poderão ser maliciosos.

- [Alguns anúncios surgiram na tela. Não tenho certeza se são maliciosos ou não](#suspicious_device_end)

> No pior cenário, ao clicar no link uma ferramenta de exploit aproveitou a brecha para executar um comando malicioso em seu sistema. Um navegador desatualizado poderia permitir que isto acontecesse, uma vez que exploits são ferramentas que se aproveitam de vulnerabilidades conhecidas. Uma situação em que tal cenário aconteça usando um navegador atualizado seria rara, e significaria que a vulnerabilidade explorada é desconhecida. Em ambos os casos, o seu dispositivo pode começar a se comportar de modo suspeito.

- [Sim, o meu navegador não está atualizado, ou meu dispositivo passou a se comportar de maneira estranha após clicar no link](../../../device-acting-suspiciously)

> Em outros cenários, ao visitar o link você pode ter sido vítima de um [ataque cross-site script (ou XSS)](https://pt.wikipedia.org/wiki/Cross-site_scripting). O resultado deste ataque pode ser o roubo do cookie utilizado para te autenticar no site visitado, de modo que o atacante poderá conectar-se utilizando o seu nome de usuárie. Dependendo da segurança do site, o atacante pode ou não conseguir mudar a sua senha. Isto se torna mais sério se o site vulnerável ao XSS é um site administrado por você, já que neste caso, o atacante será capaz de se autenticar como admin do site. Para identificar um ataque XSS, verifique se o link que você clicou contém uma [sentença de script](https://owasp.org/www-community/attacks/xss/). Eventualmente, ela pode estar codificada em hexadecimal ou Unicode.

- [O link possui um código de script, ou possui codificação parcial](#cross_site_script)
- [Não consegui identificar nenhum script](#suspicious_device_end)


### cross-site-script

Você possui uma conta no site visitado?

- [Sim](#account_security_end)
- [Não](#cross_site_script_end)

O site visitado é um site que você administra?

- [Sim](#cross_site_script_admin_compromised)
- [Não](#cross_site_script_end)


### cross-site-script-admin-compromised

> Neste caso, o atacante pode ter um cookie válido que permite o acesso à conta administradora do seu site. A primeira coisa a fazer é conectar na sua interface de administração e derrubar todas as sessões ativas, ou simplesmente trocar sua senha. Além disso, você pode verificar se o atacante transferiu algum artefato (página, script, arquivo) para o seu site, e se algum conteúdo malicioso foi postado, cuidando de removê-los se for o caso.

As organizações a seguir podem ajudar a investigar e responder a este incidente:

:[](organisations?services=forensic)


### cross-site-script_end

> Estas medidas devem ser suficientes. No entanto, em casos específicos, ataques XSS podem usar o seu navegador para lançar outros ataques.

- [Eu gostaria de avaliar se meu dispositivo foi infectado](../../../device-acting-suspiciously)
- [Acredito que está tudo bem comigo](#final_tips)

### reply-personal-info

> Dependendo do tipo de informação compartilhada, você precisará tomar ações imediatas.

Que tipo de informação você compartilhou?

- [Eu compartilhei informações da minha conta](#account-security_end)
- [Eu compartilhei informações públicas](#share)
- [Não tenho certeza se compartilhei algo sensível e preciso de ajuda](#help_end)


### share

> Compartilhar a mensagem suspeita poderá ajudar sua comunidade e colegas que possam ser afetados por ela. Você também pode contatar pessoas que você confia para perguntá-las se a mensagem em questão é perigosa. Considere compartilhar a mensagem com organizações que possam analisá-la.
>
> Para compartilhar sua mensagem suspeita, certifique-se de incluir o conteúdo desta mensagem junto com informações sobre quem enviou. Se a mensagem veio por e-mail, certifique-se de incluir o cabeçalho da mensagem junto com o conteúdo, seguindo [este guia](https://king.host/wiki/artigo/cabecalho-header/).

Sente que irá precisar de apoio adicional?

- [Sim, preciso de mais apoio](#help_end)
- [Não, resolvi meu problema](#resolved_end)


### device-security_end

> Caso algum arquivo tenha sido baixado, a segurança de seu dispositivo pode estar em risco!

Por gentileza, contate uma das organizações abaixo que poderá auxiliar você nesta situação. Além disso, [compartilhe sua mensagem suspeita](#share).

:[](organisations?services=device_security)


### account-security_end

> Caso tenha inserido suas credenciais, ou tenha sido vítima de um ataque cross-site script, suas contas podem estar em risco!
>
> Se você acredita que suas contas foram comprometidas, recomendamos que siga também o fluxo ["Não consigo acessar minhas contas"](../../../account-access-issues).
>
> Sugerimos que informe também às suas comunidades sobre esta tentativa de phishing, e compartilhe a mensagem suspeita com organizações que possam analisá-la.

Você gostaria de compartilhar informações sobre a mensagem que recebeu, ou precisa de assistência adicional?

- [Gostaria de compartilhar a mensagem suspeita](#share)
- [Preciso de ajuda para deixar minhas contas mais seguras](#account_end)
- [Preciso de ajuda para analisar a mensagem](#analysis_end)


### suspicious-device_end

> Se você clicou em um link e não tem certeza sobre o que aconteceu, seu dispositivo pode ter sido infectado sem o seu conhecimento. Se você deseja explorar esta possibilidade, ou sente que seu dispositivo pode estar infectado, siga o fluxo ["Meu dispositivo está com um comportamento estranho"](../../../device-acting-suspiciously).

### help_end

> Busque ajuda de colegas e pessoas nas suas redes e organizações para entender os riscos da exposição das informações compartilhadas. Além disso, é possível que estas outras pessoas também tenham recebido solicitações parecidas da mesma fonte ou de outras fontes suspeitas.

Por gentileza, contate uma das organizações abaixo que poderá auxiliar você nesta situação. Além disso, [compartilhe sua mensagem suspeita](#share).

:[](organisations?services=24_7_digital_support)


### account_end

> Se você está tendo suas contas comprometidas e precisa de ajuda para mantê-las seguras, por gentileza contate as organizações a seguir que poderão auxiliar com esta situação.

:[](organisations?services=account)


### analysis_end

As organizações a seguir poderão receber suas mensagens suspeitas e investigá-las mais a fundo:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).


### final_tips

Primeira regra: nunca fornecer nenhuma informação pessoal por email. Instituições, bancos e serviços jamais pedirão estas informações via email.

Nem sempre é fácil dizer quando um email ou site é legítimo, mas existem algumas dicas que podem te ajudar a avaliar o email recebido.

* Senso de urgência: emails suspeitos geralmente alertam uma mudança repentina para uma conta e pedem que você aja imediatamente para verificá-la.
* Você verá no corpo do email pedidos para "confirmar" ou "atualizar sua conta" ou algo como "a falha na atualização resultará na suspensão de conta". É geralmente seguro assumir que nenhuma organização credível à qual você forneceu suas informações irá precisar que você os envie novamente, por isso não caia nesta armadilha.
* Cuidado com as mensagens não solicitadas, anexos, links e páginas de login.
* Cuidado com erros de ortografia e gramática.
* Clique para ver o endereço completo do remetente, não apenas o nome mostrado.
* Tenha cuidado com links encurtados - eles podem esconder um link malicioso atrás dele.
* Quando você passa o mouse por cima de um link, o endereço URL real para o qual você está sendo direcionado é mostrado em um popup ou embaixo de sua janela de navegador.
* Cabeçalhos de e-mail, incluindo o remetente, podem ser forjados cuidadosamente para parecerem e-mails legítimos. Ao examinar os valores SPF e DKIM de um cabeçalho, você poderá dizer, respectivamente, se um endereço IP está (ou não) autorizado a enviar e-mails em nome do domínio da remetente, e se os cabeçalhos do conteúdo foram modificados durante o trânsito da mensagem. Em e-mails legítimos, os valores [SPF](https://escoladaprogramacao.com.br/a-seguranca-dos-e-mails-aprimorada-com-dkim-e-spf/) e [DKIM](https://escoladaprogramacao.com.br/a-seguranca-dos-e-mails-aprimorada-com-dkim-e-spf/) deverão sempre ser "PASS". Do contrário, o e-mail não deve ser considerado confiável. As razões para isso são que o e-mail pode ter sido alterado, ou em casos raros, o servidor de e-mail não está configurado corretamente.
* [Assinaturas digitais](https://www.gnupg.org/gph/en/manual/x135.html) podem nos dizer se um e-mail foi enviado por uma pessoa legítima, e se foi ou não modificado no caminho. Se o e-mail estiver assinado, observe se a assinatura é verificada ou não. Para isso, você precisará do OpenPGP, e também da chave pública associada com o ID na assinatura da mensagem. A maioria dos clientes de e-mail atuais que suportam assinaturas digitais deverão verificá-la automaticamente e informar visualmente se a assinatura é ou não verificada.
* Uma conta comprometida poderá enviar uma mensagem ou e-mail malicioso que conseguiria passar por tais verificações como um e-mail legítimo. No entanto, o conteúdo da mensagem seria incomum para a pessoa que o enviou. Se o conteúdo da mensagem de e-mail parecer estranho, é sempre uma boa ideia verificar com esta pessoa através de outros canais de comunicação antes de tomar qualquer ação a respeito.
* É sempre uma boa prática ler e escrever seus e-mail em texto puro (plain text). E-mails formatados em HTML podem ser processados de forma a ocultar código malicioso ou links suspeitos. Você poderá encontrar instruções sobre como desabilitar a HTML em diferentes clientes de e-mail [neste artigo](https://www.maketecheasier.com/read-email-in-plain-text/ )
* Use sempre a versão mais atualizada de seu sistema operacional no celular ou computador (verifique as versões atuais para [Android](https://pt.wikipedia.org/wiki/Hist%C3%B3rico_de_vers%C3%B5es_do_Android), [iOS](https://pt.wikipedia.org/wiki/IOS), [Mac](https://en.wikipedia.org/wiki/MacOS_version_history) e [Windows](https://pt.wikipedia.org/wiki/Microsoft_Windows)).
* Atualize seu sistema operacional sempre que possível, e todos os programas/apps instalados, especialmente aqueles que recebem informações frequentemente (navegadores, apps de chat e mensagem, clientes de e-mail etc) Remova todos os apps e programas que você não precisa.
* Use um navegador confiável (como o Mozilla Firefox). Incremente a segurança do seu navegador revisando sempre as extensões/add-ons instaladas nele. Mantenha apenas aquelas que você confia (por exemplo: HTTPS Everywhere, Privacy Badger, uBlock Origin, Facebook Container, Cookie AutoDelete, NoScript).
* Faça backups regulares das suas informações.
* Proteja suas contas com senhas fortes, confirmação em duas etapas, e configurações seguras.


#### Resources

Seguem alguns recursos para identificar mensagens suspeitas e escapar do phishing.

* [Avast: Guia Essencial sobre Phishing](https://www.avast.com/pt-br/c-phishing)
* [Auto-defesa contra Vigilância: Como evitar ataques de Pesca (Phishing)](https://ssd.eff.org/pt-br/module/como-evitar-ataques-de-pesca-phishing)
* [Citizen Lab: Communities at risk - Targeted Digital Threats Against Civil Society](https://targetedthreats.net)
​​​​​​​
