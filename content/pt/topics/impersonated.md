---
layout: page
title: "Outra pessoa está se passando por mim online"
author: Flo Pagano, Alexandra Hache
language: pt
summary: "Uma pessoa está tendo sua identidade roubada através de uma conta de mídia social, endereço de email, chave PGP, site ou blog falso ou app"
date: 2023-04
permalink: /pt/topics/impersonated
parent: /pt/
---

# Outra pessoa está se passando por mim online

Uma ameaça enfrentada por muites ativistas, defensores de direitos humanos, ONGs e profissionais de mídia e conteúdo independente, é ter a identidade roubada por adversários que criam perfis falsos, sites ou e-mails em seus nomes. Na maioria dos casos, o objetivo disso é criar campanhas de difamação, promover notícias falsas, facilitar práticas de engenharia social ou roubo de identidade, ou mesmo somente gerar ruído, desconfiança e violar dados que impactam na reputação de indivíduos e coletivos que estão sendo personificades. Em outras situações, estes adversários podem roubar a identidade digital de alguém por motivações financeiras, como inventar campanhas falsas de financiamento coletivo, roubar credenciais de serviços de pagamento e cartões, se passar pela pessoa para receber dinheiro de contatos dela etc.

Este é um problema que pode causar diversos impactos emocionais e ter diferentes causas e graus de consequências dependendo de como afeta suas comunicações e em quais meios estão roubando suas identidades.

É importante saber que existem muitas maneiras de se passar por alguém (perfis falsos nas redes sociais, sites clonados, e-mails falsificados, publicação não-consensual de imagens pessoais e vídeos). As estratégias podem variar desde o envio de ordens de remoção do site, disputa de propriedade do domínio, a reivindicação de direitos autorais do site ou das informações postadas, ou ameaças às suas redes e contatos pessoais através de mensagens públicas ou confidenciais. Diagnosticar o problema e encontrar possíveis soluções para personificação pode ser complicado. Em certos casos será quase impossível convencer uma pequena empresa de hospedagem a derrubar um site, e mover uma ação legal pode se tornar necessário. É uma boa prática configurar alertas e monitorar a internet para descobrir se você ou sua organização estão sendo personificados.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose potential ways of impersonating and potential mitigation strategies to remove accounts, websites and emails impersonating you or your organization.

If you are being impersonated, follow this questionnaire to identify the nature of your problem and find possible solutions.

Esta seção do Kit de Primeiros Socorros Digitais irá guiar você através de alguns passos básicos para diagnosticar potenciais formas de personificação e potenciais estratégias de mitigação para remover contas, sites e emails que se passam por você ou sua organização.

Se alguém está tomando sua identidade online, siga o questionário para identificar a natureza do problema e tentar encontrar possíveis soluções.



## Workflow

### urgent-question

Você teme por sua integridade física e bem estar?

 - [Sim](#physical-sec_end)
 - [Não](#diagnostic-start1)

### diagnostic-start1

A personificação está te afetando como indivíduo (alguém está usando seu nome legal, social ou notório) ou como coletivo/organização?

- [Como indivíduo](#individual)
- [Como organização](#organization)

### individual

Se estão afetando você como indivíduo, considere alertar seus contatos. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

- Uma vez que você informe seus contatos que estão se pasando por você, siga para o [próximo passo](#diagnostic-start2).

### organization

> Se estão afetando você como grupo, é importante fazer uma anúncio público. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

- Uma vez que tenha informado sua comunidade que estão se pasando por você, siga para o [próximo passo](#diagnostic-start2).

### diagnostic-start2

De que forma estão se passando por você?

 - [Um site fake está usando meu nome ou o da minha organização/coletivo](#fake-website)
 - [Através de uma conta de rede social](#social-network)
 - [Compartilhando vídeos e imagens minhas sem consentimento](#other-website)
 - [Através do meu email ou endereço similar](#spoofed-email1)
 - [Através de uma chave PGP conectada ao meu email](#PGP)
 - [Através de um app falso que imita meu app](#app1)

### social-network

Em qual plataforma de mídia social estão se passando por você?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)

### facebook

> Siga as instruções em ["Como faço para denunciar uma conta ou Página do Facebook que está fingindo ser eu ou outra pessoa?"](https://www.facebook.com/help/174210519303259) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### twitter

> Siga as instruções em ["Denunciar uma conta por falsa identidade"](https://help.twitter.com/forms/impersonation) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### google

> Siga as instruções em [Denunciar falsificação de identidade](https://support.google.com/plus/troubleshooter/1715140) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### instagram

> Siga as instruções em ["Contas falsas"](https://help.instagram.com/446663175382270) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### fake-website

> Verifique se o site já é reconhecido como malicioso buscando a URL nos serviços a seguir:
>    
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
>

O domínio é reconhecido como malicioso?

 - [Sim](#malicious-website)
 - [Não](#non-malicious-website)

### malicious-website

> Denuncie ao Google Safe Browsing o link da página maliciosa preenchendo o formulário ["Report malicious software" (em inglês)](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Note que pode levar algum tempo para garantir que sua denúncia teve sucesso. Neste tempo, você pode seguir para os próximos passos enviando uma ordem de remoção para os serviços de hospedagem e domínio, ou se preferir apenas salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento funcionou para você?

- [Sim](#resolved_end)
- [Não](#non-malicious-website)


### non-malicious-website

> Você pode tentar denunciar o site para os serviços de hospedagem e domínio solicitando sua retirada.
>
> Se o site que você precisa denunciar está usando o seu conteúdo, uma coisa que você deverá provar é a propriedade do conteúdo postado. Você pode comprovar istoi apresentando o contrato original com o serviço de registro de domínio ou de hospedagem, mas também prover resultados de arquivo da [Wayback Machine](https://archive.org/web/), buscando pela URL de ambos o site original em sua propriedade e o site falso. Se os sites foram indexados lá em algum momento, você terá um histórico que fará possível comprovar a existência de seu site antes da publicação do site falso.
>
> Para enviar uma solicitação de remoção do conteúdo, você precisará coletar a seguinte informação sobre o site falso:
>
> - Entre no serviço [Network Tools' NSLookup](https://network-tools.com/nslookup/) e insira o link da página falsa no campo "Host" do formulário para encontrar os endereços IP ligados a ela.
> - Anote os endereços IP.
> - Entre no serviço [Domain Tools' Whois Lookup](https://whois.domaintools.com/) e faça uma busca usando o link da página e, a seguir, pelos endereços IP do site falso.
> - Guarde o nome e endereço de email de notificação dos serviços de hospedagem e domínio. Caso esteja incluso nos resultados, guarde o nome do dono do site.
> - Escreva para ambos os serviços de hospedagem e domínio do site falso, solicitando a remoção. Em sua mensagem, inclua as informações coletadas, bem como os motivos pelos quais a existência deste site é abusiva.
> - Você pode usar este [modelo de denúncia de sites clonados fornecido pela Access Now Helpline (em inglês, com possibilidade de tradução)](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) para enviar ao serviço de hospedagem.
> - Você pode usar este [modelo de denúncia de domínios clonados ou forjados, fornecido pela Access Now Helpline (em inglês, com possibilidade de tradução)](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) para enviar ao serviço de registro de domínios.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se os procedimentos funcionaram.

Estes procedimentos funcionaram para você?

- [Sim](#resolved_end)
- [Não](#web-protection_end)


### spoofed-email1

> Autenticar e-mails é uma tarefa tecnicamente complicada por uma questão de engenharia. Por conta desta dificuldade, é fácil abusar dos protocolos existentes e criar endereços forjados e mensagens iscas.

Alguém tomou para si o endereço de email que você usa frequentemente, ou estão usando um similar, por exemplo com o mesmo nome (escrito igual) mas com outro domínio (outro endereço/@)?

- [Tomaram o endereço de email que eu uso frequentemente](#spoofed-email2)
- [Estão usando um endereço similar para se passar por mim](#similar-email)


### spoofed-email2

> A pessoa que está se passando por você pode ter invadido sua conta de email. Para descartar esta possibilidade, troque imediatamente sua senha.

Você conseguiu trocar a sua senha?

- [Sim](#spoofed-email3)
- [Não](#hacked-account)

### hacked-account

Se você não conseguiu trocar a sua senha, sua conta de email provavelmente foi comprometida.

- Você poderá resolver seu problema seguindo o fluxo ["Não consigo acessar minhas contas"](../../../account-access-issues).

### spoofed-email3

> Fraude de emails (spoofing) é uma prática que consiste em forjar um endereço de envio. A intenção é fazer parecer que a mensagem foi enviada por um remetente confiável, disfarçando sua origem falsa.
>
> Esta técnica é comum em campanhas de spam e golpes digitais (phishing), pelo fato de as pessoas estarem mais suscetíveis a abrir um e-mail quando imaginam ter vindo de uma fonte legítima.
>
> Se alguém está fraudando seu email, você deve informar seus contatos para alertá-los sobre o risco de serem atraídos para um phishing (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Se você acredita que a personificação da sua identidade foi focada em usos maliciosos e phishing, você pode querer ler também a seção [Recebi mensagens suspeitas](../../../suspicious_messages).

Os emails pararam após trocar a senha da sua conta?

- [Sim](#compromised-account)
- [Não](#secure-comms_end)


### compromised-account

> Provavelmente sua conta foi invadida por alguém que está enviando emails para se passar por você. Como sua conta foi comprometida, você pode querer ler também a seção [Perdi o acessso às minhas contas](../../../account-access-issues/).

Estas informações ajudaram a resolver o seu problema?

- [Sim](#resolved_end)
- [Não](#account_end)


### similar-email

> Se estão se passando por você através de um endereço de email similar ao seu mas de um domínio diferente ou com outro nome de usuário modificado, é uma boa ideia alertar seus contatos sobre a tentativa de fraude (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Você também pode querer ler a seção [Recebi mensagens suspeitas](../../../suspicious-messages), uma vez que esta personificação pode ser uma tentativa de fisgar contatos para um golpe (phishing).

Estas informações ajudaram a resolver o seu problema?

- [Sim](#resolved_end)
- [Não](#secure-comms_end)


### PGP

Você acredita que sua chave PGP privada possa ter sido comprometida, por exemplo por perda ou acesso indevido ao dispositivo onde estava armazenada?

- [Sim](#PGP-compromised)
- [Não](#PGP-spoofed)

### PGP-compromised

Você ainda possui acesso à sua chave privada?

- [Sim](#access-to-PGP)
- [Não](#lost-PGP)

### access-to-PGP

> - Revogue sua chave.
>     - [Instruções para o Enigmail (em inglês)](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - Crie um novo par de chaves e peça a pessoas que você confia que assinem-a.
> - Utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), informe seus contatos que você revogou a sua chave atual e gerou uma nova.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure-comms_end)
- [Não](#resolved_end)


### lost-PGP

Você tem um certificado de revogação?

- [Sim](#access-to-PGP)
- [Não](#no-revocation-cert)


### no-revocation-cert

> - Crie um novo par de chaves e peça a pessoas que você confia que assinem-a.
> - Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), para que deixem de utilizar sua chave antiga e passem a usar a nova chave.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### PGP-spoofed

Sua chave está assinada por pessoas de sua confiança?

- [Sim](#signed-key)
- [Não](#non-signed-key)

### signed-key

> Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), que alguém está tentando se passar por você. Informe seus contatos que é possível  confirmar a veracidade de sua chave (1) conferindo as assinaturas de chave feitas por pessoas próximas de sua confiança, ou (2) através do [fingerprint](http://g1.globo.com/Noticias/Tecnologia/0,,MUL982184-6174,00-SAIBA+COMO+FUNCIONA+A+CRIPTOGRAFIA+DE+DADOS+NOS+EMAILS.html) da sua chave.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### non-signed-key

> - Tenha sua chave [assinada](https://communitydocs.accessnow.org/243-PGP_keysigning.html#comments) por pessoas que você confia.
> - Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), que alguém está tentando se passar por você. Informe seus contatos que é possível  confirmar a veracidade de sua chave (1) conferindo as assinaturas de chave feitas por pessoas próximas de sua confiança, ou (2) através do [fingerprint](http://g1.globo.com/Noticias/Tecnologia/0,,MUL982184-6174,00-SAIBA+COMO+FUNCIONA+A+CRIPTOGRAFIA+DE+DADOS+NOS+EMAILS.html) da sua chave.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### other-website

> Se alguém está usando sua identidade em um site, a primeira coisa que você precisa entender é onde este site está hospedado, quem está gerenciando, e quem está provendo o domínio. Tal pesquisa se destina a identificar a melhor forma de solicitar a remoção do conteúdo malicioso.
>
> Antes de prosseguir com a investigação, caso possua cidadania da União Européia, você pode requerer ao Google a remoção deste site de buscas relacionadas a seu nome.

Você possui cidadania da União Européia?

- [Sim](#EU-privacy-removal)
- [Não](#doxing-question)


### EU-privacy-removal

> Preencha o formulário do Google [Personal Information Removal Request Form](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=en&rd=1) para remover o site das buscas relacionadas ao seu nome feitas no Google.
>
> Você irá precisar de:
>
> - Uma cópia digital de um documento de identificação (se estiver enviando a solicitação em intermédio de outra pessoa, precisará enviar o documento de identificação desta pessoa)
> - As URLs dos conteúdos que deseja que sejam removidos das buscas
> - Para cada URL fornecida, você deve explicar:
>     1. como a informação pessoal contida se relaciona com a pessoa que está solicitando a remoção
>     2. porque você acredita que tal conteúdo deve ser removido
>
> Note que será necessário autenticar com sua Conta Google, e o envio do formulário será associado a ela.
>
> Após submeter o formulário, você deverá aguardar a resposta do Google para verificar se os resultados foram devidamente removidos.

Você gostaria de submeter também uma solicitação de remoção do site com informações falsas sobre você?

- [Sim](#doxing-question)
- [Não, preciso de outras formas de apoio](#account_end)

### doxing-question

A pessoa que está se passando por você publicou informações pessoais, ou fotos e videos íntimos sobre você?

- [Sim](../../../harassed-online/questions/doxing-web)
- [Não](#fake-website)

### app1

> Se alguém está espalhando uma cópia de aplicativo modificada ou um app malicioso similar ao seu, é uma boa ideia fazer um anúncio público e alertar usuários sobre estas modificações e os meios certificados e seguros de baixar e instalar seu aplicativo oficial.
>
> Também é essencial denunciar os apps maliciosos e solicitar a remoção deles.

Em quais meios as cópias maliciosas do seu app estão sendo distribuídas?

- [no Github](#github)
- [no Gitlab.com](#gitlab)
- [na Google Play Store](#playstore)
- [na Apple App Store](#apple-store)
- [em outro site](#fake-website)

### github

> Se o software malicioso estiver hospedado no Github, leia [Guia de envio do aviso de retirada DMCA](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)

### gitlab

> Se o software malicioso estiver hospedado no Gitlab, leia [Digital Millennium Copyright Act (DMCA) takedown request requirements](https://about.gitlab.com/handbook/dmca/) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)


### playstore

>  Se o software malicioso estiver hospedado na Google Play Store, siga as instruções em ["Remoção de conteúdo do Google"](https://support.google.com/legal/troubleshooter/1114905) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)



### apple-store

> Se o software malicioso estiver hospedado na Apple App Store, siga as instruções em ["Apple App Store Content Dispute" form](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=pt) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)



### physical-sec_end

> Se você está tendo sua integridade e bem estar físicos ameaçados, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

:[](organisations?services=physical_sec)


### account_end

> Se você ainda está tendo suas contas e identidades comprometidas, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

:[](organisations?services=account&services=legal)


### app_end

> Se o app falso não foi removido, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

:[](organisations?services=account&services=legal)

### web-protection_end

> Se as solicitações de remoção não foram bem sucedidas, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

:[](organisations?services=web_protection)

### secure-comms_end

> Se você precisa de apoio ou recomendações adicionais sobre phishing, segurança de email e criptografia, e comunicações seguras como um todo, você pode entrar em contato com uma das organizações a seguir.

:[](organisations?services=secure_comms)


### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Para prevenier futuras tentativas de fraude da sua identidade, leia as dicas abaixo.

### final_tips

- Crie senhas fortes: prefira usar uma frase com palavras aleatórias, que não digam respeito a algo pessoal, usando espaços e  caracteres especiais fáceis de lembrar. Mais dicas nos recursos.
- Considere usar um gerenciador de senhas para criar e guardar suas novas senhas, assim você pode manter senhas diferentes para cada site e serviço que usar sem ter que memorizar tudo ou anotar no papel.
- Configure a autenticação em dois fatores em todas as suas contas. A autenticação em dois fatores pode ser muito efetiva para barrar alguém de acessar suas contas sem a sua permissão. Se você puder escolher, não use a autenticação baseada em SMS, prefira usar opções baseadas em aplicativos ou em uma chave de segurança.
- Faça o processo de verificação de conta em suas plataformas de rede social. Algumas plataformas oferecem a possibilidade de verificar sua identidade e sinalizar sua conta de acordo.
- Mapeie sua presença online. Use a inteligência de código aberto disponível na internet para buscar informações disponíveis sobre você e interprete de forma estratégica para conseguir prevenir atores maliciosos de usarem estas informações para se passar por você.
- Configure alertas do Google. Você pode receber emails quando novos resultados para um tópico aparecerem na busca do Google. Por exemplo, você pode receber informação sobre menções do seu nome ou da sua organização/coletivo.
- Salve e tire prints da condição atual do seu site, para que possam ser utilizados como evidência mais adiante. Se o seu site permite robôs de classificação (crawlers), você pode utilizar a Wayback Machine, oferecida pelo archive.org  Visite a [Internet Archive Wayback Machine](https://archive.org/web/), insira o link da sua página (URL) no campo de pesquisa abaixo do banner "Save page now", e depois disso clique no botão "Save page now".

#### Resources

- [Gerenciador de Senhas: Saiba quais as 5 melhores ferramentas (recomendamos usar modo leitura para melhorar a experiência do site)](https://windowsteam.com.br/gerenciador-de-senhas-saiba-quais-as-5-melhores-ferramentas/)
- [Segurança e a reciclagem de números: por que você nunca deve depender do SMS](https://olhardigital.com.br/2020/11/19/noticias/seguranca-e-a-reciclagem-de-numeros-por-que-voce-nunca-deve-depender-do-sms/)
-  [Autodefesa contra Vigilância: Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)
- [Autodefesa contra Vigilância: Como utilizar o KeePassXC](https://ssd.eff.org/pt-br/module/como-utilizar-o-keepassxc)
- [Access Now: Two-factor authentication (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Access Now Helpline Community Documentation: Choosing a password manager](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Archive.org: Archive your website](https://archive.org/web/)
