---
layout: page
title: PGP
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/pgp.md
parent: /pt/
published: true
---

PGP (ou Pretty Good Privacy) e seu equivalente de código aberto, GPG (Gnu Privacy Guard), permitem que você criptografe o conteúdo dos emails para proteger suas mensagens de serem vistas por provedores de email ou outros atores que possam acessar a mensagem. No entanto, porém, governos e autoridades na sua jurisdição e da pessoa receptora da mensagem estão respaldadas a serem notificadas sobre as suas comunicações. Para prevenir isto, você pode criar uma conta alternativa que não esteja associada com a sua identidade.

Recursos: [Mailvelope: Como usar a criptografia PGP no Gmail e no Outlook](https://itigic.com/pt/mailvelope-pgp-encryption-gmail-outlook/)

[3 serviços de e-mails seguros para substituírem o Gmail](https://olhardigital.com.br/2018/07/03/noticias/servicos-de-e-mails-seguros-para-substituirem-o-gmail/)

[Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)
