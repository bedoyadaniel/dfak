---
layout: page
title: Formulário de Contato
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/contact-form.md
parent: /pt/
published: true
---

Um formulário de contato, possivelmente, preservará sua privacidade em relação à organização de destino, de modo que só vocês possam ter acesso à mensagem. É importante reforçar que isto só será possível se o formulário tiver sido hospedado com as devidas medidas de segurança como o uso de uma [criptografia de chave pública certificada](https://ssd.eff.org/pt-br/glossary/criptografia-de-chave-p%C3%BAblica), entre outras medidas por parte de quem administra. No caso das organizações da CiviCERT, podemos garantir tais medidas.

Apesar disso, tenha em mente que a sua visita aos sites destas organizações possa ser de conhecimento para governos, agências de autoridade ou outras instituições que regulem ou intermediem o acesso local ou global às infraestruturas de vigilância e controle. O fato de ter visitado pode significar, na visão destes reguladores, que você teve contato com tais organizações.

Se você precisa manter sigilo sobre a visita (e possível contato) ao site destas organizações, é indicado acessá-los através do [Tor Browser](https://www.torproject.org/) ou utilizando VPNs e proxies de confiança. Antes disso, leve em consideração as legislações vigentes no seu território, e se você precisa esconder o uso do navegador Tor [usando estas configurações](https://tb-manual.torproject.org/pt-BR/running-tor-browser/) em conjunto com suas ferramentas de [transportes plugáveis](https://tb-manual.torproject.org/pt-BR/circumvention/). Se você estiver considerando o uso de VPN ou proxy, [investigue onde estes serviços estão localizados](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) e também [quão confiável é a empresa que provê tais serviços](https://ssd.eff.org/pt-br/module/escolhendo-vpn-rede-virtual-privada-mais-adequada-para-voc%C3%AA).
