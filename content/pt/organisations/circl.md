---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: ativistas, lgbti, mulheres, jovens, tsd
hours: horário comercial, UTC+2
response_time: 4 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

A CIRCL é a equipe comunitária de resposta à emergência (CERT) para o setor privado, comunas e entidades não-governamentais em Luxemburgo.

A CIRCL oferece um ponto de contato confiável e seguro para usuários, companhias e organizações baseadas em Luxemburgo, para a gestão de ataques e incidentes. Sua equipe de especialistas age como uma brigada anti-incêndio, com habilidades para reagir pronta e eficientemente em suspeita ou detecção de ameaças e incidentes.

O foco da CIRCL é coletar, revisar, notificar e responder a ameaças digitais de forma sistemática e proativa.
