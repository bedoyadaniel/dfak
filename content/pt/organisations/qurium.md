---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: Segunda-Sexta 8h-18h CET
response_time: 4 horas
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation é um provedor de soluções em segurança para mídias independentes, organizações de direitos humanos, jornalistas investigativos e ativistas. A Qurium provê um portfolio de soluções profissionais seguras e personalizadas com suporte especializado para organizações e indivíduos em risco, que incluem:

- Hospedagem segura com mitigação contra DDoS para sites em risco
- Apoio com resposta rápida para organizações e indivíduos sob risco iminente
- Auditorias de segurança para serviços web e aplicativos de celular
- Transposição de bloqueio de sites na internet
- Investigação forense de ataques digitais, aplicativos fraudulentos, malwares direcionados e desinformação
