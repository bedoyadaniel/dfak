---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: jornalistas, ddhs, ativistas, lgbti, mulheres, jovens, tsd
hours: 24/7, global
response_time: 2 horas
contact_methods: web, email, pgp
email: help@accessnow.org
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

A Helpline de Segurança Digital da Access Now trabalha com indivíduos e organizações ao redor do mundo com o objetivo de mantê-las online. Se você se encontra em risco, podemos ajudar com melhorias em suas práticas de segurança digital para que se mantenha distante de ameaças. Se já estiver sob ataque, fornecemos resposta rápida e assistência para incidentes.
