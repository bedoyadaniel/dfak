---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24 ชั่วโมง 7 วันต่อสัปดาห์, โซนเวลา UTC+2
response_time: 4 ชั่วโมง
contact_methods: email, pgp
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost ให้บริการด้านไอทีด้วยแนวทางอย่างมีจริยธรรมและยั่งยืน บริการของพวกเขาประกอบไปด้วย เว็บโฮสติ้ง บริการคลาวด์ และบริการที่ไม่เหมือนใครด้านความปลอดภัยของข้อมูล Greenhost มีบทบาทสำคัญในการพัฒนาโอเพนซอร์ส และมีส่วนร่วมในโครงการจำนวนมากเกี่ยวกับเทคโนโลยี สื่อ วัฒนธรรม การศึกษา ความยั่งยืน และเสรีภาพทางอินเตอร์เน็ต
