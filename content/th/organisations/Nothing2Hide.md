---
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
response_time: 1 วัน
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (เป็นแบบฟอร์มออนไลน์ที่มีความปลอดภัย – อิงข้อมูลจาก Globaleaks)
email: help@tech4press.org
pgp_key: http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) เป็นสมาคมที่มุ่งหมายในการช่วยเหลือนักข่าว ทนายความ นักปกป้องสิทธิมนุษยชน และประชาชน “ทั่วไป” ในการสรรหาเครื่องมือสำหรับปกป้องข้อมูลและการสื่อสารของพวกเขา โดยให้แนวทางการแก้ปัญหาเชิงเทคนิคและจัดอบรมตามแต่ละบริบท วิสัยทัศน์ของเราคือ การใช้เทคโนโลยีเพื่อให้บริการด้านการเผยแพร่และปกป้องข้อมูลเพื่อเสริมสร้างความแข็งแกร่งของระบอบประชาธิปไตยทั่วโลก
