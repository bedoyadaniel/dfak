---
layout: page
title: PGP
author: mfc
language: en
summary: Contact methods
date: 2021-09
permalink: /en/contact-methods/pgp.md
parent: /en/
published: true
---
พีจีพี (PGP) หรือพริตทีกูดไพรเวซี (Pretty Good Privacy) และ จีเอ็นยู ไพรเวซีการ์ด  (Gnu Privacy Guard) ซึ่งเป็นโปรแกรมในลักษณะเดียวกันแบบโอเพนซอร์ส ช่วยให้คุณสามารถเข้ารหัสเนื้อหาในอีเมลเพื่อปกป้องข้อความของคุณมิให้ผู้ให้บริการอีเมลหรือบุคคลอื่นใดที่สามารถเข้าถึงอีเมลเปิดดูได้ อย่างไรก็ดี รัฐบาลหรือหน่วยงานผู้บังคับใช้กฎหมายอาจสามารถเข้าถึงข้อมูลว่าคุณได้ส่งข้อความถึงผู้รับสารได้ ทั้งนี้ คุณสามารถใช้ที่อยู่อีเมลทางเลือกที่ไม่ได้ผูกโยงกับตัวตนของคุณเพื่อป้องกันมิให้สิ่งนี้เกิดขึ้น

แหล่งข้อมูลเพิ่มเติม: 
o	[โครงการบันทึกสายด่วนชุมชนขององค์กร Access Now: อีเมลที่ปลอดภัย](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)
o	[Freedom of the Press Foundation: เข้ารหัสอีเมลผ่านเมลวิโลป (Mailvelope): คู่มือสำหรับมือใหม่](https://freedom.press/training/encrypting-email-mailvelope-guide/)
o	[Privacy Tools: ผู้ให้บริการอีเมลส่วนตัว](https://www.privacytools.io/providers/email/)

