---
layout: page
title: WhatsApp
author: mfc
language: th
summary: วิธีการติดต่อ
date: 2021-09
permalink: /th/contact-methods/whatsapp.md
parent: /th/
published: true
---
การใช้ WhatsApp จะช่วยปกป้องบทสนทนาของคุณกับผู้รับสาร โดยจะมีคุณและผู้รับสารเท่านั้นที่สามารถอ่านข้อความที่สื่อสารกันได้ อย่างไรก็ดี รัฐบาลหรือหน่วยงานผู้บังคับใช้กฎหมายยังอาจสามารถรับรู้ถึงข้อเท็จจริงว่าคุณได้สื่อสารกับผู้รับสารได้อยู่ 

แหล่งข้อมูลเพิ่มเติม: [วิธีการใช้ WhatsApp ในระบบ Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [วิธีการใช้ WhatsApp ในระบบ ios](https://ssd.eff.org/en/module/how-use-whatsapp-ios)

