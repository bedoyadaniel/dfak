---
layout: page.pug
title: "เกี่ยวกับคู่มือ"
language: th
summary: "เกี่ยวกับคู่มือการปฐมพยาบาลทางดิจิทัล (Digital First Aid Kit)"
date: 2021-09
permalink: /th/about/
parent: Home
---
คู่มือการปฐมพยาบาลทางดิจิทัล (Digital First Aid Kit) เป็นผลงานร่วมกันระหว่าง [RaReNet (Rapid Response Network)](https://www.rarenet.org/) กับ [CiviCERT](https://www.civicert.org/).
<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Rapid Response Network เป็นเครือข่ายระดับนานาชาติสำหรับผู้ทำงานด้านการตอบสนองเร่งด่วนและผู้สนับสนุนความปลอดภัยทางดิจิทัล ซึ่งประกอบไปด้วย Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad และผู้เชี่ยวชาญด้านความปลอดภัยหลายคนที่ทำงานเกี่ยวกับความปลอดภัยทางดิจิทัลและการตอบสนองเร่งด่วน 

องค์กรและบุคคลเหล่านี้บางส่วนเป็นสมาชิกของ CiviCERT อันเป็นเครือข่ายระดับนานาชาติของหน่วยงานให้ความช่วยเหลือด้านความปลอดภัยทางดิจิทัลและผู้ให้บริการสาธารณูปโภคที่มุ่งเน้นช่วยเหลือกลุ่มและองค์กรด้านความยุติธรรมในสังคมและการปกป้องสิทธิมนุษยชนและสิทธิทางดิจิทัลเป็นหลัก CiviCERT เป็นการรวมกลุ่มอาชีพเพื่อกระจายการทำงานของคณะทำงานด้านการตอบสนองเหตุฉุกเฉินทางคอมพิวเตอร์ (Computer Emergency Response Team หรือ CERT) สำหรับชุมชนผู้ทำงานด้านการตอบสนองเร่งด่วน CiviCERT ได้รับการรับรองจาก Trusted Introducer ซึ่งเป็นเครือข่ายของคณะทำงานด้านการตอบสนองเหตุฉุกเฉินทางคอมพิวเตอร์ที่น่าเชื่อถือในภูมิภาคยุโรป

นอกจากนี้ คู่มือการปฐมพยาบาลทางดิจิทัลเป็นโครงการโอเพนซอร์สที่เปิดรับการสนับสนุนจากภายนอก [โครงการโอเพนซอร์สที่เปิดรับการสนับสนุนจากภายนอก](https://gitlab.com/rarenet/dfak)

หากคุณต้องการใช้คู่มือการปฐมพยาบาลทางดิจิทัลในบริบทที่การเชื่อมต่อสัญญาณถูกจำกัด หรือการหาสัญญาณมีความยากลำบาก คุณสามารถดาวน์โหลดคู่มือใน [รูปแบบออฟไลน์](https://digitalfirstaid.org/dfak-offline.zip) ได้
หากมีข้อติชม คำแนะนำ หรือคำถามเกี่ยวกับคู่มือการปฐมพยาบาลทางดิจิทัล คุณสามารถส่งอีเมลไปที่  dfak@digitaldefenders.org
GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B

