---
layout: sidebar.pug
title: "รู้สึกอึดอัดและท่วมท้นไปด้วยความรู้สึกอยู่หรือเปล่า?"
author: FP
language: th
summary: "การคุกคาม คำข่มขู่ หรือการจู่โจมรูปแบบอื่นๆทางออนไลน์สามารถก่อให้เกิดความรู้สึกอึดอัดและท่วมท้นไปด้วยความรู้สึกและนำไปสู่สภาวะทางอารมณ์ที่อ่อนไหวอย่างมากได้ คุณอาจรู้สึกผิด อับอาย กังวล โกรธ สับสน หมดหนทาง หรือแม้กระทั่งเกรงกลัวเกี่ยวกับสวัสดิภาพทางกายและใจของคุณ"
date: 2021-09
permalink: /th/self-care/
parent: Home
sidebar: >
  <h3>อ่านเพิ่มเติมเกี่ยวกับวิธีการปกป้องตัวเองจากความรู้สึกอึดอัดอย่างท่วมท้น:</h3>
  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://www.fightcyberstalking.org/emotional-support">Cyberstalking Victims Emotional Support</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Cyberwomen self-care training module</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Wellness and Community</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Twenty ways to help someone who's being bullied online</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
  </ul>
---

# รู้สึกอึดอัดและท่วมท้นไปด้วยความรู้สึกอยู่หรือเปล่า?"

การคุกคาม คำข่มขู่ หรือการจู่โจมรูปแบบอื่นๆทางออนไลน์สามารถก่อ ให้เกิดความรู้สึกอึดอัดและท่วมท้นไปด้วยความรู้สึกขึ้นและนำไปสู่สภาวะทางอารมณ์ที่อ่อนไหวอย่างมากได้ คุณอาจรู้สึกผิด อับอาย กังวล โกรธ สับสน หมดหนทาง หรือแม้กระทั่งเกรงกลัวเกี่ยวกับสวัสดิภาพทางกายและใจของคุณ

มันไม่มีความรู้สึกที่ “ถูกต้อง” เพราะสภาวะความเปราะบางและความสำคัญของข้อมูลส่วนบุคคลของแต่ละคนก็ล้วนแตกต่างกันออกไป ความรู้สึกใดๆก็ตามล้วนเป็นที่เข้าใจได้ และคุณไม่ควรกังวลว่าคุณกำลังมีความรู้สึกต่อสิ่งที่เกิดขึ้นอย่างถูกต้องหรือไม่

สิ่งที่แรกที่คุณควรจดจำคือว่า สิ่งที่เกิดขึ้นกับคุณไม่ใช่ความผิดของคุณและคุณไม่ควรโทษตัวเอง แต่ให้คุณอาจลองติดต่อคนที่คุณเชื่อใจและสามารถให้การสนับสนุนคุณในการแก้ปัญหาฉุกเฉินเร่งด่วนนี้ได้

สำหรับการบรรเทาเหตุการณ์จู่โจมทางออนไลน์ คุณจำเป็นต้องรวบรวมข้อมูลว่าเกิดอะไรขึ้น แต่คุณไม่ต้องดำเนินการดังกล่าวเอง หากคุณมีบุคคลที่เชื่อใจ คุณสามารถขอให้บุคคลนั้นช่วยเหลือได้ โดยให้ปฏิบัติตามวิธีการในเว็บไซต์นี้ หรือให้เขาเข้าถึงอุปกรณ์หรือบัญชีของคุณเพื่อเข้าไปรวบรวมข้อมูลให้คุณ
