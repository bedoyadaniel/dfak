---
layout: page
title: Llamada telefónica
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/phone-number.md
parent: /es/
published: true
---

Las comunicaciones de teléfonos fijos y móviles no están cifradas para sus destinatarios, por lo que los gobiernos, las organismos de seguridad u otros con el equipo técnico necesario podrían acceder al contenido de tu conversación y saber a quién llama.
