---
layout: page
title: "Recibí un mensaje sospechoso"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: es
summary: "Recibí un mensaje sospechoso, un enlace o correo electrónico, ¿qué debo hacer al respecto?"
date: 2019-08
permalink: /es/topics/suspicious-messages
parent: /es/
---

# Recibí un mensaje sospechoso

Puedes recibir mensajes _sospechosos_ en tu bandeja de entrada de correo electrónico, cuentas de redes sociales y/o aplicaciones de mensajería. La forma más común de correos electrónicos sospechosos se conocen como phishing. Este tipo de correos suplantan la identidad de otros y pretenden engañarte para que des tu información personal, financiera o de cuentas. Pueden pedirte que visites un sitio web o llames a un número de servicio al cliente falso. Los correos electrónicos de phishing también pueden contener archivos adjuntos que instalan software malicioso en tu computadora cuando se abren.

Si no estás seguro de la autenticidad del mensaje que recibes, o qué hacer al respecto, puedes usar el siguiente cuestionario como herramienta para diagnosticar la situación o compartir el mensaje con organizaciones externas de confianza que te proporcionarán un análisis más detallado del mensaje.

Ten en cuenta que recibir un correo electrónico sospechoso no significa necesariamente que tu cuenta haya sido comprometida. Si crees que un correo electrónico o un mensaje es sospechoso, no lo abras. No respondas al correo electrónico, no hagas clic en ningún enlace y no descargues ningún archivo adjunto.

## Workflow

### intro

¿Has realizado alguna acción en el mensaje o enlace?

- [Hice clic en un enlace](#link-clicked)
- [Ingresé mis credenciales](#account-security_end)
- [Descargué un archivo](#device-security_end)
- [Respondí con información](#reply-personal-info)
- [No he tomado ninguna acción todavía](#do-you-know-sender)

### do-you-know-sender

¿Reconoces el remitente del mensaje? Ten en cuenta que el remitente del mensaje pudiera ser [falsificado](https://es.wikipedia.org/wiki/Email_spoofing) para parecer alguién en quien confías.

 - [Es una persona u organización que conozco.](#known-sender)
 - [Es un proveedor de servicios (como un proveedor de correo electrónico, hosting, redes sociales o bancos).](#service-provider)
 - [El mensaje fue enviado por una persona u organización desconocida](#share)

### known-sender

> ¿Puedes comunicarte con el remitente utilizando otro canal de comunicación? Por ejemplo, si recibiste un correo electrónico, ¿puedes verificarlo directamente con el remitente por Signal, WhatsApp o por teléfono? Asegúrate de utilizar un método de contacto existente. No necesariamente puedes confiar en un número de teléfono que aparece en el mensaje sospechoso.

¿Confirmaste que el remitente es quien te envió este mensaje?

 - [Sí](#resolved_end)
 - [No](#share)

### service-provider

> En este escenario, un proveedor de servicios es cualquier compañía o marca que brinde servicios que usas o estás suscrito o suscrita. Esta lista puede incluir a tu proveedor de correo electrónico (Google, Yahoo, Microsoft, Prontonmail...), tu proveedor de redes sociales (Facebook, Twitter, Instagram...), o plataformas en línea que tienen tu información financiera (Paypal, Amazon, bancos, Netflix...).
>
> ¿Hay alguna forma de verificar si el mensaje es auténtico? Muchos proveedores de servicios también proporcionarán copias de notificaciones u otros documentos en la página de su cuenta. Por ejemplo, si el mensaje es de Facebook, debe incluirse en su [lista de correos electrónicos de notificación](https://www.facebook.com/settings?tab=security&section=recent_emails), o si es de tu banco, Puedes llamar a servicio al cliente.

Elije una de las siguientes opciones:

 - [Pude verificar que es un mensaje legítimo de mi proveedor de servicios](#resolved_end)
 - [No pude verificar el mensaje](#share)
 - [No estoy suscrito a este servicio y/o estoy esperando un mensaje de ellos](#share)

### link-clicked

> En algunos mensajes sospechosos, los enlaces pueden llevarte a páginas de inicio de sesión falsas que robarán tus credenciales, o incluso otro tipo de páginas que podrían robar tu información personal o financiera. Algunas veces, el enlace puede pedirte que descargues archivos adjuntos que instalan software malicioso en tú computadora cuando estos se abren. El enlace también puede llevarte a un sitio web especialmente preparado que puede intentar infectar tú dispositivo con software malicioso o espía.

¿Puedes decir lo que pasó después de hacer clic en el enlace?

 - [Me pidió ingresar credenciales](#account-security_end)
 - [Descargó un archivo](#device-security_end)
 - [No pasó nada, pero no estoy seguro](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

> El hecho de que hayas hecho clic en un enlace sospechoso y no hayas notado ningún comportamiento extraño no significa que no se haya realizado ninguna acción maliciosa en segundo plano. Existen algunos escenarios que deberías evaluar. Lo menos preocupante es que el mensaje que recibiste fue spam utilizado con fines publicitarios. En este caso, solo aparecerán algunos anuncios de este tipo. Solo en algunos casos, estos anuncios podrían también ser maliciosos.

- [Aparecieron algunos anuncios. No estoy seguro de si son maliciosos o no](#suspicious-device_end)

> En el peor de los casos, al hacer clic en el enlace, se pudo haber producido una vulnerabilidad para ejecutar un comando malicioso en el sistema operativo de tú dispositivo. Si esto sucediera, podría deberse a que tú navegador no está actualizado y tiene una vulnerabilidad que permite la ejecución de este exploit. En casos excepcionales en los que tú navegador está actualizado y ocurre esta situación, es posible que no se conozca dicha vulnerabilidad. En ambos casos, tú dispositivo podría comenzar a actuar de manera sospechosa.

- [Sí, mi navegador no está actualizado y/o mi dispositivo comenzó a actuar de manera sospechosa después de hacer clic en el enlace](../../../device-acting-suspiciously)

> En otros escenarios, al visitar este enlace, es posible que hayas sido víctima de un [ataque de código de referencia cruzada (cross-site script attack o XSS)](https://es.wikipedia.org/wiki/Cross-site_scripting). El resultado de este ataque será el robo de alguna cookie usada para autenticarte en el sitio web que visitaste, por lo que el atacante podrá iniciar sesión en el sitio con tu nombre de usuario. Dependiendo de la seguridad del sitio, el atacante podrá o no cambiar la contraseña. Esto se vuelve más serio si el sitio web que es vulnerable a ataques XSS es un sitio que tu administras, porque en tales casos el atacante también podrá autenticarse como administrador de ese sitio web. Para identificar un ataque XSS, verifica si el enlace en el que hiciste clic contiene un [texto codificado](https://owasp.org/www-community/attacks/xss/). Esto también podría estar codificado en hexadecimal o Unicode.

- [Hay un código en el enlace o está parcialmente codificado.](#cross-site-script)
- [Ningún código pudo ser identificado](#suspicious-device_end)

### cross-site-script

¿El sitio que visitaste es un sitio en el que tienes una cuenta?

- [Sí](#account-security_end)
- [No](#cross-site-script_end)

¿El sitio al que llegaste es un sitio web que administras?

- [Sí](#cross-site-script-admin-compromised)
- [No](#cross-site-script_end)

### cross-site-script-admin-compromised

> En este caso, el atacante puede tener una "cookie" válida que le permitirá acceder a tu cuenta de administrador. Lo primero es iniciar sesión en tú interfaz de administración y eliminar cualquier sesión activa o simplemente cambiar la contraseña. También debes verificar si el atacante cargó algún artefacto en tú sitio y/o publicó algún contenido malicioso y, de ser así, elimínalo.

Las siguientes organizaciones te pueden ayudar a investigar y responder a este incidente:

:[](organisations?services=forensic)

### cross-site-script_end

> Con esto deberías estar bien. Sin embargo, en algunos casos raros, un ataque XSS podría emplearse para usar tú navegador para lanzar otros ataques.

- [Quiero evaluar si mi dispositivo se infectó](../../../device-acting-suspiciously)
- [Creo que estoy bien](#final_tips)

### reply-personal-info

> Dependiendo del tipo de información que compartiste, es posible que debas tomar medidas inmediatas.

¿Qué tipo de información compartiste?

- [Compartí información confidencial de una cuenta](#account-security_end)
- [Compartí información pública](#share)
- [No estoy seguro de cuán sensible era la información y necesito ayuda](#help_end)


### share

> Compartir el mensaje sospechoso puede ayudar a proteger a tus colegas y a las comunidades que también podrían verse afectadas. También sería ideal pedir ayuda a alguien de tú confianza para que te confirme si es que el mensaje sospechoso es realmente peligroso. Considera compartir el mensaje con organizaciones que puedan analizarlos.
>
> Para compartir el mensaje sospechoso, asegúrate de incluir tanto el mensaje en sí, como la información del remitente. Si el mensaje era un correo electrónico, asegúrate de incluir el correo electrónico completo, con los encabezados, utilizando la [siguiente guía del centro de respuesta a incidentes informáticos de Luxemburgo (CIRCL)](https://www.circl.lu/pub/tr-07/).

¿Necesitas más ayuda?

- [Sí, necesito mas ayuda](#help_end)
- [No, he resuelto mi problema](#resolved_end)


### device-security_end

> ¡En caso de que algunos archivos se hayan descargado en tu dispositivo, la seguridad de este puede correr peligro!

Por favor, contacta las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

:[](organisations?services=device_security)


### account-security_end

> En caso de que hayas ingresado tus credenciales o hayas sido víctima de un ataque de código de referencia cruzada (XSS o cross-site script attack), ¡tus cuentas pueden estar en riesgo!
>
> Si crees que tú cuenta está comprometida, se recomienda que también sigas el flujo de trabajo del kit de Primeros Auxilios Digital en [cuentas comprometidas](../../../account-access-issues).
>
> Se sugiere que también informes a tú comunidad sobre esta campaña de phishing y compartas el mensaje sospechoso con organizaciones que puedan analizarlos.

¿Deseas compartir información sobre el mensaje que recibiste o necesitas ayuda adicional?

- [Quiero compartir el mensaje sospechoso](#share)
- [Necesito más ayuda para asegurar mi cuenta](#account_end)
- [Necesito mas ayuda para analizar el mensaje](#analysis_end)

### help_end

> Debes buscar ayuda de tus colegas u otras personas para comprender mejor los riesgos de compartir dicha información. Otras personas en tu organización o red también pueden haber recibido mensajes similares.

Por favor, contacta a las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

:[](organisations?services=digital_support)


### account_end

Si tu cuenta ha sido comprometida y necesitas ayuda para protegerla, comunicate con las organizaciones a continuación descritas que pueden ayudarte.

:[](organisations?services=account)


### analysis_end

Las siguientes organizaciones pueden recibir tu mensaje sospechoso e investigar a fondo en tu nombre:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### suspicious-device_end

> Si haces clic en un enlace y no estás seguro de lo que sucedió, es posible que tú dispositivo se haya infectado sin que te hayas dado cuenta. Si deseas explorar esta posibilidad, o tienes la sensación de que tú dispositivo puede estar infectado, intenta ir al flujo de trabajo ["Mi dispositivo está actuando de forma sospechosa"](../../../device-acting-suspiciously).

### resolved_end

Con suerte esta guía de DFAK te ha sido útil. Por favor danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

La primera regla a recordar: nunca divulgues información personal en correos electrónicos. Ninguna institución, banco u otras organizaciones solicitarán esta información por correo electrónico. Puede que no siempre sea fácil saber si un correo electrónico o sitio web es legítimo, pero hay algunos consejos que pueden ayudarte a evaluar lo que recibiste.

* Sentido de urgencia: los correos electrónicos sospechosos generalmente advierten de un cambio repentino en una cuenta y te piden que actúes de inmediato para verificar tu cuenta.
* En el cuerpo de un correo electrónico, puedes ver preguntas que te piden que "verifiques" o "actualices tu cuenta" o que "la falta de actualización de tus registros conlleva la suspensión de la cuenta". Por lo general, es seguro asumir que ninguna organización creíble a la que hayas proporcionado tu información te pedirá que la vuelvas a ingresar, no caigas en la trampa.
* Cuidado con los mensajes no solicitados, archivos adjuntos, enlaces y páginas de inicio de sesión.
* Cuidado con los errores de ortografía y gramática.
* Haga clic para ver la dirección completa del remitente, no solo el nombre que se muestra.
* Ten en cuenta los enlaces acortados, ya que pueden ocultar enlaces maliciosos.
* Cuando pasas el mouse sobre un enlace, la URL real a la que te dirigen se muestra en una ventana emergente o en la parte inferior de la ventana de tu navegador.
* Los encabezados de correo electrónico que incluyan el parámetro "De:" (from:) podrían haber sido elaborados cuidadosamente para que parezcan legítimos. Al examinar los encabezados SPF y DKIM, puedes saber, respectivamente, si una dirección IP puede (o no) enviar correos electrónicos en nombre del dominio del remitente, y si los encabezados o el contenido se han modificado en tránsito. En un correo electrónico legítimo, el [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) y [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) los valores siempre deben ser 'PASS', de lo contrario, no se debe confiar en un correo electrónico. La razón es que el correo electrónico está falsificado o, en raras ocasiones, el servidor de correo no está configurado correctamente.
* [Las firmas digitales](https://www.gnupg.org/gph/en/manual/x135.html) pueden decirnos si un correo electrónico ha sido enviado por el remitente legítimo y si ha sido modificado o no en el camino. Si el correo electrónico está firmado, verifica si la firma está verificada o no. Para verificar una firma, necesitarás OpenPGP, así como la clave pública asociada con el ID en la firma del mensaje. La mayoría de los clientes de correo electrónico modernos que admiten firmas digitales automatizarán la verificación por ti y te dirán a través de tu interfaz de usuario si una firma está verificada o no.
* Una cuenta comprometida podría emitir un correo electrónico o mensaje malicioso con todas las condiciones anteriores y parecer legítimo. Sin embargo, normalmente el contenido del mensaje será inusual. Si el contenido del mensaje de correo electrónico parece extraño, siempre es una buena idea consultar con el remitente legítimo a través de un canal de comunicación diferente antes de realizar cualquier acción.
* Siempre es una buena práctica leer y escribir los correos electrónicos en texto simple. Los correos electrónicos basados en HTML se pueden procesar de forma que oculten códigos maliciosos o direcciones URL. Puedes encontrar instrucciones sobre cómo deshabilitar HTML en diferentes clientes de correo electrónico en [este post](https://www.maketecheasier.com/read-email-in-plain-text/).
* Usa la última versión del sistema operativo en tu teléfono o computadora (ver versión para [Android](https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_Android), [iOS](https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_iOS), [macOS](https://es.wikipedia.org/wiki/Historia_de_macOS) y [Windows](https://es.wikipedia.org/wiki/Microsoft_Windows).
* Actualiza lo antes posible tú sistema operativo y todas las aplicaciones/programas que hayas instalado, especialmente aquellos que reciben información (navegadores, aplicaciones/programas de mensajería y chat, clientes de correo electrónico, etc.). Elimina todas las aplicaciones/programas que no necesites.
* Utiliza un navegador confiable (por ejemplo, Mozilla Firefox). Aumenta la seguridad de tu navegador revisando las extensiones complementos instalados. Deja solo aquellos en los que confías (por ejemplo: HTTPS Everywhere, Privacy Badger, uBlock Origin, Facebook Container, Cookie AutoDelete, NoScript, etc.).
* Realiza copias de seguridad periódicas y seguras de tu información.
* Protege tus cuentas con contraseñas seguras, autenticación en 2 factores y configuraciones seguras.


#### Resources

Aquí van una serie de recursos para detectar mensajes sospechosos y evitar ser engañados.

* [Citizen Lab: Comunidades en riesgo - Amenazas digitales dirigidas contra la sociedad civil - En Inglés)](https://targetedthreats.net) ​​​​​​​
* [Surveillance Self-Defense: Cómo evitar los ataques de phishing](https://ssd.eff.org/es/module/c%C3%B3mo-evitar-los-ataques-de-phishing-o-suplantaci%C3%B3n-de-identidad)
* [Security Security without borders: Guía de Phishing - En Inglés](https://guides.securitywithoutborders.org/guide-to-phishing/)​​​​​​​
