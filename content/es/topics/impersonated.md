---
layout: page
title: "Alguien me está suplantando en línea"
author: Flo Pagano, Alexandra Hache
language: es
summary: "Alguien me está suplantado a través de una cuenta de redes sociales, una dirección de correo electrónico, una llave PGP, un sitio web o una aplicación"
date: 2023-04
permalink: /es/topics/impersonated
parent: /es/
---

# Alguien me está suplantando en línea

Una amenaza a la que se enfrentan muchos activistas, defensores de derechos humanos, ONGs, medios independientes y blogueros es ser suplantados por adversarios que crean perfiles, sitios web o correos electrónicos falsos con sus nombres. Algunas veces, la intención es de lanzar campañas de difamación, información engañosa, ingeniería social o simplemente robar la identidad para generar ruido, problemas de confianza y violaciones de datos que afecten la reputación de las personas y colectivos que se suplantan. En otros casos, un adversario puede hacerse pasar por un tercero con fines financieros como recaudar fondos, robar credenciales de pago, recibir pagos, etc.

Este es un problema frustrante que puede afectar tu capacidad para comunicarte e informar. Puede tener diferentes causas dependiendo de dónde y cómo se esté suplantando tu identidad.

Es importante saber que hay muchas formas de hacerse pasar por otra persona (perfiles falsos en las redes sociales, sitios web clonados, correos electrónicos falsificados, publicaciones no consensuadas de imágenes y videos personales, etc.). Las estrategias pueden variar desde la solicitud de bajada de información, la prueba de propiedad original, el reclamo de derechos de autor del sitio web original o su información, o la advertencia a sus redes y contactos personales a través de comunicaciones públicas o confidenciales. Diagnosticar el problema y encontrar posibles soluciones para la suplantación de identidad puede ser complicado. A veces es casi imposible presionar a una pequeña empresa de *hosting* para que elimine un sitio web, y es posible que se requieran acciones legales. Es una buena práctica configurar alertas y monitorear Internet para averiguar si tu o tu organización están siendo suplantados.

Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunos pasos básicos para diagnosticar posibles formas de suplantación de identidad y estrategias de mitigación para eliminar cuentas, sitios web y correos electrónicos que se hacen pasar por ti o tu organización.

Si te están suplantando, sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.


## Workflow

### urgent-question

¿Temes por tu integridad física o bienestar?

  - [Sí](#physical-sec_end)
  - [No](#diagnostic-start1)

### diagnostic-start1

¿La suplantación de identidad te afecta como persona (alguien está usando tu nombre y apellido legal, o el apodo en el que basa tu reputación) o como organización/colectivo?

- [Como individuo](#individual)
- [Como organización](#organization)

### individual

> Si estás siendo afectado como individuo, es posible que desees alertar a tus contactos. Realiza este paso utilizando una cuenta de correo, un perfil o un sitio web que esté totalmente bajo tu control.

- Una vez que hayas informado a tus contactos que te están suplantando, continúa con el [próximo paso](#diagnostic-start2).

### organization

> Si estás siendo afectado como grupo, es posible que desees hacer un comunicado público. Toma este paso utilizando una cuenta de correo, un perfil o un sitio web que esté totalmente bajo tu control.

- Una vez que hayas informado a tu comunidad que te están suplantando, continúa con el [siguiente paso](#diagnostic-start2).

### diagnostic-start2

¿Cómo te están suplantando?

- [A través de un sitio web falso que se hace pasar por mi persona o grupo](#fake-website)
- [A través de una cuenta de red social](#social-network)
- [A través de la distribución de videos o imágenes sin mi/nuestro consentimiento](#other-website)
- [A través de mi dirección de correo electrónico o una dirección similar](#spoofed-email1)
- [A través de una llave PGP conectada a mi dirección de correo electrónico](#PGP)
- [A través de una aplicación falsa que imita mi aplicación](#app1)

### social-network

¿En qué plataforma de redes sociales te están suplantando?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)

### facebook

> Sigue las instrucciones en ["¿Cómo denuncio una cuenta de Facebook o una página que finge ser yo o alguien más?"](https://www.facebook.com/help/174210519303259) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Funcionó?

- [Sí](#resolved_end)
- [No](#account_end)

### twitter

>  Sigue los pasos en ["Informar una cuenta por suplantación de identidad"](https://help.twitter.com/forms/impersonation) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### google

> Sigue los pasos en [la página "Informar suplantación de identidad"](https://support.google.com/plus/troubleshooter/1715140) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### instagram

>  Sigue las instrucciones en ["Cuentas de suplantación de identidad"](https://help.instagram.com/446663175382270) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### tiktok

>  Completa el formulario para ["denunciar una cuenta suplantada en TikTok"](https://tiktokimpersonationusca.zendesk.com/hc/en-us/requests/new?ticket_form_id=7707864917275) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### twitch

>  Completa el formulario para ["denunciar a un usuario en Twitch"](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=es#BestPractices) para solicitar la eliminación de la cuenta que hace suplantación de identidad.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)


### fake-website

> Comprueba si este sitio web se conoce como malicioso buscando su URL en los siguientes servicios en línea:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/gui/home/url)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

¿Se sabe que el dominio es malicioso?

  - [Sí](#malware-website)
  - [No](#non-malicious-website)

### malware-website

> Informar la URL a Google Safe Browsing rellenando el [formulario para informar sobre software malintencionado (en inglés)](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Ten en cuenta que puede llevar algún tiempo asegurarse de que tu informe sea procesado. Mientras tanto, puedes continuar con el siguiente paso para enviar una solicitud de eliminación al proveedor de *hosting* y al registrador de dominios, o guardar esta página en tus marcadores y regresar en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#non-malicious-website)


### non-malicious-website

> Puedes intentar reportar el sitio web al proveedor de *hosting* o al registrador de dominios, solicitando su eliminación.
>
> Si el sitio web que deseas reportar está utilizando tu contenido, es posible que tengas que probar que eres el propietario legítimo del contenido original. Puedes mostrar esto presentando tu contrato original con el registrador de dominios y/o el proveedor de *hosting*, pero también puede hacer una búsqueda en [Wayback Machine](https://archive.org/web/), buscando la URL de tu sitio web y el sitio web falso. Si los sitios web se han indexado allí, encontrarás un historial que puede permitirte demostrar que tu sitio web existía antes de que se publicara el sitio web falso.
>
> Para enviar una solicitud de eliminación, también deberás recopilar información sobre el sitio web falso:
>
> - Ve al [Servicio NSLookup de Network Tools](https://network-tools.com/nslookup/) y averigua la dirección (o direcciones) IP del sitio web falso ingresando su URL en el formulario de búsqueda.
> - Anota la dirección o direcciones IP.
> - Ve al [Servicio de búsqueda de Whois de Domain Tools](https://whois.domaintools.com) y busca tanto el dominio como la dirección IP del sitio web falso..
> - Toma nota del nombre y la dirección de correo electrónico de abuso del proveedor de alojamiento y del servicio de dominio. Si se incluye en los resultados de su búsqueda, también toma nota del nombre del propietario del sitio web.
> - Escribe al proveedor de *hosting* y al registrador de dominios del sitio web falso para solicitar su eliminación. En el mensaje, incluye información sobre las direcciones IP, la URL y el propietario del sitio web que realiza suplantación de identidad, así como las razones por las que es abusivo.
> -  Puedes utilizar la [plantilla de la línea de ayuda de Access Now para informar de sitios web clonados a un proveedor de alojamiento web (en inglés)](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) para escribirle al proveedor de alojamiento web.
> - Puedes usar la [plantilla de la línea de ayuda de Access Now para informar sobre suplantaciones o clonación a un proveedor de dominio (en inglés)](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) para escribir al registrador del dominio.
>
> Ten en cuenta que puede tardar un tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#web-protection_end)


### spoofed-email1

> Por razones técnicas propias de esta tecnología, es bastante difícil autenticar correos electrónicos. Esta es la razón por la cual es muy fácil crear direcciones de remitentes falsificadas y correos electrónicos falsos.

¿Te están suplantando a través de tu dirección de correo electrónico o similar, por ejemplo, con el mismo nombre de usuario, pero con un dominio diferente?

- [Estoy siendo suplantado a través de mi dirección de correo electrónico](#spoofed-email2)
- [Estoy siendo suplantado a través de una dirección de correo electrónico similar](#similar-email)


### spoofed-email2

> La persona que se está haciendo pasar por ti podría haber pirateado tu cuenta de correo electrónico. Para descartar esta posibilidad, intenta cambiar tu contraseña.

¿Pudiste cambiar tu contraseña?

- [Sí](#spoofed-email3)
- [No](#hacked-account)

### hacked-account

Si no puedes cambiar tu contraseña, tu cuenta de correo electrónico probablemente esté comprometida.

- Puedes seguir el [flujo de trabajo "No puedo acceder a mi cuenta"](../../../account-access-issues) para resolver este problema.

### spoofed-email3

> La suplantación de correo electrónico consiste en enviar mensajes con una dirección de remitente falsificada. El mensaje parece haberse originado de alguien o en otro lugar que no es la fuente real.
>
> La suplantación de correos electrónicos es común en las campañas de *phishing* y correo basura (*spam*), ya que las personas tienen más probabilidades de abrir un correo electrónico cuando creen que proviene de una fuente legítima.
>
> Si alguien está falsificando tu correo electrónico, debes informar a tus contactos para advertirles sobre el peligro de este ataque de *phishing* (hazlo desde una cuenta de correo, perfil o sitio web que esté totalmente bajo tu control).
>
> Si crees que esta suplantación de identidad esta relacionada a campañas de *phishing* u otros intentos maliciosos, también puedes leer la sección [Recibí mensajes sospechosos](../../../suspicious_messages).

¿Se detuvieron los correos electrónicos después de cambiar la contraseña de tu cuenta de correo electrónico?

- [Sí](#compromised-account)
- [No](#secure-comms_end)


### compromised-account

> Probablemente tu cuenta haya sido accedida por alguien que la usó para enviar correos electrónicos haciéndose pasar por ti. Como tu cuenta se vio comprometida, es posible que también desees leer la sección [Perdí acceso a mis cuentas](../../../account-access-issues).

¿Esto ha ayudado a resolver tu problema?

- [Sí](#resolved_end)
- [No](#account_end)


### similar-email

> Si el imitador está utilizando una dirección de correo electrónico que es similar a la tuya pero con un nombre de dominio o usuario diferente, es recomendable advertir a tus contactos sobre este intento (hazlo desde una cuenta de correo, perfil o sitio web totalmente bajo tu control).
>
> También es posible que desees leer la sección [Recibí mensajes sospechosos](../../../suspicious-messages), ya que esta suplantación podría estar relacionada a ataques de *phishing*.

¿Esto ha ayudado a resolver tu problema?

- [Sí](#resolved_end)
- [No](#secure-comms_end)


### PGP

¿Crees que tu llave PGP privada podría haber sido comprometida, por ejemplo, porque perdiste el control del dispositivo donde estaba almacenada?

- [Sí](#PGP-compromised)
- [No](#PGP-spoofed)

### PGP-compromised

¿Todavía tienes acceso a tu llave privada?

- [Sí](#access-to-PGP)
- [No](#lost-PGP)

### access-to-PGP

> - Revoca tu llave.
> - [Instrucciones para Enigmail (En Inglés)](https://www.enigmail.net/documentation/Key_Management#Revoking_your_key_pair)
> - Crea un nuevo par de llaves y haz que lo firmen personas de confianza.
> - Comunícate a través de un canal confiable que controles, como Signal u otra [herramienta cifrada de extremo a extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-chat-%20and-conferencing-tools), informando a tus contactos que eliminaste tu llave y generaste una nueva.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure-comms_end)
- [No](#resolved_end)


### lost-PGP

¿Tienes un certificado de revocación?

- [Sí](#access-to-PGP)
- [No](#no-revocation-cert)


### no-revocation-cert

> - Crea un nuevo par de llaves y haz que lo firmen personas de confianza.
> - Informa a tus contactos a través de un canal de confianza que controles, como Signal u otra [herramienta cifrada de extremo a extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-%20chat-and-conferencing-tools), que deben usar tu nueva llave y dejar de usar la anterior.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure-comms_end)
- [No](#resolved_end)

### PGP-spoofed

¿Está tu llave firmada por personas de confianza?

- [Sí](#signed-key)
- [No](#non-signed-key)

### signed-key

> Informa a tus contactos a través de un canal de confianza que controles, como Signal u otra [herramienta cifrada de extremo a extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-%20chat-and-conferencing-tools), que alguien está tratando de hacerse pasar por ti. Diles que pueden reconocer tu llave real basándose en (1) las firmas de contactos confiables y/o (2) la [huella digital](https://help.gnome.org/users/seahorse/stable/misc-key-fingerprint.html.es) de tu llave real.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure-comms_end)
- [No](#resolved_end)

### non-signed-key

> - Obtén tu llave [firmada](https://communitydocs.accessnow.org/243-PGP\_keysigning.html\#comments) para personas de tu confianza.
> - Informa a tus contactos a través de un canal de confianza que controles, como Signal u otra [herramienta cifrada de extremo a extremo](https://www.frontlinedefenders.org/es/resource-publication/guide-secure-group-%20chat-and-conferencing-tools), que alguien está tratando de hacerse pasar por ti. Diles que pueden reconocer tu llave real basándose en (1) las firmas de contactos confiables y/o (2) la [huella digital](https://help.gnome.org/users/seahorse/stable/misc-key-fingerprint.html.es) de tu llave real.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure-comms_end)
- [No](#resolved_end)

### other-website

> Si estás siendo suplantado en un sitio web, lo primero que debes hacer es comprender en dónde está alojado ese sitio, quién lo administra y quién le ha proporcionado el nombre de dominio. Esta investigación tiene como objetivo identificar la mejor manera de solicitar la eliminación del contenido malicioso.
>
> Antes de continuar con tu investigación, si eres ciudadano de la UE, puedes solicitar a Google que elimine este sitio web de sus resultados de búsqueda en tu nombre.

¿Eres ciudadano de la Unión Europea?

- [Sí](#EU-privacy-removal)
- [No](#doxing-question)


### EU-privacy-removal

> Completa el [Formulario de solicitud para la eliminación de información personal de
Google](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=es) para eliminar un sitio web de los resultados de búsqueda de Google en tu nombre.
>
> Lo que necesitarás:
>
> - Una copia digital de un documento de identificación (si estás enviando esta solicitud en nombre de otra persona, deberás proporcionar la documentación de identificación de esta).
> - La(s) URL(s) del contenido que contiene la información personal que se desea eliminar.
> - Para cada URL que proporciones, deberás explicar:
> 1. Cómo la información personal identificada anteriormente se relaciona con la persona en cuyo nombre se realiza la solicitud.
> 2. Por qué crees que la información personal debe ser eliminada.
>
> Ten en cuenta que si has iniciado sesión en tu cuenta de Google, Google puede asociar el envío del formulario con esta cuenta.
>
> Después de enviar el formulario, tendrás que esperar una respuesta de Google para verificar que se hayan eliminado los resultados.

¿Deseas presentar una solicitud de eliminación para remover el contenido suplantado del sitio web?

- [Sí](#doxing-question)
- [No, me gustaría recibir asistencia](#account_end)

### doxing-question

¿El suplantador ha publicado información personal, videos íntimos o imágenes tuyas?

- [Sí](../../../harassed-online/questions/doxing-web)
- [No](#fake-website)

### app1

> Si alguien está difundiendo una copia maliciosa de tu aplicación u otro software, una buena idea es hacer un comunicado público para advertir a los usuarios que solo descarguen la versión legítima.
>
> También debes informar sobre la aplicación maliciosa y solicitar su eliminación.

¿En dónde se distribuye la copia maliciosa de su aplicación?

- [En Github](#github)
- [En Gitlab.com](#gitlab)
- [En Google Play Store](#playstore)
- [En Apple App Store](#apple-store)
- [En otro sitio web](#fake-website)

### github

> Si el software malicioso está alojado en Github, lee la [Guía para enviar un aviso de eliminación de contenidos a través de la Ley de derechos de autor (DMCA)](https://docs.github.com/es/site-policy/content-removal-policies/guide-to-submitting-a-dmca-takedown-notice).
>
> Puede tomar tiempo recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)

### gitlab

> Si el software malicioso está alojado en Gitlab.com, lee los [Requisitos para la solicitud de eliminación de contenidos a través de la Ley de derechos de autor (DMCA)(en inglés)](https://about.gitlab.com/handbook/dmca/).
>
> Puede tomar tiempo recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### playstore

> Si la aplicación maliciosa está alojada en Google Play Store, sigue los pasos en ["Eliminación de contenido de Google"](https://support.google.com/legal/troubleshooter/1114905?hl=es) para eliminar contenido que viola los derechos de autor.
>
> Puede tomar tiempo recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### apple-store

> Si la aplicación maliciosa está alojada en la App Store, completa el [formulario "Disputa de contenido de la App Store de
Apple"](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts%20?%20lang=es) para eliminar contenido que viola los derechos de autor.
>
> Puede tomar tiempo recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### physical-sec_end

> Si estás temiendo por tu bienestar físico, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=physical_sec)


### account_end

> Si aún experimentas suplantación de identidad o tu cuenta aún está comprometida, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=account&services=legal)


### app_end

> Si la aplicación falsa no se ha eliminado, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=account&services=legal)

### web-protection_end

> Si tus solicitudes de eliminación no han tenido éxito, puedes intentar comunicarte con las organizaciones a continuación para obtener más ayuda.

:[](organisations?services=web_protection)

### secure-comms_end

> Si necesitas ayuda o recomendaciones sobre *phishing*, seguridad y cifrado de correo electrónico y comunicaciones seguras en general, puedes comunicarte con estas organizaciones:

:[](organisations?services=secure_comms)


### resolved_end

Esperamos que esta guía de DFAK te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Para evitar nuevos intentos de hacerse pasar por ti lee los consejos a continuación.

### final_tips

- Crea contraseñas seguras, complejas y únicas para todas tus cuentas.
- Considera usar un administrador de contraseñas para crear y almacenar contraseñas, de modo que puedas usar muchas contraseñas diferentes en diferentes sitios y servicios sin tener que memorizarlas.
- Activa la autenticación de dos factores (2FA) para tus cuentas más importantes. 2FA ofrece una mayor seguridad de la cuenta al requerir usar más de un método para iniciar sesión en sus cuentas. Esto significa que incluso si alguien obtuviera tu contraseña principal, no podría acceder a tu cuenta a menos que también tuviera tu teléfono móvil u otro medio secundario de autenticación.
- Verifica tus perfiles en plataformas de redes sociales. Algunas plataformas ofrecen una función para verificar tu identidad y vincularla a tu cuenta.
- Mapea tu presencia en línea. El self-doxing consiste en explorar la inteligencia de código abierto sobre uno mismo para evitar que los actores maliciosos encuentren y utilicen esta información para hacerse pasar por ti. En esencia se trata de investigarse uno mismo.
- Configura las alertas de Google. Puedes recibir correos electrónicos cuando aparezcan nuevos resultados para un tema en la Búsqueda de Google. Por ejemplo, puedes obtener información sobre las menciones de tu nombre o el nombre de tu organización / colectivo.
- Captura tu página web tal como aparece ahora para usarla como evidencia en el futuro. Si tu sitio web permite rastreadores, puedes utilizar Wayback Machine, que ofrece archive.org. Visita [Internet Archive Wayback Machine](https://archive.org/web/), ingresa el nombre de tu sitio web en el campo debajo del encabezado "Guardar página ahora" y haz clic en el botón "Guardar página ahora".

#### resources

- [Access Now: Autenticación de dos factores o 2FA](https://www.accessnow.org/entendiendo-la-autenticacion-de-dos-factores/).
- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Elegir un administrador de contraseñas (en inglés)](https://communitydocs.accessnow.org/295-Password_managers.html).
- [Access Now: Autenticación en dos pasos basada en SMS (2FA) (en inglés)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Archive.org: Archivar tu sitio web](https://archive.org/web/).
- [FemBloc: Contraseñas seguras](https://docs.fembloc.cat/plataformas-contrasenas-seguras.html).
- [FemBloc: Suplantación de identidad](https://docs.fembloc.cat/plataformas-suplantacion-identidad.html).
- [Security Self-Defense: Crear contraseñas únicas y fuertes](https://ssd.eff.org/es/module/creando-contraseñas-seguras).
- [Security Self-Defense: Descripción general animada utilizando administradores de contraseñas](https://ssd.eff.org/es/module/visión-animada-uso-de-gestores-de-contraseñas-para-estar-seguro-en-línea).
- [Security Self-Defense: Cómo usar KeePassXC: un administrador de contraseñas de código abierto seguro](https://ssd.eff.org/es/module/cómo-usar-keepassxc).

