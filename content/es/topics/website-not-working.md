---
layout: page
title: "Mi sitio web está caído, ¿qué está pasando?"
author: Rarenet
language: es
summary: "Una amenaza que enfrentan muchas ONG, medios de comunicación independientes y blogueros es que sus voces sean silenciadas porque su sitio web está caído o ha sido borrado."
date: 2023-04
permalink: /es/topics/website-not-working/
parent: /es/
---

# Mi sitio web está caído, ¿qué está pasando?

Una amenaza que enfrentan muchas ONG, medios de comunicación independientes y blogueros es ser silenciados porque su sitio web está caído o ha sido borrado. Este es un problema frustrante y puede tener muchas causas como un mantenimiento deficiente del sitio web, un proveedor de alojamiento no confiable, [script-kiddies](https://es.wikipedia.org/wiki/Script_kiddie), un ataque de "Denegación de Servicio" (DDoS) o una toma de posesión del sitio web. Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunos pasos básicos para diagnosticar problemas potenciales utilizando material de [Mi sitio web está inactivo (en inglés)](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Encontrar el problema y las posibles soluciones a este problema puede ser engorroso. Es una buena práctica **contactar a la persona que administra tu sitio web y a la empresa donde se encuentra alojado** después del diagnóstico. Si ninguna de estas opciones está disponible para ti, [solicita la ayuda de una organización en la que confíes](questions/website-down_end).

## Consider

Para empezar, considera:

- ¿Quién desarrolló tu sitio web? ¿Están disponibles para ayudar?
- ¿Fue construido usando Wordpress u otro gestor de contenidos (CMS) popular?
- ¿Quién es tú proveedor de alojamiento web? Si no lo sabes, puedes utilizar [un servicio en línea WHOIS](https://who.is/) para descubrirlo.

## Workflow

### error-message

¿Estás viendo mensajes de error?

- [Sí, estoy viendo mensajes de error](#error-message-yes)
- [No](#hosting-message)

### hosting-message

¿Estás viendo un mensaje de tu proveedor web?

- [Sí, veo un mensaje de mi proveedor](#hosting-message-yes)
- [No](#site-not-loading)

### site-not-loading

¿Tu sitio no se carga en absoluto?

- [Sí, el sitio web no carga en absoluto](#site-not-loading-yes)
- [No, el sitio web esta cargando](#hosting-working)

### hosting-working

¿El sitio web de tu proveedor funciona, pero tu sitio web no está disponible?

- [Sí, puedo acceder al sitio web de mi proveedor](#hosting-working-yes)
- [No](#similar-content-censored)

### similar-content-censored

¿Puedes visitar otros sitios con contenido como el de tu sitio web?

- [Tampoco puedo visitar otros sitios web con contenido similar](#similar-content-censored-yes)
- [Otros sitios funcionan bien. Simplemente no puedo visitar mi sitio](#loading-intermittently)

### loading-intermittently

¿Se está cargando tu sitio web de forma intermitente o inusualmente lenta?

- [Sí, mi sitio se está cargando intermitentemente o está lento](#loading-intermittently-yes)
- [No, mi sitio web se está cargando, pero podría haber sido hackeado](#website-defaced)

### website-defaced

¿Tu sitio web se carga pero la apariencia y el contenido no son lo que esperas ver?

- [Sí, mi sitio no tiene el contenido/aspecto esperado](#defaced-attack-yes)
- [No](#website-down_end)

### error-message-yes

> Esto podría ser un **_problema de software_**: debes pensar acerca de cualquier cambio reciente que tu o tu equipo hayan realizado y comunicarte con tu administrador web. Envía a tu administrador web una captura de pantalla, el enlace de la página con la que está teniendo problemas y cualquier mensaje de error que veas. Esto le ayudará a descubrir cuál podría ser la causa del problema. También puedes copiar los mensajes de error en un motor de búsqueda para ver si hay soluciones fáciles.

¿Te ha ayudado esto?

- [Sí](#resolved_end)
- [No](#website-down_end)

### hosting-message-yes

> El sitio podría haber sido bloqueado por razones legales, [violaciones a derechos de autor (en inglés)](https://www.eff.org/issues/bloggers/legal/liability/IP), facturación u otras razones. Ponte en contacto con tu proveedor para obtener más detalles sobre por qué suspendieron tu alojamiento web.

¿Te ha ayudado esto?

- [Sí](#resolved_end)
- [No, necesito soporte legal](#legal_end)
- [No, necesito soporte técnico](#website-down_end)

### site-not-loading-yes

> Tu proveedor de alojamiento puede estar teniendo problemas, en cuyo caso puede estar enfrentando un **_problema de hosting_**. ¿Puedes visitar el sitio web de tu empresa de alojamiento? Ten en cuenta que esta **no** es la sección de administración de tu propio sitio, sino el de la empresa u organización con la que trabajas para alojar tu sitio.
>
> Busca un blog de "estado" de tu proveedor (por ejemplo, https://www.dreamhoststatus.com/), y también busca en [twitter.com](https://twitter.com) para ver a otros usuarios que discuten la potencial inactividad de la empresa de alojamiento. Una búsqueda simple como "(nombre de la compañía) down" o "(nombre de la compañía) caido/a" a menudo puedes revelar Si otros tienen el mismo problema.

¿Te ha ayudado esto?

- [Sí](#resolved_end)
- [No, el sitio web de mi proveedor no está caído](#hosting-working-yes)
- [No, necesito soporte técnico](#website-down_end)

### hosting-working-yes

> Comprueba si el sitio web funciona con [Caído para todos o solo para mí (*Down For Everyone Or Just Me*)](https://downforeveryoneorjustme.com/). Es posible que el sitio web esté activo, pero no puedas verlo.
>
> Si tu sitio está activo pero no puedes verlo, es probable que este sea un **_problema de red_**: tu propia conexión a Internet podría tener problemas de acceso a tu sitio.

¿Necesitas más ayuda?

- [No](#resolved_end)
- [Sí, necesito ayuda para restaurar mi conexión de red](#website-down_end)
- [Sí, esto no es un problema de red y mi sitio web está inactivo para todos](#similar-content-censored)

### similar-content-censored-yes

> Intenta visitar sitios web con contenido similar al tuyo. Intenta también usar [Tor](https://gettor.torproject.org/es/) o [Psiphon](https://psiphon.ca/) para acceder a tu sitio.
>
> Si puedes visitar tu sitio a través de Tor o Psiphon, tienes un **_problema de censura_**. Todavía estás en línea para otras partes del mundo, pero estás siendo censurado en su propio país.

¿Te gustaría hacer algo respecto a esta censura?

- [Sí, me gustaría informar esto públicamente y necesito apoyo para mi campaña de defensa](#advocacy_end)
- [Sí, me gustaría encontrar una solución para que mi sitio web sea accesible](#website-down_end)
- [No](#resolved_end)

### loading-intermittently-yes

> Tu sitio puede verse abrumado por la cantidad y la velocidad de las solicitudes de páginas que recibe, esto es un **_problema de rendimiento_**.
>
> Esto podría ser "bueno" en el sentido de que tú sitio se ha vuelto más popular y simplemente necesita algunas mejoras para responder a más lectores - verifica el análisis de tú sitio para ver un patrón de crecimiento a largo plazo. Ponte en contacto con tú webmaster o proveedor de alojamiento para obtener orientación. Muchas plataformas populares de blogs y sistemas de gestión de contenido (CMS) (Joomla, WordPress, Drupal...) tienen complementos para ayudar a almacenar en caché tu sitio web localmente e integrar [Redes de distribución de contenidos (CDNs)](https://es.wikipedia.org/wiki/Red_de_distribución_de_contenidos), que puede mejorar drásticamente el rendimiento y la resistencia del sitio. Muchas de las soluciones siguientes también pueden ayudar con los problemas de rendimiento.
>
> Si experimentas un **problema de rendimiento** grave, tu sitio puede ser víctima de un [** "ataque de denegación de servicio distribuido"**](https://ssd.eff.org/es/glossary/ataque-de-denegaci%C3%B3n-de-servicio-distribuido) (o DDoS). Sigue los pasos a continuación para mitigar un ataque de este tipo:
>
> - Paso 1: ponte en contacto con una persona de confianza que pueda ayudarte con tu sitio web (tu webmaster, las personas que te ayudaron a configurar el sitio, personal interno o tu proveedor de hosting).
>
> - Paso 2: trabaja con la compañía a la que le compraste tu nombre de dominio y cambia el "Time to Live" o TTL a 1 hora (puedes encontrar instrucciones sobre cómo hacerlo en los sitios web de muchos proveedores, como [Network Solutions](http://www.networksolutions.com/support/how-to-manage-advanced-dns-records/) o [GoDaddy](https://cl.godaddy.com/help/administrar-dns-680)). Esto puede ayudarte a redirigir tu sitio mucho más rápido una vez atacado (el valor predeterminado es 72 horas o tres días). Esta configuración se encontrará a menudo en las propiedades "avanzadas" de tu dominio, a veces como parte de los registros de servicio o SRV.
>
> - Paso 3: Mueve el sitio a un servicio de mitigación DDoS. Para comenzar:
>
> - [Deflect.ca](https://deflect.ca/)
> - [Project Shield de Google](https://projectshield.withgoogle.com/landing?hl=es)
> - [Proyecto Galileo de CloudFlare](https://www.cloudflare.com/galileo)
>
> Para obtener una lista completa de organizaciones de confianza que pueden ayudar a mitigar un ataque DDoS, puedes ir [a la sección final de este flujo de trabajo que aborda los ataques DDoS](#ddos_end).
>
> - Paso 4: Tan pronto como hayas recuperado el control, revisa tus opciones y decide entre un proveedor seguro o simplemente continúa con tu servicio de mitigación DDoS.

Para obtener una lista completa de organizaciones de confianza que pueden proporcionar alojamiento seguro, puedes saltar [al paso final de este flujo de trabajo que trata los problemas de alojamiento web](#web-hosting_end).

### defaced-attack-yes

> El *defacement* de un sitio web es una práctica en la que un atacante reemplaza el contenido o la apariencia visual del sitio web con su propio contenido. Estos ataques generalmente se llevan a cabo mediante la explotación de vulnerabilidades en plataformas de CMS sin mantenimiento, sin las últimas actualizaciones de seguridad o mediante el uso de nombres de usuario y contraseñas de cuentas de alojamiento robadas.
>
> - Paso 1: comprueba que se trata de un cambio malicioso de tu sitio web. Una práctica lamentable pero legal es comprar nombres de dominio caducados recientemente para "hacerse cargo" del tráfico que tenían con fines publicitarios. Es muy importante mantener los pagos de tu nombre de dominio al día.
> - Paso 2: si tu sitio web ha sido modificado, primero recupera el control de tu cuenta de inicio de sesión y restablece tu contraseña, consulta la sección de secuestro de cuentas para obtener ayuda.
> - Paso 3: haz una copia de seguridad del sitio que luego se puede usar para investigar el "defacement".
> - Paso 4: desactiva temporalmente tu sitio web: usa una página de destino simple (landing page) o una página de 'parking'.
> - Paso 5: determina cómo fue hackeado tu sitio. Tu proveedor puede ser capaz de ayudar. Los problemas comunes incluyen a las partes más antiguas de tu sitio con scripts/herramientas personalizadas que se ejecutan en ellos, sistemas de administración de contenido obsoletos y programación personalizada con fallas de seguridad.
> - Paso 6: restaura tu sitio original a partir de las copias de seguridad que hayas hecho. Si ni tú ni tu compañía de alojamiento web tienen copias de seguridad, es posible que tengas que reconstruir el sitio web desde cero. También ten en cuenta que si tus únicas copias de seguridad están en posesión tu proveedor, un atacante puede eliminarlas cuando tome el control de tu sitio.

¿Te han ayudado estas recomendaciones?

- [Sí](#resolved_end)
- [No](#website-down_end)

### website-down_end

> Si aún necesitas ayuda después de todas las preguntas que respondiste, puedes ponerte en contacto con una organización de confianza y solicitar asistencia.
>
> Antes de ponerte en contacto, hazte las siguientes preguntas:
>
> - ¿Cómo está estructurada y sostenida la empresa/organización? ¿Qué tipos de investigación o informes se deben hacer, si los hay?
> - Considerar en qué país/países tienen presencia legal y en cuáles deberían cumplir con la ley y otras solicitudes legales.
> - ¿Qué registros se crean y por cuánto tiempo están disponibles?
> - ¿Existen restricciones con respecto al tipo de contenido que alojará el servicio/proxy y qué impacto podrían tener en tu sitio?
> - ¿Existen restricciones en los países donde pueden brindar servicios?
> - ¿Aceptan alguna forma de pago que puedas usar? ¿Se puede costear el servicio?
> - Comunicaciones seguras: debes poder iniciar sesión de forma segura y comunicarte con el proveedor de servicios de forma privada.
> - ¿Existe una opción para la autenticación de dos factores (2FA), para mejorar la seguridad del acceso del administrador? Estas u otras políticas de acceso seguro similares pueden ayudar a reducir la amenaza de otras formas de ataques contra tu sitio web.
> - ¿A qué tipo de soporte continuo tendrás acceso? ¿Existe un costo adicional para el soporte técnico y/o recibirás suficiente ayuda si estás utilizando un nivel "gratuito"?
> - ¿Puedes "probar" tu sitio web antes de migrarlo a través de un sitio de prueba?

Aquí hay una lista de organizaciones que pueden ayudarte con tu problema:

:[](organisations?services=web_protection)

### legal_end

> Si tu sitio web está caído debido a razones legales y necesitas asistencia, comunícate con una organización que pueda ayudarte:

:[](organisations?services=legal)

### advocacy_end

> Si deseas recibir apoyo para lanzar una campaña contra la censura, pónte en contacto con organizaciones que pueden ayudarte con los esfuerzos de incidencia:

:[](organisations?services=advocacy)

### ddos_end

> Si necesitas ayuda para mitigar un ataque DDoS contra tu sitio web, consulta las organizaciones especializadas en la mitigación de este tipo de ataques:

:[](organisations?services=ddos)

### web-hosting_end

> Si estás buscando una organización confiable para alojar tu sitio web en un servidor seguro, consulta la siguiente lista:
>
> Antes de ponerte en contacto con estas organizaciones, piensa en estas preguntas:
>
> - ¿Ofrecen soporte completo para migrar tu sitio a su servicio?
> - ¿Los servicios son iguales o mejores que los de tu proveedor actual, al menos para las herramientas y servicios que usas? Las mejores cosas para comprobar son:
>   - Paneles de administración como cPanel.
>   - Cuentas de correo electrónico (cuántos, cuotas, acceso a través de SMTP, IMAP).
>   - Bases de datos (cuántas, tipos, acceso).
>   - Acceso remoto a través de SFTP/SSH.
>   - Soporte para lenguajes de programación (PHP, Perl, Ruby, cgi-bin access ...) o CMS (Drupal, Joomla, Wordpress ...) que usa el sitio.

Aquí hay una lista de organizaciones que pueden ayudarte con el alojamiento web:

:[](organisations?services=web_hosting)

### resolved_end

Esperamos que esta guía del Kit de Primeros Auxilios Digitales (DFAK) te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto: incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Copias de seguridad**. Además de los servicios y sugerencias a continuación, siempre es buena idea asegurarse de tener copias de seguridad (que se almacenen en un lugar que no sea el mismo en donde se encuentra tu sitio web). Muchos proveedores de alojamiento y plataformas de sitios web tienen esto incluido, pero es mejor tener copias adicionales sin conexión.
- **Manten el software actualizado**. Si estás utilizando un sistema de gestión de contenidos (CMS) como WordPress o Drupal, asegúrate de que los componentes de tu sitio web estén actualizados al software más reciente, especialmente si han habido actualizaciones de seguridad.
- **Monitoreo**. Hay muchos servicios que pueden revisar constantemente tu sitio y te pueden enviar mensajes de correo electrónico o mensajes de texto si se produce una caída. [Este artículo](https://geekflare.com/es/monitor-website-uptime//) enumera los 16 más populares. Ten en cuenta que el correo electrónico o el número de teléfono que utilizas para la supervisión estarán claramente asociados con la administración del sitio web.

#### Resources

- [EFF: Mantener tú sitio web vivo (en inglés)](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: Medidas proactivas y reactivas DDoS (en inglés)](https://www.cert.be/en/paper/ddos-proactive-and-reactive-measures)
- [Sucuri: ¿Qué es un ataque DDoS? (en inglés)](https://sucuri.net/guides/what-is-a-ddos-attack/)
- [LaBekka: Cómo montar una servidora feminista](https://labekka.red/servidoras-feministas/)

