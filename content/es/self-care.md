---
layout: sidebar.pug
title: "¿Te sientes abrumado/a?"
author: FP
language: es
summary: "El acoso en línea, las amenazas y otros tipos de ataques digitales pueden crear sentimientos abrumadores y estados emocionales muy delicados: puedes sentir culpa, vergüenza, ansiedad, enojo, confusión, desamparo o incluso temor por tu bienestar psicológico o físico."
date: 2023-04
permalink: /es/self-care/
parent: Home
sidebar: >
  <h3>Leer más sobre cómo protegerte contra sentimientos abrumadores:</h3>

  <ul>
    <li><a href="https://cyber-women.com/es/autocuidado/">Cyberwomen Contenidos formativos Autocuidado</a></li>
    <li><a href="https://im-defensoras.org/categoria/autocuidado/">IM-Defensoras Contenidos sobre Autocuidado</a></li>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/es/procurar-autocuidado/">
Cómo procurar el autocuidado</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
  </ul>
---

# ¿Te sientes abrumado/a?

El acoso en línea, las amenazas y otros tipos de ataques digitales pueden crear sentimientos abrumadores y estados emocionales muy delicados: puedes sentir culpa, vergüenza, ansiedad, enojo, confusión, desamparo o incluso temor por tu bienestar psicológico o físico.

No existe una forma "correcta" de sentirte, ya que tu estado de vulnerabilidad y lo que la información personal significa para ti es diferente de persona a persona. Cualquier emoción está justificada, y no debes preocuparte si sientes que tu reacción es la correcta o no.

Lo primero que debes recordar es que lo que te está sucediendo no es tu culpa y no debes culparte a ti mismo/a, contacta a alguien de confianza que pueda ayudarte a enfrentar esta emergencia.

Para mitigar un ataque en línea, deberás recopilar información sobre lo que sucedió. No tienes que hacerlo por tu cuenta. Si tienes una persona de confianza, puedes pedirle que te apoye mientras sigue las instrucciones de este sitio web, o darle acceso a tus dispositivos o cuentas para recopilar la información necesaria.
