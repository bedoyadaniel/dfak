---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8AM-18PM Lun-Dom CET
response_time: 4 horas
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation es un proveedor de soluciones de seguridad para medios independientes, organizaciones de derechos humanos, periodistas de investigación y activistas. Qurium proporciona una portafolio de soluciones profesionales, personalizadas y seguras con apoyo personal a organizaciones e individuos en riesgo, que incluye:

- Alojamiento seguro con proteción DDoS de sitios web en riesgo.
- Soporte de respuesta rápida a organizaciones e individuos bajo amenaza inmediata.
- Auditorías de seguridad de servicios web y aplicaciones móviles.
- Evasión de bloqueos a sitios web en Internet.
- Investigaciones forenses de ataques digitales, aplicaciones fraudulentas, malware dirigido y desinformación.
