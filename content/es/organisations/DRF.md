---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 días de la semana, 9 AM-5 PM UTC+5 (PST)
response_time: 56 horas
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) es una organización no gubernamental para la defensa enfocada en la investigación, registrada y con sede en Pakistán. Fundada en 2012, DRF se enfoca en las TICs para apoyar los derechos humanos, la inclusión, los procesos democráticos y la gobernanza digital. DRF trabaja en temas de libertad de expresión en línea, privacidad, protección de datos y violencia en línea contra las mujeres.
