---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 2 horas
contact_methods: web, email, pgp
email: help@accessnow.org
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

La Línea de ayuda de seguridad digital de Access Now trabaja con personas y organizaciones de todo el mundo para mantenerlos seguros en línea. Si estás en riesgo, podemos ayudarte a mejorar tus prácticas de seguridad digital para mantenerte fuera de peligro. Si ya estás bajo ataque, te proporcionamos asistencia de emergencia y de respuesta rápida.
