---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: အရေးပေါ် ၂၄/၇, ကမ္ဘာတစ်ဝှမ်း; အရေးပေါ်မဟုတ်သောကိစ္စများအတွက် တနင်္လာမှသောကြာ ရုံးချိန်အတွင်း, IST (UTC+1), ဝန်ထမ်းများသည် မတူသောဒေသများ ၊ time zone များတွင်ရှိပါသည်
response_time: အရေးပေါ်ဆိုပါက ခေါ်ဆိုသောနေ့အတွင်း (သို့)နောက်နေ့အတွင်း
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders သည် အန္တရာယ်ကျရောက်နေသော လူ့အခွင့်အရေး ကာကွယ်သူများကို ဘက်ပေါင်းစုံ စောင့်ရှောက်မှုပေးသော အိုင်ယာလန်အခြေစိုက် နိုင်ငံတကာ အဖွဲ့အစည်း တစ်ခုဖြစ်ပါသည်။ Front Line Defenders သည် အန္တရာယ်ချက်ချင်းကျရောက်နေသော လူ့အခွင့်အရေး ကာကွယ်သူများအား လုံခြုံရေးထောက်ပံ့ငွေများ၊ ရုပ်ပိုင်းဆိုင်ရာ နှင့် ဒီဂျစ်တယ် လုံခြုံရေးသင်တန်းများ၊ စည်းရုံးထောက်ပံ့မှုများ နှင့် စည်းရုံးလုပ်ဆောင်မှုများ မှတဆင့် လျင်မြန်ပြီး လက်တွေ့ကျသော အထောက်အပံ့များကို ပံ့ပိုးပေးပါသည်။

Front Line Defenders သည် လူ့အခွင့်အရေး ကာကွယ်သူများအတွက် အရေးပေါ်အထောက်အပံ့ပေးနိုင်ရန် အာရဗစ်၊ အင်္ဂလိပ်၊ ပြင်သစ်၊ ရုရှား သို့မဟုတ် စပိန် ဘာသာဖြင့် ၂၄/၇ ခေါ်ဆိုနိုင်သော ဖုန်းနံပါတ် ရှိပါသည်။ ဖုန်းနံပါတ်မှာ +353-121-00489 ဖြစ်ပါသည်။ လူ့အခွင့်အရေး ကာကွယ်သူများသည် ၎င်းတို့၏ ဘဝကို ချက်ချင်းခြိမ်းခြောက်ခံရသောအခါ Front Line Defenders သည် ယာယီအခြေချရန် ပံ့ပိုးကူညီပေးနိုင်ပါသည်။ Front Line Defenders သည် ရုပ်ပိုင်းဆိုင်ရာ နှင့် ဒီဂျစ်တယ် လုံခြုံရေးအတွက် သင်တန်းများ ပေးပါသည်။ Front Line Defenders သည်  အန္တရာယ်ကျရောက်နေသော လူ့အခွင့်အရေး ကာကွယ်သူများအတွက် နိုင်ငံတကာအဆင့်ရှိ အီးယူ (EU)၊ ကုလသမဂ္ဂ (UN)၊ ဒေသတွင်းယန္တရားများ နှင့် အစိုးရများ၏ စောင့်ရှောက်မှုများ ရရန်အတွက် ၎င်းတို့၏ ကိစ္စရပ်များကို ထုတ်ပြန်ကြေငြာပြီး စည်းရုံးထောက်ပံ့မှုများ နှင့် စည်းရုံးလုပ်ဆောင်မှုများ ဆောင်ရွက်ပေးပါသည်။
