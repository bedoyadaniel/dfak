---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: ၂၄/၇ ၊ ကမ္ဘာတစ်ဝှမ်း
response_time: ၁၂ နာရီ
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Internews ၏ အဓိကလုပ်ဆောင်ချက်ကို ဖြည့်စွက်ကာ Internews သည် ဒီဂျစ်တယ်လုံခြုံရေး အသိအမြင်ကို မြင့်လာစေရန်၊  ဆင်ဆာမပါပဲ ပွင့်လင်းသော အင်တာနက်ကို သုံးစွဲနိုင်ခွင့်ကို ကာကွယ်ရန် နှင့် ဒီဂျစ်တယ်လုံခြုံရေးဆိုင်ရာ အလေ့အထများကို တိုးတက်စေရန် အတွက် ကမ္ဘာတဝှမ်းရှိ ပုဂ္ဂိုလ်များ၊ အဖွဲ့အစည်းများ နှင့် လူမှုအသိုင်းအဝိုင်းများ နှင့် ပူးပေါင်း အလုပ်လုပ်ပါသည်။ Internews သည် နိုင်ငံပေါင်း ၈၀ ကျော်မှ စာနယ်ဇင်းသမားများ နှင့် လူ့အခွင့်အရေး ကာကွယ်သူများအား သင်တန်းများ ပေးခဲ့ပါသည်။ Internews မှ တည်ဆောက်ရာတွင် ဦးဆောင် ပေးခဲ့သည့် Security Auditing Framework and Evaluation Template for Advocacy Groups (SAFETAG) မူဘောင် ([https://safetag.org](https://safetag.org)) နှင့် ကျွမ်းကျင်သော လိုကယ် နှင့် ဒေသတွင်းရှိ ဒီဂျစ်တယ်လုံခြုံရေး သင်တန်းပေးသူများ နှင့် စာရင်းစစ်သူများ နှင့်လည်း Internews သည် ချိတ်ဆက်မှုရှိပါသည်။ Internews သည် အရပ်ဘက်လူမှု နှင့် ပုဂ္ဂလိက အဖွဲ့အစည်းများသို့ တိုက်ခိုက်နိုင်သည့် သတင်းအချက်အလက်များကို ထောက်လှမ်းသုံးသပ်သည့် ကုမ္ပဏီများနှင့် ခိုင်မာပြီး အပြန်အလှန်အကျိုးပြုသော မိတ်ဖက်ပြုမှုများ တည်ဆောက်နေပါသည်။ Internews သည် အွန်လိုင်းပေါ်တွင် လုံခြုံပြီး ဆင်ဆာမပါသော ချိတ်ဆက်မှုဖြင့် လုပ်ဖော်ကိုင်ဖက်များကို တိုက်ရိုက် ပံ့ပိုးပေးနိုင်ပါသည်။ Internews သည် SAFETAG မူဘောင်ကိုအသုံးပြုပြီး အခြေခံလုံခြုံရေး အကဲဖြတ်မှုများ၊ အဖွဲ့အစည်းမူဝါဒ အကဲဖြတ်မှုများ၊ နှင့် ခြိမ်းခြောက်မှုသုတေသနအပေါ် မူတည်၍ ပြန်လည်သုံးသပ်ပြီး ထိခိုက်မှုလျော့နည်းစေရန် ဗျူဟာဆွဲခြင်းများ စသဖြင့် နည်းပညာ နှင့် နည်းပညာမဆိုင်သော ကြားဝင်ရောက်စွက်ဖက်သည့် အစီအစဥ်များ ဆောင်ရွက်ပေးပါသည်။ ၎င်းဝန်ဆောင်မှုများသည် ဒီဂျစ်တယ်တိုက်ခိုက်မှုများကို ကြုံတွေ့ခံစားခဲ့ရသော လူ့အခွင့်အရေး နှင့် မီဒီယာ အဖွဲ့များအတွက် ပုဂ္ဂိုလ်အချက်အလက်များ တရားမဝင်စွာ ယူဆောင်ခြင်းများ (phishing) နှင့် သူခိုးဆော့ဝဲများ (malware) ကို သုံးသပ်ရာတွင် ကူညီပံ့ပိုးပါသည်။
