---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: တနင်္လာမှသောကြာ, ၂၄/၅, UTC-4
response_time: ၆ နာရီ
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect သည် အရပ်ဘက်လူ့အဖွဲ့အစည်း နှင့် လူ့အခွင့်အရေးအဖွဲ့အစည်း များကို ဒီဂျစ်တယ်ခြိမ်းခြောက်မှုများမှ ကာကွယ်ပေးသော အခမဲ့ လုံခြုံရေးဝန်ဆောင်မှု ဝဘ်ဆိုဒ်တစ်ခု ဖြစ်ပါသည်။
