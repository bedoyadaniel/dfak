---
layout: page
title: အီးမေးလ်
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/email.md
parent: /my/
published: true
---

အစိုးရများနှင့် ဥပဒေစိုးမိုးရေးအေဂျင်စီများသည် သင့် မက်ဆေ့ချ်အကြောင်းအရာ နှင့် သင်နှင့်ဆက်သွယ်မှုရှိမရှိ ကို ကြည့်ရှုသိရှိနိုင်ပါသည်။
