---
layout: page
title: PGP
author: mfc
language: ru
summary: Способы связи
date: 2018-09
permalink: /ru/contact-methods/pgp.md
parent: /ru/
published: true
---

PGP (Pretty Good Privacy) и аналог с открытым исходным кодом GPG (Gnu Privacy Guard) — программы, позволяющие шифровать содержание электронных писем. Ни ваш почтовый провайдер, ни какое-либо другое лицо, получившее доступ к вашему сообщению, не смогут его прочесть. Однако сам факт того, что вы отправили письмо адресату, может стать известен государственным учреждениям и правоохранительным органам. Чтобы этого не допустить, лучше создать дополнительный адрес email, не связанный с вашей личностью.

Материалы [о безопасной электронной почте от службы цифровой поддержки Access Now](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

- [Freedom of the Press Foundation, "Шифрование электронной почты с помощью Mailvelope: руководство для начинающих"](https://freedom.press/training/encrypting-email-mailvelope-guide/)
- [Mailvelope: шифрование электронной почты ](https://safe.roskomsvoboda.org/mailvelope/)
- [Privacy Tools, "Частные сервисы email"](https://www.privacytools.io/providers/email/)
- [ProtonMail - защищенная электронная почта ](https://safe.roskomsvoboda.org/protonmail/)
- [Tutanota - защищенная электронная почта ](https://safe.roskomsvoboda.org/tutanota)
