---
layout: page
title: WhatsApp
author: mfc
language: ru
summary: Способы связи
date: 2018-09
permalink: /ru/contact-methods/whatsapp.md
parent: /ru/
published: true
---

При использовании WhatsApp ваша беседа защищена. Только вы и собеседник можете видеть содержание. Однако сам факт того, что вы связывались с адресатом, может стать известен государственным учреждениям и правоохранительным органам.

Что почитать: [Руководство по WhatsApp для Android](https://ssd.eff.org/ru/module/%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE-whatsapp-%D0%B4%D0%BB%D1%8F-android), [Руководство по WhatsApp для iOS](https://ssd.eff.org/ru/module/%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE-whatsapp-%D0%B4%D0%BB%D1%8F-ios).
