---
layout: page
title: "Проблема с доступом к аккаунту"
author: RaReNet
language: ru
summary: "Проблемы с доступом к email, аккаунту в соцсети или на каком-то сайте? В аккаунте происходят какие-то действия без вашего ведома? Есть много способов решить эту проблему."
date: 2023-04
permalink: /ru/topics/account-access-issues/
parent: /ru/
---


# У меня проблема с доступом к аккаунту

Гражданские активисты широко используют для связи социальные сети и прочие аккаунты коммуникаций. И не только для связи, но и для обмена знаниями и продвижения собственных интересов. Неудивительно, что эти аккаунты часто становятся мишенями. Злоумышленники вредят аккаунтам, а значит, самим активистам и тем, с кем они общаются.

В этом руководстве мы попытаемся вам помочь, если вы потеряли доступ к своему аккаунту из-за таких злоумышленников.

Вот вопросы, которые позволят лучше понять суть вашей проблемы и подыскать решения.

## Workflow

### Password-Typo

> Иногда человек не может зайти в аккаунт, потому что неправильно вводит пароль, или забыл переключить раскладку клавиатуры, или у него просто включён CapsLock.
>
> Попробуйте набрать логин и пароль в текстовом редакторе. Скопируйте и вставьте их из редактора в форму на сайте. Убедитесь, что раскладка клавиатуры правильная, а CapsLock выключен.

Это помогло получить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#What-Type-of-Account-or-Service)

### What-Type-of-Account-or-Service

С каким аккаунтом проблема?

- [Facebook](#Facebook)
- [Страница Facebook](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->


### Facebook-Page

Есть ли у страницы другие администраторы?

- [Да](#Other-admins-exist)
- [Нет](#Facebook-Page-recovery-form)

### Other-admins-exist

У других администраторов та же проблема?

- [Да](#Facebook-Page-recovery-form)
- [Нет](#Other-admin-can-help)

### Other-admin-can-help

> Пожалуйста, попросите других администраторов добавить вас к списку администраторов.

Проблема решена?

- [Да](#Fb-Page_end)
- [Нет](#account_end)


### Facebook-Page-recovery-form

> Пожалуйста, войдите в Facebook и используйте [форму Facebook для восстановления страницы](https://www.facebook.com/help/contact/164405897002583).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-google)
- [Нет](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Вы получили письмо о критической проблеме с безопасностью от Google?

- [Да](#Email-received-google)
- [Нет](#Recovery-Form-google)

### Email-received-google

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-google)
- [Нет](#Recovery-Form-google)

### Recovery-Link-Found-google

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-google)

### Recovery-Form-google

> Пожалуйста, попробуйте следовать инструкциям ["Как восстановить доступ к Gmail или аккаунту Google"](https://support.google.com/accounts/answer/7682439?hl=ru).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-yahoo)
- [Нет](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

Вы получали письмо от Yahoo о смене пароля для вашего аккаунта?

- [Да](#Email-received-yahoo)
- [Нет](#Recovery-Form-Yahoo)

### Email-received-yahoo

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Yahoo)
- [Нет](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Чтобы восстановить доступ к аккаунту, пожалуйста, следуйте инструкциям ["Исправьте проблемы с доступом к вашему аккаунту Yahoo"](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Twitter)
- [Нет](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

Вы получали письмо от Twitter о смене пароля для вашего аккаунта?

- [Да](#Email-received-Twitter)
- [Нет](#Recovery-Form-Twitter)

### Email-received-Twitter

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Twitter)
- [Нет](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Пожалуйста, попробуйте рекомендации ["Запросить помощь в восстановлении учетной записи"](https://help.twitter.com/forms/restore).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### ProtonMail

> Чтобы восстановить доступ к аккаунту, пожалуйста, следуйте [инструкциям для сброса пароля](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Эти советы помогли?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

Вы получали письмо от Hotmail о смене пароля для аккаунта Microsoft?

- [Да](#Email-received-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### Email-received-Hotmail

В письме есть ссылка для восстановления пароля?

- [Да](#Recovery-Link-Found-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Пожалуйста, попробуйте форму ["Восстановление учетной записи"](https://account.live.com/acsr).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


### Facebook

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Facebook)
- [Нет](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

Вы получали от Facebook письмо "Смена пароля на Facebook"?

- [Да](#Email-received-Facebook)
- [Нет](#Recovery-Form-Facebook)

### Email-received-Facebook

Говорится ли в письме "Если это были не вы, узнайте, как вы можете защитить свой аккаунт" (с гиперссылкой)?

- [Да](#Recovery-Link-Found-Facebook)
- [Нет](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Используйте ссылку "защитить свой аккаунт" для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту по этой ссылке?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Пожалуйста, попробуйте [форму для восстановления аккаунта](https://www.facebook.com/login/identify).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Instagram)
- [Нет](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

Вы получали от Instagram письмо со словами о том, что "ваш пароль... был изменён"?

- [Да](#Email-received-Instagram)
- [Нет](#Recovery-Form-Instagram)

### Email-received-Instagram

В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Instagram)
- [Нет](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Используйте ссылку "сбросить его здесь" для восстановления доступа к аккаунту.> [there is no link here]

Удалось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Для восстановления доступа к аккаунту, пожалуйста, попробуйте инструкции ["Думаю, мой аккаунт Instagram взломали"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)


### Fb-Page_end

Ваша проблема решена? Прекрасно! Пожалуйста, прочтите эти рекомендации. Они помогут минимизировать риск потери доступа к вашей странице в будущем:

- Всем администраторам страницы лучше включить двухфакторную аутентификацию в настройках своих аккаунтов.
- Предоставляйте права администратора только тем людям, кому верите, с кем есть оперативная связь.

### account_end

Если все предложенные меры не помогли вам восстановить доступ к аккаунту, можете обратиться к организациям, предлагающим помощь в этой сфере:

:[](organisations?services=account)

### resolved_end

Надеемся, Комплект экстренной цифровой помощи оказался вам полезен. Пожалуйста, поделитесь своим мнением [по email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Пожалуйста, прочтите эти рекомендации, чтобы снизить вероятность потери доступа к аккаунтам в будущем:

- Для всех аккаунтов, где это возможно, есть смысл включать двухфакторную аутентификацию.
- Никогда не используйте один и тот же пароль для двух и более аккаунтов. Если всё-таки использовали, смените пароли как можно скорее.
- Парольный менеджер поможет создавать и запоминать уникальные, надёжные пароли для всех ваших аккаунтов.
- Будьте осторожны, используя открытые сети wi-fi, которым нет оснований доверять. Возможно, лучше работать в них через VPN или Tor.

#### Resources

- [Социальные сети: 22 правила](https://safe.roskomsvoboda.org/socialnetworks/)
- [Двухфакторная аутентификация](https://safe.roskomsvoboda.org/twofactor/)
- [Материалы службы поддержки Access Now по парольным менеджерам](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Security Self-Defense, "Самозащита в социальных сетях"](https://ssd.eff.org/ru/module/%D1%81%D0%B0%D0%BC%D0%BE%D0%B7%D0%B0%D1%89%D0%B8%D1%82%D0%B0-%D0%B2-%D1%81%D0%BE%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85-%D1%81%D0%B5%D1%82%D1%8F%D1%85)​​​​​​​
- [Security Self-Defense, "Создание надёжных паролей""](https://ssd.eff.org/ru/module/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0%D0%B4%D1%91%D0%B6%D0%BD%D1%8B%D1%85-%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9)




<!--- Edit the following to add another service recovery workflow:
#### service-name

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-google)
- [Нет](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Вы получили от service-name сообщение "[Password Change Email Subject]"?

- [Да](#Email-received-service-name)
- [Нет](#Recovery-Form-service-name

### Email-received-service-name

> В письме есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-service-name)
- [Нет](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Пожалуйста, используйте [Recovery Link Description](URL) для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту с помощью ссылки "[Recovery Link Description]"?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Пожалуйста, используйте эту форму для восстановления доступа к аккаунту: [Link to the standard recovery form].
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

-->
