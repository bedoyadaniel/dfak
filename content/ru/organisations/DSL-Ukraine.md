---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: понедельник – четверг, 9:00-17:00 EET/EEST
response_time: 2 дня
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Лаборатория цифровой безопасности Украины - это неправительственная организация из Киева, основанная в 2017 с миссией поддержки реализации прав человека в Интернете путем наращивания потенциала НКО и независимых медиа для решения проблем цифровой безопасности и воздействия на государственную и корпоративную политику в области цифровых прав.
