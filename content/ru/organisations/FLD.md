---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: по срочным вопросам 24/7 (из любой точки мира); офис работает с понедельника по пятницу в рабочие часы по IST (UTC+1); сотрудники рассредоточены в разных регионах и разных часовых поясах
response_time: (по срочным вопросам) в день обращения или на следующий день
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 (по срочным вопросам); +353-1-212-3750 (офис)
skype: front-line-emergency?call
email: info@frontlinedefenders.org (для вопросов и комментариев)
initial_intake: yes
---

Front Line Defenders — международная организация со штаб-квартирой в Ирландии. Её задача — комплексная защита правозащитников из "группы риска". Front Line Defenders обеспечивает им быструю практическую помощь в виде грантов для повышения безопасности, тренингов по физической и цифровой безопасности, поддержки их интересов и кампаний.

У Front Line Defenders есть горячая линия для срочных вопросов: +353-121-00489. Она работает круглосуточно (24/7) на арабском, английском, французском, русском и испанском языках. Горячей линией могут воспользоваться правозащитники, находящиеся в ситуации острого риска. Таким активистам Front Line Defenders также может помочь с временным перемещением. Эксперты Front Line Defenders проводят тренинги по физической и цифровой безопасности. Организация публикует истории правозащитников в ситуациях риска, проводит кампании, защищает интересы активистов на международном уровне, включая ЕС, ООН, использует межрегиональные механизмы и взаимодействует с правительствами для выполнения своей миссии.
