---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 дней в неделю, 9:00-17:00 UTC+5 (PST)
response_time: 56 часов
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) – зарегистрированная исследовательская неправительственная организация из Пакистана. Основана в 2012 году. DRF сосредоточена на том, как информационно-коммуникационные технологии поддерживают права человека, инклюзивность, демократические процессы и цифровое управление. DRF работает по направлениям свободы слова, приватности, защиты данных и онлайнового насилия в отношении женщин.
