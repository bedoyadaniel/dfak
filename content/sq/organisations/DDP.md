---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: E hënë-e enjte 9-17 CET
response_time: 4 ditë
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Qëllimi i Digital Defenders Partnership (DDP) është të mbrojë dhe avancojë lirinë e internetit, dhe ta mbajë internetin të lirë nga kërcënimet në zhvillim, posaçërisht në mjedise shtypëse. Ne koordinojmë mbështetjen urgjente për individë dhe organizata siç janë mbrojtësit e të drejtave të njeriut, gazetarët, aktivistët e shoqërisë civile dhe bloguesit. Ne kemi një qasje të përqendruar te njerëzit, duke u fokusuar në vlerat tona thelbësore të transparencës, të drejtave të njeriut, përfshirjes dhe diversitetit, barazisë, konfidencialitetit dhe lirisë. DDP u formua në vitin 2012 nga Freedom Online Coalition (FOC).

DDP ka tre lloje të ndryshme të financimit që adresojnë situata urgjente emergjente, si dhe grante afatgjata të përqendruara në ngritjen e kapaciteteve brenda një organizate. Për më tepër, ne koordinojmë Bursën për integritet digjital (Fellowship Digital Integrity), ku organizatat marrin trajnime të personalizuara për sigurinë digjitale dhe privatësinë, dhe një program të Rrjetit për Reagim të Shpejtë.
