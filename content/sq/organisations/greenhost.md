---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 orë
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost ofron shërbime të TI-së me një qasje etike dhe të qëndrueshme. Ofertat tona të shërbimit përfshijnë web-hosting, shërbime në re (cloud) dhe ofertat e fuqishme të specializuara për sigurinë e informacionit. Duke bashkëpunuar me organizatat kulturore dhe pionierët teknikë, ne përpiqemi t'u japim përdoruesve tanë mundësitë e plota të Internetit duke mbrojtur në të njëjtën kohë privatësinë e tyre. Ne jemi të përfshirë në mënyrë aktive në zhvillimin e kodit me burim të hapur, dhe marrim pjesë në projekte të ndryshme në fushat e teknologjisë, gazetarisë, kulturës, arsimit, qëndrueshmërisë dhe lirisë së internetit.
