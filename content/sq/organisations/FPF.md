---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Nga e hëna deri të premten, orët e punës, Zona e Kohës Lindore, SH.B.A.
response_time: 1 ditë
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Fondacioni Liria e Shtypit (Freedom of the Press Foundation - FPF)  është një organizatë 501(c)3 jofitimprurëse që mbron dhe fuqizon gazetarinë me interes publik në shekullin XXI. FPF trajnon të gjithë, duke filluar nga organizatat e mëdha mediatike e deri te gazetarët e pavarur për përdorimin e një larmie mjetesh dhe teknikash të sigurisë dhe privatësisë për të mbrojtur më mirë veten, burimet dhe organizatat e tyre. 
