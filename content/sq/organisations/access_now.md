---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, në nivel global
response_time: 2 orë
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Linja për ndihmë e Access Now punon me individë dhe organizata në mbarë botën për të qenë të sigurt në internet. Nëse jeni në rrezik, ne mund t'ju ndihmojmë të përmirësoni praktikat tuaja të sigurisë digjitale që të mos pësoni dëme. Nëse tashmë jeni të sulmuar, ne ofrojmë ndihmë urgjente me reagim të shpejtë.
