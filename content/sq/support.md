---
layout: page.pug
language: sq
permalink: /sq/support
type: support
---

Ja lista e organizatave që ofrojnë mbështetje të llojeve të ndryshme. Klikoni mbi secilën organizatë për t'i parë informacionet e plota në lidhje me shërbimet e tyre dhe si t'i kontaktoni ato.
