---
layout: page
title: "Mora një mesazh të dyshimtë"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone, Metamorphosis Foundation
language: sq
summary: "Kam marrë një mesazh, lidhje ose e-mail të dyshimtë, çfarë duhet të bëj?"
date: 2019-03-12
permalink: /sq/topics/suspicious-messages
parent: Home
---

# Mora një mesazh të dyshimtë

Mund të merrni mesazhe të _dyshimta_ në kutinë tuaj mbërritëse të e-mailit, në llogaritë e mediave sociale dhe/ose në aplikacionet për mesazhe. Forma më e zakonshme e e-maileve të dyshimta janë e-mail-et që përpiqen të vjedhin të dhëna të ndjeshme (phishing). E-mailet phishing kanë për qëllim t'ju mashtrojnë që të jepni informacionin tuaj personal, financiar ose të llogarisë së përdoruesit. Ato mund t'ju kërkojnë të vizitoni një faqe të rreme ose të telefononi një numër të rremë të shërbimit të klientit. E-mailet phishing gjithashtu mund të përmbajnë dokumente të bashkëngjitura që instalojnë softuer keqdashës në kompjuterin tuaj kur të hapen.

Nëse nuk jeni të sigurt për vërtetësinë e mesazhit që keni marrë, ose çfarë të bëni në lidhje me të, mund të përdorni pyetësorin e mëposhtëm si një mjet udhëzues për të diagnostikuar më tej situatën ose për ta ndarë mesazhin me organizatat e jashtme të besueshme që do t'ju ofrojnë një analizë më të hollësishme të mesazhit tuaj.

Mbani në mend se marrja e një e-maili të dyshimtë nuk do të thotë se llogaria juaj është komprometuar. Nëse mendoni se një e-mail ose një mesazh është i dyshimtë, mos e hapni atë. Mos u përgjigjuni në atë e-mail, mos klikoni asnjë lidhje dhe mos shkarkoni asnjë dokument të bashkëngjitur.

## Workflow

### intro

A keni kryer ndonjë veprim në atë mesazh apo lidhje?

- [Klikova në lidhje](#link-clicked)
- [I futa kredencialet](#account-security_end)
- [E shkarkova një skedar](#device-security_end)
- [U përgjigja me informacion](#reply-personal-info)
- [Ende nuk kam ndërmarr ndonjë veprim](#do-you-know-sender)

### do-you-know-sender

A e njihni dërguesin e mesazhit? Kini parasysh se dërguesi i mesazhit mund të jetë [i rremë] (https://en.wikipedia.org/wiki/Email_spoofing) që të duket se është dikush që ju i besoni.

 - [Është person ose organizatë që e njoh](#known-sender)
 - [Është ofrues shërbimesh (si p.sh. ofrues i e-mailit, hostingut, media sociale ose bankë)](#service-provider)
 - [Mesazhi është dërguar nga një person ose organizatë e panjohur](#share)

### known-sender

> A mund ta kontaktoni dërguesin duke përdorur ndonjë tjetër kanal komunikimi? Për shembull nëse keni marrë një e-mail, a mund ta verifikoni atë direkt me dërguesin me telefon ose WhatsApp? Sigurohuni që të përdorni një metodë ekzistuese të kontaktit. Nuk mund t'i besoni domosdoshmërisht një numri të telefonit në nënshkrimin e një mesazhi të dyshimtë.

A e konfirmuat që dërguesi është ai që ua ka dërguar këtë mesazh?

 - [Po](#resolved_end)
 - [Jo](#share)

### service-provider

> Në këtë skenar, një ofrues i shërbimit është çdo kompani ose markë që ofron shërbime që i përdorni ose që jeni parapaguar. Kjo listë mund të përmbajë ofruesin tuaj të e-mailit (Google, Yahoo, Microsoft, ProtonMail...), ofruesin tuaj të mediave sociale (Facebook, Twitter, Instagram...), ose platforma në internet që kanë informacionin tuaj financiar (Paypal, Amazon, bankat, Netflix...).
>
> A ka ndonjë mënyrë që të mund të kontrolloni nëse mesazhi është autentik? Shumë ofrues të shërbimeve do të japin gjithashtu kopje të njoftimeve ose dokumente të tjera në faqen e llogarisë tuaj. Për shembull, nëse mesazhi është nga Facebook, ai duhet të përfshihet në [listën tuaj të e-maileve të njoftimit](https://www.facebook.com/settings?tab=security&section=recent_emails), ose nëse është nga banka juaj, mund të telefononi shërbimin e klientit të bankës suaj.

Zgjidhni një nga opsionet e mëposhtme:

 - [Arrita ta verifikoj se është një mesazh legjitim nga ofruesi im i shërbimit](#resolved_end)
 - [Nuk isha në gjendje të verifikoj mesazhin](#share)
 - [Nuk jam i parapaguar në këtë shërbim dhe/ose nuk pres mesazh prej tyre](#share)

### link-clicked

> Në disa mesazhe të dyshimta, lidhjet mund t'ju dërgojnë në faqe të rreme identifikimi që do t'jua vjedhin kredencialet ose lloje të tjera të faqeve që mund t'ua vjedhin informacionin tuaj personal ose financiar. Lidhja në disa raste mund t'ju kërkojë të shkarkoni dokumente të bashkëngjitura që instalojnë softuer keqdashës në kompjuterin tuaj kur të hapen.

A mund të tregoni se çfarë ndodhi pasi klikuat në lidhjen?

 - [Më kërkoi të fus kredencialet](#account-security_end)
 - [Shkarkoi një skedar (fajll)](#device-security_end)
 - [Asgjë nuk ndodhi, por nuk jam i sigurt](#device-security_end)

### reply-personal-info

> Në varësi të llojit të informacionit që keni ndarë, mund t'ju duhet të veproni menjëherë.

Çfarë lloj informacioni keni ndarë?

- [Kam ndarë informacione konfidenciale të llogarisë](#account-security_end)
- [Kam ndarë informacione publike](#share)
- [Nuk jam i sigurt se sa i ndjeshëm ishte informacioni dhe kam nevojë për ndihmë](#help_end)


### share

> Shpërndarja e mesazhit tuaj të dyshimtë mund të ndihmojë në mbrojtjen e kolegëve dhe komunitetit tuaj të cilët gjithashtu mund të preken prej tij.
>
> Për të shpërndarë mesazhin tuaj të dyshimtë, sigurohuni që të përfshini vetë mesazhin, si dhe informacion rreth dërguesit. Nëse mesazhi ishte një e-mail, ju lutemi sigurohuni që të përfshini e-mailin e plotë, duke përfshirë titujt, duke përdorur [udhëzimin e mëposhtëm nga CIRCL](https://www.circl.lu/pub/tr-07/).

Keni nevojë për ndihmë të mëtejshme?

- [Po, kam nevojë për më shumë ndihmë](#help_end)
- [Jo, e kam zgjidhur problemin](#resolved_end)


### device-security_end

> Në rast se disa skedarë janë shkarkuar në pajisjen tuaj, pajisja mund të jetë në rrezik!

Ju lutemi kontaktoni organizatat më poshtë të cilat mund t'ju mbështesin. Më pas, ju lutemi [shpërndajeni mesazhin tuaj të dyshimtë](#share).

:[](organisations?services=device_security)


### account-security_end

> Në rast se i keni futur kredencialet, llogaritë tuaja mund të jenë në rrezik!
>
> Nëse mendoni se llogaria juaj është komprometuar, ju rekomandojmë të ndiqni rrjedhën e punës së Veglërisë së ndihmës së parë digjitale për [llogaritë e komprometuara](../../../account-access-issues).
>
> Ju sugjerojmë ta informoni komunitetin tuaj për këtë fushatë të vjedhjes së të dhënave të ndjeshme (phishing) dhe ta shpërndani mesazhin e dyshimtë me organizatat që mund t'i analizojnë ato.

A dëshironi të ndani informacion mbi mesazhin që keni marrë apo keni nevojë për ndihmë të mëtejshme më parë?

- [Dua ta ndaj mesazhin e dyshimtë](#share)
- [Më duhet më shumë ndihmë për të siguruar llogarinë time](#account_end)
- [Më duhet më shumë ndihmë për ta analizuar mesazhin](#analysis_end)

### help_end

> Duhet të kërkoni ndihmë nga kolegët tuaj ose të tjerët për të kuptuar më mirë rreziqet nga informacioni që keni ndarë. Njerëz të tjerë në organizatën ose rrjetin tuaj gjithashtu mund të kenë marrë kërkesa të ngjashme.

Ju lutemi kontaktoni organizatat më poshtë të cilat mund t'ju mbështesin. Më pas, ju lutemi [shpërndajeni mesazhin tuaj të dyshimtë] (#share).

:[](organisations?services=digital_support)


### account_end

Nëse llogaria juaj është komprometuar dhe keni nevojë për ndihmë për ta siguruar atë, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=account)


### analysis_end

Organizatat e mëposhtme mund të marrin mesazhin tuaj të dyshimtë dhe ta hetojnë atë më tej për ju:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

Rregulli i parë për t'u mbajtur mend: Asnjëherë mos jepni asnjë informacion personal në e-mail. Asnjë  institucion, bankë ose ndonjë institucion tjetër nuk do t'i kërkojë kurrë këto informacione përmes e-mail-it. Mund të mos jetë gjithmonë e lehtë të tregoni nëse një e-mail ose ueb-faqe është e ligjshme, por ka disa këshilla që mund t'ju ndihmojnë të vlerësoni e-mailin që keni marrë.

* Ndjenja e urgjencës: e-mailet e dyshimta zakonisht paralajmërojnë për ndryshim të papritur në llogari dhe kërkojnë nga ju që të veproni menjëherë për ta verifikuar llogarinë tuaj.
* Në pjesën kryesore të një e-maili, mund të shihni pyetje që ju kërkojnë të "verifikoni" ose "azhurnoni llogarinë tuaj" ose "mosazhurnimi i të dhënave tuaja do të rezultojë me pezullimin e llogarisë". Zakonisht është e sigurt të supozohet se asnjë organizatë e besueshme të cilës ju i keni dhënë informacionin tuaj asnjëherë nuk do t'ju kërkojë ta rifusni atë, prandaj mos bini pre e këtij kurthi.
* Kini kujdes nga mesazhet, bashkëngjitjet, lidhjet dhe faqet e hyrjes të panjohura.
* Kini kujdes nga gabimet drejtshkrimore dhe gramatikore.
* Klikoni për të parë adresën e plotë të dërguesit, jo vetëm emrin e shfaqur.
* Kini kujdes nga lidhjet e shkurtuara - ato mund të fshehin lidhje keqdashëse pas tyre.
* Kur e vendosni treguesin e miut mbi një lidhje, URL-ja aktuale tek e cila ju dërgon lidhja shfaqet në një dritare të vogël mbi lidhjen ose në fund të dritares së shfletuesit tuaj.

#### resources

Ja një sërë burimesh për të zbuluar mesazhet e dyshimta dhe për të shmangur rënien pre e vjedhjes së të dhënave të ndjeshme (phishing).

* [EFF: How to Avoid Phishing Attacks](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
* [PhishMe: How to Spot a Phish infographic](https://cofense.com/wp-content/uploads/2016/07/phishme-how-to-spot-a-phish.pdf)
* [Citizen Lab: Communities @ Risk report](https://targetedthreats.net) - [Appendix](https://targetedthreats.net/media/5-Appendix.pdf) përfshin shembuj të e-maileve të vërteta të phishing-ut, duke përfshirë një kategorizim se sa të personalizuara dhe të synuara janë ato.
