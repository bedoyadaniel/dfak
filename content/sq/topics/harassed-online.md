---
layout: page
title: "A jeni cak i ngacmimit në internet?"
author: Floriana Pagano, Metamorphosis Foundation
language: sq
summary: "A jeni cak i ngacmimit në internet?"
date: 2019-04-01
permalink: /sq/topics/harassed-online/
parent: Home
---

# Are you being targeted by online harassment?

Interneti, e veçanërisht platformat e mediave sociale, janë bërë një hapësirë kritike për anëtarët dhe organizatat e shoqërisë civile, veçanërisht për gratë, njerëzit LGBTIQ dhe pakicat e tjera, për t'u shprehur dhe për ta ngritur zërin e tyre. Por, në të njëjtën kohë, ato janë bërë edhe hapësira ku këto grupe synohen lehtësisht për shprehjen e pikëpamjeve të tyre. Dhuna dhe abuzimi në internet u mohojnë grave, personave LGBTIQ dhe shumë njerëzve të tjerë të paprivilegjuar të drejtën të shprehen në mënyrë të barabartë, lirshëm dhe pa frikë.

Dhuna dhe abuzimi në internet kanë forma të ndryshme, dhe kryerësit shpesh mund të mbështeten në pandëshkueshmëri, edhe për shkak të mungesës së ligjeve që i mbrojnë viktimat e ngacmimit në shumë vende, por mbi të gjitha sepse strategjitë e mbrojtjes duhet të përshtaten në mënyrë kreative në varësi të llojit të sulmit që realizohet.

Prandaj është e rëndësishme të identifikohet tipologjia e sulmit që na drejtohet për të vendosur se çfarë hapash mund të ndërmarrim.

Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa hapa themelorë për të planifikuar se si të mbroheni nga sulmi që u bëhet.

Nëse jeni cak i ngacmimit në internet, ndiqni këtë pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.

## Workflow

### physical-wellbeing

A keni frikë për integritetin fizik ose mirëqenien tuaj?

- [Po](#physical-risk_end)
- [Jo](#no-physical-risk)

### no-physical-risk

A mendoni se sulmuesi është qasur ose është duke u qasur në pajisjen tuaj?

 - [Po](#device-compromised)
 - [Jo](#account-compromised)

### device-compromised

> Ndryshoni fjalëkalimin për të hyrë në pajisjen tuaj me një fjalëkalim unik, të gjatë dhe të ndërlikuar:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

A është bllokuar në mënyrë efektive sulmuesi nga pajisja juaj?

 - [Po](#account-compromised)
 - [Jo](../../../device-acting-suspiciously)

### account-compromised

> Nëse dikush ka qasje në pajisjen tuaj, ata mund të kenë hyrë gjithashtu në llogaritë tuaja në internet, kështu që ata mund të lexojnë mesazhet tuaja private, të identifikojnë kontaktet tuaja, dhe të publikojnë postime, fotografi ose video sikur ju jeni duke e bërë këtë.

A keni vërejtur postime ose mesazhe që zhduken, ose aktivitete të tjera që ju japin arsye të mira për të menduar se llogaria juaj mund të jetë komprometuar? Rishikoni gjithashtu dosjen (folderin) tuaj të dërgimit (Sent) për aktivitete të mundshme të dyshimta.

 - [Po](../../../account-access-issues)
 - [Jo](#impersonation)

### impersonation

A paraqitet dikush si ju?

- [Po](../../../impersonated)
- [Jo](#doxing)

### doxing

A ka publikuar dikush informacione apo fotografi private pa pëlqimin tuaj?

- [Po](#doxing-yes)
- [Jo](#hate-speech)

### doxing-yes

Ku janë botuar informacionet apo fotografitë tuaja private?

- [Në platformë të rrjeteve sociale](#doxing-sn)
- [Në ueb-faqe](#doxing-web)

### doxing-sn

> Nëse informacionet ose fotografitë tuaja private janë publikuar në ndonjë platformë të mediave sociale, ju mund ta raportoni shkeljen e standardeve të komunitetit duke i ndjekur procedurat e raportimit që u jepen përdoruesve nga ueb-faqet e rrjeteve sociale. Udhëzimet për platformat kryesore mund t'i gjeni në listën e mëposhtme:
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

A janë fshirë informacionet apo fotografitë tuaja?

 - [Po](#one-more-persons)
 - [Jo](#harassment_end)

### doxing-web

> Ndiqni [këto udhëzime] (https://withoutmyconsent.org/resources/take-down) për të hequr përmbajtjen nga një ueb-faqe.

A është hequr përmbajtja nga ueb-faqja?

- [Po](#one-more-persons)
- [Jo](#harassment_end)

### hate-speech

A është sulmi i bazuar në atribute si raca, gjinia apo feja?

- [Po](#one-more-persons)
- [Jo](#harassment_end)


### one-more-persons

A jeni sulmuar nga një ose më shumë persona?

- [Një person](#one-person)
- [Më shumë persona](#more-persons)

### one-person

E njihni këtë person?

- [Po](#known-harasser)
- [Jo](#block-harasser)


### known-harasser

> Nëse e dini se kush po ju ngacmon, mund të mendoni t'i raportoni ato tek autoritetet e vendit tuaj, nëse është e përshtatshme në kontekstin tuaj. Secili vend ka ligje të ndryshme për mbrojtjen nga ngacmimi në internet, dhe ju duhet të shqyrtoni legjislacionin e vendit tuaj për të vendosur se çfarë do të bëni.
>
> Nëse vendosni ta padisni këtë person, duhet t'i drejtoheni një eksperti juridik.

A doni të padisni sulmuesin?

 - [Po](#legal_end)
 - [Jo](#block-harasser)


### block-harasser

> Pavarësisht se a e dini kush është ngacmuesi juaj ose jo, është gjithmonë ide e mirë që t'i bllokoni ata në platformat e rrjeteve sociale kur do që është e mundur.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

A e keni bllokuar ngacmuesin tuaj me sukses?

 - [Po](#resolved_end)
 - [Jo](#harassment_end)


### more-persons

> Nëse jeni duke u sulmuar nga më shumë se një person, mund të jeni objektiv i një fushate ngacmimi dhe do të duhet të mendoni për atë se cila është strategjia më e mirë për rastin tuaj.
>
> Për të mësuar në lidhje me të gjitha strategjitë e mundshme, lexoni këtë [faqe] (https://www.takebackthetech.net/be-safe/hate-speech-strategies).

A e keni identifikuar strategjinë më të mirë për ju?

 - [Po](#resolved_end)
 - [Jo](#harassment_end)

### harassment_end

> Nëse ngacmimi ndaj jush vazhdon akoma dhe keni nevojë për një zgjidhje të personalizuar, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=harassment)


### physical-risk_end

> Nëse jeni në rrezik fizik, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=physical_security)


### legal_end

> Nëse keni nevojë për mbështetje ligjore, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=legal)

### resolved_end

Shpresojmë që ky udhëzues për zgjidhjen e problemeve të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Dokumentimi i ngacmimit:** Është e dobishme të dokumentohen sulmet ose ndonjë incident tjetër ku mund të jeni dëshmitar: kopjoni pamjet e ekranit (screenshots), ruani mesazhet që i merrni nga ngacmuesit, etj. Nëse është e mundur, krijoni një ditar ku mund të sistemoni këto të dhëna që regjistrojnë orët, platformat dhe URL-të, ID-të e përdoruesit, pamjet e ekranit (screenshots), përshkrimin e asaj që ka ndodhur, etj. Ditarët mund t'ju ndihmojnë të zbuloni modelet dhe indikacionet e mundshme në lidhje me sulmuesit tuaj të mundshëm. Nëse ndjeheni të mbingarkuar, përpiquni të gjeni dikë që i besoni, i cili mund t'i dokumentojë incidentet për ju për një kohë. Duhet t'i besoni thellë personit që do ta menaxhojë këtë dokumentacion, pasi do t'ju duhet t'ia dorëzoni të dhënat për të hyrë në llogaritë tuaja personale. Pasi të ndjeheni se mund të rimerrni kontrollin mbi këto llogari, mos harroni t'i ndryshoni fjalëkalimet tuaja.

    - Udhëzime se si t'i dokumentoni incidentet mund të gjeni në [këtë faqe](https://www.techsafety.org/documentationtips/).

- **Vendosni autentifikimin me 2-faktorë** në të gjitha llogaritë tuaja. Autentifikimi me 2 faktorë mund të jetë shumë efektiv për ta ndaluar dikë të hyjë në llogaritë tuaja pa lejen tuaj. Nëse mund të zgjidhni, mos e përdorni autentifikimin me 2 faktorë të bazuar në SMS dhe zgjidhni një mundësi tjetër, të bazuar në aplikacion telefonik ose në çelës sigurie.

    - Nëse nuk e dini se cila është zgjidhja më e mirë për ju, mund ta shikoni [këtë infografikë] (https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) dhe [këtë artikull](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Udhëzime për vendosjen e autentifikimit me 2-faktorë në platformat kryesore mund të gjeni [këtu](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Bëni hartë të pranisë tuaj online**. "Vetë-doxing"-u konsiston në eksplorimin e të dhënave me burim të hapur për veten tuaj për t'i parandaluar aktorët dashakeq që ta gjejnë dhe ta përdorin këtë informacion duke ju imituar juve.


#### resources

- [Çfarë duhet të bëni nëse përjetoni doxing](https://www.wired.com/story/what-do-to-if-you-are-being-doxed/)
- [Mbyllja e identitetit tuaj digjital](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Udhëzues anti-doxing për aktivistët që përballen me sulme nga djathtistët Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [Udhëzues për të parandaluar "doxing"-un](https://guides.accessnow.org/self-doxing/self-doxing.html)
