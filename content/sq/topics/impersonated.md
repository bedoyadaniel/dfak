---
layout: page
title: "A shtiret dikush si ju online?"
author: Floriana Pagano, Alexandra Hache, Metamorphosis Foundation
language: sq
summary: "Dikush shtiret sikurse jeni ju përmes një llogarie të mediave sociale, adresës së e-mailit, çelësit PGP, ueb-faqes së rreme ose aplikacionit të rremë"
date: 2019-04-01
permalink: /sq/topics/impersonated
parent: Home
---

# A shtiret dikush si ju online?

Një kërcënim me të cilin përballen shumë aktivistë, mbrojtës të të drejtave të njeriut, OJQ-të, media të pavarura dhe bloguesit është shtirja sikurse jeni ju nga kundërshtarët që do të krijojnë profile false, ueb-faqe ose e-maile të rreme në emrat e tyre. Kjo ka për qëllim ndonjëherë të krijojë fushata njollosëse, informacione mashtruese, inxhinieri sociale ose vjedhje të identitetit të një personi në mënyrë që të krijojë zhurmë, çështje të besimit dhe dëmtim të të dhënave që ndikojnë në reputacionin e individëve dhe kolektivëve, në emrin e të cilës shtiren gjegjësisht paraqiten. Ky është një problem shqetësues që mund të ndikojë në nivele të ndryshme në aftësinë tuaj për të komunikuar dhe informuar, dhe mund të ketë shkaqe të ndryshme në varësi të vendit ku dhe si dikush shtiret sikurse jeni ju.

Është e rëndësishme të dini se ekzistojnë shumë mënyra për t'u shtirur si dikush tjetër (profile të rreme në mediat sociale, ueb-faqe të klonuara, e-maile të rreme, publikimi jokonsensual i fotografive dhe videove personale). Strategjitë mund të variojnë nga paraqitja e njoftimeve për heqje të ueb-faqeve, vërtetimi i pronësisë origjinale, pretendimi i të drejtës së autorit të ueb-faqes origjinale ose informacionit, ose paralajmërimi i rrjeteve dhe kontakteve tuaja personale përmes komunikimeve publike ose konfidenciale. Diagnostikimi i problemit dhe gjetja e zgjidhjeve të mundshme për mbrojtje mund të jetë e ndërlikuar. Ndonjëherë do të jetë pothuajse e pamundur të detyroni një kompani të vogël hostingu që ta heqë një ueb-faqe, dhe veprimi juridik mund të bëhet i domosdoshëm. Është praktikë e mirë të vendosni alarme dhe të monitoroni internetin për të zbuluar nëse dikush shtiret sikurse ju ose organizata juaj.

Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa hapa themelorë për të diagnostikuar mënyrat e mundshme të shtirjes si dikush tjetër dhe strategjitë e mundshme të zbutjes për të hequr llogaritë, ueb-faqet dhe e-mailet që shtiren sikurse ju ose organizata juaj.

Nëse dikush shtiret sikurse ju, ndiqni këtë pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.


## Workflow

### urgent-question

A keni frikë për integritetin fizik ose mirëqenien tuaj?

 - [Po](#physical-sec_end)
 - [Jo](#diagnostic-start1)

### diagnostic-start1

A ndikon shtirja e dikujt sikurse ju tek ju si individ (dikush e përdor emrin dhe mbiemrin tuaj ligjor, ose pseudonimin mbi të cilin e bazoni reputacionin tuaj) apo si organizatë/kolektiv?

- [Si individ](#individual)
- [Si organizatë](#organization)

### individual

> Nëse jeni prekur si individ, ju mund të dëshironi të paralajmëroni kontaktet tuaja. Merrni këtë hap duke përdorur një llogari e-maili, një profil ose një ueb-faqe që është plotësisht nën kontrollin tuaj.

- Pasi të keni njoftuar kontaktet tuaja se dikush shtiret si ju, vazhdoni në [hapin tjetër] (#diagnostic-start2).

### organization

> Nëse jeni prekur si grup, mund të dëshironi të bëni një njoftim publik. Merrni këtë hap duke përdorur një llogari e-maili, një profil ose një ueb-faqe që është plotësisht nën kontrollin tuaj.

- Pasi të keni njoftuar komunitetin tuaj se dikush shtiret si ju, vazhdoni në [hapin tjetër] (#diagnostic-start2).

### diagnostic-start2

Në çfarë mënyre shtiren sikurse ju?

 - [Një ueb-faqe e rreme shtiret sikurse unë ose grupi im](#fake-website)
 - [Përmes një llogarie në rrjetet sociale](#social-network)
 - [Përmes shpërndarjes jokonsensuale të videove ose fotografive](#other-website)
 - [Përmes adresës sime të e-mailit ose një adrese të ngjashme](#spoofed-email1)
 - [Përmes çelësit PGP të lidhur me adresën time të e-mail-it](#PGP)
 - [Përmes një aplikacioni të rremë që e imiton aplikacionin tim](#app1)

### social-network

Në cilën platformë të rrjeteve sociale shtiren sikurse ju?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google+](#google+)
- [Instagram](#instagram)

###  facebook

> Ndiqni [këto udhëzime](https://www.facebook.com/help/174210519303259) për të kërkuar që llogaria që rrejshëm paraqitet si e juaja të fshihet.
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi?

- [Po](#resolved_end)
- [Jo](#account_end)

### twitter

> Plotësoni [këtë formular](https://help.twitter.com/forms/impersonation) për të kërkuar që llogaria që rrejshëm paraqitet si e juaja të fshihet.
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### google+

>  Plotësoni [këtë formular](https://support.google.com/plus/troubleshooter/1715140) për të kërkuar që llogaria që rrejshëm paraqitet si e juaja të fshihet.
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)

### instagram

> Ndiqni [këto udhëzime](https://help.instagram.com/446663175382270) për të kërkuar që llogaria që rrejshëm paraqitet si e juaja të fshihet.
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#account_end)


### fake-website

> Kontrolloni nëse kjo ueb-faqe është e njohur si ueb-faqe keqdashëse duke e kërkuar URL-në e saj në shërbimet e mëposhtme online:
>    
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

A dihet se domeni ka qëllim të keq?

 - [Po](#malicious-website)
 - [Jo](#non-malicious-website)

### malicious-website

> Raportoni URL-në në Google Safe Browsing duke e plotësuar [këtë formular](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Ju lutemi vini re se mund të duhet ca kohë për t'u siguruar që raporti juaj ishte i suksesshëm. Ndërkohë mund të vazhdoni në hapin tjetër për dërgimin e një kërkese për heqjen e ueb-faqes tek ofruesi i hostingut dhe regjistruesi i domenit, ose ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi?

- [Po](#resolved_end)
- [Jo](#non-malicious-website)


### non-malicious-website

> Mund të provoni ta raportoni ueb-faqen tek ofruesi i hostingut ose regjistruesi i domenit, duke kërkuar ta heqin feb-faqen.
>
> Nëse ueb-faqja që dëshironi ta raportoni është duke përdorur përmbajtjet tuaja, një gjë që mund t'ju duhet ta dëshmoni është se ju jeni pronari legjitim i përmbajtjes origjinale. Ju mund ta tregoni këtë duke paraqitur kontratën tuaj origjinale me regjistruesin e domenit dhe/ose ofruesin e hostingut, por gjithashtu mund të bëni një kërkim në [Machine Wayback](https://archive.org/web/), duke i kërkuar të dyja URL-të, edhe atë të ueb-faqes suaj dhe të ueb-faqes së rreme. Nëse ueb-faqet janë indeksuar atje, do të gjeni një histori që mund ta bëjë të mundur të tregoni që faqja juaj e internetit ekzistonte përpara se të publikohej ueb-faqja e rreme.
>
> Për të dërguar një kërkesë për heqjen e ueb-faqes, do t'ju duhet të mblidhni informacione për ueb-faqen e rreme:
>
> - Shkoni në [këtë ueb-faqe](https://network-tools.com/nslookup/) dhe zbuloni adresën (ose adresat) e IP të ueb-faqes së rreme duke e futur URL-në e saj në formularin e kërkimit.
> - Shkruajeni adresën ose adresat e IP.
> - Shkoni në [këtë ueb-faqe](https://whois.domaintools.com/) dhe kërkoni edhe domenin edhe adresën/at e IP të ueb-faqes së rreme.
> - Regjistroni emrin dhe adresën e abuzimit me e-mailin e ofruesit të hostingut dhe shërbimit të domenit. Nëse paraqitet në rezultatet e kërkimit tuaj, regjistroni edhe emrin e pronarit të ueb-faqes.
> - Shkruajini ofruesit të hostingut dhe regjistruesit të domenit të ueb-faqes së rreme për të kërkuar heqjen e saj. Në mesazhin tuaj, përfshini informacione për adresën e IP, URL dhe pronarin e ueb-faqes që paraqitet si e juaja, si dhe arsyet pse është abuzive.
> - Mund ta përdorni [këtë gjedhë](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) për t'i shkruar ofruesit të hostingut.
> - Mund ta përdorni [këtë gjedhë](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) për t'i shkruar regjistruesit të domenit.
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi kjo?

- [Po](#resolved_end)
- [Jo](#web-protection_end)


### spoofed-email1

> Për arsye teknike, është mjaft e vështirë të vërtetohen e-mailet. Kjo është edhe arsyeja pse është shumë e thjeshtë të krijohen adresa të falsifikuara të dërguesve dhe e-mail-e të rreme.

A është duke u shtirë dikush si ju përmes adresës tuaj të e-mailit, ose një e-maili të ngjashëm, për shembull me të njëjtin emër përdoruesi, por me domen tjetër?

- [Dikush shtiret sikurse jam unë përmes adresës time të e-mail-it](#spoofed-email2)
- [Dikush shtiret sikurse jam unë përmes një adrese të ngjashme të e-mail-it](#similar-email)


### spoofed-email2

> Personi që shtiret si ju mund të ketë hakuar llogarinë tuaj të e-mailit. Për ta përjashtuar këtë mundësi, provoni të ndryshoni fjalëkalimin tuaj.

A jeni në gjendje të ndryshoni fjalëkalimin tuaj?

- [Po](#spoofed-email3)
- [Jo](#hacked-account)

### hacked-account

Nëse nuk mund ta ndryshoni fjalëkalimin tuaj, llogaria juaj e e-mailit me gjasë është komprometuar.

- Ju mund të ndiqni [këtë rrjedhë pune](../../../account-access-issues)  për të zgjidhur këtë çështje.

### spoofed-email3

> Mashtrimi përmes e-mail-it konsiston në mesazhe me e-mail me adresë të falsifikuar të dërguesit. Mesazhi duket se ka origjinën nga dikush ose diku që nuk është burimi i vërtetë.
>
> Mashtrimi përmes e-mail-it është i zakonshëm në fushatat për vjedhjen e të dhënave të ndjeshme (phishing) dhe spam sepse njerëzit kanë më shumë të ngjarë të hapin një e-mail kur mendojnë se vijnë nga një burim i ligjshëm.
>
> Nëse dikush e përdor e-mailin tuaj për mashtrim, ju duhet të informoni kontaktet tuaja për t'i paralajmëruar ata në lidhje me rrezikun e vjedhjes së të dhënave të ndjeshme (phishing) (bëjeni këtë nga një llogari e-maili, profil ose ueb-faqe që është plotësisht nën kontrollin tuaj).
>
> Nëse mendoni se shtirja sikur jeni ju kishte për qëllim vjedhjen e të dhënave të ndjeshme (phishing) ose qëllime të tjera dashakeqe, ju gjithashtu mund të dëshironi të lexoni seksionin [kam marrë mesazhe të dyshimta](../../../suspicious_messages).

A u ndalën e-mailet pasi e ndryshuat fjalëkalimin në llogarinë tuaj të e-mailit?

- [Po](#compromised-account)
- [Jo](#secure-comms_end)


### compromised-account

> Me sa duket llogaria juaj u hakua nga dikush që e përdori atë për të dërguar e-mail për t'u shtirur se jeni ju. Pasi që llogaria juaj është komprometuar, gjithashtu mund të dëshironi të lexoni seksionin [Kam humbur qasjen në llogaritë e mia](../../../account-access-issues/).

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#account_end)


### similar-email

> Nëse personi që shtiret si ju e përdor një adresë e-maili që është e ngjashme me tuajën, por me një domen tjetër ose emër tjetër përdoruesi, është ide e mirë t'i paralajmëroni kontaktet tuaja në lidhje me këtë përpjekje për t'u shtirur si ju (bëjeni këtë nga një llogari e-maili, profil ose ueb-faqe që është plotësisht nën kontrollin tuaj).
>
> Ju gjithashtu mund të dëshironi të lexoni seksionin [Kam marrë mesazhe të dyshimta](../../../suspicious-messages), pasi që kjo shtirje/paraqitje në emrin tuaj mund të ketë për qëllim vjedhjen e të dhënave të ndjeshme (phishing).

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#secure-comms_end)


### PGP

A mendoni se çelësi juaj privat PGP mund të ketë jetë komprometuar, për shembull sepse keni humbur kontrollin e pajisjes ku ishte ruajtur?

- [Po](#PGP-compromised)
- [Jo](#PGP-spoofed)

### PGP-compromised

A keni akoma qasje në çelësin tuaj privat?

- [Po](#access-to-PGP)
- [Jo](#lost-PGP)

### access-to-PGP

> - Revokoni çelësin tuaj.
>     - [Udhëzime për Enigmail](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - Krijoni një çift të ri çelësash dhe le ta nënshkruajnë atë njerëz që u besoni.
> - Komunikoni përmes një kanali që kontrolloni, që t'i informoni kontaktet tuaja se e keni revokuar çelësin tuaj dhe keni gjeneruar një çelës të ri.

Keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)


### lost-PGP

A keni një certifikatë revokimi?

- [Po](#access-to-PGP)
- [Jo](#no-revocation-cert)


### no-revocation-cert

> - Krijoni një çift të ri çelësash dhe le ta nënshkruajnë atë njerëz që u besoni.
> - Informoni kontaktet tuaja përmes një kanali që ju e kontrolloni se duhet ta përdorin çelësin tuaj të ri dhe ndërpriteni përdorimin e çelësit të vjetër.

Keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)

### PGP-spoofed

A është nënshkruar çelësi juaj nga njerëz të besuar?

- [Po](#signed-key)
- [Jo](#non-signed-key)

### signed-key

> Informoni kontaktet tuaja përmes një kanali që e kontrolloni se dikush po përpiqet të shtiret si ju dhe tregoni atyre se mund ta njohin çelësin tuaj aktual duke u bazuar në nënshkrimet nga kontaktet e besuara.

Keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)

### non-signed-key

> - Le ta nënshkruajnë çelësin njerëz të cilëve u besoni.
> - Informoni kontaktet tuaja përmes një kanali që e kontrolloni se dikush po përpiqet të shtiret sikur ju dhe tregoni atyre se mund ta njohin çelësin tuaj aktual duke u bazuar në nënshkrimet nga kontaktet e besuara.

Keni nevojë për më shumë ndihmë për të zgjidhur problemin tuaj?

- [Po](#secure-comms_end)
- [Jo](#resolved_end)

### other-website

> Nëse dikush shtiret si ju në një ueb-faqe, gjëja e parë që duhet të bëni është të kuptoni se ku është hostuar ajo ueb-faqe, kush e menaxhon atë dhe kush e ka siguruar emrin e domenit. Ky hulumtim ka për qëllim të identifikojë mënyrën më të mirë për të kërkuar heqjen e përmbajtjes keqdashëse.
>
> Para se të filloni me hetimin tuaj, nëse jeni qytetar i BE-së mund t'i kërkoni Google të heqë këtë ueb-faqe nga rezultatet e tyre të kërkimit në emrin tuaj.

A jeni qytetar i Bashkimit Evropian?

- [Po](#EU-privacy-removal)
- [Jo](#doxing-question)


### EU-privacy-removal

> Plotësoni [këtë formular](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=en&rd=1) për të hequr këtë ueb-faqe nga rezultatet e kërkimit në Google në emrin tuaj.
>
> Çfarë do t'ju duhet:
>
> - Një kopje digjitale e një dokumenti të identifikimit (nëse e paraqisni këtë kërkesë në emër të dikujt tjetër, do t'ju duhet të siguroni dokumentacionin e identifikimit për ta).
> - URL(të) e përmbajtjes me informacionin personal që dëshironi të hiqet
> - Për secilën URL që e paraqisni, duhet të shpjegoni:
>     1. se si informacioni personal i identifikuar më sipër lidhet me personin në emër të të cilit bëhet kjo kërkesë
>     2. pse mendoni se informacioni personal duhet të hiqet
>
> Ju lutemi vini re se nëse jeni regjistruar në llogarinë tuaj të Google, Google mund ta shoqërojë paraqitjen tuaj me atë llogari.
>
> Pas paraqitjes së kësaj forme, do të duhet të prisni një përgjigje nga Google për të verifikuar që ueb-faqja është hequr nga rezultatet.

A dëshironi të paraqisni një kërkesë për heqje të përmbajtjes për të hequr përmbajtjen që është falsifikuar sikur të ishte e juaja nga ueb-faqja?

- [Po](#doxing-question)
- [Jo, do të doja të merrja mbështetje](#account_end)

### doxing-question

A ka publikuar personi që shtiret si ju informacione personale, apo video ose fotografi intime tuajat?

- [Po](../../../harassed-online/questions/doxing-web)
- [Jo](#fake-website)

### app1

> Nëse dikush është duke përhapur kopje keqdashëse të aplikacionit tuaj ose softuer tjetër, është mirë të bëni një njoftim publik për të paralajmëruar përdoruesit që ta shkarkojnë vetëm versionin legjitim.
>
> Ju gjithashtu duhet të raportoni aplikacionin keqdashës dhe të kërkoni heqjen e tij.

Ku po shpërndahet kopja keqdashëse e aplikacionit tuaj?

- [Në Github](#github)
- [Në Gitlab.com](#gitlab)
- [Në Google Play Store](#playstore)
- [Në Apple App Store](#apple-store)
- [Në ueb-faqe tjetër](#fake-website)

### github

> Nëse softueri keqdashës është hostuar në Github, lexoni [këtë udhëzues](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) për heqjen e përmbajtjes që shkel të drejtat e autorit.
>
> Mund të duhet të prisni ca kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)

### gitlab

>Nëse softueri keqdashës është hostuar në Gitlab.com, lexoni [këtë udhëzues] (https://about.gitlab.com/handbook/dmca/) për heqjen e përmbajtjes që shkel të drejtat e autorit.
>
> Mund të duhet të prisni [ca kohë](https://about.gitlab.com/handbook/engineering/security/dmca-removal-requests.html) për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)


### playstore

> Nëse softueri keqdashës është hostuar në Google Play Store, ndiqni [këto udhëzime](https://support.google.com/legal/troubleshooter/1114905) për heqjen e përmbajtjes që shkel të drejtat e autorit.
>
> Mund të duhet ca kohë të prisni për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)



### apple-store

> Nëse softueri keqdashës është hostuar në App Store, ndiqni [këto udhëzime](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=en) për heqjen e përmbajtjes që shkel të drejtat e autorit.
>
> Mund të duhet ca kohë të prisni për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A ka ndihmuar kjo për ta zgjidhur problemin tuaj?

- [Po](#resolved_end)
- [Jo](#app_end)



### physical-sec_end

> Nëse keni frikë për mirëqenien tuaj fizike, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=physical_sec)


### account_end

> Nëse ende dikush shtiret sikur ju ose llogaria juaj është akoma e komprometuar, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=account&services=legal)


### app_end

> Nëse aplikacioni i rremë nuk është hequr, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=account&services=legal)

### web-protection_end

> Nëse kërkesat tuaja për heqjen e ueb-faqeve/përmbajtjeve nuk kanë qenë të suksesshme, mund të përpiqeni t'iu drejtoheni organizatave më poshtë për mbështetje të mëtejshme.

:[](organisations?services=web_protection)

### secure-comms_end

> Nëse keni nevojë për ndihmë ose rekomandime për vjedhjen e të dhënave të ndjeshme (phishing), sigurinë e e-mailit dhe enkriptimin, dhe komunikimin e sigurt në përgjithësi, mund t'u drejtoheni këtyre organizatave:

:[](organisations?services=secure_comms)


### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Për të parandaluar ndonjë përpjekje të mëtejshme për t'u shtirur sikur ju, lexoni këshillat më poshtë.

### final_tips

- Krijoni fjalëkalime të forta, komplekse dhe unike për të gjitha llogaritë tuaja.
- Konsideroni të përdorni një softuer për menaxhimin e fjalëkalimeve për krijimin dhe ruajtjen e fjalëkalimeve, në mënyrë që të mund të përdorni shumë fjalëkalime në ueb-faqe dhe shërbime të ndryshme, pa pasur nevojë t'i mësoni përmendësh.
- Aktivizoni autentifikimin me dy faktorë (2FA) për llogaritë tuaja më të rëndësishme. 2FA ofron siguri më të madhe të llogarisë duke kërkuar të përdorni më shumë se një metodë për të hyrë në llogaritë tuaja. Kjo do të thotë që edhe nëse dikush do të merrte fjalëkalimin tuaj parësor, ata nuk mund të hynin në llogarinë tuaj nëse nuk kishin gjithashtu telefonin tuaj celular ose një mjet tjetër sekondar të autentifikimit.
- Verifikoni profilet tuaja në platformat e rrjeteve sociale. Disa platforma ofrojnë një veçori për verifikimin e identitetit tuaj dhe lidhjen e tij me llogarinë tuaj.
- Bëni hartë të pranisë tuaj online. "Vetë-doxing"-u konsiston në eksplorimin e të dhënave të inteligjencës me burim të hapur për veten tuaj për t'i parandaluar aktorët dashakeq që ta gjejnë dhe ta përdorin këtë informacion duke je imituar juve.
- Vendosni alarmet e Google. Mund të merrni e-maile kur rezultatet e reja për një temë shfaqen në Kërkimin e Google. Për shembull, mund të merrni informacione kur përmendet emri juaj ose i organizatës/kolektivit tuaj.
- Bëni kopje të pamjes së ekranit të ueb-faqes suaj ashtu siç duket tani për ta përdorur si dëshmi në të ardhmen. Nëse ueb-faqja juaj lejon "crawlers", mund të përdorni Wayback Machine, të ofruar nga archive.org. Vizitoni [këtë faqe](https://archive.org/web/), shkruani emrin e ueb-faqes suaj në fushën "Save Page Now" dhe klikoni në butonin "Save Page Now".

#### resources

- [Krijoni fjalëkalime të forta dhe unike](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Pasqyrë e animuar: Përdorimi i softuerit për menaxhim të fjalëkalimeve për të qëndruar të sigurt në internet](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Zgjedhja e softuerit për menaxhim të fjalëkalimeve](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Si ta përdorni KeePassXC - një menaxhues i sigurt me kod të hapur i fjalëkalimeve](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Autentifikimi me dy faktorë (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Udhëzues për të parandaluar "doxing"-un](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Arkivoni ueb-faqen tuaj](https://archive.org/web/)
