---
layout: page
title: "Nuk mund të hyj në llogarinë time"
author: RaReNet, Metamorphosis Foundation
language: sq
summary: "A keni problem të hyni në e-mail-in tuaj, rrjetet sociale ose llogarinë tuaj në ndonjë ueb-faqe? A tregon llogaria/profili ndonjë aktivitet që nuk e njihni? Ka shumë gjëra që mund të bëni për ta zbutur këtë problem."
date: 2019-03
permalink: /sq/topics/account-access-issues/
parent: Home
---

# Kam humbur qasjen në llogarinë time

Llogaritë në rrjetet sociale dhe në mediat për komunikim përdoren gjerësisht nga anëtarët e shoqërisë civile për të komunikuar, shkëmbyer njohuri dhe për t'i përfaqësuar idetë e tyre. Si pasojë, këto llogari janë në shënjestër të madhe nga aktorët dashakeq, të cilët shpesh përpiqen të komprometojnë këto llogari, duke shkaktuar dëme për anëtarët e shoqërisë civile dhe kontaktet e tyre.

Ky udhëzues shërben për t'ju ndihmuar në rast se keni humbur qasjen në njërën prej llogarive tuaja sepse është komprometuar.

Në vazhdim është një pyetësor për ta identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhje të mundshme.

## Workflow

### Password-Typo

> Nganjëherë ne mund të mos jemi në gjendje të hymë në llogarinë tonë sepse e shkruajmë gabimisht fjalëkalimin, ose sepse zgjedhja e gjuhës së tastierës mund të mos jetë ajo që e përdorim zakonisht ose mund ta kemi të aktivizuar CapsLock.
>
> Provoni ta shkruani emrin e përdoruesit dhe fjalëkalimin tuaj në një redaktues teksti dhe pastaj t'i kopjoni ato nga redaktuesi dhe t'i ngjitni në fushat për hyrje në llogari.

A ju ndihmoi sugjerimi i mësipërm të hyni në llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#What-Type-of-Account-or-Service)

### What-Type-of-Account-or-Service

Në cilin lloj të llogarisë ose shërbimit e keni humbur qasjen?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

A ka faqja administratorë të tjerë?

- [Po](#Other-admins-exist)
- [Jo](#Facebook-Page-recovery-form)

### Other-admins-exist

A kanë administratorët e tjerë problem me të njëjtën çështje?

- [Po](#Facebook-Page-recovery-form)
- [Jo](#Other-admin-can-help)

### Other-admin-can-help

> Ju lutemi kërkoni nga administratorët e tjerë që t'ju shtojnë përsëri si administrator të faqes.

A u zgjidh problemi në këtë mënyrë?

- [Po](#Fb-Page_end)
- [Jo](#account_end)

### Facebook-Page-recovery-form

> Ju lutemi hyni në Facebook dhe përdorni [këtë formular për të rikthyer faqen](https://www.facebook.com/help/contact/164405897002583)).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

A keni qasje në e-mailin/celularin e lidhur? për rikuperim?

- [Po](#I-have-access-to-recovery-email-google)
- [Jo](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Kontrolloni nëse keni marrë një e-mail me titullin "Critical security alert for your linked Google Account" ose porosi SMS nga Google. A keni marrë një e-mail ose porosi SMS të tillë?

- [Po](#Email-received-google)
- [Jo](#Recovery-Form-google)

### Email-received-google

Ju lutemi kontrolloni nëse ekziston linku "recover your account" ("rikuperoni llogarinë tuaj"). A është aty?

- [Po](#Recovery-Link-Found-google)
- [Jo](#Recovery-Form-google)

### Recovery-Link-Found-google

> Ju lutemi përdorni lidhjen "recover your account" ("rikuperoni llogarinë tuaj") për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-google)

### Recovery-Form-google

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.


A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

A keni qasje në e-mailin/celularin të lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-yahoo)
- [Jo](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

Kontrolloni nëse keni marrë një e-mail me titullin "Password change for your Yahoo account" (ndryshimi i fjalëkalimit për llogarinë tuaj Yahoo) nga Yahoo. A keni marrë një e-mail të tillë?

- [Po](#Email-received-yahoo)
- [Jo](#Recovery-Form-Yahoo)

### Email-received-yahoo

Ju lutemi kontrolloni nëse ekziston lidhja "Recover your account" (rikuperoni llogarinë tuaj). A është aty?

- [Po](#Recovery-Link-Found-Yahoo)
- [Jo](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Ju lutemi përdorni lidhjen "Recover your account" ("rikuperoni llogarinë tuaj") për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [No](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Twitter)
- [Jo](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

Kontrolloni nëse keni marrë një e-mail me titullin "Your Twitter Password has been changed" (fjalëkalimi në Twitter është ndryshuar) nga Twitter. A keni marrë një e-mail të tillë?

- [Po](#Email-received-Twitter)
- [Jo](#Recovery-Form-Twitter)

### Email-received-Twitter

Ju lutemi kontrolloni nëse porosia përmban lidhjen "recover your account" ("rikuperoni llogarinë tuaj"). A është aty?

- [Po](#Recovery-Link-Found-Twitter)
- [Jo](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Ju lutemi përdorni lidhjen "recover your account" ("rikuperoni llogarinë tuaj") për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://help.twitter.com/forms/restore).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

Kontrolloni nëse keni marrë një e-mail me titullin "Microsoft account password change" (ndryshimi i fjalëkalimit të llogarisë Microsoft) nga Hotmail. A keni marrë një e-mail të tillë?

- [Po](#Email-received-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Ju lutemi kontrolloni nëse mesazhi përmban lidhjen "Reset your password" (rikuperoni llogarinë tuaj). A është aty?

- [Po](#Recovery-Link-Found-Hotmail)
- [Jo](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Ju lutemi përdorni lidhjen "Reset your password" (rikuperoni llogarinë tuaj) për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj përmes lidhjes "Reset your password" (resetoni fjalëkalimin tuaj)?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://account.live.com/acsr).)
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

### Facebook

A keni qasje në e-mailin/celularin e lidhur për rikuperim?

- [Po](#I-have-access-to-recovery-email-Facebook)
- [Jo](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

Kontrolloni nëse keni marrë një e-mail me titullin "Facebook password change" (ndryshimi i fjalëkalimit në Facebook) nga Facebook. A keni marrë një e-mail të tillë?

- [Po](#Email-received-Facebook)
- [Jo](#Recovery-Form-Facebook)

### Email-received-Facebook

A përmban e-maili një mesazh ku thotë "Nëse nuk e keni bërë këtë, ju lutemi siguroni llogarinë tuaj" me një lidhje?

- [Po](#Recovery-Link-Found-Facebook)
- [Jo](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Ju lutemi përdorni lidhjen "Recover your account" ("rikuperoni llogarinë tuaj") në mesazh për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj duke klikuar në lidhjen?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Ju lutemi provoni [këtë formular rikuperimi për të rikuperuar llogarinë tuaj](https://www.facebook.com/login/identify)
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

A keni qasje në e-mailin/celularin e rikuperuar të lidhur?

- [Po](#I-have-access-to-recovery-email-Instagram)
- [Jo](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

Kontrolloni nëse keni marrë një e-mail me titullin "Your Instagram Password has been changed" nga Instagram. A keni marrë një e-mail të tillë?

- [Po](#Email-received-Instagram)
- [Jo](#Recovery-Form-Instagram)

### Email-received-Instagram

Ju lutemi kontrolloni nëse ekziston linku për rikuperim. A është aty?

- [Po](#Recovery-Link-Found-Instagram)
- [Jo](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Ju lutemi përdorni lidhjen "recover your account" (rikuperoni llogarinë tuaj) për të rikuperuar llogarinë tuaj.

A arritët të rikuperoni llogarinë tuaj?

- [Po](#resolved_end)
- [Jo](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Ju lutemi ndiqni [këto udhëzime për të rikuperuar llogarinë tuaj](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Ju lutemi vini re se mund të duhet pak më tepër kohë për të marrë përgjigje në lidhje me kërkesat tuaja. Ruani këtë faqe në faqeshënuesit (Bookmarks) tuaj dhe kthehuni te kjo rrjedhë pune brenda disa ditësh.

A funksionoi procedura për rikthimin e faqes?

- [Po](#resolved_end)
- [Jo](#account_end)

### Fb-Page_end

Ne jemi vërtet të kënaqur që problemi juaj është zgjidhur. Ju lutemi lexoni këto rekomandime për t'ju ndihmuar të minimizoni mundësinë e humbjes së qasjes në faqen tuaj në të ardhmen:

- Aktivizoni 2FA për të gjithë administratorët në faqe.
- Caktoni role të administratorit vetëm për njerëzit që u besoni dhe që janë të përgjegjshëm.

### account_end

Nëse procedurat e sugjeruara në këtë rrjedhë pune nuk ju kanë ndihmuar të rikuperoni qasjen në llogarinë tuaj, mund t'i drejtoheni organizatave të mëposhtme për të kërkuar ndihmë të mëtejshme:

:[](organisations?services=account)

### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Ju lutemi lexoni këto rekomandime për t'ju ndihmuar të minimizoni mundësinë e humbjes së qasjes në llogaritë tuaja në të ardhmen.

- Gjithmonë është një këshillë e mirë ta aktivizoni autentifikimin me dy faktorë (2FA) për të gjitha llogaritë tuaja që e mbështesin atë.
- Asnjëherë mos e përdorni të njëjtën fjalëkalim për më shumë se një llogari. Nëse jeni duke e bërë këtë, duhet t'i ndryshoni ato, duke përdorur një fjalëkalim unik për secilën prej llogarive tuaja.
- Përdorimi i një menaxheri të fjalëkalimeve do t'ju ndihmojë të krijoni dhe të mbani mend fjalëkalime unike, të forta për të gjitha llogaritë tuaja.
- Jini të kujdesshëm kur përdorni rrjete Wi-Fi të pabesueshme publike, dhe nëse keni mundësi lidhuni përmes VPN ose Tor.

#### resources

- [Siguria në një kuti - Krijoni dhe mirëmbani fjalëkalime të forta](https://securityinabox.org/en/guide/passwords/)
- [Vetë-mbrojtje e sigurisë - Mbrojtja e vetvetes në rrjetet sociale](https://ssd.eff.org/en/module/protecting-yourself-social-networks)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service-name. Did you receive it?

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
