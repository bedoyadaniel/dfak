---
layout: page
title: Posta
author: mfc, Metamorphosis Foundation
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/postal-mail.md
parent: /sq/
published: true
---

Dërgimi i postës është një metodë e ngadaltë e komunikimit nëse përballeni me një situatë urgjente. Në varësi të juridiksionit ku udhëton posta, autoritetet mund të hapin postën, dhe ata shpesh përcjellin dërguesin, vendin e dërgesës, marrësin dhe vendin e destinacionit.
