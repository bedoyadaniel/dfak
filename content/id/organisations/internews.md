---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 jam
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Melengkapi pekerjaan inti Internews, Internews juga bekerja dengan individu, organisasi, dan komunitas di seluruh dunia untuk meningkatkan kesadaran keamanan digital, melindungi akses ke Internet yang terbuka dan tanpa sensor, dan meningkatkan praktik keamanan digital. Internews telah melatih jurnalis dan pembela hak asasi manusia di lebih dari 80 negara, dan memiliki jaringan yang kuat dari pelatih dan auditor keamanan digital lokal dan regional yang akrab dengan Kerangka Kerja Audit Keamanan dan Kerangka Evaluasi untuk Kelompok Advokasi (SAFETAG) ([https://safetag. org](https://safetag.org)), yang dikembangkan oleh Internews. Internews sedang membangun kemitraan yang kuat dan responsif dengan lembaga intelijen dan analisis ancaman masyarakat sipil dan sektor swasta, dan dapat mendukung mitra secara langsung dalam mempertahankan kehadiran online, aman, dan tanpa sensor. Internews menawarkan intervensi teknis dan non-teknis dari penilaian keamanan dasar menggunakan kerangka SAFETAG hingga penilaian kebijakan organisasi hingga strategi mitigasi yang direvisi berdasarkan penelitian ancaman. Mereka secara langsung mendukung analisis phishing dan malware untuk pembela HAM dan kelompok media yang sama-sama mengalami serangan digital yang ditargetkan.
