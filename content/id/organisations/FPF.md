---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Senin-Jumat, jam kerja, Zona Waktu Timur, USA
response_time: 1 hari
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) adalah organisasi nirlaba 501(c)3 yang melindungi, membela, dan memberdayakan jurnalisme kepentingan publik pada abad ke-21. FPF melatih semua orang mulai dari organisasi media besar hingga jurnalis independen tentang berbagai alat dan teknik keamanan serta privasi untuk melindungi dirinya, narasumber, dan organisasi mereka dengan lebih baik.
