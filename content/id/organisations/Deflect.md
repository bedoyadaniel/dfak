---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: Senin-Jumat, 24/5, UTC-4
response_time: 6 jam
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect adalah layanan gratis keamanan website demi mempertahankan kelompok-kelompok masyarakat sipil dan pembela hak asasi manusia dari serangan digital.
