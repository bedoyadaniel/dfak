---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Senin-Kamis, 9am-5pm EET/EEST
response_time: 2 hari
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine adalah organisasi nonpemerintah yang berbasis di Kyiv dan didirikan pada 2017 oleh empat lulusan program ISC Project's ToT. Misinya adalah untuk mendukung penerapan hak asasi manusia di internet dengan membangun kapasitas organisasi sipil dan media independen untuk mengatasi masalah-masalah keamanan digital serta memberi dampak terhadap kebijakan pemerintah dan korporasi dalam bidang hak-hak digital.
