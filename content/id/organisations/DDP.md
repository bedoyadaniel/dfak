---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Senin-Kamis, 9am-5pm CET
response_time: 4 hari
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership menawarkan dukungan bagi pembela hak asasi manusia yang mengalami ancaman digital, serta bekerja untuk menguatkan jaringan respon cepat. DDP mengelola dukungan darurat bagi individu dan organisasi seperti pembela HAM, jurnalis, aktivis masyarakat sipil, dan bloggers.

DDP memiliki lima tipe pendanaan bagi situasi darurat maupun pendanaan jangka panjang yang berfokus pada penguatan kapasitas organisasi. DDP juga menjalankan Digital Integrity Fellowship yang mendukung organisasi dalam bentuk pelatihan keamanan digital dan privasi yang sesuai kebutuhan, dan program jaringan Rapid Response.
