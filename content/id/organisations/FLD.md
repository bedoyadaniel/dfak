---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: darurat 24/7, global; kerja regular Senin-Jumat pada jam kerja, IST (UTC+1), staf berkolasi di berbagai zona waktu di berbagai area
response_time: di hari yang sama atau hari berikutnya untuk darurat
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders merupakan organisasi internasional berbasis di Irlandia yang bekerja untuk perlindungan terpadu bagi pembela hak asasi manusia yang berisiko. Front Line Defenders menyediakan dukungan cepat dan praktis bagi pembela HAM yang menghadapi risiko melalui dana keamanan, pelatihan keamanan fisik dan digital, advokasi dan kampanye.

Front Line Defenders menjalankan dukungan darurat berupa hotline 24/7 di +353-121-00489 untuk pembela HAM yang dalam risiko dengan bahasa Arab, Inggris, Perancis, Rusia, atau Spanyol. Ketika pembela HAM menghadapi ancaman hidup, Front Line Defenders dapat membantu dengan relokasi sementara. Front Line Defenders menyediakan pelatihan keamanan fisik dan digital. Front Line Defenders juga mempublikasikan kasus-kasus pembela HAM dalam risiko dan berkampanye serta advokasi di tingkat internasional termasuk Uni Eropa, PBB, mekanisme-mekanisme antarwilayah dan dengan pemerintah langsung untuk perlindungannya.
