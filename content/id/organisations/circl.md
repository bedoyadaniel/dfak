---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: hari kerja, UTC+2
response_time: 4 jam
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL merupakan CERT (computer emergency response team) atau tim respon cepat darurat komputer untuk sektor privat, komune, dan entitas nonpemerintah di Luxembourg.

CIRCL menyediakan titik kontak yang bisa diandalkan dan terpercaya untuk siapa pun, perusahaan dan organisasi yang berbasis di Luxembourgh, untuk menangani serangan dan insiden. Tim ahlinya bekerja seperti pemadam kebakaran dengan kemampuan untuk bereaksi segera dan efisien kapan pun ancaman dicurigai, terdeteksi atau insiden terjadi.

Tujuan CIRCL adalah untuk mengumpulkan, mengulas, melaporkan dan merespon ancaman siber dengan cara yang sistematis dan tepat.
