---
layout: sidebar.pug
title: "Seseorang yang saya kenal ditangkap"
author: Peter Steudtner, Shakeeb Al-Jabri
language: id
summary: "Seorang teman, kolega, atau anggota keluarga Anda ditangkap oleh aparat keamanan. Anda ingin membatasi dampak penangkapan tersebut terhadap mereka dan orang lain yang mungkin terlibat."
date: 2023-04
permalink: /id/arrested/
parent: Home
sidebar: >
  <h3>Baca selengkapnya tentang apa yang harus dilakukan jika seseorang yang Anda kenal ditangkap:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspirasi dan panduan untuk tahanan beserta keluarga, pengacara, dan para pendukungnya</a></li>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Mendapatkan dukungan untuk kampanye atas nama tahanan</a></li>
    <li><a href="https://perpustakaan.bantuanhukum.or.id/index.php?p=show_detail&id=3531">Hak Tersangka di dalam KUHAP dari Lembaga Bantuan Hukum Jakarta</a></li>
  </ul>

---

# Seseorang yang saya kenal ditahan

Penangkapan pembela HAM, jurnalis, dan aktivis menempatkan mereka dan siapapun yang bekerja dan tinggal dengan mereka dalam risiko tinggi.
Panduan ini ditujukan khususnya untuk negara-negara yang belum mumpuni dalam hal hak asasi manusia serta proses hukumnya, atau di mana pihak berwenang dapat menghindari proses hukum, atau badan non-pemerintahan dapat beroperasi dengan bebas, penahanan atau penangkapan menyebabkan risiko yang lebih besar bagi korban serta kolega dan kerabat mereka.
Dalam panduan ini, kami bertujuan untuk meringankan bahaya yang mereka hadapi dan membatasi akses pihak penahan ke data sensitif yang dapat memberatkan korban dan kolega mereka, atau dapat digunakan untuk membahayakan hal lainnya.
Mengurus tahanan serta dampak digital dari penahanan tersebut bisa jadi melelahkan dan menantang. Cobalah untuk mencari pihak lain untuk membantu Anda dan mengkoordinasikan tindakan Anda dengan komunitas yang terlibat.
Penting pula bagi Anda untuk menjaga diri Anda sendiri dan orang lain yang terdampak oleh penangkapan ini, dengan cara:

- [menjaga kesehatan dan kebutuhan psikososial Anda,](../self-care),
<!-- - [taking care of the legal side of your support work](***link to RaReNet section on organizations helping on legal matters***) -->
- [mencari bantuan untuk kampanye atas nama pihak yang ditahan](https://www.newtactics.org/search/solr/arrest)

Jika Anda merasa kewalahan secara teknis atau secara emosional, atau (untuk alasan apapun) tidak berada di posisi di mana Anda dapat mengikuti langkah-langkah yang diuraikan di bawah ini, harap carilah bantuan dan bimbingan ke organisasi yang tercantum di sini. Mereka menawarkan analisis tahap awal sebagai salah satu layanan mereka.


## Membuat rencana

Sebelum Anda menindaklanjuti bagian lain yang kami uraikan di bawah ini, harap perhatikan langkah-langkah berikut:

- Usahakan untuk membaca panduan ini sampai selesai supaya mendapatkan gambaran umum dari semua area penting yang terdampak, sebelum Anda mulai mengambil tindakan atas aspek-aspek khusus. Alasannya karena tiap-tiap bagian menyoroti skenario ancaman yang berbeda yang bisa jadi saling tumpang tindih, sehingga Anda perlu menyusun sendiri urutan tindakan Anda.
- Luangkan waktu bersama rekan-rekan atau tim untuk melakukan beragam penilaian risiko yang diperlukan di bagian-bagian yang berbeda.

<a name="harm-reduction"></a>
## Bersiap sebelum bertindak

Apakah Anda memiliki alasan untuk percaya bahwa penangkapan atau penahanan ini dapat mengakibatkan dampak lanjutan pada anggota keluarga, rekan, atau kolega dari tahanan, termasuk Anda sendiri?

Panduan ini akan membimbing Anda melalui serangkaian langkah untuk membantu menyediakan solusi yang dapat menolong mengurangi eksposur terhadap tahanan dan siapapun yang terlibat dengan mereka.

**Tips respon yang terkoordinir**:

Dalam segala situasi dimana seseorang ditahan, hal pertama yang harus diingat adalah seringkali saat insiden terjadi, sejumlah rekan atau kolega akan bereaksi di saat yang bersamaan, yang mengakibatkan upaya ganda atau bertolak belakang. Maka dari itu, penting untuk diingat bahwa tindakan yang terkoordinir dan berdasarkan mufakat, pada tataran lokal dan internasional, diperlukan untuk mendukung tahanan dan menjaga semua orang yang termasuk dalam jaringan pendukung, keluarga, dan rekan mereka.

- Bentuk tim krisis yang akan mengkoordinasi semua aktivitas dukungan, perawatan, kampanye, dll.
- Libatkan anggota keluarga, pasangan, dll. sebanyak mungkin (hormati batasan mereka, jika misalnya mereka kewalahan).
- Tetapkan tujuan yang jelas untuk kampanye dukungan Anda (dan tinjau sesering mungkin): misalnya, Anda ingin membebaskan tahanan, memastikan kesejahteraan mereka, atau melindungi keluarga dan para pendukung serta memastikan kesejahteraan mereka sebagai tujuan Anda yang paling mendesak.
- Sepakati saluran dan frekuensi komunikasi yang aman, beserta batasan-batasannya (misalnya dilarang berkomunikasi antara jam 10 malam hingga 8 pagi kecuali untuk situasi darurat atau berita terbaru).
- Bagikan tugas di antara anggota tim dan hubungi pihak ketiga untuk bantuan (analisis, advokasi, kerja media, dokumentasi, dll.)
- Minta bantuan lebih lanjut di luar “tim krisis” untuk memenuhi kebutuhan dasar (misalnya asupan makanan rutin, dll.)


**Tips pencegahan keamanan digital**

Jika Anda memiliki alasan untuk mengkhawatirkan akibat buruk untuk diri Anda sendiri atau pendukung lain, sebelum menangani situasi darurat digital yang berkaitan dengan penahanan rekan Anda, penting pula bagi Anda untuk mengambil langkah-langkah pencegahan pada tingkat keamanan digital untuk melindungi diri Anda sendiri dan orang lain dari bahaya.

- Sepakati kanal komunikasi (yang aman) manakah yang akan digunakan jaringan pendukung Anda untuk mengkoordinasikan penanggulangan dan berkomunikasi tentang tahanan dan akibat lebih lanjut.
- Kurangi data pada perangkat Anda seminimal mungkin, dan lindungi data di perangkat Anda dengan  [enkripsi](https://ssd.eff.org/en/module/keeping-your-data-safe#1).
- Buat [cadangan semua data Anda yang aman dan terenkripsi](https://communitydocs.accessnow.org/182-Secure_Backup.html), dan simpan di tempat yang tidak akan ditemukan saat pencarian atau penahanan lebih lanjut.
- Bagi kata sandi untuk perangkat-perangkat, akun-akun daring, dll dengan orang yang terpercaya yang tidak berada dalam bahaya langsung.
- Sepakati langkah yang akan diambil (seperti penangguhan akun, penghapusan perangkat dari jarak jauh, dll.) sebagai reaksi pertama dari kemungkinan penahanan Anda.

## Penilaian risiko
### Kurangi kemungkinan kerugian yang disebabkan oleh tindakan kita sendiri

Secara umum, Anda harus mendasarkan tindakan Anda pada pertanyaan berikut:

- Apa dampak dari setiap maupun seluruh tindakan terhadap tahanan, dan juga terhadap komunitas, sesama aktivis, rekan, keluarga mereka, dll, termasuk Anda sendiri?

Masing-masing bagian berikut ini akan menguraikan aspek-aspek khusus dari penilaian risiko ini.

Pertimbangan dasarnya adalah:

- Sebelum menghapus akun, data, utas media sosial, dll., pastikan Anda telah mendokumentasikan konten dan informasi yang Anda hapus, terutama jika Anda perlu memulihkan konten dan informasi tersebut nantinya, atau membutuhkannya untuk bukti di lain waktu.
- Jika Anda menghapus akun atau file, ketahuilah bahwa:
    - Pihak berwenang dapat menafsirkan ini sebagai penghancuran atau penghapusan bukti.
    - Hal ini dapat membuat situasi tahanan semakin sulit, jika mereka memberi akses ke akun atau file tersebut, dan pihak berwenang tidak bisa menemukannya, karena tahanan akan tampak tidak bisa dipercaya dan dapat menyebabkan tindakan yang merugikan tahanan akibat adanya penghapusan tersebut.
- Jika Anda memberi tahu orang-orang bahwa informasi pribadi mereka yang disimpan di sebuah perangkat atau akun yang disita oleh pihak berwenang, dan komunikasi tersebut disadap, maka ini dapat digunakan sebagai bukti tambahan tentang kaitan dengan tahanan.
- Perubahan dalam prosedur komunikasi (termasuk penghapusan akun, dll.) dapat memicu perhatian pihak berwenang.


### Menginformasikan kontak

Secara umum, tidaklah mungkin untuk menentukan apakah otoritas penahanan memiliki kapasitas untuk memetakan jaringan kontak tahanan, dan apakah mereka telah melakukannya atau tidak. Maka dari itu kita harus mengasumsikan kemungkinan terburuknya, yaitu bahwa mereka telah melakukannya atau akan melakukannya.

Sebelum menginformasikan kontak tahanan, harap perkirakan risikonya:

- Apakah Anda memiliki salinan daftar kontak milik tahanan? Dapatkah Anda mengecek siapa saja yang ada dalam daftar kontak mereka, baik itu di perangkat, akun surel, dan platform media sosial mereka? Kumpulkan daftar kontak yang memungkinkan untuk mendapatkan gambaran umum dari siapa yang mungkin terdampak.
- Apakah ada risiko bahwa menginformasikan orang-orang dalam daftar kontak dapat mengaitkan mereka lebih dekat kepada tahanan dan hal ini dapat di(salah)gunakan oleh otoritas penahanan untuk melawan mereka?
- Haruskah semua orang diberitahu atau hanya sekelompok orang tertentu yang ada di dalam kontak?
- Siapa yang akan menginformasikan kontak yang mana? Siapa yang sudah berhubungan dengan siapa? Apa dampak dari keputusan tersebut?
- Bentuk kanal komunikasi yang paling aman, termasuk pertemuan pribadi di ruang yang tidak memiliki pengawasan CCTV, untuk menginformasikan kontak yang terlibat.

### Dokumen untuk Menyimpan Bukti

Sebelum Anda menghapus konten apapun dari situs web, situs media sosial, utas, dll., Anda perlu memastikan bahwa Anda telah mendokumentasikannya sebelumnya. Alasan mengapa Anda perlu mendokumentasikannya adalah untuk menyimpan tanda atau bukti dari akun yang disalahgunakan - seperti konten tambahan atau mengandung peniruan identitas - atau konten yang Anda butuhkan sebagai bukti hukum.

Tergantung dari situs web atau platform media sosial yang ingin Anda dokumentasikan feed atau data daringnya, pendekatan yang dipakai dapat berbeda:

- Anda dapat mengambil gambar tangkapan layar (screenshot) dari bagian yang relevan (pastikan cap waktu, alamat URL, dll. disertakan dalam screenshotnya).
- Anda dapat mengecek bahwa situs web atau blog yang relevan diindeks di [Wayback Machine](https://archive.org/web), atau unduh situs web atau blog ke perangkat lokal Anda.

*Ingatlah pentingnya menyimpan informasi yang telah Anda unduh di perangkat yang aman yang disimpan di tempat yang juga aman.*

### Device Seizure

Jika ada perangkat milik tahanan yang disita selama atau setelah penangkapan, harap baca panduan [Saya kehilangan perangkat saya](../topics/lost-device), khususnya bagian [penghapusan jarak jauh](../topics/lost-device/questions/find-erase-device), termasuk rekomendasi jika ada kasus penahanan.  


### Data dan Akun Daring yang Memberatkan

Jika tahanan memiliki informasi dalam perangkat mereka yang mungkin dapat membahayakan mereka atau orang lain, ada baiknya untuk mencoba membatasi akses yang dimiliki pihak penahan pada informasi ini.

Sebelum melakukannya, bandingkan risiko yang ditimbulkan oleh informasi ini dengan risiko yang ditimbulkan oleh kemarahan aparat keamanan akibat tidak adanya akses ke informasi ini (atau pengambilan tindakan hukum karena penghancuran bukti). Jika risiko yang ditimbulkan oleh informasi ini lebih tinggi, Anda dapat meneruskan dengan menghapus data yang relevan dan/atau menutup/menangguhkan dan menghapus tautan akun dengan mengikuti instruksi berikut ini.

#### Suspend or Close Online Accounts

Jika Anda memiliki akses ke akun yang ingin Anda tutup, Anda dapat mengikuti prosesnya untuk akun-akun lainnya. Harap pastikan bahwa Anda memiliki cadangan atau salinan dari konten dan data yang dihapus! Ketahuilah bahwa setelah menutup akun, kontennya tidak akan langsung tidak dapat diakses: misalnya di Facebook, perlu dua minggu untuk menghapus konten dari semua server.

Jika Anda tidak mempunyai akses ke akun tahanan atau Anda butuh tindakan yang lebih mendesak di akun media sosial, harap cari bantuan dari organisasi yang terdaftar [di sini](../support), yang menawarkan keamanan akun sebagai salah satu layanan mereka.

#### Delink Accounts from Devices

Kadang Anda mungkin ingin menghapus akun-akun yang tertaut dengan perangkat tertentu, karena tautan tersebut dapat memberi akses ke data sensitif kepada siapapun yang mengendalikan perangkat tersebut. Untuk melakukannya, Anda dapat mengikuti instruksi di alur kerja “Saya kehilangan perangkat saya”
 [instruksi di alur kerja “Saya kehilangan perangkat saya”](../topics/lost-device/questions/accounts).

 Jangan lupa untuk menghapus tautan [akun bank online](#online_bank_accounts) dari perangkat-perangkat tersebut.


#### Ubah Kata Sandi

Jika Anda memutuskan untuk tidak menutup atau menangguhkan akun, ada baiknya untuk tetap mengubah kata sandinya, dengan mengikuti [instruksi di alur kerja “Saya kehilangan perangkat saya”](../topics/lost-device/questions/passwords).

Pertimbangkan pula untuk mengaktifkan fitur verifikasi 2 langkah guna meningkatkan keamanan akun tahanan, dengan mengikuti [instruksi di alur kerja “Saya kehilangan perangkat saya”](../topics/lost-device/questions/2fa).

Jika kata sandi yang sama digunakan untuk sejumlah akun yang berbeda, Anda harus mengubah semua kata sandi yang terpengaruh pada akun tersebut karena akun-akun tersebut bisa jadi telah disusupi juga.

**Tips lain mengubah kata sandi**

- Gunakan aplikasi pengelola kata sandi (misalnya [KeepassXC](https://keepassxc.org)) untuk mendokumentasikan kata sandi yang telah diubah untuk penggunaan lebih lanjut atau untuk diserahkan ke tahanan setelah pembebasan.
- Pastikan untuk memberitahu tahanan, selambat-lambatnya setelah pembebasan mereka, mengenai perubahan kata sandi ini dan kembalikan kepemilikan mereka atas akun-akun tersebut.


#### Hapus Keanggotaan Kelompok dan Berhenti Membagikan Folder

Jika tahanan merupakan anggota dari grup (misalnya, tetapi tidak terbatas pada) grup Facebook, grup obrolan WhatsApp, Signal, atau Wire, atau dapat mengakses folder daring bersama, dan keberadaan mereka di grup-grup tersebut memberikan penahan mereka akses ke informasi istimewa dan berpotensi bahaya, Anda mungkin ingin menghapus mereka dari grup atau ruang daring bersama.

**Cara menghapus keanggotaan grup di berbagai aplikasi pesan singkat:**

- [WhatsApp](https://faq.whatsapp.com/841426356990637/?cms_platform=web&cms_id=841426356990637&draft=false)
- Telegram
    - Di iOS: Orang yang membuat grup dapat menghapus anggota dengan memilih “Info Grup” dan geser ke kiri pengguna yang ingin dihapus.
    - Di Android: Ketuk ikon pensil. Pilih “Anggota” - ketuk tiga titik di sebelah nama anggota yang ingin Anda hapus, dan pilih “Hapus dari grup”.
- Wire
    - [instruksi untuk seluler](https://support.wire.com/hc/en-us/articles/203526410)
    - [instruksi untuk aplikasi desktop](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- [Signal](https://support.signal.org/hc/en-us/articles/360050427692-Manage-a-group#remove)

**Cara berhenti membagikan folder di berbagai layanan daring:**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- [Google Drive](https://support.google.com/docs/answer/2494893?hl=id&co=GENIE.Platform%3DAndroid)
- [Dropbox](https://help.dropbox.com/id-id/share/unshare-folder)
- [iCloud](https://support.apple.com/en-us/HT201081)


### Menghapus Feed

Dalam beberapa kasus, Anda mungkin ingin menghapus konten dari linimasa media sosial tahanan, atau feed lain yang terhubung dengan akun mereka. Hal-hal tersebut dapat disalahgunakan sebagai bukti untuk melawan mereka atau menciptakan kebingungan dan konflik di dalam komunitas di mana tahanan tergabung maupun untuk mendiskreditkan mereka.

Beberapa layanan mempermudah penghapusan feed dan postingan dari akun dan linimasa. Panduan untuk Twitter, Facebook, dan Instagram tertaut di bawah ini. Harap pastikan bahwa Anda telah mendokumentasikan konten yang ingin Anda hapus, siapa tahu Anda masih membutuhkannya sebagai bukti jika terjadi gangguan dll.

- Untuk Twitter, Anda bisa menggunakan [Tweet Deleter](https://tweetdeleter.com/).
- Untuk Facebook, Anda bisa mengikuti [panduan Louis Barclay “Cara menghapus Umpan Berita Facebook Anda”](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), berdasarkan aplikasi untuk Browser Chrome, [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Untuk Instagram, Anda dapat mengikuti [instruksi di laman Instagram tentang mengedit dan menghapus postingan](https://help.instagram.com/997924900322403) (ingat bahwa orang yang memiliki akses ke pengaturan akun Anda akan dapat melihat sebagian besar riwayat interaksi Anda, bahkan setelah menghapus konten).


### Menghapus Asosiasi Daring yang Berbahaya

Jika ada informasi daring apapun yang menyebut nama tahanan, yang mungkin dapat berdampak negatif bagi mereka atau kontak mereka, sebaiknya hapus informasi tersebut. Lakukan ini hanya jika tidak merugikan tahanan lebih lanjut.

- Buat daftar ruang dan informasi daring yang perlu dihapus atau diubah.
- Jika Anda telah mengidentifikasi konten yang akan dihapus atau diubah, sebaiknya Anda membuat cadangannya sebelum memproses penghapusannya atau mengajukan permintaan penghapusan.
- Perkirakan jika penghapusan nama tahanan dapat berdampak negatif pada situasi mereka (misalnya, menghapus nama mereka dari daftar staf suatu organisasi dapat melindungi organisasi tersebut, tetapi hal itu juga dapat menghapus justifikasi bagi tahanan, contohnya bahwa mereka bekerja di organisasi tersebut).
- Jika Anda memiliki akses ke situs atau akun terkait, ubah atau hapus konten dan informasi sensitif.
- Jika Anda tidak mempunyai aksesnya, mintalah pada orang yang memiliki akses untuk menghapus informasi sensitif tersebut.
- Temukan instruksi untuk menghapus konten pada layanan Google [di sini](https://developers.google.com/search/docs/crawling-indexing/remove-information?hl=id)
- Cek apakah situs web yang berisi informasi tersebut telah diindeks di Wayback Machine atau Google Cache. Jika iya, konten tersebut juga harus dihapus.

<a name="online_bank_accounts"></a>
### Online Bank Accounts

Seringkali, rekening bank dikelola dan diakses secara daring, dan verifikasi melalui perangkat seluler menjadi penting untuk bertransaksi atau bahkan untuk sekedar mengakses rekening daring tersebut. Jika tahanan tidak mengakses rekening bank mereka untuk jangka waktu lama, hal ini dapat menyebabkan implikasi pada situasi keuangan tahanan dan kemampuan mereka untuk mengakses rekening tersebut. Dalam kasus ini, pastikan untuk:

- Hapus tautan perangkat yang disita dari rekening bank milik tahanan.
- Dapatkan otorisasi dan surat kuasa dari tahanan untuk dapat mengoperasikan rekening bank mereka atas nama mereka di awal proses penahanan (dengan persetujuan dari kerabat mereka).


## Tips Penutup

- Pastikan bahwa Anda mengembalikan seluruh kepemilikan data kembali ke tahanan setelah pembebasan.
- Baca [tips di alur kerja “Saya telah kehilangan perangkat saya”](../topics/lost-device/questions/device-returned) tentang cara menangani perangkat yang disita setelah dikembalikan oleh pihak berwenang.
