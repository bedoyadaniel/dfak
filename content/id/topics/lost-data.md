---
layout: page
title: "Saya Kehilangan Data Saya"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: id
summary: "Apa yang harus dilakukan jika Anda kehilangan sejumlah data"
date: 2023-04
permalink: /id/topics/lost-data
parent: Home
---

# Saya Kehilangan Data Saya

Data digital bisa bersifat sangat sementara dan tidak stabil, serta banyak cara Anda bisa kehilangannya. Kerusakan fisik perangkat, penghentian akun, kekeliruan penghapusan, pembaruan perangkat lunak, dan kerusakan perangkat lunak dapat menyebabkan hilangnya data Anda. Di samping itu, kadang Anda mungkin tidak mengetahui bagaimana sistem pencadangan (backup) bekerja dan apakah sistemnya bekerja seperti seharusnya, atau Anda hanya lupa kredensial atau rute untuk mencari atau memulihkan data Anda.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis bagaimana Anda bisa kehilangan data serta kemungkinan strategi penanggulangan untuk memulihkannya.

Berikut adalah kuesioner untuk mengidentifikasi masalah Anda dan menemukan solusi yang cocok.

## Workflow

### entry-point

> Di bagian ini kita akan lebih berfokus pada data berbasis perangkat. Untuk konten dan kredensial daring, kami akan mengarahkan Anda ke bagian lain dari Pertolongan Pertama Darurat Digital.
>
> Perangkat yang dimaksud di sini meliputi komputer, perangkat seluler, hard disk eksternal, stik USB, dan kartu memori.

Anda kehilangan jenis data apa?

- [Konten daring](../../../account-access-issues)
- [Kredensial](../../../account-access-issues)
- [Konten di perangkat](#content-on-device)

### content-on-device

> Seringkali data Anda yang hilang adalah data yang telah terhapus - baik disengaja ataupun tidak. Di komputer Anda, periksalah Recycle Bin atau Trash. Di perangkat Android, Anda mungkin bisa menemukan data yang hilang di direktori LOST.DIR. Jika file yang hilang tidak bisa ditemukan di Recycle Bin atau Trash sistem operasi Anda, mungkin data tersebut belum terhapus, tapi ada di lokasi yang berbeda dari yang Anda kira. Cobalah fungsi pencarian di sistem operasi Anda untuk menemukannya.
>
> Periksa juga file dan folder tersembunyi karena data yang hilang mungkin ada di sana. Begini cara melakukannya di [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) dan [Windows](https://support.microsoft.com/en-ie/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). Untuk Linux, buka direktori beranda Anda di pengelola file dan tekan Ctrl + H. Anda juga dapat melihat file tersembunyi di [Android](https://www.technologyhint.com/show-hidden-files/) dan iOS.
>
> Coba cari file Anda yang hilang dengan nama yang sama persis. Jika tidak berhasil, atau jika Anda tidak yakin apa nama persisnya, Anda dapat mencoba pencarian wildcard (yaitu jika Anda kehilangan file .docx, tetapi tidak yakin dengan namanya, Anda bisa mencari *.docx) yang hanya akan menunjukkan hasil pencarian file dengan ekstensi .docx. Urutkan berdasarkan Tanggal Modifikasi guna menemukan file terbaru dengan cepat saat menggunakan opsi pencarian wildcard.
>
> Selain mencari data yang hilang di perangkat Anda, tanyakan pada diri Anda sendiri apakah Anda pernah mengirimkannya melalui email atau membaginya dengan seseorang (termasuk Anda sendiri) atau menambahkannya ke penyimpanan cloud Anda suatu waktu. Jika demikian, Anda dapat melakukan pencarian di sana dan mungkin bisa mendapatkannya kembali, atau beberapa versinya.
>
> Untuk meningkatkan peluang Anda memulihkan data, segera hentikan pengeditan dan penambahan informasi ke perangkat Anda. Jika Anda bisa, sambungkan drive sebagai perangkat eksternal (misalnya melalui USB) ke komputer aman yang terpisah, untuk mencari dan memulihkan informasi. Menulis ke drive (seperti mengunduh atau membuat dokumen baru) dapat menurunkan kemungkinan menemukan dan memulihkan data yang hilang.

Bagaimana Anda kehilangan data Anda?

- [Perangkat penyimpanan data mengalami kerusakan fisik](#tech-assistance_end)
- [Perangkat penyimpanan data dicuri/hilang](#device-lost-theft_end)
- [Datanya terhapus](#where-is-data)
- [Datanya menghilang setelah pembaruan perangkat lunak](#software-update)
- [Sistem atau alat perangkat lunak rusak dan datanya hilang](#where-is-data)


### software-update

> Terkadang, saat Anda memperbarui atau memutakhirkan perangkat lunak atau keseluruhan sistem operasi, masalah tak terduga dapat terjadi, menyebabkan sistem atau perangkat lunak berhenti bekerja sebagaimana mestinya, atau berbagai perilaku yang tidak seharusnya terjadi, termasuk kerusakan data. Jika Anda kehilangan data tepat setelah pembaruan perangkat lunak atau sistem, ada baiknya mempertimbangkan untuk melakukan roll back untuk kembali ke keadaan sebelumnya. Roll back berguna karena ini berarti perangkat lunak atau basis data Anda dapat dipulihkan ke keadaan yang bersih dan konsisten bahkan setelah terjadi kekeliruan pengerjaan atau kerusakan perangkat lunak.
>
> Metode untuk melakukan roll back pada perangkat lunak ke versi sebelumnya tergantung pada bagaimana perangkat lunak tersebut dibangun - pada beberapa kasus hal ini bisa dilakukan dengan mudah, di kasus lain tidak, dan pada beberapa kasus dibutuhkan usaha yang cukup. Jadi Anda perlu melakukan pencarian daring mengenai roll back ke versi yang lebih lama untuk perangkat lunak tersebut. Harap diingat bahwa melakukan rollback juga dikenal sebagai “menurunkan versi”, jadi lakukan pencarian dengan istilah ini juga. Untuk sistem operasi seperti Windows atau Mac, masing-masing memiliki metodenya sendiri untuk menurunkan versi ke versi sebelumnya.
>
> - Untuk sistem Mac, kunjungi [Mac Support Center knowledge base](https://support.apple.com) dan gunakan kata kunci “menurunkan” dengan versi macOS Anda untuk membaca lebih banyak dan menemukan petunjuknya.
> - Untuk sistem Windows, prosedurnya beragam, tergantung apakah Anda ingin mengulang pembaruan tertentu saja atau rollback keseluruhan sistem. Di kedua kasus, Anda dapat menemukan petunjuk di [Microsoft Support Center](https://support.microsoft.com), misalnya untuk Windows 10 Anda dapat mencari pertanyaan “Bagaimana cara menghapus pembaruan yang sudah diinstal” di [laman FAQ](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

Apakah rollback perangkat lunaknya bermanfaat?

- [Ya](#resolved_end)
- [Tidak](#where-is-data)


### where-is-data

> Pencadangan data (backup) secara berkala adalah kebiasaan yang baik dan disarankan. Kadang kita lupa jika kita telah mengaktifkan pencadangan otomatis atau belum, jadi sebaiknya periksalah perangkat Anda untuk melihat apakah opsi tersebut telah diaktifkan, dan gunakan cadangan data tersebut untuk memulihkan data Anda. Jika Anda belum mengaktifkannya, disarankan untuk merencanakan pencadangan data di masa yang akan datang, dan Anda dapat menemukan lebih banyak tips tentang cara menyetel cadangan data di bagian tips penutup dari alur kerja ini.

Untuk memeriksa apakah Anda memiliki cadangan dari data yang hilang, tanyakan pada diri Anda sendiri di mana data yang hilang disimpan.

- [Di perangkat penyimpanan (hard disk eksternal, stik USB, kartu memori)](#storage-devices_end)
- [Di komputer](#computer)
- [Di perangkat seluler](#mobile)

### computer

Jenis sistem operasi apa yang Anda gunakan di komputer Anda?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Untuk memeriksa apakah perangkat MacOS Anda mengaktifkan opsi pencadangan, dan menggunakan cadangan data tersebut untuk memulihkan data Anda, periksa opsi [iCloud](https://support.apple.com/en-us/HT208682) atau [Time Machine](https://support.apple.com/en-za/HT201250) untuk melihat apakah ada cadangan data yang tersedia.
>
> Salah satu tempat yang perlu diperiksa adalah daftar Recent Items yang terus berjalan, yang mana hal ini melacak aplikasi, file, dan server yang telah Anda gunakan selama beberapa sesi terakhir di komputer. Untuk mencari file dan membukanya kembali, buka Apple Menu di pojok kiri atas, pilih Recent Items dan telusuri daftar file. Detail lebih lanjut dapat ditemukan di artikel [The New York Times, "Tech Tip: Find Recently Lost Files on Your Mac"](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-lost-files.html).

Apakah Anda dapat menemukan data Anda atau memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#macos-restore)

### macos-restore

> Di beberapa kasus ada alat gratis dan open source yang dapat membantu menemukan konten yang hilang dan memulihkannya. Terkadang penggunaannya terbatas, dan sebagian besar alat yang terkenal berbayar.
>
> Sebagai contoh, Anda bisa mencoba [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) atau [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### windows-computer

> Untuk memeriksa apakah Anda memiliki cadangan data yang aktif di perangkat Windows Anda, lihat [informasi dukungan Microsoft: Pencadangan dan Pemulihan di Windows 10](https://support.microsoft.com/id-id/windows/pencadangan-dan-pemulihan-di-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows 10 menyertakan fitur Timeline yang dimaksudkan untuk meningkatkan produktivitas Anda dengan menyimpan catatan file yang Anda gunakan, situs yang Anda telusuri, dan tindakan lain yang Anda lakukan di komputer Anda. Jika Anda tidak bisa mengingat di mana Anda menyimpan suatu dokumen, Anda dapat mengklik ikon Timeline di taskbar Windows 10 untuk melihat log visual yang diatur berdasarkan tanggal, dan Anda dapat kembali ke apa yang Anda butuhkan dengan mengklik ikon pratinjau yang sesuai. Hal ini  membantu Anda menemukan file yang telah diganti namanya. Jika fitur Timeline diaktifkan, beberapa aktivitas komputer Anda — seperti file yang Anda sunting di Microsoft Office — juga dapat disinkronkan dengan perangkat seluler Anda atau komputer lain yang Anda gunakan, sehingga Anda mungkin memiliki cadangan dari data Anda yang hilang di perangkat lainnya. Baca selengkapnya tentang fitur Timeline dan cara menggunakannya di artikel [The New York Times, "Tech Tip: How to Use Timeline With Windows 10"](https://www.nytimes.com/2018/07/05/technology/personaltech/windows-10-timeline.html).

Apakah Anda bisa menemukan data Anda atau memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#windows-restore)

### windows-restore

> Di beberapa kasus ada perangkat gratis dan open source yang dapat membantu menemukan konten yang hilang dan memulihkannya. Terkadang penggunaannya terbatas dan sebagian besar alat yang terkenal berbayar.
>
> Sebagai contoh, Anda bisa mencoba [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (lihat [langkah-langkahnya](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/data-recovery-software.html), atau [Recuva](http://www.ccleaner.com/recuva).

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)


### linux-computer

> Beberapa rilisan Linux yang paling populer seperti Ubuntu memiliki perangkat pencadangan (backup) bawaan, misalnya [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) di Ubuntu. Jika ada pencadangan bawaan yang disertakan di sistem operasi yang Anda gunakan, Anda mungkin telah diminta untuk mengaktifkan pencadangan otomatis saat Anda pertama kali mulai menggunakan komputer Anda. Cari distribusi Linux Anda untuk melihat apakah alat pencadangan bawaan disertakan di dalamnya, dan seperti apa prosedur untuk memeriksa apakah alat tersebut sudah aktif serta bagaimana cara memulihkan data dari cadangan tersebut.
>
> Bacalah [Ubuntu & Deja Dup - Get up, backup](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) untuk memeriksa apakah Anda sudah mengaktifkan pencadangan otomatis di komputer Anda. Baca ["Full System Restore with Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) untuk petunjuk memulihkan data Anda yang hilang dari cadangan yang ada.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### mobile

Sistem operasi apa yang Anda gunakan di ponsel Anda?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Anda mungkin telah mengaktifkan pencadangan otomatis dengan iCloud atau iTunes. Bacalah ["Restore or set up your device from an iCloud backup"](https://support.apple.com/kb/ph12521?locale=id) untuk memeriksa apakah Anda memiliki cadangan yang tersedia dan mempelajari bagaimana caranya memulihkan data Anda.

Apakah Anda telah menemukan cadangan data Anda dan memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#phone-which-data)

###  phone-which-data

Mana yang berlaku untuk kehilangan data Anda?

- [Data yang dihasilkan oleh aplikasi, misalnya kontak, feed, dll.](#app-data-phone)
- [Data yang dihasilkan oleh pengguna, misalnya foto, video, audio, catatan](#ugd-phone)

### app-data-phone

> Aplikasi seluler adalah aplikasi perangkat lunak yang didesain untuk dijalankan di perangkat seluler. Namun, sebagian besar aplikasi tersebut dapat pula diakses melalui browser di desktop. Jika Anda pernah mengalami kehilangan data yang dihasilkan aplikasi di telepon seluler Anda, coba akses aplikasi tersebut di browser desktop Anda, dengan cara masuk ke antarmuka situs web aplikasi tersebut dengan menggunakan kredensial Anda untuk aplikasi tersebut. Anda mungkin menemukan data yang hilang di antarmuka browser tersebut.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### ugd-phone

> Data yang dibuat pengguna adalah jenis data yang Anda buat atau hasilkan melalui aplikasi tertentu. Jika terjadi kehilangan data maka Anda perlu memeriksa apakah aplikasi tersebut memiliki pengaturan cadangan data yang diaktifkan secara default atau memungkinkan cara untuk memulihkannya. Misalnya, jika Anda menggunakan WhatsApp di ponsel Anda dan percakapannya menghilang atau terjadi suatu kesalahan, Anda dapat memulihkan percakapan Anda jika Anda telah mengaktifkan pengaturan pemulihan WhatsApp, atau jika Anda menggunakan suatu aplikasi untuk membuat dan menyimpan catatan dengan informasi sensitif atau bersifat pribadi, kadang mungkin aplikasi tersebut juga telah mengaktifkan opsi cadangan datanya tanpa Anda sadari.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### android-phone

> Google memiliki layanan bawaan yang disertakan dalam Android, yaitu Android Backup Service. Secara default, layanan ini membuat cadangan sejumlah tipe data dan mengasosiasikannya dengan layanan Google yang sesuai, di mana Anda juga bisa mengaksesnya melalui web. Di sebagian besar perangkat Android, Anda dapat melihat pengaturan Sinkronisasi dengan membuka Pengaturan > Akun > Google, kemudian pilih alamat Gmail Anda. Jika Anda kehilangan data dari jenis yang Anda sinkronisasikan dengan akun Google Anda, Anda mungkin dapat memulihkannya dengan masuk ke akun Google Anda melalui browser web.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#phone-which-data)


### tech-assistance_end

> Jika kehilangan data Anda disebabkan oleh kerusakan fisik seperti perangkat yang jatuh ke lantai atau air, terkena pemadaman listrik atau masalah lainnya, situasi yang paling memungkinkan adalah Anda harus melakukan pemulihan data yang tersimpan di perangkat keras Anda. Jika Anda tidak tahu bagaimana cara melakukannya, Anda perlu menghubungi teknisi IT yang menguasai pemeliharaan perangkat keras dan peralatan elektronik yang dapat membantu Anda. Namun, tergantung konteks Anda dan sensitivitas data yang perlu Anda pulihkan, menghubungi sembarang toko IT tidaklah disarankan, lebih baik hubungi teknisi IT yang Anda kenal dan percaya.


### device-lost-theft_end

> Jika perangkat Anda hilang, dicuri, atau disita, pastikan untuk mengganti semua kata sandi Anda sesegera mungkin dan kunjungi sumber informasi khusus kami untuk mengetahui apa yang harus dilakukan jika perangkat Anda hilang.
>
> Jika Anda anggota masyarakat sipil, dan Anda membutuhkan bantuan untuk mendapatkan perangkat baru untuk menggantikan yang hilang, Anda dapat menghubungi organisasi yang terdaftar di bawah ini.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Untuk mendapatkan kembali data yang telah hilang, ingatlah bahwa waktu adalah faktor yang penting. Sebagai contoh, memulihkan file yang tidak sengaja Anda hapus beberapa jam yang lalu atau sehari sebelumnya kemungkinan memiliki tingkat keberhasilan yang lebih tinggi dari file yang hilang sebulan sebelumnya.
>
> Pertimbangkan untuk menggunakan perangkat lunak untuk memulihkan data yang Anda hilangkan (akibat terhapus atau kerusakan) seperti [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (untuk Windows, Mac, atau Linux - lihat [langkah-langkahnya](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (untuk Windows, Mac, Android, or iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (untuk Windows atau  Mac), atau [Recuva](http://www.ccleaner.com/recuva) (untuk Windows). Perlu diingat bahwa alat-alat tersebut tidak selalu berfungsi, karena sistem operasi Anda mungkin telah menindih (_overwrite_) data baru di atas informasi yang telah Anda hapus. Oleh karena itu, Anda harus melakukan sesedikit mungkin aktivitas di komputer jika file Anda terhapus dan sedang mencoba mendapatkannya kembali dengan perangkat di atas.

>
> Untuk meningkatkan peluang Anda memulihkan data, segera hentikan penggunaan perangkat Anda. Penindihan data terus-menerus pada hard drive dapat menurunkan peluang untuk menemukan dan mendapatkan kembali data tersebut.
>
> Jika opsi di atas tidak ada yang berhasil, hal yang paling memungkinkan adalah untuk melakukan pemulihan data yang disimpan di harddisk. Jika Anda tidak tahu bagaimana cara melakukannya, hubungi teknisi IT yang menguasai pemeliharaan perangkat keras dan peralatan elektronik yang dapat membantu Anda. Namun, tergantung konteks dan sensitivitas data yang perlu Anda pulihkan, menghubungi sembarang toko IT tidaklah disarankan, lebih baik hubungi teknisi IT yang Anda kenal dan percaya.


### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital(Digital First Aid Kit, DFAK) ini bermanfaat. Silahkan berikan masukan melalui [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### final_tips

- Cadangan - selalu lebih baik jika Anda memastikan memiliki cadangan data - banyak cadangan yang berbeda-beda, yang Anda simpan di tempat-tempat yang berbeda dengan data aslinya! Tergantung dari konteksnya, pilih untuk menyimpan cadangan data Anda di layanan cloud dan di perangkat fisik eksternal yang tidak Anda sambungkan ke komputer saat tersambung dengan internet.
- Untuk kedua tipe cadangan data, Anda harus melindungi data Anda dengan enkripsi. Lakukan pencadangan rutin dan bertahap untuk data Anda yang paling penting, periksa apakah Anda sudah menyiapkannya, dan uji apakah Anda dapat memulihkannya sebelum melakukan pembaruan perangkat lunak atau sistem operasi.
- Buat sistem folder yang terstruktur - Tiap orang memiliki caranya sendiri dalam mengorganisir data dan informasi penting, tidak ada satu solusi yang cocok untuk semuanya. Meski demikian, penting halnya untuk mempertimbangkan membuat sistem folder yang sesuai dengan kebutuhan Anda. Dengan membuat sistem folder yang konsisten, Anda membuat hidup Anda lebih mudah, misalnya dengan lebih mengenal folder dan file mana yang harus lebih sering dibuat cadangannya, di mana informasi penting yang sedang Anda kerjakan terletak, di mana Anda harus menyimpan data yang mengandung informasi pribadi dan sensitif tentang Anda dan kolaborator Anda, dan lain sebagainya. Seperti biasa, tarik napas panjang dan sediakan cukup waktu untuk merencanakan tipe data yang Anda produksi atau kelola, dan pikirkan sistem folder yang dapat membuatnya lebih konsisten dan rapi. 

#### Resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: Back-up Tactics](https://securityinabox.org/en/files/backup/)
- [Official page on Mac backups](https://support.apple.com/mac-backup)
- [How to back up data regularly on Windows 10](https://support.microsoft.com/en-hk/help/4027408/windows-10-backup-and-restore)
- [How to enable regular backups on iPhone](https://support.apple.com/en-us/HT203977)
- [How to enable regular backups on Android](https://support.google.com/android/answer/2819582?hl=en)
- [Securily backup your data](https://johnopdenakker.com/securily-backup-your-data/)
- [How to Back Up Your Digital Life](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Data recovery](https://en.wikipedia.org/wiki/Data_recovery)
