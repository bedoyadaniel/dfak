---
layout: page
title: "Saya tidak bisa mengakses akun saya"
author: RaReNet
language: id
summary: "Apakah Anda mengalami masalah dalam mengakses email, media sosial, atau akun web? Apakah salah satu akun Anda menunjukkan aktivitas yang tidak Anda kenali? Ada banyak hal yang dapat Anda lakukan untuk meringankan masalah ini."
date: 2020-11
permalink: /id/topics/account-access-issues/
parent: /id/
---

# Saya kehilangan akses ke akun saya

Media sosial dan akun komunikasi banyak digunakan oleh anggota masyarakat sipil untuk berkomunikasi, berbagi pengetahuan, dan mengadvokasi pergerakan mereka. Konsekuensinya, akun-akun tersebut menjadi sasaran empuk oknum-oknum berniat jahat, yang sering kali mencoba menyusupi akun-akun tersebut, yang menyebabkan kerugian bagi anggota masyarakat sipil dan kontak mereka.
Panduan ini hadir untuk membantu Anda jika Anda kehilangan akses ke salah satu akun Anda karena akun tersebut telah disusupi.

Berikut adalah kuesioner untuk mengidentifikasi sifat masalah Anda dan menemukan solusi yang memungkinkan.

## Workflow

### Password-Typo

> Kadang Anda mungkin tidak bisa masuk ke akun Anda karena Anda salah mengetik kata sandi, atau karena pengaturan bahasa di keyboard Anda bukanlah yang biasa Anda gunakan, atau CapsLock Anda menyala.

>
> Coba ketikkan nama pengguna dan kata sandi Anda di editor teks kemudian salinlah, lalu tempelkan pada formulir <i>login</i>.

Apakah saran di atas membantu Anda masuk ke akun Anda?

- [Ya](#resolved_end)
- [Tidak](#What-Type-of-Account-or-Service)

### What-Type-of-Account-or-Service

Tipe akun atau layanan apakah yang hilang aksesnya?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

Apakah halaman tersebut memiliki admin lain selain Anda?
- [Ya](#Other-admins-exist)
- [Tidak](#Facebook-Page-recovery-form)

### Other-admins-exist

Apakah admin lainnya mengalami masalah yang sama?


- [Yes](#Facebook-Page-recovery-form)
- [No](#Other-admin-can-help)

### Other-admin-can-help

> Silakan minta admin lainnya untuk menambahkan Anda sebagai admin halaman lagi.

Apakah ini menyelesaikan masalahnya?

- [Ya](#Fb-Page_end)
- [Tidak](#account_end)

### Facebook-Page-recovery-form

> Silakan masuk ke Facebook dan gunakan [formulir Facebook untuk memulihkan halaman](https://www.facebook.com/help/contact/164405897002583).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-google)
- [Tidak](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Periksa apakah Anda menerima email atau SMS "Peringatan keamanan penting untuk Akun Google Anda yang tertaut" dari Google. Apakah Anda sudah menerimanya?
- [Ya](#Email-received-google)
- [Tidak](#Recovery-Form-google)

### Email-received-google

Mohon periksa apakah ada tautan “pulihkan akun Anda”. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-google)
- [Tidak](#Recovery-Form-google)

### Recovery-Link-Found-google

> Silakan gunakan tautan “pulihkan akun Anda” tersebut untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-google)

### Recovery-Form-google

> Harap mengikuti instruksi [“Cara memulihkan Akun Google atau Gmail Anda”](https://support.google.com/accounts/answer/7682439?hl=id).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

Mohon periksa apakah Anda menerima email “Perubahan kata sandi untuk akun Yahoo Anda” dari Yahoo. Apakah Anda menerimanya?

- [Ya](#Email-received-yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### Email-received-yahoo

Mohon periksa apakah ada tautan “Pulihkan akun Anda di sini”. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-Yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Silakan gunakan tautan “Pulihkan akun Anda di sini” tersebut untuk memulihkan akun Anda.

Apakah Anda berhasil memulihkan akun Anda?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Harap mengikuti instruksi di [“Memperbaiki masalah masuk ke akun Yahoo”](https://id.bantuan.yahoo.com/kb/Memperbaiki-masalah-masuk-ke-akun-Yahoo-sln2051.html) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

Periksa apakah Anda menerima email “Kata sandi Twitter Anda telah diubah” dari Twitter. Apakah Anda menerimanya?

- [Ya](#Email-received-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### Email-received-Twitter

Mohon periksa apakah ada tautan “pulihkan akun Anda” dalam pesan tersebut. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Silakan gunakan tautan “pulihkan akun Anda” tersebut untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Harap mengikuti instruksi di [“Mengajukan permintaan bantuan pemulihan akun”](https://help.twitter.com/id/managing-your-account/log-in-issues).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Harap ikuti [instruksi untuk mengatur ulang kata sandi Anda](https://protonmail.com/support/knowledge-base/reset-password/) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

Periksa apakah Anda menerima email “Perubahan kata sandi akun Microsoft” dari Hotmail. Apakah Anda menerimanya?

- [Ya](#Email-received-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Mohon periksa apakah ada tautan “Atur ulang kata sandi Anda” dalam pesan tersebut. Apakah tautannya ada?
- [Ya](#Recovery-Link-Found-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Silakan gunakan tautan “Atur ulang kata sandi Anda” tersebut untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda dengan tautan “Atur ulang kata sandi Anda” tersebut?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Silakan isi [formulir “Pulihkan akun Anda”](https://account.live.com/acsr).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### Facebook

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

Periksa apakah Anda menerima email “Perubahan kata sandi Facebook” dari Facebook. Apakah Anda menerimanya?

- [Ya](#Email-received-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### Email-received-Facebook

Apakah emailnya mengandung pesan yang berbunyi “Jika Anda tidak merasa melakukannya, harap amankan akun Anda” dengan sebuah tautan?

- [Ya](#Recovery-Link-Found-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Silakan gunakan tautan “Pulihkan akun Anda di sini” yang ada di dalam pesan tersebut untuk memulihkan akun Anda.
Apakah Anda berhasil memulihkan akun Anda dengan mengklik tautan tersebut?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Harap isi [formulir untuk memulihkan akun Anda](https://id-id.facebook.com/login/identify).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

Periksa apakah Anda menerima email “Kata sandi Instagram Anda telah diubah” dari Instagram. Apakah Anda menerimanya?

- [Ya](#Email-received-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### Email-received-Instagram

Mohon periksa apakah ada tautan pemulihan dalam pesan tersebut. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Silakan gunakan tautan “Pulihkan akun Anda di sini” tersebut untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Harap ikuti instruksi di [“Sepertinya akun Instagram saya diretas”](https://id-id.facebook.com/help/instagram/149494825257596?helpref=related) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### Fb-Page_end

Kami ikut senang masalah Anda telah terpecahkan! Silakan baca rekomendasi kami berikut ini untuk membantu meminimalkan kemungkinan kehilangan akses ke halaman Anda di masa mendatang:

Aktifkan 2FA untuk semua admin halaman.
Hanya serahkan peran admin pada orang-orang yang responsif dan Anda percayai.

### account_end

Jika prosedur yang disarankan dalam alur kerja ini tidak membantu memulihkan akses ke akun Anda, Anda dapat menghubungi organisasi-organisasi berikut untuk meminta bantuan lebih lanjut:

:[](organisations?services=account)

### resolved_end

Semoga panduan P2D2 ini bermanfaat. Silakan beri masukan [melalui email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Harap baca rekomendasi kami berikut ini untuk membantu meminimalkan kemungkinan kehilangan akses ke halaman Anda di masa mendatang.

Sangat disarankan untuk mengaktifkan verifikasi 2 langkah (2FA) untuk semua akun yang mendukungnya.
Jangan pernah menggunakan kata sandi yang sama untuk lebih dari satu akun. Jika Anda masih melakukannya, Anda sebaiknya mengubahnya menjadi kata sandi unik untuk tiap-tiap kaun Anda.
Menggunakan pengelola kata sandi dapat membantu Anda membuat dan mengingat kata sandi yang unik dan kuat untuk semua akun Anda.
Waspadalah saat menggunakan jaringan wifi terbuka publik yang tak tepercaya, sebisa mungkin gunakanlah VPN atau Tor saat terhubung dengan jaringan tersebut.

#### Resources

- [Dokumentasi Komunitas Access Now Helpline: Rekomendasi Pengelola Kata Sandi Tim](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Bela Diri Keamanan: Melindungi Diri Anda di Jejaring Sosial](https://ssd.eff.org/en/module/protecting-yourself-social-networks)​​​​​​​
- [Bela Diri Keamanan: Membuat Kata Sandi yang Kuat Menggunakan Pengelola Kata Sandi](https://ssd.eff.org/en/module/creating-strong-passwords#0)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Apakah Anda memiliki akses ke email/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-email-google)
- [Tidak](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Periksa apakah Anda menerima email “[Judul Email Perubahan Kata Sandi]” dari nama_layanan. Apakah Anda menerimanya?

- [Ya](#Email-received-service-name)
- [Tidak](#Recovery-Form-service-name

### Email-received-service-name

> Mohon periksa apakah ada tautan “pulihkan akun Anda” dalam pesan tersebut. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-service-name)
- [Tidak](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Silakan gunakan tautan [Deskripsi Tautan Pemulihan](URL) tersebut untuk memulihkan akun Anda.
Apakah Anda dapat memulihkan akun Anda dengan tautan “[Deskripsi Tautan Pemulihan]” tersebut?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Harap isi formulir pemulihan berikut ini untuk memulihkan akun ini: [Tautan ke formulir pemulihan standar].
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

-->
