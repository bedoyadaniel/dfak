---
layout: page
title: "Apakah Anda menjadi sasaran pelecehan online?"
author: Flo Pagano
language: id
summary: "Apakah Anda menjadi sasaran pelecehan online?"
date: 2023-04
permalink: /id/topics/harassed-online/
parent: Home
---

# Apakah Anda menjadi sasaran pelecehan online?

Internet, dan khususnya platform media sosial, telah menjadi ruang yang kritis bagi anggota dan organisasi masyarakat sipil, terutama untuk perempuan, kelompok LGBTIQ, dan minoritas lainnya, untuk mengekspresikan diri dan membuat suara mereka didengar. Tapi di saat yang bersamaan, internet juga telah menjadi ruang di mana kelompok-kelompok tersebut dengan mudah menjadi sasaran karena mengekspresikan pandangan mereka. Kekerasan dan pelecehan online menyangkal hak perempuan, kelompok LGBTIQ, dan banyak orang kurang beruntung lainnya untuk mengekspresikan diri mereka secara setara, bebas, dan tanpa rasa takut.

Kekerasan dan pelecehan online memiliki banyak bentuk, dan pihak jahat seringkali mengandalkan impunitas, juga karena kurangnya undang-undang yang melindungi korban pelecehan di banyak negara, tapi terutama karena strategi perlindungan perlu diubah secara kreatif tergantung pada jenis serangannya.

Oleh karena itu, penting halnya untuk mengidentifikasi tipologi serangan yang menargetkan Anda guna memutuskan langkah apa yang bisa diambil.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk merencanakan cara mendapatkan perlindungan dari serangan yang menerpa Anda.

Jika Anda menjadi sasaran pelecehan online, ikuti kuesioner ini untuk mengidentifikasi sifat masalah Anda dan menemukan solusi yang memungkinkan.

## Workflow

### physical-wellbeing

Apakah Anda mengkhawatirkan kesehatan dan keberadaan fisik Anda?

- [Ya](#physical-risk_end)
- [Tidak](#no-physical-risk)

### no-physical-risk

Apakah Anda merasa penyerang telah mengakses atau sedang mengakses perangkat Anda?

 - [Ya](#device-compromised)
 - [Tidak](#account-compromised)

### device-compromised

> Ubah kata sandi untuk mengakses perangkat Anda menjadi kata sandi yang unik, panjang, dan rumit:
>
> - [Mac OS](https://support.apple.com/id-id/HT202860)
> - [Windows](https://support.microsoft.com/id-id/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/id-id/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=id)

Apakah Anda telah berhasil mengunci penyerang dari perangkat Anda?

 - [Ya](#account-compromised)
 - [Tidak](../../../device-acting-suspiciously)

### account-compromised

> Jika seseorang mendapatkan akses ke perangkat Anda, mereka mungkin juga telah mengakses akun online Anda, sehingga mereka dapat membaca pesan pribadi Anda, mengenali kontak Anda, dan mempublikasikan postingan, gambar, atau video yang meniru identitas Anda.

Pernahkah Anda melihat postingan atau pesan yang menghilang, atau aktivitas lain yang memberi Anda alasan kuat untuk berpikir bahwa akun Anda mungkin telah disusupi? Tinjau pula folder terkirim Anda untuk melihat aktivitas yang mungkin mencurigakan.

 - [Ya](../../../account-access-issues)
 - [Tidak](#impersonation)

### impersonation

Apakah seseorang memalsukan identitas Anda?

- [Ya](../../../impersonated)
- [Tidak](#doxing)

### doxing

Apakah seseorang telah mempublikasikan informasi atau gambar pribadi tanpa persetujuan Anda?

- [Ya](#doxing-yes)
- [Tidak](#hate-speech)

### doxing-yes

Di mana informasi atau gambar pribadi Anda telah terpublikasikan?

- [Di platform media sosial](#doxing-sn)
- [Di situs web](#doxing-web)

### doxing-sn

> Jika informasi atau gambar pribadi Anda telah terpublikasikan di platform media sosial, Anda dapat melaporkan pelanggaran terhadap standar komunitas dengan mengikuti prosedur pelaporan yang disediakan untuk pengguna oleh situs web jejaring sosial. Anda akan menemukan petunjuk untuk platform utama pada daftar berikut ini:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://cybercivilrights.org/facebook/)
> - [Twitter](https://cybercivilrights.org/twitter)
> - [Tumblr](https://cybercivilrights.org/tumblr)
> - [Instagram](https://cybercivilrights.org/instagram)

Apakah informasi atau medianya telah dihapus?

 - [Ya](#one-more-persons)
 - [Tidak](#harassment_end)

### doxing-web

> Ikuti petunjuk di [“Tanpa Persetujuan Saya - Hapus”](https://withoutmyconsent.org/resources/take-down)  untuk menghapus konten dari suatu situs web.

Apakah kontennya telah dihapus oleh situs web tersebut?

- [Ya](#one-more-persons)
- [Tidak](#harassment_end)

### hate-speech

Apakah serangannya berdasarkan atribut seperti ras, gender, atau agama?

- [Ya](#one-more-persons)
- [Tidak](#harassment_end)


### one-more-persons

Apakah Anda diserang oleh satu orang atau lebih?

- [Satu orang](#one-person)
- [Lebih dari seorang](#more-persons)

### one-person

Apakah Anda mengenal orang tersebut?

- [Ya](#known-harasser)
- [Tidak](#block-harasser)


### known-harasser

> Jika Anda tahu siapa yang melecehkan Anda, Anda dapat mempertimbangkan untuk melaporkan mereka ke otoritas negara Anda, jika sesuai dengan konteks Anda. Tiap negara memiliki hukum yang berbeda untuk melindungi orang dari pelecehan online, dan Anda harus mempelajari undang-undang di negara Anda guna memutuskan apa yang harus dilakukan.
>
> Jika Anda memutuskan untuk menuntut orang ini, Anda harus menghubungi ahli hukum.

Apakah Anda ingin menuntut penyerang?

 - [Ya](#legal_end)
 - [Tidak](#block-harasser)


### block-harasser

> Apakah Anda tahu siapa pelaku yang melecehkan Anda atau tidak, tidak ada salahnya memblokir mereka di platform media sosial saat memungkinkan.
>
> - [Facebook](https://www.facebook.com/help/168009843260943/?helpref=search&query=cara%20blokir&search_session_id=6b2666d7d6cada80cfd0654c08ba3cf4&sr=5)
> - [Twitter](https://help.twitter.com/id/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?hl=id&co=GENIE.Platform%3DDesktop)
> - [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/id/using-tiktok/followers-and-following/blocking-the-users)

Sudahkah Anda berhasil memblokir orang yang melecehkan Anda?

 - [Ya](#resolved_end)
 - [Tidak](#harassment_end)


### more-persons

> Jika Anda diserang oleh lebih dari satu orang, Anda mungkin menjadi target kampanye pelecehan, dan Anda akan perlu merenungkan strategi terbaik apa yang sesuai dengan kasus Anda.
>
> Untuk mempelajari seluruh strategi yang memungkinkan, baca [laman Take Back The Tech tentang strategi melawan ujaran kebencian](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

Sudahkah Anda mengidentifikasi strategi terbaik untuk Anda?

 - [Yes](#resolved_end)
 - [No](#harassment_end)

### harassment_end

> Jika Anda masih dilecehkan dan membutuhkan solusi khusus, harap hubungi organisasi di bawah ini yang bisa membantu Anda.

:[](organisations?services=harassment)


### physical-risk_end

> Jika Anda berada dalam risiko fisik, harap hubungi organisasi di bawah ini yang bisa membantu Anda.

:[](organisations?services=physical_security)


### legal_end

> Jika Anda membutuhkan bantuan hukum, harap hubungi organisasi di bawah ini yang bisa membantu Anda.

:[](organisations?services=legal)

### resolved_end

Semoga panduan pemecahan masalah ini bermanfaat. Silakan beri masukan melalui [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Dokumentasikan pelecehan:** Mendokumentasikan serangan atau insiden lainnya yang mungkin Anda saksikan dapat membantu di kemudian hari: ambil tangkapan layar, simpan pesan yang Anda terima dari pelaku pelecehan, dll. Jika memungkinkan, buatlah jurnal di mana Anda dapat mensistemasikan tanggal, jam, platform dan URL, ID pengguna, tangkapan layar, deskripsi atas apa yang terjadi, dll dari rekaman dokumentasi ini. Jurnal dapat membantu Anda mendeteksi kemungkinan pola dan indikasi mengenai orang yang mungkin merupakan penyerang Anda. Jika Anda merasa kewalahan, coba pertimbangkan seseorang yang Anda percaya yang dapat mendokumentasikan insidennya untuk Anda sementara waktu. Anda harus sangat mempercayai orang yang akan mengelola dokumentasi ini, karena Anda harus memberi mereka kredensial akun pribadi Anda. Sebelum membagi kata sandi Anda dengan orang ini, ubahlah ke sesuatu yang berbeda dan bagilah melalui cara yang aman - misalnya menggunakan alat dengan [end-to-end encryption](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Begitu Anda merasa Anda dapat mendapatkan kembali kendali akun tersebut, ingatlah untuk mengubah kata sandi kembali ke sesuatu yang unik, [aman](https://ssd.eff.org/en/module/creating-strong-passwords), dan hanya diketahui oleh Anda.

    - Anda dapat menemukan petunjuk cara mendokumentasikan insiden di [Tips Dokumentasi untuk Penyintas Penyalahgunaan Teknologi dan Penguntitan](https://www.techsafety.org/documentationtips/).

- **Pasang verifikasi 2 langkah** pada semua akun Anda. Verifikasi 2 langkah bisa menjadi sangat efektif untuk menghentikan seseorang mengakses akun Anda tanpa persetujuan Anda. Jika Anda dapat memilih, jangan menggunakan verifikasi 2 langkah yang berbasis pesan SMS dan pilihlah opsi lainnya, yaitu yang berbasis aplikasi ponsel atau menggunakan kunci keamanan.

    - Jika Anda tidak tahu solusi apa yang terbaik untuk Anda, Anda bisa melihat [infografis “Tipe verifikasi multifaktor apa yang paling sesuai untuk saya?” dari Access Now](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) and [“Panduan Tipe-tipe Umum Verifikasi Dua-Langkah di Web” dari EFF](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Anda bisa menemukan petunjuk untuk mengatur verifikasi 2 langkah pada platform-platform besar di [12 Hari 2FA: Cara Mengaktifkan Verifikasi Dua Langkah untuk Akun Online Anda](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Petakan kehadiran online Anda**. Self-doxing berupa kecerdasan open source tentang diri seseorang untuk mencegah pelaku kejahatan menemukan dan menggunakan informasi ini untuk meniru identitas Anda.


#### Resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
