---
layout: page
title: "Perangkat saya bertingkah mencurigakan"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: id
summary: "Jika komputer atau ponsel Anda bertingkah mencurigakan, mungkin ada perangkat lunak yang tidak diinginkan atau berbahaya di perangkat Anda."
date: 2023-04
permalink: /id/topics/device-acting-suspiciously
parent: Home
---

# Perangkat saya mencurigakan
Serangan malware telah berevolusi dan menjadi sangat canggih seiring berjalannya waktu. Serangan-serangan tersebut menimbulkan berbagai macam ancaman serta dapat berdampak serius pada infrastruktur dan data pribadi maupun organisasi Anda.

Serangan malware hadir dalam berbagai bentuk, misalnya virus, phishing, ransomware, trojan dan rootkit. Beberapa contoh ancaman tersebut yaitu komputer mengalami crash, pencurian data (berupa kredensial akun sensitif, info keuangan, login rekening bank), penyerang memeras Anda untuk membayar uang tebusan dengan mengambil alih perangkat Anda, atau mengambil alih perangkat Anda dan menggunakannya untuk meluncurkan serangan DDoS.

Beberapa metode yang umum digunakan oleh penyerang untuk mengganggu Anda dan perangkat Anda tampak seperti aktivitas biasa, misalnya:

- Email atau postingan di media sosial yang akan menggoda Anda untuk membuka lampiran atau mengklik tautan.

- Mendorong orang untuk mengunduh atau memasang perangkat lunak dari sumber yang tidak terpercaya.

- Mendorong orang untuk memasukkan nama pengguna dan kata sandi mereka ke situs web yang dibuat sedemikian rupa agar terlihat terpercaya, padahal tidak.

Bagian Pertolongan Pertama pada Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mengetahui apakah perangkat Anda terinfeksi malware atau tidak.

Jika Anda merasa bahwa komputer atau perangkat seluler Anda mulai bertingkah mencurigakan, pertama-tama pikirkan apa saja gejalanya.

Gejala yang umumnya dapat diartikan sebagai aktivitas perangkat yang mencurigakan, tapi seringkali tak cukup menjadi alasan untuk khawatir, antara lain:

- Bunyi klik saat panggilan telepon
- Baterai terkuras secara tak terduga
- Perangkat sangat panas padahal sedang tidak digunakan
- Perangkat beroperasi dengan lambat

Gejala-gejala tersebut seringkali disalahpahami sebagai indikator pasti dari aktivitas perangkat yang mengkhawatirkan. Akan tetapi, jika gejala tersebut berdiri sendiri maka itu tidak cukup untuk menjadi alasan kekhawatiran.

Gejala pasti dari perangkat yang mengalami gangguan biasanya adalah:

- Perangkat kerap kali memulai ulang (restart) dengan sendirinya
- Aplikasi mengalami crash, terutama setelah melakukan suatu tindakan
- Pembaruan sistem operasi dan/atau patch keamanan gagal berulang kali
- Lampu indikator aktivitas webcam menyala saat sedang tidak digunakan
- Kemunculan berulang kali ["Blue Screens of Death"](https://en.wikipedia.org/wiki/Blue_Screen_of_Death) atau kernel panic
- Jendela yang berkedip
- Peringatan antivirus


## Workflow

### start

Dengan adanya informasi yang disediakan di pendahuluan, jika Anda masih merasa bahwa perangkat Anda mungkin disusupi, panduan berikut ini dapat membantu Anda mengidentifikasi masalahnya.

 - [Saya yakin perangkat seluler saya bertingkah mencurigakan](#phone-intro)
 - [Saya yakin komputer saya bertingkah mencurigakan](#computer-intro)
 - [Saya tak lagi merasa bahwa perangkat saya mungkin disusupi](#device-clean)


### device-clean

> Bagus! Namun, ingatlah bahwa petunjuk ini hanya untuk membantu Anda melakukan analisis cepat. Meskipun seharusnya cukup untuk mengidentifikasi anomali yang terlihat, spyware yang lebih canggih mampu bersembunyi dengan lebih efektif.

Jika Anda masih curiga perangkat Anda disusupi, Anda sebaiknya:

 - [mencari bantuan tambahan](#malware_end)
 - [langsung reset perangkat tersebut.](#reset).


### phone-intro

> Penting halnya untuk meninjau bagaimana perangkat Anda bisa disusupi.
>
> - Berapa lama sampai Anda mulai mencurigai perangkat Anda?
> - Apakah Anda ingat pernah mengeklik tautan apapun dari sumber yang tak diketahui?
> - Pernahkah Anda menerima pesan dari pihak yang tidak Anda kenal?
> - Apakah Anda memasang (install) perangkat lunak apapun yang diunduh dari sumber yang tak terpercaya?
> - Apakah perangkat tersebut sudah bukan milik Anda lagi?

Pikirkan pertanyaan-pertanyaan tersebut guna mencoba dan mengidentifikasi keadaan yang mengarah ke tersusupinya perangkat Anda.

 - [Saya memiliki perangkat Android](#android-intro)
 - [Saya memiliki perangkat iOS](#ios-intro)


### android-intro

> Pertama-tama periksalah apakah ada aplikasi yang tidak familiar pada perangkat Android Anda.
>
> Temukan daftar aplikasi di bagian “Aplikasi” pada menu pengaturan. Kenali aplikasi yang tidak terpasang sebelumnya di perangkat Anda dan Anda tidak ingat pernah mengunduhnya.
>
> Jika Anda mencurigai aplikasi manapun yang ada di daftar tersebut, lakukan pencarian di web dan carilah sumber informasi untuk melihat apakah ada laporan terpercaya yang mengidentifikasi aplikasi tersebut sebagai berbahaya.

Apakah Anda menemukan aplikasi yang mencurigakan?

 - [Tidak](#android-unsafe-settings)
 - [Ya, saya menemukan aplikasi yang berpotensi berbahaya](#android-badend)


### android-unsafe-settings

> Android memberi penggunanya pilihan untuk mengaktifkan akses tingkat rendah ke perangkat mereka. Hal ini dapat bermanfaat bagi pengembang perangkat lunak, tapi ini juga dapat membuat perangkat terkena serangan tambahan. Anda harus meninjau pengaturan keamanan ini dan memastikannya disetel ke opsi yang lebih aman. Produsen bisa jadi mengirimkan perangkat dengan setelan default yang tidak aman. Pengaturan tersebut harus ditinjau meskipun Anda sendiri belum mengubah apapun.
>
> #### Aplikasi dari Sumber Tak Terpercaya
>
> Android biasanya memblokir pemasangan aplikasi yang tidak berasal dari Google Play Store. Google memiliki serangkaian proses untuk meninjau dan mengidentifikasi aplikasi berbahaya di Play Store. Penyerang seringkali mencoba menghindari pemeriksaan ini dengan menyediakan aplikasi berbahaya langsung kepada pengguna dengan membagikan tautan atau file kepada mereka. Penting halnya untuk memastikan perangkat Anda tidak mengizinkan pemasangan aplikasi dari sumber yang tidak terpercaya.
>
> Pastikan pemasangan aplikasi dari sumber yang tidak terpercaya dinonaktifkan di perangkat Anda. Di banyak versi Android, Anda bisa menemukan opsi ini di pengaturan keamanan, tapi bisa juga ada di bagian lain, tergantung dari versi Android mana yang Anda jalankan, dan pada tipe ponsel terbaru opsi ini bisa jadi berada di pengaturan perizinan di tiap aplikasi.
>
> #### Mode Pengembang
>
> Android mengizinkan para pengembang untuk secara langsung menjalankan perintah pada sistem operasi yang mendasarinya dalam “Mode Pengembang”. Saat diaktifkan, mode ini dapat membuat perangkat terpapar serangan fisik. Seseorang dengan akses fisik ke perangkat tersebut dapat menggunakan Mode Pengembang untuk mengunduh salinan data pribadi dari perangkat tersebut atau untuk memasang aplikasi berbahaya.
>
> Jika Anda melihat menu Mode Pengembang di pengaturan perangkat Anda, sebaiknya Anda pastikan fitur tersebut dinonaktifkan.
>
> #### Google Play Protect
>
> Layanan Google Play Protect tersedia di semua perangkat Android terkini. Layanan ini melakukan pemindaian rutin pada semua aplikasi yang terpasang di perangkat Anda. Play Protect juga dapat secara otomatis menghapus aplikasi berbahaya yang diketahui berada di perangkat Anda. Mengaktifkan layanan ini akan mengirimkan informasi mengenai perangkat Anda (misalnya, aplikasi-aplikasi yang terpasang) pada Google.
>
> Google Play Protect dapat diaktifkan melalui pengaturan keamanan perangkat Anda. Informasi lebih lanjut tersedia di situs [Play Protect](https://www.android.com/play-protect/).

Apakah Anda menemukan setelan yang tidak aman?

- [Tidak](#android-bootloader)
- [Ya, saya menemukan setelan yang berpotensi tidak aman](#android-badend)


### android-bootloader

> Bootloader Android adalah perangkat lunak utama yang berjalan begitu Anda menyalakan perangkat Anda. Bootloader memungkinkan sistem operasi untuk menyala dan menggunakan perangkat keras. Bootloader yang tersusupi memberi penyerang akses penuh ke perangkat keras Anda. Mayoritas produsen mengirim perangkat mereka dengan bootloader yang terkunci. Cara umum untuk mengetahui apakah bootloader resmi dari produsen telah diubah adalah dengan memulai ulang (restart) perangkat Anda dan carilah logo bootnya. Jika muncul sebuah segitiga kuning dengan tanda seru, maka bootloader aslinya telah diganti. Perangkat Anda mungkin juga telah disusupi jika ia menunjukkan jendela peringatan bootloader yang tak terkunci dan Anda belum pernah membuka kuncinya sendiri untuk memasang Android ROM khusus seperti CyanogenMod. Anda harus melakukan reset ke setelan pabrik pada perangkat Anda jika ia menunjukkan jendela peringatan bootloader yang tak terkunci yang tidak Anda harapkan.

Apakah bootloadernya disusupi atau perangkat Anda menggunakan bootloader asli?

- [Bootloader di perangkat saya telah diubah](#android-badend)
- [Perangkat saya menggunakan bootloader asli](#android-goodend)


### android-goodend

> Tampaknya perangkat Anda tidak tersusupi.

Apakah Anda masih khawatir perangkat Anda disusupi?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya sudah mengatasi permasalahan saya](#resolved_end)


### android-badend

> Perangkat Anda mungkin disusupi. Melakukan reset ke setelan pabrik (_factory reset_) dapat menghapus ancaman apapun yang terdapat di perangkat Anda. Namun, ini tak selalu menjadi solusi terbaik. Sebagai tambahan, Anda sebaiknya menyelidiki masalah ini lebih jauh untuk mengidentifikasi tingkat keterpaparan Anda dan bagaimana tepatnya sifat serangan yang Anda alami.
>
> Anda sebaiknya menggunakan alat untuk swa-diagnosis bernama [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn), atau mencari bantuan dari organisasi yang bisa membantu.

Apakah Anda ingin mencari bantuan lebih lanjut?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya memiliki jaringan bantuan lokal yang bisa saya hubungi](#resolved_end)


### ios-intro

> Periksa pengaturan iOS untuk melihat apakah ada hal yang tidak biasa.
>
> Di Pengaturan, periksa apakah perangkat Anda terhubung dengan Apple ID Anda. Item menu pertama di sisi kiri harus berupa nama Anda atau nama yang Anda gunakan untuk Apple ID Anda. Klik dan periksa apakah itu menampilkan alamat email yang benar. Di bagian bawah halaman ini Anda bisa melihat daftar berisi nama dan model perangkat iOS yang terhubung dengan dengan Apple ID ini.

 - [Semua informasi benar dan saya masih memegang kendali atas Apple ID saya](#ios-goodend)
 - [Nama dan detail lainnya salah atau saya menemukan perangkat dalam daftar tersebut yang bukan milik saya](#ios-badend)


### ios-goodend

> Sepertinya perangkat Anda tidak tersusupi.

Apakah Anda masih khawatir perangkat Anda disusupi?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya telah memecahkan masalah saya](#resolved_end)


### ios-badend

> Perangkat Anda mungkin disusupi. Melakukan reset setelan pabrik dapat menghapus ancaman apapun yang terdapat di perangkat Anda. Namun, hal tersebut tak selalu menjadi solusi terbaik. Sebagai tambahan, Anda sebaiknya menyelidiki masalah ini lebih jauh untuk mengidentifikasi tingkat keterpaparan Anda dan bagaimana tepatnya sifat serangan yang Anda alami.
>
> Anda sebaiknya menggunakan alat untuk swa-diagnosis bernama [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn), atau mencari bantuan dari organisasi yang bisa membantu.

Apakah Anda ingin mencari bantuan lebih lanjut?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya memiliki jaringan bantuan lokal yang bisa saya hubungi ](#resolved_end)


### computer-intro

> **Catatan: Jika Anda mengalami serangan ransomware, segeralah menuju [No More Ransom!](https://www.nomoreransom.org/).**
>
> Alur kerja ini akan membantu Anda untuk menyelidiki aktivitas mencurigakan pada perangkat komputer Anda. Jika Anda membantu seseorang dari jarak jauh, Anda bisa coba mengikuti langkah-langkah yang dijelaskan pada tautan di bawah ini menggunakan perangkat lunak desktop jarak jauh seperti TeamViewer, atau Anda dapat melihat kerangka kerja forensik jarak jauh semacam [Google Rapid Response (GRR)](https://github.com/google/grr). Ingatlah bahwa latensi dan keandalan jaringan memegang peranan penting untuk bisa melakukan hal ini dengan benar.

Silakan pilih sistem operasi yang ingin Anda periksa:

 - [Windows](#windows-intro)
 - [macOS](#mac-intro)


### windows-intro

> Anda bisa mengikuti panduan pengantar ini untuk menyelidiki aktivitas mencurigakan pada perangkat Windows:
>
> - [Cara melakukan Live Forensic pada Windows dengan menggunakan Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Apakah petunjuk ini membantu Anda mengidentifikasi aktivitas berbahaya?

 - [Ya, saya rasa komputer saya telah terinfeksi](#device-infected)
 - [Tidak, tidak ditemukan aktivitas berbahaya apapun](#device-clean)


### mac-intro

> Untuk mengidentifikasi potensi infeksi pada komputer Mac, ikuti langkah-langkah berikut:
>
> 1. Periksa apakah ada program mencurigakan yang memulai secara otomatis
> 2. Periksa apakah ada proses mencurigakan yang berjalan
> 3. Periksa apakah ada ekstensi kernel yang mencurigakan
>
> Situs web [Objective-See](https://objective-see.com) menyediakan sejumlah perangkat gratis untuk berbagai keperluan guna memfasilitasi proses ini:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) dapat digunakan untuk mengidentifikasi semua program yang terdaftar untuk memulai secara otomatis.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) dapat digunakan untuk memeriksa proses yang berjalan dan mengidentifikasi proses yang tampak mencurigakan (misalnya proses yang tidak memiliki tanda resmi, atau proses yang diberi tanda bahaya oleh VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) dapat digunakan untuk mengidentifikasi ekstensi kernel mencurigakan yang termuat di komputer Mac.
>
> Jika hal-hal tersebut tidak langsung mengungkap hal apapun yang mencurigakan dan Anda ingin melakukan triase lebih lanjut, Anda bisa menggunakan [Snoopdigg](https://github.com/botherder/snoopdigg). Snoopdigg adalah perangkat berguna yang menyederhanakan proses mengumpulkan informasi pada sistem dan mengambil snapshot keseluruhan memori.
>
> Alat tambahan yang dapat bermanfaat untuk mengumpulkan detail lebih lanjut (tapi membutuhkan pemahaman tentang terminal command) adalah [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) buatan perusahaan keamanan siber Amerika, CrowdStrike.

Apakah petunjuk ini membantu Anda mengidentifikasi aktivitas berbahaya?

 - [Ya, saya rasa komputer saya telah terinfeksi](#device-infected)
 - [idak, tidak ditemukan aktivitas berbahaya apapun](#device-clean)

### device-infected

Ya ampun! Untuk menghilangkan infeksinya, Anda perlu:

- [mencari bantuan tambahan](#malware_end)
- [langsung reset perangkat tersebut](#reset).

### reset

> Sebaiknya Anda mempertimbangkan mengatur ulang perangkat Anda demi keamanan ekstra. Panduan di bawah ini akan menyediakan petunjuk yang sesuai untuk tipe perangkat Anda:
>
> - [Android](https://www.howtogeek.com/248127/how-to-wipe-your-android-device-and-restore-it-to-factory-settings/)
> - [iOS](https://support.apple.com/en-us/HT201252)
> - [Windows](https://support.microsoft.com/en-us/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/en-us/HT201314)

Apakah Anda merasa perlu bantuan tambahan?

- [Ya](#malware_end)
- [Tidak](#resolved_end)


### malware_end

Jika Anda memerlukan bantuan tambahan dalam mengurus perangkat yang terinfeksi, Anda dapat menghubungi organisasi yang terdaftar di bawah ini.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Kami harap Pertolongan Pertama Darurat Digital (P2D2) ini bermanfaat. Silakan beri kami masukan melalui [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Berikut beberapa tips untuk mencegah diri Anda menjadi korban dari penyerang yang berusaha menyusupi perangkat dan data Anda:

- Selalu cek dua kali keabsahan email apapun yang Anda terima, file yang Anda unduh, atau tautan yang meminta Anda memberikan detail login akun Anda.
- Baca mengenai cara melindungi perangkat Anda dari infeksi malware di panduan-panduan yang tertaut di sumber informasi ini.

#### Sumber informasi

- [Security in a Box: Melindungi perangkat Anda dari serangan malware dan phishing](https://securityinabox.org/id/phones-and-computers/malware/)
- [Surveillance Self-Defense: How to: Avoid Phishing Attacks](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)