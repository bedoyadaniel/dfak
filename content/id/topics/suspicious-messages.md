---
layout: page
title: "Saya Menerima Pesan Mencurigakan"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: id
summary: "Saya menerima pesan, tautan, atau email yang mencurigakan, apa yang harus saya lakukan terhadapnya?"
date: 2023-04
permalink: /id/topics/suspicious-messages
parent: Home
---

# Saya Menerima Pesan Mencurigakan

Anda mungkin menerima pesan _mencurigakan_ di kotak masuk email, akun media sosial, atau aplikasi pesan singkat Anda. Bentuk email mencurigakan yang paling umum adalah email phishing. Email phishing bertujuan untuk mengelabui Anda agar memberikan informasi pribadi, finansial, atau akun Anda. Email tersebut mungkin akan meminta Anda untuk mengunjungi situs web palsu atau menelpon nomor layanan pelanggan palsu. Email phishing juga dapat mengandung lampiran yang memasang perangkat lunak berbahaya pada komputer Anda saat dibuka.

Jika Anda tidak yakin tentang keaslian pesan yang Anda terima, atau apa yang harus dilakukan terhadapnya, Anda bisa menggunakan kuesioner berikut ini sebagai alat pemandu untuk mendiagnosis lebih jauh situasinya atau untuk membagi pesan dengan organisasi luar yang terpercaya yang akan memberi Anda analisis lebih detail mengenai pesan Anda.
Ingatlah bahwa menerima email mencurigakan tidak selalu berarti akun Anda telah disusupi. Jika Anda merasa ada sebuah email atau pesan yang mencurigakan, jangan dibuka. Jangan membalas email tersebut, jangan klik tautan apapun, dan jangan unduh lampiran apapun.


## Workflow

### intro

Apakah Anda melakukan sesuatu pada pesan atau tautan yang ada?

- [Saya mengklik tautannya](#link-clicked)
- [Saya memasukkan kredensial](#account-security_end)
- [Saya mengunduh file](#device-security_end)
- [Saya membalas dengan informasi](#reply-personal-info)
- [Saya belum pernah melakukan tindakan apapun](#do-you-know-sender)

### do-you-know-sender

Apakah Anda mengenal pengirim pesannya? Ketahuilah bahwa pengirim pesan mungkin [dipalsukan](https://en.wikipedia.org/wiki/Email_spoofing) supaya tampak seperti seseorang yang Anda percayai.


 - [Pengirim pesan adalah seseorang atau organisasi yang saya kenal](#known-sender)
 - [Pengirim pesan adalah penyedia layanan (seperti penyedia layanan email, hosting, media sosial, atau bank)](#service-provider)
 - [Pesan tersebut dikirim oleh orang atau organisasi yang tidak saya kenal](#share)

### known-sender

> Bisakah Anda menghubungi pengirim menggunakan kanal komunikasi lain? Misalnya, jika Anda menerima pesannya melalui email, bisakah Anda memverifikasinya secara langsung dengan pengirim melalui Signal, WhatsApp, atau telepon? Pastikan Anda menggunakan metode kontak yang ada. Anda tak semestinya mempercayai nomor telepon yang tercantum di pesan yang mencurigakan.

Apakah Anda mengkonfirmasi bahwa pengirim adalah yang mengirimi Anda pesan tersebut?

 - [Ya](#resolved_end)
 - [Tidak](#share)

### service-provider

> Pada skenario ini, penyedia layanan adalah perusahaan atau merek apapun yang menyediakan layanan yang Anda gunakan atau berlangganan. Daftar ini dapat berisi penyedia layanan email Anda (Google, Yahoo, Microsoft, ProtonMail...), penyedia layanan media sosial Anda (Facebook, Twitter, Instagram...), atau platform daring yang memiliki informasi finansial Anda (Paypal, Amazon, bank, Netflix...).
>
> Apakah ada cara untuk memeriksa apakah pesan tersebut asli? Banyak penyedia layanan yang juga akan menyediakan salinan pemberitahuan atau dokumen lain di halaman akun Anda. Contohnya, jika pesannya dari Facebook, seharusnya pesan tersebut termuat di [daftar email pemberitahuan](https://www.facebook.com/settings?tab=security&section=recent_emails), atau jika itu dari bank Anda, Anda bisa menghubungi layanan pelanggannya.

Pilih salah satu dari pilihan di bawah ini:

 - [Saya dapat memverifikasi bahwa pesan tersebut adalah pesan resmi dari penyedia layanan saya](#resolved_end)
 - [Saya tidak dapat memverifikasi pesan tersebut](#share)
 - [Saya tidak berlangganan layanan ini dan/atau mengharapkan pesan dari mereka](#share)

### link-clicked

> Dalam beberapa pesan mencurigakan, tautan yang ada dapat membawa Anda ke halaman login palsu yang akan mencuri kredensial Anda atau tipe halaman lain yang mungkin mencuri informasi pribadi dan finansial Anda. Kadang, tautan tersebut akan meminta Anda untuk mengunduh lampiran yang memasang perangkat lunak berbahaya pada komputer Anda saat dibuka. Tautan tersebut juga bisa membawa Anda ke situs web yang disediakan secara khusus yang mungkin mencoba untuk menginfeksi perangkat Anda dengan perangkat lunak berbahaya atau memiliki fungsi mata-mata.

Bisakah Anda menyampaikan apa yang terjadi setelah Anda mengklik tautan tersebut?

- [Tautan tersebut meminta saya untuk memasukkan kredensial](#account-security_end)
- [Tautan tersebut mengunduh sebuah file](#device-security_end)
- [Tak ada yang terjadi, tapi saya tidak yakin](#clicked-but-nothing-happened)


### clicked-but-nothing-happened

> Fakta bahwa Anda telah mengklik tautan mencurigakan dan Anda tidak menyadari perilaku aneh apapun bukan berarti tidak ada tindakan berbahaya terjadi di latar belakang. Ada sejumlah skenario yang harus Anda pikirkan. Yang paling tidak mengkhawatirkan adalah bahwa pesan yang Anda terima adalah spam yang digunakan untuk keperluan iklan. Pada kasus ini, beberapa iklan akan muncul. Pada beberapa kasus, iklan-iklan tersebut dapat juga menjadi berbahaya.

- [Beberapa iklan muncul. Saya tidak yakin apakah itu berbahaya atau tidak.](#suspicious-device_end)

> Dalam skenario terburuk, dengan mengklik tautan, eksploitasi terjadi untuk mengeksekusi perintah berbahaya pada sistem Anda. Jika ini terjadi, ini mungkin karena browser Anda tidak menggunakan versi terbaru dan memiliki kerentanan yang memungkinkan eksploitasi ini. Dalam kasus yang jarang terjadi di mana browser Anda menggunakan versi terbaru dan skenario ini terjadi, kerentanan yang tereksploitasi bisa jadi tak diketahui. Pada kedua kasus, perangkat Anda dapat mulai bertindak mencurigakan.

- [Ya, browser saya tidak menggunakan versi terbaru dan/atau perangkat saya mulai bertindak mencurigakan setelah saya mengklik tautan tersebut](../../../device-acting-suspiciously)

> Pada skenario lain, dengan mengunjungi tautan tersebut Anda mungkin telah menjadi korban [cross-site script attack (or XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting). Hasil dari serangan ini adalah pencurian cookie Anda yang digunakan untuk mengautentikasi Anda pada situs web yang Anda kunjungi, jadi penyerang akan dapat masuk ke situs tersebut dengan nama pengguna Anda. Tergantung dari keamanan situs tersebut, penyerang mungkin bisa atau tidak bisa mengganti kata sandinya. Hal ini menjadi makin serius jika situs web yang rentan terhadap XSS adalah situs yang Anda kelola, karena dalam kasus demikian penyerang akan dapat mengautentikasi sebagai admin situs web Anda. Untuk mengidentifikasi serangan XSS, periksa apakah tautan yang Anda klik mengandung [script string](https://owasp.org/www-community/attacks/xss/). Ini bisa juga dikodekan dalam HEX atau Unicode.

- [Ada script di tautan tersebut, atau sebagian dalam kode](#cross-site-script)
- [Tidak ada script yang teridentifikasi](#suspicious-device_end)

### cross-site-script

Apakah Anda memiliki akun pada situs yang Anda kunjungi?

- [Ya](#account-security_end)
- [Tidak](#cross-site-script_end)

Apakah Anda berakhir di sebuah situs web yang Anda kelola?

- [Ya](#cross-site-script-admin-compromised)
- [Tidak](#cross-site-script_end)

### cross-site-script-admin-compromised

> Dalam kasus ini, penyerang mungkin memiliki cookie yang valid yang memungkinkan mereka untuk mengakses akun admin Anda. Hal pertama yang harus dilakukan adalah masuk ke antarmuka administrasi dan mematikan sesi yang aktif, atau cukup ubah kata sandinya. Anda juga harus memeriksa apakah penyerang mengunggah sesuatu ke situs Anda dan/atau memposting konten berbahaya, dan jika ada maka hapuslah.

Organisasi berikut dapat membantu menyelidiki dan merespon insiden ini:

:[](organisations?services=forensic)

### cross-site-script_end

> Ini seharusnya baik-baik saja. Namun, di beberapa kasus langka, XSS dapat digunakan untuk menggunakan browser Anda untuk meluncurkan serangan lain.

- [Saya ingin menilai apakah perangkat saya terinfeksi](../../../device-acting-suspiciously)
- [Saya rasa saya baik-baik saja](#final_tips)

### reply-personal-info

> Tergantung dari tipe informasi yang Anda bagikan, Anda mungkin perlu mengambil tindakan langsung.

Jenis informasi seperti apa yang Anda bagikan?

- [Saya membagikan informasi akun yang bersifat rahasia](#account-security_end)
- [Saya membagikan informasi publik](#share)
- [Saya tidak yakin seberapa sensitif informasi tersebut dan saya membutuhkan bantuan](#help_end)


### share

> Membagikan pesan mencurigakan yang Anda terima dapat membantu melindungi kolega dan komunitas Anda yang mungkin juga terdampak. Anda mungkin juga ingin meminta bantuan kepada seseorang yang Anda percaya untuk memberitahu Anda jika pesan tersebut berbahaya. Pertimbangkan untuk membagi pesan tersebut dengan organisasi yang dapat menganalisisnya.
>
> Untuk membagikan pesan mencurigakan yang Anda terima, pastikan untuk menyertakan pesan itu sendiri berikut informasi tentang pengirim. Jika pesan tersebut berupa email, harap pastikan untuk menyertakan email lengkap termasuk header menggunakan [ panduan oleh Computer Incident Response Center Luxembourg (CIRCL) berikut ini](https://www.circl.lu/pub/tr-07/).

Apakah Anda memerlukan bantuan lebih lanjut?

- [Ya, saya butuh bantuan lebih lanjut](#help_end)
- [Tidak, saya sudah mengatasi masalah saya](#resolved_end)


### device-security_end

> Jika beberapa file terunduh ke perangkat Anda, bisa jadi perangkat Anda berisiko!
Harap hubungi organisasi di bawah ini yang dapat membantu Anda. Setelahnya, harap  [bagikan pesan mencurigakan yang Anda terima](#share).

:[](organisations?services=device_security)


### account-security_end

> Jika Anda memasukkan kredensial Anda, atau Anda telah menjadi korban serangan skrip lintas situs, akun Anda bisa jadi berisiko!
>
> Jika Anda merasa akun Anda telah disusupi, Anda disarankan untuk juga mengikuti alur kerja Pertolongan Pertama  Darurat Digital pada akun yang disusupi.
>
> Disarankan pula agar Anda memberitahukan komunitas Anda mengenai kampanye phishing ini, dan bagilah pesan mencurigakan tersebut dengan organisasi yang dapat menganalisisnya.

Apakah Anda ingin berbagi informasi tentang pesan yang Anda terima atau Anda butuh bantuan lebih lanjut sebelumnya?

- [Saya ingin membagikan pesan mencurigakan tersebut](#share)
- [Saya butuh bantuan lebih untuk mengamankan akun saya](#account_end)
- [Saya butuh bantuan lebih untuk menganalisis pesan tersebut](#analysis_end)

### suspicious-device_end

> Jika Anda mengklik sebuah tautan dan tidak yakin apa yang terjadi, perangkat Anda mungkin telah terinfeksi tanpa Anda sadari. Jika Anda ingin menjelajahi kemungkinan ini, atau merasa perangkat Anda mungkin terinfeksi, bukalah [alur kerja “Perangkat saya bertindak mencurigakan”](../../../device-acting-suspiciously).

### help_end

> Anda harus mencari bantuan dari kolega Anda atau orang lain untuk lebih memahami risiko dari informasi yang Anda bagikan. Orang lain di organisasi atau jaringan Anda mungkin juga pernah menerima permintaan yang sama.

Harap hubungi organisasi di bawah ini yang dapat membantu Anda. Setelahnya, harap  [bagikan pesan mencurigakan yang Anda terima](#share).

:[](organisations?services=digital_support)


### account_end

Jika akun Anda telah disusupi dan Anda membutuhkan bantuan untuk mengamankannya, harap hubungi organisasi di bawah ini yang dapat membantu Anda.

:[](organisations?services=account)


### analysis_end

Organisasi berikut dapat menerima pesan mencurigakan yang Anda terima dan menyelidikinya lebih jauh untuk Anda:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Kami harap panduan Pertolongan Pertama pada Darurat Digital (P2D2) ini bermanfaat. Silahkan beri masukan [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).


### final_tips

Peraturan pertama yang perlu diingat: jangan pernah memberikan informasi pribadi apapun melalui email. Tidak ada institusi, bank, dll. yang akan meminta informasi ini melalui email atau aplikasi chat. Mungkin tidak semudah itu mengetahui apakah sebuah email atau situs web resmi atau tidak, tetapi ada beberapa tips yang dapat membantu Anda menilai email yang Anda terima.

* Perasaan mendesak: email mencurigakan biasanya memperingatkan tentang perubahan mendadak pada sebuah akun dan meminta Anda untuk segera bertindak untuk memverifikasi akun Anda.
* Di badan email, Anda mungkin menemukan pertanyaan yang meminta Anda untuk “memverifikasi” atau “memperbarui akun Anda” atau “kegagalan memperbarui catatan Anda akan mengakibatkan penangguhan akun”. Biasanya aman untuk mengasumsikan bahwa tak ada organisasi kredibel yang mana telah Anda sediakan data Anda akan meminta Anda untuk memasukkan kembali informasi tersebut, jadi jangan jatuh ke perangkap ini
* Waspadalah terhadap pesan, lampiran, tautan, dan halaman login yang muncul tanpa diminta.
* Perhatikan kesalahan pengejaan dan tata bahasa.
* Klik untuk melihat alamat lengkap pengirim, tidak hanya nama yang ditampilkan.
* Perhatikan tautan yang disingkat - tautan tersebut dapat menyembunyikan tautan berbahaya di baliknya.
* Saat Anda mengarahkan mouse ke suatu tautan, URL sebenarnya yang akan Anda tuju akan terpampang dalam bentuk popup atau di bagian bawah jendela browser Anda.
* Header email menyertakan kata “dari:” dapat dibuat sedemikian rupa untuk terlihat resmi dan terpercaya. Dengan memeriksa header SPF dan DKIM, Anda dapat mengetahui apakah tiap-tiap alamat IP diizinkan (atau tidak diizinkan) untuk mengirim email atas nama domain pengirim, dan apakah header atau kontennya telah diubah saat transit. Pada email yang terpercaya, bagian [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) dan [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) harus berstatus ‘PASS’, jika tidak maka sebaiknya jangan mempercayai email tersebut. Alasannya karena email tersebut dipalsukan, atau dalam kasus yang langka, server email tidak terkonfigurasi dengan benar.
* [Tanda tangan digital](https://www.gnupg.org/gph/en/manual/x135.html) dapat memberi tahu kita apakah suatu email dikirim dari pengirim yang terpercaya dan apakah email tersebut telah dimodifikasi dalam prosesnya atau tidak. Jika email tersebut ditandatangani, periksa apakah tanda tangannya terverifikasi atau tidak. Untuk memverifikasi tanda tangannya, Anda memerlukan OpenPGP serta kunci publik yang terkait dengan ID dalam tanda tangan di pesan tersebut. Sebagian besar klien email modern yang mendukung tanda tangan digital akan mengotomatiskan verifikasinya untuk Anda dan memberitahu Anda melalui antarmuka pengguna mereka jika sebuah tanda tangan telah diverifikasi atau belum.
* Akun yang tersusupi dapat mengeluarkan email atau pesan berbahaya yang dapat memverifikasi semua kondisi di atas dan terlihat terpercaya. Namun, biasanya isi pesannya tampak tak biasa. Jika isi pesan emailnya tampak aneh, selalu ada baiknya untuk memeriksa dengan pengirim yang sah melalui kanal komunikasi lain sebelum mengambil tindakan apapun.
* Tidak ada salahnya untuk membaca dan menulis email Anda dalam teks biasa. Email berbasis HTML dapat dirender dengan cara menyembunyikan kode atau URL berbahaya. Anda dapat menemukan instruksi untuk menonaktifkan HTML pada berbagai klien email dalam [postingan ini](https://www.maketecheasier.com/read-email-in-plain-text/).
* Gunakan sistem operasi versi terbaru di ponsel atau komputer Anda (periksa versi untuk [Android](https://en.wikipedia.org/wiki/Android_version_history), [iOS](https://en.wikipedia.org/wiki/IOS_version_history), [macOS](https://en.wikipedia.org/wiki/MacOS_version_history) dan [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)).
* Segera perbarui sistem operasi Anda dan semua aplikasi/program yang telah Anda pasang, terutama yang menerima informasi (browser, aplikasi/program perpesanan dan obrolan, klien email, dll.). Hapus semua aplikasi/program yang tidak Anda butuhkan.
* Gunakan browser yang bisa diandalkan (misalnya Mozilla Firefox). Tingkatkan keamanan browser Anda dengan meninjau ulang ekstensi/add-on yang terpasang di browser Anda. Sisakan hanya yang Anda percaya (misalnya: HTTPS Everywhere, Privacy Badger, uBlock Origin, Facebook Container, Cookie AutoDelete, NoScript).
* Buat cadangan informasi Anda yang aman secara teratur.
* Lindungi akun Anda dengan kata sandi yang kuat, verifikasi 2 langkah, dan pengaturan yang aman.


#### Resources

Berikut adalah sejumlah sumber informasi untuk menemukan pesan mencurigakan dan menghindari phishing.

* [Citizen Lab: Communities at risk - Targeted Digital Threats Against Civil Society](https://targetedthreats.net)
* [Surveillance Self-Defense: How to Avoid Phishing Attacks](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
