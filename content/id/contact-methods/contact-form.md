---
layout: page
title: Contact form
author: mfc
language: id
summary: Contact methods
date: 2020-10
permalink: /id/contact-methods/contact-form.md
parent: /id/
published: true
---

A contact form will likely preserve the secrecy of your message to the recipient organization, so that only you and the recipient organization can read it. This will only be the case if the website hosting the contact form implements proper security measures such as [TLS/SSL](https://ssd.eff.org/en/glossary/secure-sockets-layer-ssl) encryption among others, which is the case with CiviCERT organizations.

However, the fact that you have visited the organization's website where the contact form is hosted is likely knowable to governments, law enforcement agencies, or other parties with access to local, regional, or global surveillance infrastructure. That you visited the organization's website would indicate that you may have contacted the organisation.

If you would like to keep private the fact that you visited the organization's website (and potentially contacted them), then it is preferable to access their website through the [Tor Browser](https://www.torproject.org/) or a trusted VPN or proxy. Before doing so, consider the legal context in which you reside and if you need to obfuscate your use of Tor Browser by [configuring it](https://tb-manual.torproject.org/running-tor-browser/) with a [pluggable transport](https://tb-manual.torproject.org/circumvention/). If you are considering using a VPN or proxy, [investigate where that VPN server or proxy](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) is based and [your trust of the VPN entity](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you).
