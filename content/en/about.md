---
layout: page.pug
title: "About"
language: en
summary: "About the Digital First Aid Kit."
date: 2020-11
permalink: /en/about/
parent: Home
---

The Digital First Aid Kit is a collaborative effort of the [RaReNet (Rapid Response Network)](https://www.rarenet.org/) and [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

The Rapid Response Network is an international network of rapid responders and digital security champions which includes Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad as well as individual security experts who are working in the field of digital security and rapid response.

Some of these organisations and individuals are part of CiviCERT, an international network of digital security help desks and infrastructure providers that are mainly focused on supporting groups and organizations striving towards social justice and the defense of human and digital rights. CiviCERT is a professional framing for the rapid response community’s distributed CERT (Computer Emergency Response Team) efforts. CiviCERT is accredited by Trusted Introducer, the European network of trusted computer emergency response teams.

The Digital First Aid Kit is also an [open-source project that accepts outside contributions](https://gitlab.com/rarenet/dfak).

Thanks to [Metamorphosis Foundation](https://metamorphosis.org.mk) for the [Albanian localization](https://digitalfirstaid.org/sq/) and to [EngageMedia](https://engagemedia.org/) for the [Burmese](https://digitalfirstaid.org/my/), [Indonesian](https://digitalfirstaid.org/id/), and [Thai](https://digitalfirstaid.org/th/) localization of the Digital First Aid Kit. 

If you want to use the Digital First Aid Kit in contexts where connectivity is limited, or finding a connection is difficult, you can download [an offline version](https://digitalfirstaid.org/dfak-offline.zip).

For any comment, suggestion or question about the Digital First Aid Kit, you can write to: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
