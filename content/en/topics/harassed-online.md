---
layout: page
title: "Are you being targeted by online harassment?"
author: Floriana Pagano
language: en
summary: "Are you being targeted by online harassment?"
date: 2020-11
permalink: /en/topics/harassed-online/
parent: Home
---

# Are you being targeted by online harassment?

The Internet, and social media platforms in particular, have become a critical space for civil society members and organizations, especially for women, LGBTQIA+ people, and other minorities, to express themselves and make their voices heard. But at the same time, they have also become spaces where these groups are easily targeted for expressing their views. Online violence and abuse denies women, LGBTQIA+ persons, and many other marginalized folks the right to express themselves equally, freely, and without fear.

Online violence and abuse has many different forms, and malicious entities can often rely on impunity, also due to a lack of laws that protect victims of harassment in many countries, but most of all because protection strategies need to be tweaked creatively depending on what kind of attack is being launched.

It is therefore important to identify the typology of the attack targeting you to decide what steps can be taken.

This section of the Digital First Aid Kit will walk you through some basic steps to plan how to get protected against the attack you are suffering.

If you are targeted by online harassment, follow this questionnaire to find out what's really happening and find possible solutions.

## Workflow

### physical-wellbeing

Do you fear for your physical integrity or wellbeing?

- [Yes](#physical-risk_end)
- [No](#no-physical-risk)

### no-physical-risk

Do you think the attacker has accessed or is accessing your device?

 - [Yes](#device-compromised)
 - [No](#account-compromised)

### device-compromised

> Change the password to access your device to a unique, long and complex one:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

Has the attacker been effectively locked out of your device?

 - [Yes](#account-compromised)
 - [No](../../../device-acting-suspiciously)

### account-compromised

> If someone got access to your device, they might have also accessed your online accounts, so they could be reading your private messages, identify your contacts, and publish posts, images, or videos impersonating you.

Have you noticed posts or messages disappearing, or other activities that give you good reason to think your account may have been compromised? Also review your send folder for possible suspicious activity.

 - [Yes](../../../account-access-issues)
 - [No](#impersonation)

### impersonation

Is someone impersonating you?

- [Yes](../../../impersonated)
- [No](#doxing)

### doxing

Has someone published private information or pictures without your consent?

- [Yes](#doxing-yes)
- [No](#hate-speech)

### doxing-yes

Where has your private information or pictures been published?

- [On a social networking platform](#doxing-sn)
- [On a website](#doxing-web)

### doxing-sn

> If your private information or pictures have been published in a social media platform, you can report a violation of the community standards following the reporting procedures provided to users by social networking websites. You will find instructions for the main platforms in the following list:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://cybercivilrights.org/facebook/)
> - [Twitter](https://cybercivilrights.org/twitter)
> - [Tumblr](https://cybercivilrights.org/tumblr)
> - [Instagram](https://cybercivilrights.org/instagram)

Have the information or media been deleted?

 - [Yes](#one-more-persons)
 - [No](#harassment_end)

### doxing-web

> Follow the instructions in ["Without My Consent - Take Down"](https://withoutmyconsent.org/resources/take-down) to take down content from a website.

Has the content been taken down by the website?

- [Yes](#one-more-persons)
- [No](#harassment_end)

### hate-speech

Is the attack based on attributes like race, gender, or religion?

- [Yes](#one-more-persons)
- [No](#harassment_end)


### one-more-persons

Have you been attacked by one or more persons?

- [One person](#one-person)
- [More persons](#more-persons)

### one-person

Do you know this person?

- [Yes](#known-harasser)
- [No](#block-harasser)


### known-harasser

> If you know who is harassing you, you can think of reporting them to your country's authorities, if appropriate in your context. Each country has different laws for protecting people from online harassment, and you should explore the legislation in your country to decide what to do.
>
> If you decide to sue this person, you should reach out to a legal expert.

Do you want to sue the attacker?

 - [Yes](#legal_end)
 - [No](#block-harasser)


### block-harasser

> Whether you know who your harasser is or not, it's always a good idea to block them on social networking platforms whenever possible.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Have you blocked your harasser effectively?

 - [Yes](#resolved_end)
 - [No](#harassment_end)


### more-persons

> If you are being attacked by more than one person, you might be the target of a harassment campaign, and you will need to reflect on what is the best strategy that applies to your case.
>
> To learn about all the possible strategies, read [Take Back The Tech's page on strategies against hate speech](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

Have you identified the best strategy for you?

 - [Yes](#resolved_end)
 - [No](#harassment_end)

### harassment_end

> If you are still under harassment and need a customized solution, please contact the organizations below who can support you.

:[](organisations?services=harassment)


### physical-risk_end

> If you are at physical risk, please contact the organizations below who can support you.

:[](organisations?services=physical_security)


### legal_end

> If you need legal support, please contact the organizations below who can support you.

:[](organisations?services=legal)

### resolved_end

Hopefully this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Document harassment:** It is helpful to document the attacks or any other incident you may be witnessing: take screenshots, save the messages you receive from harassers, etc. If possible, create a journal where you can systematize this documentation recording dates, times, platforms and URLs, user ID, screenshots, description of what happened, etc. Journals can help you detect possible patterns and indications about your possible attackers. If you feel overwhelmed, try to think of someone you trust who could document the incidents for you for a while. You should deeply trust the person who will manage this documentation, as you will need to hand them over credentials to your personal accounts. Before sharing your password with this person, change it to something different and share it via a secure means - such as using a tool with [end-to-end encryption](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Once you feel you can regain control of the account, remember to change your password back to something unique, [secure](https://ssd.eff.org/en/module/creating-strong-passwords), and that only you know.

    - You can find instructions on how to document the incident in these [Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips/).

- **Set up 2-factor authentication** on all your accounts. 2-factor authentication can be very effective for stopping someone from accessing your accounts without your permission. If you can choose, don't use SMS-based 2-factor authentication and choose a different option, based on a phone app or on a security key.

    - If you don't know what solution is best for you, you can check out [Access Now's "What is the best type of multi-factor authentication for me?" infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) and [EFF's "Guide to Common Types of Two-Factor Authentication on the Web"](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - You can find instructions for setting up 2-factor authentication on the major platforms in [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Map your online presence**. Self-doxing consists in exploring open source intelligence on oneself to prevent malicious actors from finding and using this information for impersonating you.


#### Resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
