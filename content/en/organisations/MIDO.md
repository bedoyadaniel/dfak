---
name: Myan ICT for Development Organization (MIDO)
website: https://http://www.mido.ngo/
logo: MIDO_logo_blue_fill.png
languages: ဗမာ, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 days a week, 9 AM-5 PM UTC +6:30
response_time: 1 day
contact_methods: email, phone, signal
email: help@mido.ngo
phone: +95(9)777788258 +95(9)777788246
whatsapp: +95(9)777788258
---

MIDO’s mission is to be a catalyst for the adoption of technology to drive social and political progress in Myanmar.