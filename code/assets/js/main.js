(function(window, document) {
  var menu = document.getElementById("menu"),
    WINDOW_CHANGE_EVENT =
      "onorientationchange" in window ? "orientationchange" : "resize";

  function toggleHorizontal() {
    [].forEach.call(
      document.getElementById("menu").querySelectorAll(".custom-can-transform"),
      function(el) {
        el.classList.toggle("pure-menu-horizontal");
      }
    );
  }

  function toggleMenu() {
    toggleHorizontal();
    menu.classList.toggle("open");
    document.getElementById("toggle").classList.toggle("x");
  }

  function closeMenu() {
    if (menu.classList.contains("open")) {
      toggleMenu();
    }
  }

  document.getElementById("toggle").addEventListener("click", function(e) {
    toggleMenu();
    e.preventDefault();
  });

  window.addEventListener(WINDOW_CHANGE_EVENT, closeMenu);

  var htmlElement = document.getElementsByTagName("html")[0];
  if (window.location.pathname.indexOf("/ar/") != -1) {
    htmlElement.setAttribute("dir", "rtl");
    htmlElement.setAttribute("lang", "ar");
    document.querySelector("p").style.textAlign = "right";
  }
})(this, this.document);
